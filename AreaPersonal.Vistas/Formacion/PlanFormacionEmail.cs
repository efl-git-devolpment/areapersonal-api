﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class PlanFormacionEmail
    {
        public int AssistProgramsNumber { get; set; }

        public string Desc_familia { get; set; }

        public int Id { get; set; }

        public int Id_familia { get; set; }

        public string Name { get; set; }

        public int PastRegisterProgramsNumber { get; set; }

        public int RegisterProgramsNumber { get; set; }

        public int TotalPrograms { get; set; }
    }
}
