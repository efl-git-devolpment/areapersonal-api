﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class Evento
    {
        public bool AbsenteeEmail { get; set; }

        public bool AbsenteeEmailSended { get; set; }

        public int AssistInd { get; set; }

        public long AttendeeCount { get; set; }

        public string Description { get; set; }

        public int Duration { get; set; }

        public string EndDate { get; set; }

        public string EndDateProxy { get; set; }

        public string EntryExitTone { get; set; }

        public string Family { get; set; }

        public string FamilyDescription { get; set; }

        public bool GreetingEmail { get; set; }

        public bool GreetingEmailSended { get; set; }

        public int Id { get; set; }

        public string ImagePath { get; set; }

        public string ListStatus { get; set; }

        public bool Mute { get; set; }

        public string Name { get; set; }

        public int ParticipantLimit { get; set; }

        public string Password { get; set; }

        public long ProgramId { get; set; }

        public string ProgramName { get; set; }

        public long ProgramWebExId { get; set; }

        public bool ReminderEmail { get; set; }

        public bool ReminderEmail2 { get; set; }

        public bool ReminderEmailSended { get; set; }

        public bool ReminderEmailSended2 { get; set; }

        public int ReminderEmailTime { get; set; }

        public int ReminderEmailTime2 { get; set; }

        public string SessionSubtypeDescription { get; set; }

        public int SessionSubtypeId { get; set; }

        public string SessionTypeDescription { get; set; }

        public int SessionTypeId { get; set; }

        public string StartDate { get; set; }

        public string StartDateProxy { get; set; }

        public string Status { get; set; }

        public int TimeZoneId { get; set; }

        public int Type { get; set; }

        public int Vacancy { get; set; }

        public string ViewAttendeeList { get; set; }

        public string WebExHostId { get; set; }

        public long WebExId { get; set; }

    }
}
