﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class PlanFormacionCursoEmail
    {
        public string AssistDate { get; set; }

        public long CursoWebexId { get; set; }

        public string PastSessionDate { get; set; }

        public int ProgramId { get; set; }

        public string ProgramName { get; set; }

        public int ProgramType { get; set; }

        public string ProgramTypeDesc { get; set; }

        public int RegisterProgramInd { get; set; }

        public string SessionDate { get; set; }

        public long WebexId { get; set; }

    }
}
