﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class EventoExtendido
    {

        public string valor { get; set; }

        public string titulo { get; set; }

        public string resumencontenido { get; set; }

        public string modalidad { get; set; }

        public string precio { get; set; }

        public string nivel { get; set; }

        public string recomendadopara { get; set; }

        public string observaciones { get; set; }

        public string requisitostecnicos { get; set; }

        public string enlaceayuda { get; set; }

        public string imagen { get; set; }

        public long WebExId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Duration { get; set; }

        public string StartDate { get; set; }

        public string StartDateProxy { get; set; }

        public string EndDate { get; set; }

        public string impartidopor { get; set; }

        public string textoinscripcion { get; set; }

        public string numeroplazasvisibles { get; set; }

        public string numeroplazasreales { get; set; }

        public string ultimasplazas { get; set; }


    }
}
