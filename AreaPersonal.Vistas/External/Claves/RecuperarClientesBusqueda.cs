﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas.External.Claves
{

    public class RecuperarClientesPeticion
    {
        public int IdClienteNav { get; set; }
        public int IdClienteCrm { get; set; }
        public string IdUsuarioPro { get; set; }
        public string strNombreCompleto { get; set; }
        public string strRazonSocial { get; set; }
        public string strTelefono { get; set; }
        public string strCifNif { get; set; }
        public string strEmail { get; set; }
        public string strLogin { get; set; }
        public string strIp { get; set; }
        public short iIndTipoBusqueda { get; set; }
        public int iIdTipoCliente { get; set; }
        public string strCampoOrdenar { get; set; }
        public bool bIndOrdenarAsc { get; set; }
        public int iPaginaInicial { get; set; }
        public int iNumeroDeFilas { get; set; }
        public string strIdUsuarioAsignadoCrm { get; set; }
        public int iIdUsuarioApp { get; set; }


    }
    public class ClienteBusqueda
    {
        public int? NumTdcSugar { get; set; }
        public int? CodCuentaNavision { get; set; }
        public int? Bloqueo { get; set; }
        public string NombreCompleto { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
        public string CifNif { get; set; }
        public string Email { get; set; }
        public string Via { get; set; }
        public string Ciudad { get; set; }
        public string Provincia { get; set; }
        public string CodigoPostal { get; set; }
        public int? IndGranCuenta { get; set; }
    }
}
