﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Vistas.External.Claves
{
    public class ModificarEmailPeticion
    {
        public int IdClienteNav { get; set; }
        public string IdUsuarioPro { get; set; }

        public string strEmailAnterior { get; set; }

        public string strEmailNuevo { get; set; }

        public int IdUsuarioApp { get; set; }

    }
}
