﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Vistas
{
    public class Modelo347PDFNav
    {
        /// <summary>
        /// Nombre de fichero para Modelo 347
        /// </summary>
        public string strNombreArchivo { get; set; }

        /// <summary>
        /// Contenido del archivo PDF del Modelo 347
        /// </summary>
        public string strArchivoBase64 { get; set; }

    }
}
