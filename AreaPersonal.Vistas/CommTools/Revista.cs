﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace AreaPersonal.Vistas
{
    [Serializable]
    [XmlRoot("export")]
    public class Revista
    {
        [XmlElement("magazineTitle")]
        public string magazineTitle { get; set; }

        [XmlElement("numberTitle")]
        public string numberTitle { get; set; }

        [XmlElement("publishedDate")]
        public string publishedDate { get; set; }

        [XmlElement("url")]
        public string url { get; set; }
    }
}
