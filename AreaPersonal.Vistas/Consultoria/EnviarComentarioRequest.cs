﻿using System;
using System.Collections.Generic;
using System.Text;


namespace AreaPersonal.Vistas
{
    public class EnviarComentarioRequest
    {
        public String strIdentifier { get; set; }
        public int icodQuestion { get; set; }
        public int iCodCRM { get; set; }
        public String strText { get; set; }
        public int iIdClientNav { get; set; }
        public Boolean bCopy { get; set; }
    }
}
