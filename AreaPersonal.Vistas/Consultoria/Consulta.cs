﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{

    public class ResultadoConsultas
    {
        public int iResultado { get; set; }
        public int iTotalConsultas { get; set; }
        public IEnumerable<Consulta> listadoConsultas { get; set; }
        public IEnumerable<ConsultaMateria> listadoMaterias { get; set; }
    }

    public class Consulta
    {

        public ConsultaPropiedades propiedades { get; set; }
        public List<string> ficheros { get; set; }
        public List<SubConsulta> lstSubQueries { get; set; }
        public Comentarios lastRequest  { get; set; }
        public string stimeLastRequest { get; set; }
        public String sEmail { get; set; }
        public String sFecha { get; set; }
        public String sUserNameAssigned { get; set; }
        public String sEmailAssigned { get; set; }
        public String sFullNameEtiqueta { get; set; }
        public int iIdUserAssigned { get; set; }
        public int iStatus { get; set; }
        public String MotivoRechazo { get; set; }
        public List<string> nrefs { get; set; }
        public List<Nref> arrNRefs { get; set; }

    }

    public class ConsultaPropiedades
    {
        public int idConsulta { get; set; }
        public int idClienteNav { get; set; }
        public int idClienteCRM { get; set; }
        public string idUsuarioPro { get; set; }
        public int idTipoConsulta { get; set; }
        public string descTipoConsulta { get; set; }
        public int idTipoSuscripcion { get; set; }
        public string descTipoSuscripcion { get; set; }
        public int idMateria { get; set; }
        public string descMateria { get; set; }
        public int idSubMateria { get; set; }
        public string descSubMateria { get; set; }
        public int idConsultaEstado { get; set; }
        public string descConsultaEstado { get; set; }
        public int idProvincia { get; set; }
        public string descProvincia { get; set; }
        public string fechaConsulta { get; set; }
        public string fechaConsultaAsignada { get; set; }
        public string fechaConsultaCerrada { get; set; }
        public string fechaConsultaResuelta { get; set; }
        public string fechaAltaSac { get; set; }
        public string tituloConsulta { get; set; }
        public int idVoz { get; set; }
        public string descVoz { get; set; }
        public string descConsulta { get; set; }
        public string nombreUsuarioPeticion { get; set; }
        public string cargoUsuarioPeticion { get; set; }
        public string email { get; set; }
        public string tlf { get; set; }
        public string nombreClienteSuscripcion { get; set; }
        public string nombreComercial { get; set; }
        public string delegacionComercial { get; set; }
        public int idSuscripcionNav { get; set; }
        public string idProductoNav { get; set; }
        public string descProductoNav { get; set; }
        public int idUsuarioAsignado { get; set; }
        public string descUsuarioAsignado { get; set; }
        public string descUsuarioCreador { get; set; }
        public int idContactoCrm { get; set; }
        public int idColor { get; set; }
        public string cargo { get; set; }
        public string disponibilidad { get; set; }
        public int idColaborador { get; set; }
        public string nombreColaborador { get; set; }
        public string emailColaborador { get; set; }
        public int numeroPartes { get; set; }
        public int indPrivada { get; set; }
        public string facturacionColaborador { get; set; }
        public string referenciaEditorial { get; set; }
        public int indColaboradorAnulado { get; set; }
        public int indTraducidoCatalan { get; set; }
        public int indTraducidoEuskera { get; set; }
        public int indTraducidoGallego { get; set; }
        public string motivoRechazo { get; set; }
        public int indFacturacionColaborador { get; set; }
        public int idTipoOrigen { get; set; }
        public int indUrgente { get; set; }
        public int indNivel1 { get; set; }
        public int indNivel2 { get; set; }
        public int indNivel { get; set; }
        public int timePrepare { get; set; }

    }
    public class Comentarios
    {
        public int IdSubConsulta { get; set; }
        public bool OwnerUser { get; set; }
        public string sName { get; set; }
        public string sDate { get; set; }
        public string sTime { get; set; }
        public string sComment { get; set; }
        public string sLabel { get; set; }
        public int indInterno { get; set; }


    }

    public class SubConsulta
    {
        public int idSubConsulta { get; set; }
        public string descSubConsulta { get; set; }
        public string fechaSubConsulta { get; set; }
        public int idUsuarioApp { get; set; }
        public string descUsuarioApp { get; set; }
        public int  indInterno { get; set; }
        public int indRespuesta { get; set; }
        public string sName { get; set; }
        public string sLabel { get; set; }
    }

    public class Nref
    {
        public int idNref { get; set; }
        public string nref { get; set; }
        public string tipo {get;set;}
        public string xml { get; set; }

    }

    public class Mementos
    {
        public string codigoMemento { get; set; }
        public string descripcion { get; set; }
    }

    public class ConsultaMateria
    {
        public int IdMatter { get; set; }
        public string DescMatter { get; set; }
        public int NumTotal { get; set; }
        public int NumEnabled { get; set; }

        public ConsultaMateria(int idMatter, string descMatter, int iNumTotal, int iNumEnabled)
        {
            this.IdMatter = idMatter;
            this.DescMatter = descMatter;
            this.NumTotal = iNumTotal;
            this.NumEnabled = iNumEnabled;
        }
    }


}
