﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class TipoConsulta
    {
        /// <summary>
        /// Descripción de Tipo de Consulta
        /// </summary>
        public string descTipoConsulta { get; set; }

        /// <summary>
        /// Identificador interno de Tipo de Consulta
        /// </summary>
        public int idTipoConsulta { get; set; }

        //public int indSuscripcionDemo { get; set; }

    }
}
