﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class Provincia
    {
        ///// <summary>
        ///// Codigo interno de provincia
        ///// </summary>
        //public string codigoProvincia { get; set; } 

        /// <summary>
        /// Descripción de Provincia
        /// </summary>
        public string descProvincia { get; set; }

        /// <summary>
        /// Identificador interno de Provincia
        /// </summary>
        public int idProvincia { get; set; }

    }
}
