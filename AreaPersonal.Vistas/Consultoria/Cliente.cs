﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class Cliente
    {

        public string cifNif { get; set; }

        public int codCuentaNavision { get; set; }

        public string email { get; set; }

        public int indGranCuenta { get; set; }

        public string nombreCompleto { get; set; }

        public int numTdcSugar { get; set; }

        public string provincia { get; set; }

        public string razonSocial { get; set; }

        public string telefono { get; set; }
    }
}
