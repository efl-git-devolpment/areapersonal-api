﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{

    public class ConsultaRequest
    {

        public string strIdentifier { get; set; }
        public String strEmail { get; set; }
        public String strNombre { get; set; }
        public string strApellido { get; set; }
        public String strDescripcion { get; set; }
        public String strTelefono { get; set; }
        public string strTitular { get; set; }
        public string strCargo { get; set; }

        public string sDisponibilidad { get; set; }
        public String sNombreCliente { get; set; }
        public int iIdMateria { get; set; }
        public int iIdTipoPeticion { get; set; }
        public int iIdSubMateria { get; set; }
        public int iIdProvincia { get; set; }
        public int iIdClientNav { get; set; }
        public int iCodCRM { get; set; }
    }
  
}
