﻿using System;

namespace AreaPersonal.Vistas
{
    public class SubMateria
    {
        /// <summary>
        /// Descripción de SubMateria
        /// </summary>
        public string descSubMateria { get; set; }

        /// <summary>
        /// Identificador interno de SubMateria
        /// </summary>
        public int idSubMateria { get; set; }

    }
}
