﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class CertificadoPropiedad
    {

        public string url { get; set; }
        public string toAddresses { get; set; }
        public string subject { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string accuracy  { get; set; }
        public string address  { get; set; }
        public string filename { get; set; }
        public string size  { get; set; }
        public string md5  { get; set; }
        public string sha1 { get; set; }
        public string sha256 { get; set; }
        public string sha512 { get; set; }
    }
}
