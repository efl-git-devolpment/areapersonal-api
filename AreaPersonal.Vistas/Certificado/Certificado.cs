﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class Certificado
    {

        public string code { get; set; }

        public string entryDate { get; set; }

        public string recipientMail { get; set; }

        public string recipientName { get; set; }

        public string state { get; set; }

        public string subject { get; set; }

        public string thumbnailUrl { get; set; }

        public string url { get; set; }

        public CertificadoPropiedad property { get; set; }

    }
}
