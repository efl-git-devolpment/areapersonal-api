﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class CertificadoResult
    {
        public int count { get; set; }
        public int pageSize { get; set; }
        public int pageTotal { get; set; }
        public List<Certificado> list { get; set; }
    }
}
