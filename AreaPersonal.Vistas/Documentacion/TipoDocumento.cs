﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class TipoDocumento
    {
        /// <summary>
        /// Descripción de Tipo de Documento
        /// </summary>
        public string descTipoDocumento { get; set; }

        /// <summary>
        /// Identificador Tipo de Documento
        /// </summary>
        public int idTipoDocumento { get; set; }
    }
}

