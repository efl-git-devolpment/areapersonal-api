﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class Abono
    {

        public string idAbonoNav { get; set; }
        public string idClienteFacturacionNav { get; set; }
        public string idClienteAbonoNav { get; set; }
        public string idClienteEnvioNav { get; set; }
        public string importe { get; set; }
        public string fecha { get; set; }
        public string fechaVencimiento { get; set; }
    }
    public class ResultadoAbonos
    {
        public int iResultado { get; set; }
        public string descResultado { get; set; }
        public int regTotal { get; set; }
        public int numPage { get; set; }
        public int recsPerPage { get; set; }
        public List<Abono> list { get; set; }
    }

    public class AbonoNav
    {

        public string IdAbonoNav { get; set; }
        public string IdClienteEnvioNav { get; set; }
        public string IdClienteFacturacionNav { get; set; }
        public string strFechaRegistro { get; set; }
        public string strFechaVencimiento { get; set; }
        public string strImporteIVA { get; set; }
        public string IdContactoCrm { get; set; }
        public string strCIF { get; set; }
        public string strNombreCliente { get; set; }

        public string strNombreCliente2 { get; set; }
    }

    /// <summary>
    /// Clase con la información de Abono en formato PDF
    /// </summary>
    public class AbonoPDFNav
    {

        /// <summary>
        /// Nombre de fichero para un Abono
        /// </summary>
        public string strNombreArchivo { get; set; }

        /// <summary>
        /// Contenido del archivo PDF de un Abono
        /// </summary>
        public string strArchivoBase64 { get; set; }
    }
}
