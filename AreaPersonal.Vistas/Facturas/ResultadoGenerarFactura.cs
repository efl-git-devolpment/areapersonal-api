﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class ResultadoGenerarFactura : ResultadoOperacion
    {
        public int idColaFactura { get; set; }
        public string idFacturaNav { get; set; }
        public string path { get; set; }
        public int estado { get; set; }
        public string mensajeError { get; set; }
        //public string pathFtp { get; set; }
    }


}
