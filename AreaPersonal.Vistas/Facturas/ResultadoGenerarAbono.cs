﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class ResultadoGenerarAbono : ResultadoOperacion
    {
        public  int idColaAbono { get; set; }
        public string idAbonoNav { get; set; }
        public string path { get; set; }
        public int estado { get; set; }
        public string mensajeError { get; set; }
    }
}
