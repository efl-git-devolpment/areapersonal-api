﻿using System.ComponentModel.DataAnnotations;

namespace AreaPersonal.Vistas
{
    public class InsertarFacturaPagadaPeticion
    {
        [Required(ErrorMessage = "IdClienteNav es necesario y no puede ser nulo o estar vacío.")]
        [Range(int.MinValue, int.MaxValue, ErrorMessage = "IdClienteNav debe ser mayor que 1.")]
        public int? IdClienteNav { get; set; }
        [Required(ErrorMessage = "StrIdFacturaNav es necesario y no puede ser nulo o estar vacío.", AllowEmptyStrings = false)]
        public string? StrIdFacturaNav { get; set; }
        [Required(ErrorMessage = "IdUsuarioApp es necesario y no puede ser nulo o estar vacío.")]
        [Range(int.MinValue, int.MaxValue, ErrorMessage = "IdUsuarioApp debe ser mayor que 1.")]
        public int? IdUsuarioApp { get; set; }
    }
}
