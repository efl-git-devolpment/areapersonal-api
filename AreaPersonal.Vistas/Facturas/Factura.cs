﻿using AreaPersonal.Comun.Enums;
using System.Collections.Generic;

namespace AreaPersonal.Vistas
{
    public class Factura
    {
        public string idFacturaNav { get; set; }
        public string idClienteFacturacionNav { get; set; }
        public string idClienteEnvioNav { get; set; }
        public string importe { get; set; }
        public string importeImpago { get; set; }
        public string fecha { get; set; }
        public string fechaVencimiento { get; set; }
        public EstadoFacturasEnum IEstadoPagada { get; set; }
    }

    public class FacturaNav
    {
        public string IdFacturaNav { get; set; }
        public string IdClienteEnvioNav { get; set; }
        public string IdClienteFacturacionNav { get; set; }
        public string strFechaRegistro { get; set; }
        public string strFechaVencimiento { get; set; }
        public string strImporteIVA { get; set; }
        public string IdContactoCrm { get; set; }
        public string strCIF { get; set; }
        public string strNombreCliente { get; set; }

        public string strNombreCliente2 { get; set; }

        public string strImpato { get; set; }
    }

    public class FacturaPDFNav
    {
        /// <summary>
        /// Nombre de fichero para una Factura
        /// </summary>
        public string strNombreArchivo { get; set; }

        /// <summary>
        /// Contenido del archivo PDF de una Factura
        /// </summary>
        public string strArchivoBase64 { get; set; }
    }

    public class ResultadoFacturas
    {
        public int iResultado { get; set; }
        public string descResultado { get; set; }
        public int regTotal { get; set; }
        public int numPage { get; set; }
        public int recsPerPage { get; set; }
        public List<Factura> list { get; set; }
    }
}
