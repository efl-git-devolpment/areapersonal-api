﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class ModificarLogoRequest
    {

        public String strIdentifier { get; set; }
        public String strUsuarioPro { get; set; }

        public Byte[] bdata { get; set; }

    }
}
