﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas.DatosPersonales
{
    public class ModificarUsuarioRequest
    {
        public String strIdentifier { get; set; }
        public String strUsuarioPro { get; set; }

        public String strName { get; set; }
        public String strFirstName { get; set; }
        public String strSurName { get; set; }
        public String strTreatment { get; set; }
        public String strEmail { get; set; }
        //public String strPassword { get; set; }
        public String strColor { get; set; }
        public String strNombreComercial { get; set; }
    }
}
