﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Vistas
{
    public class UsuarioPersonal
    {
        public string email { get; set; }

        public string estado { get; set; }

        public string fechaCreacionFormateada { get; set; }

        public int idCliente { get; set; }

        public int idClienteCrm { get; set; }

        public int idClienteNav { get; set; }

        public int idEntrada { get; set; }

        public string ip { get; set; }

        public string login { get; set; }

        public string nombre { get; set; }

        public string numConcurrencias { get; set; }

        public string numNavision { get; set; }

        public string primerApellido { get; set; }

        public string pwd { get; set; }

        public string segundoApellido { get; set; }

        public string tipoEntrada { get; set; }

        public string tratamiento { get; set; }

        public string pathLogo {get; set;}
        public string pathLogo2 { get; set; }
        public string nombreComercial { get; set; }
        public string color { get; set; }

    }
}

