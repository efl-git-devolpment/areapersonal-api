﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Vistas.DatosPersonales
{
    public class ModificarEmailPeticion
    {
        public string strIdentifier { get; set; }
        public int IdClienteNav { get; set; }
        public string IdUsuarioPro { get; set; }
        public string strEmailAnterior { get; set; }
        public string strEmailNuevo { get; set; }

    }
}
