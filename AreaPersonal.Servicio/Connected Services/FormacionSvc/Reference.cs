﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//     //
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace FormacionSvc
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Attendee", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Attendee : DllWcfUtility.BeanBase
    {
        
        private string AddressField;
        
        private bool AssistField;
        
        private int AssistIndField;
        
        private string AssociatedEmailField;
        
        private long AssociatedWebExIdField;
        
        private string CompanyField;
        
        private int ContactCodeField;
        
        private string DateField;
        
        private string EmailField;
        
        private long EventIdField;
        
        private string FamilyDescriptionField;
        
        private int FamilyIdField;
        
        private string FirstNameField;
        
        private int IdField;
        
        private string InscriptionDateField;
        
        private string LastNameField;
        
        private string MobilePhoneField;
        
        private string NameField;
        
        private int NavisionAccountCodeField;
        
        private int NewsletterIdField;
        
        private string NotesField;
        
        private string OriginDescriptionField;
        
        private int OriginIdField;
        
        private bool PastEventField;
        
        private string PhoneField;
        
        private int ProgramIdField;
        
        private bool SendReminderField;
        
        private string SessionDateField;
        
        private string SessionDescriptionField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        private long SessionWebexIdField;
        
        private int StatusField;
        
        private string TitleField;
        
        private string TypeField;
        
        private string URLField;
        
        private string UpdateDateField;
        
        private int UserIdField;
        
        private string UserNameField;
        
        private long WebexIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Address
        {
            get
            {
                return this.AddressField;
            }
            set
            {
                this.AddressField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Assist
        {
            get
            {
                return this.AssistField;
            }
            set
            {
                this.AssistField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AssistInd
        {
            get
            {
                return this.AssistIndField;
            }
            set
            {
                this.AssistIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AssociatedEmail
        {
            get
            {
                return this.AssociatedEmailField;
            }
            set
            {
                this.AssociatedEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long AssociatedWebExId
        {
            get
            {
                return this.AssociatedWebExIdField;
            }
            set
            {
                this.AssociatedWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Company
        {
            get
            {
                return this.CompanyField;
            }
            set
            {
                this.CompanyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ContactCode
        {
            get
            {
                return this.ContactCodeField;
            }
            set
            {
                this.ContactCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Date
        {
            get
            {
                return this.DateField;
            }
            set
            {
                this.DateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long EventId
        {
            get
            {
                return this.EventIdField;
            }
            set
            {
                this.EventIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int FamilyId
        {
            get
            {
                return this.FamilyIdField;
            }
            set
            {
                this.FamilyIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FirstName
        {
            get
            {
                return this.FirstNameField;
            }
            set
            {
                this.FirstNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string InscriptionDate
        {
            get
            {
                return this.InscriptionDateField;
            }
            set
            {
                this.InscriptionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LastName
        {
            get
            {
                return this.LastNameField;
            }
            set
            {
                this.LastNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MobilePhone
        {
            get
            {
                return this.MobilePhoneField;
            }
            set
            {
                this.MobilePhoneField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NavisionAccountCode
        {
            get
            {
                return this.NavisionAccountCodeField;
            }
            set
            {
                this.NavisionAccountCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Notes
        {
            get
            {
                return this.NotesField;
            }
            set
            {
                this.NotesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OriginDescription
        {
            get
            {
                return this.OriginDescriptionField;
            }
            set
            {
                this.OriginDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OriginId
        {
            get
            {
                return this.OriginIdField;
            }
            set
            {
                this.OriginIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool PastEvent
        {
            get
            {
                return this.PastEventField;
            }
            set
            {
                this.PastEventField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Phone
        {
            get
            {
                return this.PhoneField;
            }
            set
            {
                this.PhoneField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool SendReminder
        {
            get
            {
                return this.SendReminderField;
            }
            set
            {
                this.SendReminderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionDate
        {
            get
            {
                return this.SessionDateField;
            }
            set
            {
                this.SessionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionDescription
        {
            get
            {
                return this.SessionDescriptionField;
            }
            set
            {
                this.SessionDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long SessionWebexId
        {
            get
            {
                return this.SessionWebexIdField;
            }
            set
            {
                this.SessionWebexIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Status
        {
            get
            {
                return this.StatusField;
            }
            set
            {
                this.StatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Title
        {
            get
            {
                return this.TitleField;
            }
            set
            {
                this.TitleField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string URL
        {
            get
            {
                return this.URLField;
            }
            set
            {
                this.URLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UpdateDate
        {
            get
            {
                return this.UpdateDateField;
            }
            set
            {
                this.UpdateDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int UserId
        {
            get
            {
                return this.UserIdField;
            }
            set
            {
                this.UserIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserName
        {
            get
            {
                return this.UserNameField;
            }
            set
            {
                this.UserNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebexId
        {
            get
            {
                return this.WebexIdField;
            }
            set
            {
                this.WebexIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FamilyReport", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class FamilyReport : DllWcfUtility.BeanBase
    {
        
        private string DateField;
        
        private int IdField;
        
        private string NameField;
        
        private int NumAttendeesField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Date
        {
            get
            {
                return this.DateField;
            }
            set
            {
                this.DateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAttendees
        {
            get
            {
                return this.NumAttendeesField;
            }
            set
            {
                this.NumAttendeesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FamilyAssistReport", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class FamilyAssistReport : DllWcfUtility.BeanBase
    {
        
        private int AssistNumAccountsField;
        
        private string DateField;
        
        private int IdField;
        
        private string NameField;
        
        private int NumAccountsField;
        
        private int NumAssistField;
        
        private int NumAttendeesField;
        
        private string OriginDescriptionField;
        
        private int OriginIdField;
        
        private string PercentField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AssistNumAccounts
        {
            get
            {
                return this.AssistNumAccountsField;
            }
            set
            {
                this.AssistNumAccountsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Date
        {
            get
            {
                return this.DateField;
            }
            set
            {
                this.DateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAccounts
        {
            get
            {
                return this.NumAccountsField;
            }
            set
            {
                this.NumAccountsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAssist
        {
            get
            {
                return this.NumAssistField;
            }
            set
            {
                this.NumAssistField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAttendees
        {
            get
            {
                return this.NumAttendeesField;
            }
            set
            {
                this.NumAttendeesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OriginDescription
        {
            get
            {
                return this.OriginDescriptionField;
            }
            set
            {
                this.OriginDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OriginId
        {
            get
            {
                return this.OriginIdField;
            }
            set
            {
                this.OriginIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Percent
        {
            get
            {
                return this.PercentField;
            }
            set
            {
                this.PercentField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OriginReport", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class OriginReport : DllWcfUtility.BeanBase
    {
        
        private string DateField;
        
        private int IdField;
        
        private string NameField;
        
        private int NumAttendeesField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Date
        {
            get
            {
                return this.DateField;
            }
            set
            {
                this.DateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAttendees
        {
            get
            {
                return this.NumAttendeesField;
            }
            set
            {
                this.NumAttendeesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionTypeReport", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class SessionTypeReport : DllWcfUtility.BeanBase
    {
        
        private string DateField;
        
        private int IdField;
        
        private string NameField;
        
        private int NumAttendeesField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Date
        {
            get
            {
                return this.DateField;
            }
            set
            {
                this.DateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NumAttendees
        {
            get
            {
                return this.NumAttendeesField;
            }
            set
            {
                this.NumAttendeesField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Program", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Program : DllWcfUtility.BeanBase
    {
        
        private string AfterEnrollmentURLField;
        
        private int AssistIndField;
        
        private double BudgetField;
        
        private DllWcfUtility.BeanList<FormacionSvc.Event> EventsField;
        
        private int ExpectedEnrollmentField;
        
        private string FamilyField;
        
        private string FamilyNameField;
        
        private long IdField;
        
        private string NameField;
        
        private string StatusField;
        
        private string TypeDescriptionField;
        
        private int TypeIdField;
        
        private string URLField;
        
        private string WebExHostIdField;
        
        private long WebExIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AfterEnrollmentURL
        {
            get
            {
                return this.AfterEnrollmentURLField;
            }
            set
            {
                this.AfterEnrollmentURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AssistInd
        {
            get
            {
                return this.AssistIndField;
            }
            set
            {
                this.AssistIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Budget
        {
            get
            {
                return this.BudgetField;
            }
            set
            {
                this.BudgetField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public DllWcfUtility.BeanList<FormacionSvc.Event> Events
        {
            get
            {
                return this.EventsField;
            }
            set
            {
                this.EventsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ExpectedEnrollment
        {
            get
            {
                return this.ExpectedEnrollmentField;
            }
            set
            {
                this.ExpectedEnrollmentField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Family
        {
            get
            {
                return this.FamilyField;
            }
            set
            {
                this.FamilyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyName
        {
            get
            {
                return this.FamilyNameField;
            }
            set
            {
                this.FamilyNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Status
        {
            get
            {
                return this.StatusField;
            }
            set
            {
                this.StatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TypeDescription
        {
            get
            {
                return this.TypeDescriptionField;
            }
            set
            {
                this.TypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int TypeId
        {
            get
            {
                return this.TypeIdField;
            }
            set
            {
                this.TypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string URL
        {
            get
            {
                return this.URLField;
            }
            set
            {
                this.URLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string WebExHostId
        {
            get
            {
                return this.WebExHostIdField;
            }
            set
            {
                this.WebExHostIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebExId
        {
            get
            {
                return this.WebExIdField;
            }
            set
            {
                this.WebExIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Event", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Event : DllWcfUtility.BeanBase
    {
        
        private bool AbsenteeEmailField;
        
        private bool AbsenteeEmailSendedField;
        
        private int AssistIndField;
        
        private long AttendeeCountField;
        
        private string DescriptionField;
        
        private int DurationField;
        
        private string EndDateField;
        
        private string EndDateProxyField;
        
        private string EntryExitToneField;
        
        private string FamilyField;
        
        private string FamilyDescriptionField;
        
        private bool GreetingEmailField;
        
        private bool GreetingEmailSendedField;
        
        private int IdField;
        
        private string ImagePathField;
        
        private string ListStatusField;
        
        private bool MuteField;
        
        private string NameField;
        
        private int ParticipantLimitField;
        
        private string PasswordField;
        
        private long ProgramIdField;
        
        private string ProgramNameField;
        
        private long ProgramWebExIdField;
        
        private bool ReminderEmailField;
        
        private bool ReminderEmail2Field;
        
        private bool ReminderEmailSendedField;
        
        private bool ReminderEmailSended2Field;
        
        private int ReminderEmailTimeField;
        
        private int ReminderEmailTime2Field;
        
        private string SessionSubtypeDescriptionField;
        
        private int SessionSubtypeIdField;
        
        private string SessionTypeDescriptionField;
        
        private int SessionTypeIdField;
        
        private string StartDateField;
        
        private string StartDateProxyField;
        
        private string StatusField;
        
        private int TimeZoneIdField;
        
        private int TypeField;
        
        private int VacancyField;
        
        private string ViewAttendeeListField;
        
        private string WebExHostIdField;
        
        private long WebExIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool AbsenteeEmail
        {
            get
            {
                return this.AbsenteeEmailField;
            }
            set
            {
                this.AbsenteeEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool AbsenteeEmailSended
        {
            get
            {
                return this.AbsenteeEmailSendedField;
            }
            set
            {
                this.AbsenteeEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AssistInd
        {
            get
            {
                return this.AssistIndField;
            }
            set
            {
                this.AssistIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long AttendeeCount
        {
            get
            {
                return this.AttendeeCountField;
            }
            set
            {
                this.AttendeeCountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Duration
        {
            get
            {
                return this.DurationField;
            }
            set
            {
                this.DurationField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndDate
        {
            get
            {
                return this.EndDateField;
            }
            set
            {
                this.EndDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndDateProxy
        {
            get
            {
                return this.EndDateProxyField;
            }
            set
            {
                this.EndDateProxyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EntryExitTone
        {
            get
            {
                return this.EntryExitToneField;
            }
            set
            {
                this.EntryExitToneField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Family
        {
            get
            {
                return this.FamilyField;
            }
            set
            {
                this.FamilyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GreetingEmail
        {
            get
            {
                return this.GreetingEmailField;
            }
            set
            {
                this.GreetingEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GreetingEmailSended
        {
            get
            {
                return this.GreetingEmailSendedField;
            }
            set
            {
                this.GreetingEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ImagePath
        {
            get
            {
                return this.ImagePathField;
            }
            set
            {
                this.ImagePathField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ListStatus
        {
            get
            {
                return this.ListStatusField;
            }
            set
            {
                this.ListStatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Mute
        {
            get
            {
                return this.MuteField;
            }
            set
            {
                this.MuteField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ParticipantLimit
        {
            get
            {
                return this.ParticipantLimitField;
            }
            set
            {
                this.ParticipantLimitField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password
        {
            get
            {
                return this.PasswordField;
            }
            set
            {
                this.PasswordField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramWebExId
        {
            get
            {
                return this.ProgramWebExIdField;
            }
            set
            {
                this.ProgramWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail
        {
            get
            {
                return this.ReminderEmailField;
            }
            set
            {
                this.ReminderEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail2
        {
            get
            {
                return this.ReminderEmail2Field;
            }
            set
            {
                this.ReminderEmail2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmailSended
        {
            get
            {
                return this.ReminderEmailSendedField;
            }
            set
            {
                this.ReminderEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmailSended2
        {
            get
            {
                return this.ReminderEmailSended2Field;
            }
            set
            {
                this.ReminderEmailSended2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime
        {
            get
            {
                return this.ReminderEmailTimeField;
            }
            set
            {
                this.ReminderEmailTimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime2
        {
            get
            {
                return this.ReminderEmailTime2Field;
            }
            set
            {
                this.ReminderEmailTime2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionSubtypeDescription
        {
            get
            {
                return this.SessionSubtypeDescriptionField;
            }
            set
            {
                this.SessionSubtypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionSubtypeId
        {
            get
            {
                return this.SessionSubtypeIdField;
            }
            set
            {
                this.SessionSubtypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionTypeDescription
        {
            get
            {
                return this.SessionTypeDescriptionField;
            }
            set
            {
                this.SessionTypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionTypeId
        {
            get
            {
                return this.SessionTypeIdField;
            }
            set
            {
                this.SessionTypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDate
        {
            get
            {
                return this.StartDateField;
            }
            set
            {
                this.StartDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDateProxy
        {
            get
            {
                return this.StartDateProxyField;
            }
            set
            {
                this.StartDateProxyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Status
        {
            get
            {
                return this.StatusField;
            }
            set
            {
                this.StatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int TimeZoneId
        {
            get
            {
                return this.TimeZoneIdField;
            }
            set
            {
                this.TimeZoneIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                this.TypeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Vacancy
        {
            get
            {
                return this.VacancyField;
            }
            set
            {
                this.VacancyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ViewAttendeeList
        {
            get
            {
                return this.ViewAttendeeListField;
            }
            set
            {
                this.ViewAttendeeListField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string WebExHostId
        {
            get
            {
                return this.WebExHostIdField;
            }
            set
            {
                this.WebExHostIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebExId
        {
            get
            {
                return this.WebExIdField;
            }
            set
            {
                this.WebExIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionInfo", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class SessionInfo : DllWcfUtility.BeanBase
    {
        
        private string ImageURLField;
        
        private int UsersLimitField;
        
        private int VacancyField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ImageURL
        {
            get
            {
                return this.ImageURLField;
            }
            set
            {
                this.ImageURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int UsersLimit
        {
            get
            {
                return this.UsersLimitField;
            }
            set
            {
                this.UsersLimitField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Vacancy
        {
            get
            {
                return this.VacancyField;
            }
            set
            {
                this.VacancyField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DeletedEvent", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class DeletedEvent : DllWcfUtility.BeanBase
    {
        
        private bool AbsenteeEmailField;
        
        private bool AbsenteeEmailSendedField;
        
        private long AttendeeCountField;
        
        private string DeleteDateField;
        
        private string DescriptionField;
        
        private int DurationField;
        
        private string EndDateField;
        
        private string EndDateProxyField;
        
        private string EntryExitToneField;
        
        private string FamilyField;
        
        private string FamilyDescriptionField;
        
        private bool GreetingEmailField;
        
        private bool GreetingEmailSendedField;
        
        private int IdField;
        
        private bool MuteField;
        
        private string NameField;
        
        private int ParticipantLimitField;
        
        private long ProgramIdField;
        
        private string ProgramNameField;
        
        private long ProgramWebExIdField;
        
        private bool ReminderEmailField;
        
        private bool ReminderEmail2Field;
        
        private bool ReminderEmailSendedField;
        
        private bool ReminderEmailSended2Field;
        
        private int ReminderEmailTimeField;
        
        private int ReminderEmailTime2Field;
        
        private string SessionSubtypeDescriptionField;
        
        private int SessionSubtypeIdField;
        
        private string SessionTypeDescriptionField;
        
        private int SessionTypeIdField;
        
        private string StartDateField;
        
        private string StartDateProxyField;
        
        private int TimeZoneIdField;
        
        private string UserEmailField;
        
        private string UserNameField;
        
        private string ViewAttendeeListField;
        
        private string WebExHostIdField;
        
        private long WebExIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool AbsenteeEmail
        {
            get
            {
                return this.AbsenteeEmailField;
            }
            set
            {
                this.AbsenteeEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool AbsenteeEmailSended
        {
            get
            {
                return this.AbsenteeEmailSendedField;
            }
            set
            {
                this.AbsenteeEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long AttendeeCount
        {
            get
            {
                return this.AttendeeCountField;
            }
            set
            {
                this.AttendeeCountField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DeleteDate
        {
            get
            {
                return this.DeleteDateField;
            }
            set
            {
                this.DeleteDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Duration
        {
            get
            {
                return this.DurationField;
            }
            set
            {
                this.DurationField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndDate
        {
            get
            {
                return this.EndDateField;
            }
            set
            {
                this.EndDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndDateProxy
        {
            get
            {
                return this.EndDateProxyField;
            }
            set
            {
                this.EndDateProxyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EntryExitTone
        {
            get
            {
                return this.EntryExitToneField;
            }
            set
            {
                this.EntryExitToneField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Family
        {
            get
            {
                return this.FamilyField;
            }
            set
            {
                this.FamilyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GreetingEmail
        {
            get
            {
                return this.GreetingEmailField;
            }
            set
            {
                this.GreetingEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GreetingEmailSended
        {
            get
            {
                return this.GreetingEmailSendedField;
            }
            set
            {
                this.GreetingEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Mute
        {
            get
            {
                return this.MuteField;
            }
            set
            {
                this.MuteField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ParticipantLimit
        {
            get
            {
                return this.ParticipantLimitField;
            }
            set
            {
                this.ParticipantLimitField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramWebExId
        {
            get
            {
                return this.ProgramWebExIdField;
            }
            set
            {
                this.ProgramWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail
        {
            get
            {
                return this.ReminderEmailField;
            }
            set
            {
                this.ReminderEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail2
        {
            get
            {
                return this.ReminderEmail2Field;
            }
            set
            {
                this.ReminderEmail2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmailSended
        {
            get
            {
                return this.ReminderEmailSendedField;
            }
            set
            {
                this.ReminderEmailSendedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmailSended2
        {
            get
            {
                return this.ReminderEmailSended2Field;
            }
            set
            {
                this.ReminderEmailSended2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime
        {
            get
            {
                return this.ReminderEmailTimeField;
            }
            set
            {
                this.ReminderEmailTimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime2
        {
            get
            {
                return this.ReminderEmailTime2Field;
            }
            set
            {
                this.ReminderEmailTime2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionSubtypeDescription
        {
            get
            {
                return this.SessionSubtypeDescriptionField;
            }
            set
            {
                this.SessionSubtypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionSubtypeId
        {
            get
            {
                return this.SessionSubtypeIdField;
            }
            set
            {
                this.SessionSubtypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionTypeDescription
        {
            get
            {
                return this.SessionTypeDescriptionField;
            }
            set
            {
                this.SessionTypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionTypeId
        {
            get
            {
                return this.SessionTypeIdField;
            }
            set
            {
                this.SessionTypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDate
        {
            get
            {
                return this.StartDateField;
            }
            set
            {
                this.StartDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDateProxy
        {
            get
            {
                return this.StartDateProxyField;
            }
            set
            {
                this.StartDateProxyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int TimeZoneId
        {
            get
            {
                return this.TimeZoneIdField;
            }
            set
            {
                this.TimeZoneIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserEmail
        {
            get
            {
                return this.UserEmailField;
            }
            set
            {
                this.UserEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserName
        {
            get
            {
                return this.UserNameField;
            }
            set
            {
                this.UserNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ViewAttendeeList
        {
            get
            {
                return this.ViewAttendeeListField;
            }
            set
            {
                this.ViewAttendeeListField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string WebExHostId
        {
            get
            {
                return this.WebExHostIdField;
            }
            set
            {
                this.WebExHostIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebExId
        {
            get
            {
                return this.WebExIdField;
            }
            set
            {
                this.WebExIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProgramType", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class ProgramType : DllWcfUtility.BeanBase
    {
        
        private int IdField;
        
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionType", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class SessionType : DllWcfUtility.BeanBase
    {
        
        private string DescriptionField;
        
        private int IdField;
        
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionSubtype", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class SessionSubtype : DllWcfUtility.BeanBase
    {
        
        private string DescriptionField;
        
        private int IdField;
        
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Familia", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Familia : DllWcfUtility.BeanBase
    {
        
        private int CodeField;
        
        private string TittleField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Code
        {
            get
            {
                return this.CodeField;
            }
            set
            {
                this.CodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Tittle
        {
            get
            {
                return this.TittleField;
            }
            set
            {
                this.TittleField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Origin", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Origin : DllWcfUtility.BeanBase
    {
        
        private int IdField;
        
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="EventTemplate", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class EventTemplate : DllWcfUtility.BeanBase
    {
        
        private bool AbsenteeEmailField;
        
        private string DescriptionField;
        
        private string FamilyField;
        
        private string FamilyDescriptionField;
        
        private bool GreetingEmailField;
        
        private int IdField;
        
        private string ImageBase64Field;
        
        private string ImagePathField;
        
        private string NameField;
        
        private string PasswordField;
        
        private bool ReminderEmailField;
        
        private bool ReminderEmail2Field;
        
        private int ReminderEmailTimeField;
        
        private int ReminderEmailTime2Field;
        
        private string SessionSubtypeDescriptionField;
        
        private int SessionSubtypeIdField;
        
        private string SessionTypeDescriptionField;
        
        private int SessionTypeIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool AbsenteeEmail
        {
            get
            {
                return this.AbsenteeEmailField;
            }
            set
            {
                this.AbsenteeEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Family
        {
            get
            {
                return this.FamilyField;
            }
            set
            {
                this.FamilyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool GreetingEmail
        {
            get
            {
                return this.GreetingEmailField;
            }
            set
            {
                this.GreetingEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ImageBase64
        {
            get
            {
                return this.ImageBase64Field;
            }
            set
            {
                this.ImageBase64Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ImagePath
        {
            get
            {
                return this.ImagePathField;
            }
            set
            {
                this.ImagePathField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Password
        {
            get
            {
                return this.PasswordField;
            }
            set
            {
                this.PasswordField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail
        {
            get
            {
                return this.ReminderEmailField;
            }
            set
            {
                this.ReminderEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ReminderEmail2
        {
            get
            {
                return this.ReminderEmail2Field;
            }
            set
            {
                this.ReminderEmail2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime
        {
            get
            {
                return this.ReminderEmailTimeField;
            }
            set
            {
                this.ReminderEmailTimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ReminderEmailTime2
        {
            get
            {
                return this.ReminderEmailTime2Field;
            }
            set
            {
                this.ReminderEmailTime2Field = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionSubtypeDescription
        {
            get
            {
                return this.SessionSubtypeDescriptionField;
            }
            set
            {
                this.SessionSubtypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionSubtypeId
        {
            get
            {
                return this.SessionSubtypeIdField;
            }
            set
            {
                this.SessionSubtypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionTypeDescription
        {
            get
            {
                return this.SessionTypeDescriptionField;
            }
            set
            {
                this.SessionTypeDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionTypeId
        {
            get
            {
                return this.SessionTypeIdField;
            }
            set
            {
                this.SessionTypeIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FamilyProgram", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class FamilyProgram : DllWcfUtility.BeanBase
    {
        
        private int FamilyIdField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private int SelectedField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int FamilyId
        {
            get
            {
                return this.FamilyIdField;
            }
            set
            {
                this.FamilyIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Selected
        {
            get
            {
                return this.SelectedField;
            }
            set
            {
                this.SelectedField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AttendeeReport", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class AttendeeReport : DllWcfUtility.BeanBase
    {
        
        private string AtendeeLastNameField;
        
        private string AttendeeEmailField;
        
        private string AttendeeNameField;
        
        private int DeletedField;
        
        private int NavisionCodeField;
        
        private string OriginField;
        
        private int OriginIdField;
        
        private string SessionInitDateField;
        
        private string SessionNameField;
        
        private string UserField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AtendeeLastName
        {
            get
            {
                return this.AtendeeLastNameField;
            }
            set
            {
                this.AtendeeLastNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AttendeeEmail
        {
            get
            {
                return this.AttendeeEmailField;
            }
            set
            {
                this.AttendeeEmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AttendeeName
        {
            get
            {
                return this.AttendeeNameField;
            }
            set
            {
                this.AttendeeNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Deleted
        {
            get
            {
                return this.DeletedField;
            }
            set
            {
                this.DeletedField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NavisionCode
        {
            get
            {
                return this.NavisionCodeField;
            }
            set
            {
                this.NavisionCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Origin
        {
            get
            {
                return this.OriginField;
            }
            set
            {
                this.OriginField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OriginId
        {
            get
            {
                return this.OriginIdField;
            }
            set
            {
                this.OriginIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionInitDate
        {
            get
            {
                return this.SessionInitDateField;
            }
            set
            {
                this.SessionInitDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string User
        {
            get
            {
                return this.UserField;
            }
            set
            {
                this.UserField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NotificationSendResult", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NotificationSendResult : DllWcfUtility.BeanBase
    {
        
        private string EmailField;
        
        private int ErrorIndField;
        
        private int OpenedIndField;
        
        private int SendingIdField;
        
        private int SendingIndField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ErrorInd
        {
            get
            {
                return this.ErrorIndField;
            }
            set
            {
                this.ErrorIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OpenedInd
        {
            get
            {
                return this.OpenedIndField;
            }
            set
            {
                this.OpenedIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingId
        {
            get
            {
                return this.SendingIdField;
            }
            set
            {
                this.SendingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingInd
        {
            get
            {
                return this.SendingIndField;
            }
            set
            {
                this.SendingIndField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Newsletter", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class Newsletter : DllWcfUtility.BeanBase
    {
        
        private string ContentPathField;
        
        private string DescriptionField;
        
        private int IdField;
        
        private string NameField;
        
        private string SendDateField;
        
        private string SenderField;
        
        private int SendingIdField;
        
        private string SubjectField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ContentPath
        {
            get
            {
                return this.ContentPathField;
            }
            set
            {
                this.ContentPathField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SendDate
        {
            get
            {
                return this.SendDateField;
            }
            set
            {
                this.SendDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Sender
        {
            get
            {
                return this.SenderField;
            }
            set
            {
                this.SenderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingId
        {
            get
            {
                return this.SendingIdField;
            }
            set
            {
                this.SendingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Subject
        {
            get
            {
                return this.SubjectField;
            }
            set
            {
                this.SubjectField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SavedNewsletter", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class SavedNewsletter : DllWcfUtility.BeanBase
    {
        
        private string DescriptionField;
        
        private string HeaderField;
        
        private int IdField;
        
        private bool Ind_plazas_limitadasField;
        
        private string NameField;
        
        private string SavedDateField;
        
        private string SenderField;
        
        private string SubjectField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Header
        {
            get
            {
                return this.HeaderField;
            }
            set
            {
                this.HeaderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Ind_plazas_limitadas
        {
            get
            {
                return this.Ind_plazas_limitadasField;
            }
            set
            {
                this.Ind_plazas_limitadasField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SavedDate
        {
            get
            {
                return this.SavedDateField;
            }
            set
            {
                this.SavedDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Sender
        {
            get
            {
                return this.SenderField;
            }
            set
            {
                this.SenderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Subject
        {
            get
            {
                return this.SubjectField;
            }
            set
            {
                this.SubjectField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterRecipient", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterRecipient : DllWcfUtility.BeanBase
    {
        
        private int CSVNewsletterRecipientIdField;
        
        private int ContactCodeField;
        
        private string EmailField;
        
        private int IdField;
        
        private int NavisionCodeField;
        
        private int NewsletterIdField;
        
        private int SendingIdField;
        
        private string UserProIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int CSVNewsletterRecipientId
        {
            get
            {
                return this.CSVNewsletterRecipientIdField;
            }
            set
            {
                this.CSVNewsletterRecipientIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ContactCode
        {
            get
            {
                return this.ContactCodeField;
            }
            set
            {
                this.ContactCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NavisionCode
        {
            get
            {
                return this.NavisionCodeField;
            }
            set
            {
                this.NavisionCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingId
        {
            get
            {
                return this.SendingIdField;
            }
            set
            {
                this.SendingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserProId
        {
            get
            {
                return this.UserProIdField;
            }
            set
            {
                this.UserProIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProgramData", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class ProgramData : DllWcfUtility.BeanBase
    {
        
        private string descriptionField;
        
        private int idField;
        
        private string programNameField;
        
        private long programWebExIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string programName
        {
            get
            {
                return this.programNameField;
            }
            set
            {
                this.programNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long programWebExId
        {
            get
            {
                return this.programWebExIdField;
            }
            set
            {
                this.programWebExIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="VideoData", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class VideoData : DllWcfUtility.BeanBase
    {
        
        private string controlImageIdField;
        
        private int idField;
        
        private string imageURLField;
        
        private string titleField;
        
        private int typeField;
        
        private string videoIdField;
        
        private string videoURLField;
        
        private string vimeoVideoIDField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string controlImageId
        {
            get
            {
                return this.controlImageIdField;
            }
            set
            {
                this.controlImageIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string imageURL
        {
            get
            {
                return this.imageURLField;
            }
            set
            {
                this.imageURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string videoId
        {
            get
            {
                return this.videoIdField;
            }
            set
            {
                this.videoIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string videoURL
        {
            get
            {
                return this.videoURLField;
            }
            set
            {
                this.videoURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string vimeoVideoID
        {
            get
            {
                return this.vimeoVideoIDField;
            }
            set
            {
                this.vimeoVideoIDField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterSession", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterSession : DllWcfUtility.BeanBase
    {
        
        private int IdField;
        
        private int NewsletterIdField;
        
        private string ProgramDescriptionField;
        
        private string ProgramNameField;
        
        private long ProgramWebExIdField;
        
        private string SessionDateField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        private long WebExSessionIdField;
        
        private int newslettersessionsavedIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramDescription
        {
            get
            {
                return this.ProgramDescriptionField;
            }
            set
            {
                this.ProgramDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramWebExId
        {
            get
            {
                return this.ProgramWebExIdField;
            }
            set
            {
                this.ProgramWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionDate
        {
            get
            {
                return this.SessionDateField;
            }
            set
            {
                this.SessionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebExSessionId
        {
            get
            {
                return this.WebExSessionIdField;
            }
            set
            {
                this.WebExSessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int newslettersessionsavedId
        {
            get
            {
                return this.newslettersessionsavedIdField;
            }
            set
            {
                this.newslettersessionsavedIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterData", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterData : DllWcfUtility.BeanBase
    {
        
        private int ContactCodeField;
        
        private string EmailField;
        
        private int NavisionCodeField;
        
        private int NewsletterIdField;
        
        private int NewsletterRecipientIdField;
        
        private int NewsletterSessionIdField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        private long SessionWebexIdField;
        
        private string StartDateAproxField;
        
        private string UserIdProField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ContactCode
        {
            get
            {
                return this.ContactCodeField;
            }
            set
            {
                this.ContactCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NavisionCode
        {
            get
            {
                return this.NavisionCodeField;
            }
            set
            {
                this.NavisionCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterRecipientId
        {
            get
            {
                return this.NewsletterRecipientIdField;
            }
            set
            {
                this.NewsletterRecipientIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterSessionId
        {
            get
            {
                return this.NewsletterSessionIdField;
            }
            set
            {
                this.NewsletterSessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long SessionWebexId
        {
            get
            {
                return this.SessionWebexIdField;
            }
            set
            {
                this.SessionWebexIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDateAprox
        {
            get
            {
                return this.StartDateAproxField;
            }
            set
            {
                this.StartDateAproxField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UserIdPro
        {
            get
            {
                return this.UserIdProField;
            }
            set
            {
                this.UserIdProField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterRecipientProgramData", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterRecipientProgramData : DllWcfUtility.BeanBase
    {
        
        private int NewsletterIdField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private FormacionSvc.NewsletterRecipient RecipientField;
        
        private long SelectedSessionField;
        
        private DllWcfUtility.BeanList<FormacionSvc.NewsletterSession> SessionsField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public FormacionSvc.NewsletterRecipient Recipient
        {
            get
            {
                return this.RecipientField;
            }
            set
            {
                this.RecipientField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long SelectedSession
        {
            get
            {
                return this.SelectedSessionField;
            }
            set
            {
                this.SelectedSessionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public DllWcfUtility.BeanList<FormacionSvc.NewsletterSession> Sessions
        {
            get
            {
                return this.SessionsField;
            }
            set
            {
                this.SessionsField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="AttendeeNotification", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class AttendeeNotification : DllWcfUtility.BeanBase
    {
        
        private int AttendeeIdField;
        
        private long AttendeeWebExIdField;
        
        private string EmailField;
        
        private int IdField;
        
        private string LastNameField;
        
        private string NameField;
        
        private string NotifyDateField;
        
        private string NotifyDescriptionField;
        
        private int NotifyTypeIdField;
        
        private int SendingIdField;
        
        private long SessionWebExIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AttendeeId
        {
            get
            {
                return this.AttendeeIdField;
            }
            set
            {
                this.AttendeeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long AttendeeWebExId
        {
            get
            {
                return this.AttendeeWebExIdField;
            }
            set
            {
                this.AttendeeWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LastName
        {
            get
            {
                return this.LastNameField;
            }
            set
            {
                this.LastNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string NotifyDate
        {
            get
            {
                return this.NotifyDateField;
            }
            set
            {
                this.NotifyDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string NotifyDescription
        {
            get
            {
                return this.NotifyDescriptionField;
            }
            set
            {
                this.NotifyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NotifyTypeId
        {
            get
            {
                return this.NotifyTypeIdField;
            }
            set
            {
                this.NotifyTypeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingId
        {
            get
            {
                return this.SendingIdField;
            }
            set
            {
                this.SendingIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long SessionWebExId
        {
            get
            {
                return this.SessionWebExIdField;
            }
            set
            {
                this.SessionWebExIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterSendingStats", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterSendingStats : DllWcfUtility.BeanBase
    {
        
        private int BounceNumberField;
        
        private int ClicksNumberField;
        
        private string ClicksRatioField;
        
        private int DeletedUsesNumberField;
        
        private int DeliveredNumberField;
        
        private string DeliveredRatioField;
        
        private int OpenedNumbersField;
        
        private string OpenedRatioField;
        
        private int SendingNumberField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int BounceNumber
        {
            get
            {
                return this.BounceNumberField;
            }
            set
            {
                this.BounceNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ClicksNumber
        {
            get
            {
                return this.ClicksNumberField;
            }
            set
            {
                this.ClicksNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ClicksRatio
        {
            get
            {
                return this.ClicksRatioField;
            }
            set
            {
                this.ClicksRatioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int DeletedUsesNumber
        {
            get
            {
                return this.DeletedUsesNumberField;
            }
            set
            {
                this.DeletedUsesNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int DeliveredNumber
        {
            get
            {
                return this.DeliveredNumberField;
            }
            set
            {
                this.DeliveredNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string DeliveredRatio
        {
            get
            {
                return this.DeliveredRatioField;
            }
            set
            {
                this.DeliveredRatioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OpenedNumbers
        {
            get
            {
                return this.OpenedNumbersField;
            }
            set
            {
                this.OpenedNumbersField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OpenedRatio
        {
            get
            {
                return this.OpenedRatioField;
            }
            set
            {
                this.OpenedRatioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SendingNumber
        {
            get
            {
                return this.SendingNumberField;
            }
            set
            {
                this.SendingNumberField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterEmailStats", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterEmailStats : DllWcfUtility.BeanBase
    {
        
        private int BounceIndField;
        
        private string ClickDateField;
        
        private int ClicksNumberField;
        
        private string EmailField;
        
        private string ErrorDescriptionField;
        
        private string OpenDateField;
        
        private int OpenIndField;
        
        private string OperatingSystemField;
        
        private string PlatformField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int BounceInd
        {
            get
            {
                return this.BounceIndField;
            }
            set
            {
                this.BounceIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ClickDate
        {
            get
            {
                return this.ClickDateField;
            }
            set
            {
                this.ClickDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ClicksNumber
        {
            get
            {
                return this.ClicksNumberField;
            }
            set
            {
                this.ClicksNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorDescription
        {
            get
            {
                return this.ErrorDescriptionField;
            }
            set
            {
                this.ErrorDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OpenDate
        {
            get
            {
                return this.OpenDateField;
            }
            set
            {
                this.OpenDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OpenInd
        {
            get
            {
                return this.OpenIndField;
            }
            set
            {
                this.OpenIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OperatingSystem
        {
            get
            {
                return this.OperatingSystemField;
            }
            set
            {
                this.OperatingSystemField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Platform
        {
            get
            {
                return this.PlatformField;
            }
            set
            {
                this.PlatformField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterClicksStats", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterClicksStats : DllWcfUtility.BeanBase
    {
        
        private int ClicksNumberField;
        
        private string ClicksPercentField;
        
        private string LastClickDateField;
        
        private string URLField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ClicksNumber
        {
            get
            {
                return this.ClicksNumberField;
            }
            set
            {
                this.ClicksNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ClicksPercent
        {
            get
            {
                return this.ClicksPercentField;
            }
            set
            {
                this.ClicksPercentField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LastClickDate
        {
            get
            {
                return this.LastClickDateField;
            }
            set
            {
                this.LastClickDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string URL
        {
            get
            {
                return this.URLField;
            }
            set
            {
                this.URLField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterPlatformStats", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterPlatformStats : DllWcfUtility.BeanBase
    {
        
        private int DisplayNumberField;
        
        private string PlatformDescField;
        
        private string PlatformPercentField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int DisplayNumber
        {
            get
            {
                return this.DisplayNumberField;
            }
            set
            {
                this.DisplayNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PlatformDesc
        {
            get
            {
                return this.PlatformDescField;
            }
            set
            {
                this.PlatformDescField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PlatformPercent
        {
            get
            {
                return this.PlatformPercentField;
            }
            set
            {
                this.PlatformPercentField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterOSStats", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterOSStats : DllWcfUtility.BeanBase
    {
        
        private int DisplayNumberField;
        
        private string OSDescField;
        
        private string OSPercentField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int DisplayNumber
        {
            get
            {
                return this.DisplayNumberField;
            }
            set
            {
                this.DisplayNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OSDesc
        {
            get
            {
                return this.OSDescField;
            }
            set
            {
                this.OSDescField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OSPercent
        {
            get
            {
                return this.OSPercentField;
            }
            set
            {
                this.OSPercentField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NewsletterAttendee", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class NewsletterAttendee : DllWcfUtility.BeanBase
    {
        
        private int AttendeeIdField;
        
        private long AttendeeWebexIdField;
        
        private int ContactCodeField;
        
        private string EmailField;
        
        private string LastNameField;
        
        private string NameField;
        
        private int NavisionAccountCodeField;
        
        private int NewsletterIdField;
        
        private string NotesField;
        
        private int OriginIdField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private string RegistrationDateField;
        
        private string SessionDateField;
        
        private int SessionIdField;
        
        private string SessionNameField;
        
        private long SessionWebexIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AttendeeId
        {
            get
            {
                return this.AttendeeIdField;
            }
            set
            {
                this.AttendeeIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long AttendeeWebexId
        {
            get
            {
                return this.AttendeeWebexIdField;
            }
            set
            {
                this.AttendeeWebexIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ContactCode
        {
            get
            {
                return this.ContactCodeField;
            }
            set
            {
                this.ContactCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LastName
        {
            get
            {
                return this.LastNameField;
            }
            set
            {
                this.LastNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NavisionAccountCode
        {
            get
            {
                return this.NavisionAccountCodeField;
            }
            set
            {
                this.NavisionAccountCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int NewsletterId
        {
            get
            {
                return this.NewsletterIdField;
            }
            set
            {
                this.NewsletterIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Notes
        {
            get
            {
                return this.NotesField;
            }
            set
            {
                this.NotesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int OriginId
        {
            get
            {
                return this.OriginIdField;
            }
            set
            {
                this.OriginIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string RegistrationDate
        {
            get
            {
                return this.RegistrationDateField;
            }
            set
            {
                this.RegistrationDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionDate
        {
            get
            {
                return this.SessionDateField;
            }
            set
            {
                this.SessionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int SessionId
        {
            get
            {
                return this.SessionIdField;
            }
            set
            {
                this.SessionIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionName
        {
            get
            {
                return this.SessionNameField;
            }
            set
            {
                this.SessionNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long SessionWebexId
        {
            get
            {
                return this.SessionWebexIdField;
            }
            set
            {
                this.SessionWebexIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PlanFormacionCurso", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class PlanFormacionCurso : DllWcfUtility.BeanBase
    {
        
        private string AfterEnrollmentURLField;
        
        private double BudgetField;
        
        private int ExpectedEnrollmentField;
        
        private string FamilyField;
        
        private string FamilyDescriptionField;
        
        private int IdField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private long ProgramWebExIdField;
        
        private string StatusField;
        
        private string URLField;
        
        private string WebExHostIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AfterEnrollmentURL
        {
            get
            {
                return this.AfterEnrollmentURLField;
            }
            set
            {
                this.AfterEnrollmentURLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Budget
        {
            get
            {
                return this.BudgetField;
            }
            set
            {
                this.BudgetField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ExpectedEnrollment
        {
            get
            {
                return this.ExpectedEnrollmentField;
            }
            set
            {
                this.ExpectedEnrollmentField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Family
        {
            get
            {
                return this.FamilyField;
            }
            set
            {
                this.FamilyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long ProgramWebExId
        {
            get
            {
                return this.ProgramWebExIdField;
            }
            set
            {
                this.ProgramWebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Status
        {
            get
            {
                return this.StatusField;
            }
            set
            {
                this.StatusField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string URL
        {
            get
            {
                return this.URLField;
            }
            set
            {
                this.URLField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string WebExHostId
        {
            get
            {
                return this.WebExHostIdField;
            }
            set
            {
                this.WebExHostIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PlanFormacion", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class PlanFormacion : DllWcfUtility.BeanBase
    {
        
        private string FamilyDescriptionField;
        
        private int FamilyIdField;
        
        private int IdField;
        
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FamilyDescription
        {
            get
            {
                return this.FamilyDescriptionField;
            }
            set
            {
                this.FamilyDescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int FamilyId
        {
            get
            {
                return this.FamilyIdField;
            }
            set
            {
                this.FamilyIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PlanFormacionEmail", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class PlanFormacionEmail : DllWcfUtility.BeanBase
    {
        
        private int AssistProgramsNumberField;
        
        private string Desc_familiaField;
        
        private int IdField;
        
        private int Id_familiaField;
        
        private string NameField;
        
        private int PastRegisterProgramsNumberField;
        
        private int RegisterProgramsNumberField;
        
        private int TotalProgramsField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AssistProgramsNumber
        {
            get
            {
                return this.AssistProgramsNumberField;
            }
            set
            {
                this.AssistProgramsNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Desc_familia
        {
            get
            {
                return this.Desc_familiaField;
            }
            set
            {
                this.Desc_familiaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id_familia
        {
            get
            {
                return this.Id_familiaField;
            }
            set
            {
                this.Id_familiaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PastRegisterProgramsNumber
        {
            get
            {
                return this.PastRegisterProgramsNumberField;
            }
            set
            {
                this.PastRegisterProgramsNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int RegisterProgramsNumber
        {
            get
            {
                return this.RegisterProgramsNumberField;
            }
            set
            {
                this.RegisterProgramsNumberField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int TotalPrograms
        {
            get
            {
                return this.TotalProgramsField;
            }
            set
            {
                this.TotalProgramsField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PlanFormacionCursoEmail", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class PlanFormacionCursoEmail : DllWcfUtility.BeanBase
    {
        
        private string AssistDateField;
        
        private long CursoWebexIdField;
        
        private string PastSessionDateField;
        
        private int ProgramIdField;
        
        private string ProgramNameField;
        
        private int ProgramTypeField;
        
        private string ProgramTypeDescField;
        
        private int RegisterProgramIndField;
        
        private string SessionDateField;
        
        private long WebexIdField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AssistDate
        {
            get
            {
                return this.AssistDateField;
            }
            set
            {
                this.AssistDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long CursoWebexId
        {
            get
            {
                return this.CursoWebexIdField;
            }
            set
            {
                this.CursoWebexIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PastSessionDate
        {
            get
            {
                return this.PastSessionDateField;
            }
            set
            {
                this.PastSessionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramId
        {
            get
            {
                return this.ProgramIdField;
            }
            set
            {
                this.ProgramIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramName
        {
            get
            {
                return this.ProgramNameField;
            }
            set
            {
                this.ProgramNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProgramType
        {
            get
            {
                return this.ProgramTypeField;
            }
            set
            {
                this.ProgramTypeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramTypeDesc
        {
            get
            {
                return this.ProgramTypeDescField;
            }
            set
            {
                this.ProgramTypeDescField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int RegisterProgramInd
        {
            get
            {
                return this.RegisterProgramIndField;
            }
            set
            {
                this.RegisterProgramIndField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionDate
        {
            get
            {
                return this.SessionDateField;
            }
            set
            {
                this.SessionDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebexId
        {
            get
            {
                return this.WebexIdField;
            }
            set
            {
                this.WebexIdField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PlanFormacionCursoNavisionCode", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class PlanFormacionCursoNavisionCode : DllWcfUtility.BeanBase
    {
        
        private string AssistDatesField;
        
        private string AssistenteLastNameField;
        
        private string AssistenteNameField;
        
        private string ContactCodeField;
        
        private string EmailField;
        
        private string PastSessionsDatesField;
        
        private int PlanIdField;
        
        private FormacionSvc.PlanFormacionCursoEmail[] ProgramsField;
        
        private string ProgramsIdsField;
        
        private string ProgramsNamesField;
        
        private string RegistersProgramsField;
        
        private string SessionsDatesField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AssistDates
        {
            get
            {
                return this.AssistDatesField;
            }
            set
            {
                this.AssistDatesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AssistenteLastName
        {
            get
            {
                return this.AssistenteLastNameField;
            }
            set
            {
                this.AssistenteLastNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string AssistenteName
        {
            get
            {
                return this.AssistenteNameField;
            }
            set
            {
                this.AssistenteNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ContactCode
        {
            get
            {
                return this.ContactCodeField;
            }
            set
            {
                this.ContactCodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email
        {
            get
            {
                return this.EmailField;
            }
            set
            {
                this.EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PastSessionsDates
        {
            get
            {
                return this.PastSessionsDatesField;
            }
            set
            {
                this.PastSessionsDatesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int PlanId
        {
            get
            {
                return this.PlanIdField;
            }
            set
            {
                this.PlanIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public FormacionSvc.PlanFormacionCursoEmail[] Programs
        {
            get
            {
                return this.ProgramsField;
            }
            set
            {
                this.ProgramsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramsIds
        {
            get
            {
                return this.ProgramsIdsField;
            }
            set
            {
                this.ProgramsIdsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ProgramsNames
        {
            get
            {
                return this.ProgramsNamesField;
            }
            set
            {
                this.ProgramsNamesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string RegistersPrograms
        {
            get
            {
                return this.RegistersProgramsField;
            }
            set
            {
                this.RegistersProgramsField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionsDates
        {
            get
            {
                return this.SessionsDatesField;
            }
            set
            {
                this.SessionsDatesField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="EventExtended", Namespace="http://schemas.datacontract.org/2004/07/BOWcfFormacion")]
    public partial class EventExtended : object
    {
        
        private string DescriptionField;
        
        private int DurationField;
        
        private string EndDateField;
        
        private string NameField;
        
        private string StartDateField;
        
        private string StartDateProxyField;
        
        private long WebExIdField;
        
        private string enlaceayudaField;
        
        private string imagenField;
        
        private string impartidoporField;
        
        private string modalidadField;
        
        private string nivelField;
        
        private string numeroplazasrealesField;
        
        private string numeroplazasvisiblesField;
        
        private string observacionesField;
        
        private string precioField;
        
        private string recomendadoparaField;
        
        private string requisitostecnicosField;
        
        private string resumencontenidoField;
        
        private string textoinscripcionField;
        
        private string tituloField;
        
        private string ultimasplazasField;
        
        private string valorField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                this.DescriptionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Duration
        {
            get
            {
                return this.DurationField;
            }
            set
            {
                this.DurationField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EndDate
        {
            get
            {
                return this.EndDateField;
            }
            set
            {
                this.EndDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDate
        {
            get
            {
                return this.StartDateField;
            }
            set
            {
                this.StartDateField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StartDateProxy
        {
            get
            {
                return this.StartDateProxyField;
            }
            set
            {
                this.StartDateProxyField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long WebExId
        {
            get
            {
                return this.WebExIdField;
            }
            set
            {
                this.WebExIdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string enlaceayuda
        {
            get
            {
                return this.enlaceayudaField;
            }
            set
            {
                this.enlaceayudaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string imagen
        {
            get
            {
                return this.imagenField;
            }
            set
            {
                this.imagenField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string impartidopor
        {
            get
            {
                return this.impartidoporField;
            }
            set
            {
                this.impartidoporField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string modalidad
        {
            get
            {
                return this.modalidadField;
            }
            set
            {
                this.modalidadField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string nivel
        {
            get
            {
                return this.nivelField;
            }
            set
            {
                this.nivelField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string numeroplazasreales
        {
            get
            {
                return this.numeroplazasrealesField;
            }
            set
            {
                this.numeroplazasrealesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string numeroplazasvisibles
        {
            get
            {
                return this.numeroplazasvisiblesField;
            }
            set
            {
                this.numeroplazasvisiblesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string observaciones
        {
            get
            {
                return this.observacionesField;
            }
            set
            {
                this.observacionesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string precio
        {
            get
            {
                return this.precioField;
            }
            set
            {
                this.precioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string recomendadopara
        {
            get
            {
                return this.recomendadoparaField;
            }
            set
            {
                this.recomendadoparaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string requisitostecnicos
        {
            get
            {
                return this.requisitostecnicosField;
            }
            set
            {
                this.requisitostecnicosField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string resumencontenido
        {
            get
            {
                return this.resumencontenidoField;
            }
            set
            {
                this.resumencontenidoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string textoinscripcion
        {
            get
            {
                return this.textoinscripcionField;
            }
            set
            {
                this.textoinscripcionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string titulo
        {
            get
            {
                return this.tituloField;
            }
            set
            {
                this.tituloField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ultimasplazas
        {
            get
            {
                return this.ultimasplazasField;
            }
            set
            {
                this.ultimasplazasField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="FormacionSvc.IWcfFormacion")]
    public interface IWcfFormacion
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByFamilyAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesByOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesByOriginResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByOriginAsync(int ai_originId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesBySessionType", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesBySessionTypeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesBySessionTypeAsync(int ai_sessionTypeId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesByNavisionCode", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesByNavisionCodeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByNavisionCodeAsync(int ai_navisionAccountCode, int ai_familyId, string ai_dateFrom, string ai_dateUp, int ai_assistInd, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesProgramsByNavisionCode", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesProgramsByNavisionCodeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesProgramsByNavisionCodeAsync(int ai_navisionAccountCode, string ai_search, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesByFamilyAsync(int ai_familyId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamilyAssist", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamilyAssistResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyAssistReport>> GetGroupByAttendeesByFamilyAssistAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamilyByOriginAssist", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByFamilyByOriginAssistRespons" +
            "e")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyAssistReport>> GetGroupByAttendeesByFamilyByOriginAssistAsync(int ai_familyId, int ai_originId, int ai_programId, int ai_sessionId, string ai_date, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByProgramByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByProgramByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesByProgramByFamilyAsync(int ai_familyId, int ai_programId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesBySessionByFamilyAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByOriginResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesByOriginAsync(int ai_originId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByProgramByOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesByProgramByOriginResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesByProgramByOriginAsync(int ai_originId, int ai_programId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionByOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionByOriginResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesBySessionByOriginAsync(int ai_originId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionType", ReplyAction="http://tempuri.org/IWcfFormacion/GetGroupByAttendeesBySessionTypeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionTypeReport>> GetGroupByAttendeesBySessionTypeAsync(int ai_sessionTypeId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPrograms", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Program>> GetProgramsAsync(int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateProgram", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateProgramResponse")]
        System.Threading.Tasks.Task<int> UpdateProgramAsync(int ai_programId, int ai_familyId, int ai_typeId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNavisionClientPrograms", ReplyAction="http://tempuri.org/IWcfFormacion/GetNavisionClientProgramsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Program>> GetNavisionClientProgramsAsync(int IntNavisionClientId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetProgramEvents", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramEventsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramEventsAsync(long ai_webexProgramId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetProgramSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramSessionsAsync(int ai_programId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAllProgramEvents", ReplyAction="http://tempuri.org/IWcfFormacion/GetAllProgramEventsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetAllProgramEventsAsync(long ai_webexProgramId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPastProgramEvents", ReplyAction="http://tempuri.org/IWcfFormacion/GetPastProgramEventsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetPastProgramEventsAsync(long ai_webexProgramId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetUpdateAssistPastProgramEvents", ReplyAction="http://tempuri.org/IWcfFormacion/GetUpdateAssistPastProgramEventsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetUpdateAssistPastProgramEventsAsync(long ai_webexProgramId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetProgramEventsDate", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramEventsDateResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramEventsDateAsync(long ai_webexProgramId, System.DateTime ai_date, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/RegisterAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/RegisterAttendeeResponse")]
        System.Threading.Tasks.Task<int> RegisterAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/AssociateAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/AssociateAttendeeResponse")]
        System.Threading.Tasks.Task<int> AssociateAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateAttendeeResponse")]
        System.Threading.Tasks.Task<int> UpdateAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, string StrEmail, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/RegisterAttendeeExternal", ReplyAction="http://tempuri.org/IWcfFormacion/RegisterAttendeeExternalResponse")]
        System.Threading.Tasks.Task<int> RegisterAttendeeExternalAsync(long LongSessionKey, long LongMemberWebexId, string StrMemberName, string StrMemberLastName, string StrMemberMail, int IntNavisionAccountCode, int IntContactId, string StrNotes, int IntOriginId, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateAttendeesSessionAssist", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateAttendeesSessionAssistResponse")]
        System.Threading.Tasks.Task<int> UpdateAttendeesSessionAssistAsync(long ai_webExSessionId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateAttendeeAssist", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateAttendeeAssistResponse")]
        System.Threading.Tasks.Task<int> UpdateAttendeeAssistAsync(long ai_webExSessionId, string ai_email, int ai_assistInd, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SendRegisterMail", ReplyAction="http://tempuri.org/IWcfFormacion/SendRegisterMailResponse")]
        System.Threading.Tasks.Task<int> SendRegisterMailAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteAttendeeResponse")]
        System.Threading.Tasks.Task<int> DeleteAttendeeAsync(long LongSessionKey, string StrEmail, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SendDeleteMail", ReplyAction="http://tempuri.org/IWcfFormacion/SendDeleteMailResponse")]
        System.Threading.Tasks.Task<int> SendDeleteMailAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteAssociatedAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteAssociatedAttendeeResponse")]
        System.Threading.Tasks.Task<int> DeleteAssociatedAttendeeAsync(long LongSessionKey, string StrEmail, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/AcceptAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/AcceptAttendeeResponse")]
        System.Threading.Tasks.Task<int> AcceptAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/RejectAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/RejectAttendeeResponse")]
        System.Threading.Tasks.Task<int> RejectAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/PendingAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/PendingAttendeeResponse")]
        System.Threading.Tasks.Task<int> PendingAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetEventMembers", ReplyAction="http://tempuri.org/IWcfFormacion/GetEventMembersResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetEventMembersAsync(long LongSessionKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetEventAttendeeHistory", ReplyAction="http://tempuri.org/IWcfFormacion/GetEventAttendeeHistoryResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetEventAttendeeHistoryAsync(long LongSessionKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSessionUsersInfo", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionUsersInfoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.SessionInfo>> GetSessionUsersInfoAsync(long LongSessionKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/CreateEvent", ReplyAction="http://tempuri.org/IWcfFormacion/CreateEventResponse")]
        System.Threading.Tasks.Task<int> CreateEventAsync(
                    int ai_programId, 
                    string ai_name, 
                    string ai_password, 
                    long ai_initDateTimestamp, 
                    int ai_duration, 
                    int ai_sessionType, 
                    int ai_sessiontSubtype, 
                    bool ai_mute, 
                    string ai_entryExitTone, 
                    string ai_viewAttendeeList, 
                    int ai_participantLimit, 
                    bool ai_reminderEmail, 
                    int ai_reminderEmailTime, 
                    bool ai_reminderEmail2, 
                    int ai_reminderEmailTime2, 
                    bool ai_greetingEmail, 
                    bool ai_absenteeEmail, 
                    string ai_sessionFamily, 
                    byte[] ai_image, 
                    int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateEvent", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateEventResponse")]
        System.Threading.Tasks.Task<int> UpdateEventAsync(long ai_sessionKey, long ai_initDateTimestamp, int ai_duration, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateSessionEvent", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateSessionEventResponse")]
        System.Threading.Tasks.Task<int> UpdateSessionEventAsync(
                    long ai_sessionKey, 
                    string ai_name, 
                    string ai_password, 
                    long ai_initDateTimestamp, 
                    int ai_duration, 
                    int ai_sessionType, 
                    int ai_sessiontSubtype, 
                    bool ai_mute, 
                    string ai_entryExitTone, 
                    string ai_viewAttendeeList, 
                    int ai_participantLimit, 
                    bool ai_reminderEmail, 
                    int ai_reminderEmailTime, 
                    bool ai_reminderEmail2, 
                    int ai_reminderEmailTime2, 
                    bool ai_greetingEmail, 
                    bool ai_absenteeEmail, 
                    string ai_sessionFamily, 
                    byte[] ai_image, 
                    int ai_imageLength, 
                    int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/NotifyUpdateSession", ReplyAction="http://tempuri.org/IWcfFormacion/NotifyUpdateSessionResponse")]
        System.Threading.Tasks.Task<int> NotifyUpdateSessionAsync(long LongSessionKey, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/ResendUpdateSession", ReplyAction="http://tempuri.org/IWcfFormacion/ResendUpdateSessionResponse")]
        System.Threading.Tasks.Task<int> ResendUpdateSessionAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteEvent", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteEventResponse")]
        System.Threading.Tasks.Task<int> DeleteEventAsync(long LongSessionKey, string StrUserName, string StrUserEmail, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteHistoricEvent", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteHistoricEventResponse")]
        System.Threading.Tasks.Task<int> DeleteHistoricEventAsync(long LongSessionKey, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/NotifyDeleteSession", ReplyAction="http://tempuri.org/IWcfFormacion/NotifyDeleteSessionResponse")]
        System.Threading.Tasks.Task<int> NotifyDeleteSessionAsync(long LongSessionKey, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetDeletedSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetDeletedSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.DeletedEvent>> GetDeletedSessionsAsync(int ai_programId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendees", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesAsync(long ai_sessionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetDeletedAttendees", ReplyAction="http://tempuri.org/IWcfFormacion/GetDeletedAttendeesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetDeletedAttendeesAsync(long ai_sessionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetTiposCurso", ReplyAction="http://tempuri.org/IWcfFormacion/GetTiposCursoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.ProgramType>> GetTiposCursoAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetTiposSesion", ReplyAction="http://tempuri.org/IWcfFormacion/GetTiposSesionResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionType>> GetTiposSesionAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSubtiposSesion", ReplyAction="http://tempuri.org/IWcfFormacion/GetSubtiposSesionResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionSubtype>> GetSubtiposSesionAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetFamiliasSesion", ReplyAction="http://tempuri.org/IWcfFormacion/GetFamiliasSesionResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Familia>> GetFamiliasSesionAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetFamilias", ReplyAction="http://tempuri.org/IWcfFormacion/GetFamiliasResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Familia>> GetFamiliasAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeeOrigins", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeeOriginsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Origin>> GetAttendeeOriginsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAllAttendeeOrigins", ReplyAction="http://tempuri.org/IWcfFormacion/GetAllAttendeeOriginsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Origin>> GetAllAttendeeOriginsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/AddAttendeeOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/AddAttendeeOriginResponse")]
        System.Threading.Tasks.Task<int> AddAttendeeOriginAsync(string ai_name, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateAttendeeOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateAttendeeOriginResponse")]
        System.Threading.Tasks.Task<int> UpdateAttendeeOriginAsync(int ai_originId, string ai_name, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteAttendeeOrigin", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteAttendeeOriginResponse")]
        System.Threading.Tasks.Task<int> DeleteAttendeeOriginAsync(int ai_originId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/CheckAttendee", ReplyAction="http://tempuri.org/IWcfFormacion/CheckAttendeeResponse")]
        System.Threading.Tasks.Task<int> CheckAttendeeAsync(string StrEmail, long IntSessionId, int IntUserId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetParticipantLimit", ReplyAction="http://tempuri.org/IWcfFormacion/GetParticipantLimitResponse")]
        System.Threading.Tasks.Task<int> GetParticipantLimitAsync(long ai_sessionWebexId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSessionsReminder", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionsReminderResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSessionsReminderAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SendReminderMails", ReplyAction="http://tempuri.org/IWcfFormacion/SendReminderMailsResponse")]
        System.Threading.Tasks.Task<int> SendReminderMailsAsync(long ai_sessionKey, int ai_reminderMail, int ai_reminderTime, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetGreetingSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetGreetingSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetGreetingSessionsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SendGreetingMails", ReplyAction="http://tempuri.org/IWcfFormacion/SendGreetingMailsResponse")]
        System.Threading.Tasks.Task<int> SendGreetingMailsAsync(long ai_sessionKey, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAbsenteeSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetAbsenteeSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetAbsenteeSessionsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SendAbsenteeMails", ReplyAction="http://tempuri.org/IWcfFormacion/SendAbsenteeMailsResponse")]
        System.Threading.Tasks.Task<int> SendAbsenteeMailsAsync(long ai_sessionKey, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSessionsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSession", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.Event>> GetSessionAsync(long ai_webexId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSessionExtended", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionExtendedResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.EventExtended>> GetSessionExtendedAsync(long ai_webexId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/AddEventTemplate", ReplyAction="http://tempuri.org/IWcfFormacion/AddEventTemplateResponse")]
        System.Threading.Tasks.Task<int> AddEventTemplateAsync(string ai_name, string ai_password, string ai_description, int ai_sessionType, int ai_sessionSubtype, bool ai_reminderEmail, int ai_reminderEmailTime, bool ai_reminderEmail2, int ai_reminderEmailTime2, bool ai_greetingEmail, bool ai_absenteeEmail, string ai_sessionFamily, byte[] ai_image, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateEventTemplate", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateEventTemplateResponse")]
        System.Threading.Tasks.Task<int> UpdateEventTemplateAsync(int ai_templateId, string ai_name, string ai_password, string ai_description, int ai_sessionType, int ai_sessionSubtype, bool ai_reminderEmail, int ai_reminderEmailTime, bool ai_reminderEmail2, int ai_reminderEmailTime2, bool ai_greetingEmail, bool ai_absenteeEmail, string ai_sessionFamily, byte[] ai_image, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteEventTemplate", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteEventTemplateResponse")]
        System.Threading.Tasks.Task<int> DeleteEventTemplateAsync(int ai_templateId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetEventTemplates", ReplyAction="http://tempuri.org/IWcfFormacion/GetEventTemplatesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.EventTemplate>> GetEventTemplatesAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdateSessionAttendees", ReplyAction="http://tempuri.org/IWcfFormacion/UpdateSessionAttendeesResponse")]
        System.Threading.Tasks.Task<int> UpdateSessionAttendeesAsync(long ai_sessionKey);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetProgramsByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramsByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyProgram>> GetProgramsByFamilyAsync(int ai_familyId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeesByProgramByFamily", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeesByProgramByFamilyResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByProgramByFamilyAsync(int ai_familyId, int ai_assistInd, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeeReport", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeeReportResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.AttendeeReport>> GetAttendeeReportAsync(string ai_dateFrom, string ai_dateUp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetProgramNotificationResults", ReplyAction="http://tempuri.org/IWcfFormacion/GetProgramNotificationResultsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NotificationSendResult>> GetProgramNotificationResultsAsync(int IntProgramId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSessionNotificationResults", ReplyAction="http://tempuri.org/IWcfFormacion/GetSessionNotificationResultsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NotificationSendResult>> GetSessionNotificationResultsAsync(long LongSessionWebexId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletters", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewslettersResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Newsletter>> GetNewslettersAsync(int ai_pageNumber, int ai_registersPerPage, string ai_dateFrom, string ai_dateUp, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSavedNewsletters", ReplyAction="http://tempuri.org/IWcfFormacion/GetSavedNewslettersResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SavedNewsletter>> GetSavedNewslettersAsync(int ai_pageNumber, int ai_registersPerPage, string ai_dateFrom, string ai_dateUp, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/CreateNewsletters", ReplyAction="http://tempuri.org/IWcfFormacion/CreateNewslettersResponse")]
        System.Threading.Tasks.Task<int> CreateNewslettersAsync(string ai_name, string ai_description, string ai_sender, string ai_subject, DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient> ai_recipients, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, string ai_html, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/CreateNewsletterV2", ReplyAction="http://tempuri.org/IWcfFormacion/CreateNewsletterV2Response")]
        System.Threading.Tasks.Task<int> CreateNewsletterV2Async(string ai_name, string ai_description, string ai_sender, string ai_subject, DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient> ai_recipients, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, string ai_html, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SaveNewsletter", ReplyAction="http://tempuri.org/IWcfFormacion/SaveNewsletterResponse")]
        System.Threading.Tasks.Task<int> SaveNewsletterAsync(int id, string ai_name, string ai_description, string ai_sender, string ai_subject, string ai_header, int ai_template, int limitedAssitants, FormacionSvc.ProgramData[] ai_programs, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, int ai_userId, FormacionSvc.VideoData[] VideoDataList, int NewsletterModifOrCreate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteSavedNewsletter", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteSavedNewsletterResponse")]
        System.Threading.Tasks.Task<int> DeleteSavedNewsletterAsync(int ai_savedNewsletterId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterRecipients", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterRecipientsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterRecipient", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterRecipientResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientAsync(int ai_newsletterId, int ai_recipientId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSession>> GetNewsletterSessionsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSavedNewsletterSessions", ReplyAction="http://tempuri.org/IWcfFormacion/GetSavedNewsletterSessionsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSavedNewsletterSessionsAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSavedNewsletterPrograms", ReplyAction="http://tempuri.org/IWcfFormacion/GetSavedNewsletterProgramsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.ProgramData>> GetSavedNewsletterProgramsAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetSavedNewsletterVideos", ReplyAction="http://tempuri.org/IWcfFormacion/GetSavedNewsletterVideosResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.VideoData>> GetSavedNewsletterVideosAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterSessionsByProgram", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterSessionsByProgramResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSession>> GetNewsletterSessionsByProgramAsync(int ai_newsletterId, int ai_programId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletter", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.Newsletter>> GetNewsletterAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterHTML", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterHTMLResponse")]
        System.Threading.Tasks.Task<string> GetNewsletterHTMLAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetHTMLNewsletter", ReplyAction="http://tempuri.org/IWcfFormacion/GetHTMLNewsletterResponse")]
        System.Threading.Tasks.Task<string> GetHTMLNewsletterAsync(int templateId, int limitedAssistants, string header, FormacionSvc.Event[] sessionsData, FormacionSvc.ProgramData[] programsData, FormacionSvc.VideoData[] videoData);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterData", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterDataResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterData>> GetNewsletterDataAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterDataByRecipient", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterDataByRecipientResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterData>> GetNewsletterDataByRecipientAsync(int ai_newsletterId, int ai_recipientId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterProgramDataByRecipient", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterProgramDataByRecipientResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipientProgramData>> GetNewsletterProgramDataByRecipientAsync(int ai_newsletterId, int ai_recipientId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetAttendeeNotifications", ReplyAction="http://tempuri.org/IWcfFormacion/GetAttendeeNotificationsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.AttendeeNotification>> GetAttendeeNotificationsAsync(int ai_attendeeId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/SetRecipientsCSVFile", ReplyAction="http://tempuri.org/IWcfFormacion/SetRecipientsCSVFileResponse")]
        System.Threading.Tasks.Task<int> SetRecipientsCSVFileAsync(string ai_path);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterRecipientsCSV", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterRecipientsCSVResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientsCSVAsync(int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeleteNewsletterRecipientsCSV", ReplyAction="http://tempuri.org/IWcfFormacion/DeleteNewsletterRecipientsCSVResponse")]
        System.Threading.Tasks.Task<int> DeleteNewsletterRecipientsCSVAsync(int ai_newsletterRecipientCSVId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterSendingStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterSendingStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSendingStats>> GetNewsletterSendingStatsAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterOpenedStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterOpenedStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterOpenedStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterClickStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterClickStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterClickStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterStatsError", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterStatsErrorResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterStatsErrorAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterSendingClicksStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterSendingClicksStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterClicksStats>> GetNewsletterSendingClicksStatsAsync(int ai_newsletterId, string ai_email);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterEmailSendingStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterEmailSendingStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterEmailSendingStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterPlatformStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterPlatformStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterPlatformStats>> GetNewsletterPlatformStatsAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterOperatingSystemStats", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterOperatingSystemStatsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterOSStats>> GetNewsletterOperatingSystemStatsAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetNewsletterRegistrationAttendees", ReplyAction="http://tempuri.org/IWcfFormacion/GetNewsletterRegistrationAttendeesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterAttendee>> GetNewsletterRegistrationAttendeesAsync(int ai_newsletterId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/CreatePlan", ReplyAction="http://tempuri.org/IWcfFormacion/CreatePlanResponse")]
        System.Threading.Tasks.Task<int> CreatePlanAsync(string ai_name, int ai_familyId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeletePlan", ReplyAction="http://tempuri.org/IWcfFormacion/DeletePlanResponse")]
        System.Threading.Tasks.Task<int> DeletePlanAsync(int ai_planId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/UpdatePlan", ReplyAction="http://tempuri.org/IWcfFormacion/UpdatePlanResponse")]
        System.Threading.Tasks.Task<int> UpdatePlanAsync(int ai_planId, string ai_name, int ai_familyId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/AssociatePlan", ReplyAction="http://tempuri.org/IWcfFormacion/AssociatePlanResponse")]
        System.Threading.Tasks.Task<int> AssociatePlanAsync(int ai_planId, int ai_programId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/DeassociatePlan", ReplyAction="http://tempuri.org/IWcfFormacion/DeassociatePlanResponse")]
        System.Threading.Tasks.Task<int> DeassociatePlanAsync(int ai_associationId, int ai_userId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanPrograms", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanProgramsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCurso>> GetPlanProgramsAsync(int ai_planId, string ai_search, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanes", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacion>> GetPlanesAsync(int ai_planId, string ai_search, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanesByNavisionCode", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanesByNavisionCodeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacion>> GetPlanesByNavisionCodeAsync(int ai_navisionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanesByEmail", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanesByEmailResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionEmail>> GetPlanesByEmailAsync(string ai_email);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanesByEmailByEntrada", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanesByEmailByEntradaResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionEmail>> GetPlanesByEmailByEntradaAsync(string ai_email, string ai_userIdPro);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanProgramsByEmail", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanProgramsByEmailResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCursoEmail>> GetPlanProgramsByEmailAsync(int ai_planId, string ai_email);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IWcfFormacion/GetPlanProgramsByNavisionCode", ReplyAction="http://tempuri.org/IWcfFormacion/GetPlanProgramsByNavisionCodeResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCursoNavisionCode>> GetPlanProgramsByNavisionCodeAsync(int ai_navisionId, int ai_planId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface IWcfFormacionChannel : FormacionSvc.IWcfFormacion, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class WcfFormacionClient : System.ServiceModel.ClientBase<FormacionSvc.IWcfFormacion>, FormacionSvc.IWcfFormacion
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar el punto de conexión de servicio.
    /// </summary>
    /// <param name="serviceEndpoint">El punto de conexión para configurar</param>
    /// <param name="clientCredentials">Credenciales de cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public WcfFormacionClient() : 
                base(WcfFormacionClient.GetDefaultBinding(), WcfFormacionClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IWcfFormacion.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WcfFormacionClient(EndpointConfiguration endpointConfiguration) : 
                base(WcfFormacionClient.GetBindingForEndpoint(endpointConfiguration), WcfFormacionClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WcfFormacionClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(WcfFormacionClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WcfFormacionClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(WcfFormacionClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public WcfFormacionClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByFamilyAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesByFamilyAsync(ai_familyId, ai_programId, ai_sessionId, ai_typeGroupingId, ai_date, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByOriginAsync(int ai_originId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesByOriginAsync(ai_originId, ai_programId, ai_sessionId, ai_typeGroupingId, ai_date, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesBySessionTypeAsync(int ai_sessionTypeId, int ai_typeGroupingId, string ai_date, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesBySessionTypeAsync(ai_sessionTypeId, ai_typeGroupingId, ai_date, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByNavisionCodeAsync(int ai_navisionAccountCode, int ai_familyId, string ai_dateFrom, string ai_dateUp, int ai_assistInd, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesByNavisionCodeAsync(ai_navisionAccountCode, ai_familyId, ai_dateFrom, ai_dateUp, ai_assistInd, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesProgramsByNavisionCodeAsync(int ai_navisionAccountCode, string ai_search, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesProgramsByNavisionCodeAsync(ai_navisionAccountCode, ai_search, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesByFamilyAsync(int ai_familyId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByFamilyAsync(ai_familyId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyAssistReport>> GetGroupByAttendeesByFamilyAssistAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByFamilyAssistAsync(ai_familyId, ai_programId, ai_sessionId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyAssistReport>> GetGroupByAttendeesByFamilyByOriginAssistAsync(int ai_familyId, int ai_originId, int ai_programId, int ai_sessionId, string ai_date, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByFamilyByOriginAssistAsync(ai_familyId, ai_originId, ai_programId, ai_sessionId, ai_date, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesByProgramByFamilyAsync(int ai_familyId, int ai_programId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByProgramByFamilyAsync(ai_familyId, ai_programId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyReport>> GetGroupByAttendeesBySessionByFamilyAsync(int ai_familyId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesBySessionByFamilyAsync(ai_familyId, ai_programId, ai_sessionId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesByOriginAsync(int ai_originId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByOriginAsync(ai_originId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesByProgramByOriginAsync(int ai_originId, int ai_programId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesByProgramByOriginAsync(ai_originId, ai_programId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.OriginReport>> GetGroupByAttendeesBySessionByOriginAsync(int ai_originId, int ai_programId, int ai_sessionId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesBySessionByOriginAsync(ai_originId, ai_programId, ai_sessionId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionTypeReport>> GetGroupByAttendeesBySessionTypeAsync(int ai_sessionTypeId, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetGroupByAttendeesBySessionTypeAsync(ai_sessionTypeId, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Program>> GetProgramsAsync(int ai_userId)
        {
            return base.Channel.GetProgramsAsync(ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateProgramAsync(int ai_programId, int ai_familyId, int ai_typeId, int ai_userId)
        {
            return base.Channel.UpdateProgramAsync(ai_programId, ai_familyId, ai_typeId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Program>> GetNavisionClientProgramsAsync(int IntNavisionClientId)
        {
            return base.Channel.GetNavisionClientProgramsAsync(IntNavisionClientId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramEventsAsync(long ai_webexProgramId, int ai_userId)
        {
            return base.Channel.GetProgramEventsAsync(ai_webexProgramId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramSessionsAsync(int ai_programId, int ai_userId)
        {
            return base.Channel.GetProgramSessionsAsync(ai_programId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetAllProgramEventsAsync(long ai_webexProgramId, int ai_userId)
        {
            return base.Channel.GetAllProgramEventsAsync(ai_webexProgramId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetPastProgramEventsAsync(long ai_webexProgramId, int ai_userId)
        {
            return base.Channel.GetPastProgramEventsAsync(ai_webexProgramId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetUpdateAssistPastProgramEventsAsync(long ai_webexProgramId, int ai_userId)
        {
            return base.Channel.GetUpdateAssistPastProgramEventsAsync(ai_webexProgramId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetProgramEventsDateAsync(long ai_webexProgramId, System.DateTime ai_date, int ai_userId)
        {
            return base.Channel.GetProgramEventsDateAsync(ai_webexProgramId, ai_date, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> RegisterAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.RegisterAttendeeAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> AssociateAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.AssociateAttendeeAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, string StrEmail, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.UpdateAttendeeAsync(LongSessionKey, MemberTimeZoneId, StrEmail, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> RegisterAttendeeExternalAsync(long LongSessionKey, long LongMemberWebexId, string StrMemberName, string StrMemberLastName, string StrMemberMail, int IntNavisionAccountCode, int IntContactId, string StrNotes, int IntOriginId, int IntUserId)
        {
            return base.Channel.RegisterAttendeeExternalAsync(LongSessionKey, LongMemberWebexId, StrMemberName, StrMemberLastName, StrMemberMail, IntNavisionAccountCode, IntContactId, StrNotes, IntOriginId, IntUserId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateAttendeesSessionAssistAsync(long ai_webExSessionId, int ai_userId)
        {
            return base.Channel.UpdateAttendeesSessionAssistAsync(ai_webExSessionId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateAttendeeAssistAsync(long ai_webExSessionId, string ai_email, int ai_assistInd, int ai_userId)
        {
            return base.Channel.UpdateAttendeeAssistAsync(ai_webExSessionId, ai_email, ai_assistInd, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> SendRegisterMailAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId)
        {
            return base.Channel.SendRegisterMailAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteAttendeeAsync(long LongSessionKey, string StrEmail, int ai_userId)
        {
            return base.Channel.DeleteAttendeeAsync(LongSessionKey, StrEmail, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> SendDeleteMailAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId)
        {
            return base.Channel.SendDeleteMailAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteAssociatedAttendeeAsync(long LongSessionKey, string StrEmail, int ai_userId)
        {
            return base.Channel.DeleteAssociatedAttendeeAsync(LongSessionKey, StrEmail, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> AcceptAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.AcceptAttendeeAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> RejectAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.RejectAttendeeAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> PendingAttendeeAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_navisionAccountCode, int ai_contactId, int ai_userId)
        {
            return base.Channel.PendingAttendeeAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_navisionAccountCode, ai_contactId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetEventMembersAsync(long LongSessionKey)
        {
            return base.Channel.GetEventMembersAsync(LongSessionKey);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetEventAttendeeHistoryAsync(long LongSessionKey)
        {
            return base.Channel.GetEventAttendeeHistoryAsync(LongSessionKey);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.SessionInfo>> GetSessionUsersInfoAsync(long LongSessionKey)
        {
            return base.Channel.GetSessionUsersInfoAsync(LongSessionKey);
        }
        
        public System.Threading.Tasks.Task<int> CreateEventAsync(
                    int ai_programId, 
                    string ai_name, 
                    string ai_password, 
                    long ai_initDateTimestamp, 
                    int ai_duration, 
                    int ai_sessionType, 
                    int ai_sessiontSubtype, 
                    bool ai_mute, 
                    string ai_entryExitTone, 
                    string ai_viewAttendeeList, 
                    int ai_participantLimit, 
                    bool ai_reminderEmail, 
                    int ai_reminderEmailTime, 
                    bool ai_reminderEmail2, 
                    int ai_reminderEmailTime2, 
                    bool ai_greetingEmail, 
                    bool ai_absenteeEmail, 
                    string ai_sessionFamily, 
                    byte[] ai_image, 
                    int ai_userId)
        {
            return base.Channel.CreateEventAsync(ai_programId, ai_name, ai_password, ai_initDateTimestamp, ai_duration, ai_sessionType, ai_sessiontSubtype, ai_mute, ai_entryExitTone, ai_viewAttendeeList, ai_participantLimit, ai_reminderEmail, ai_reminderEmailTime, ai_reminderEmail2, ai_reminderEmailTime2, ai_greetingEmail, ai_absenteeEmail, ai_sessionFamily, ai_image, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateEventAsync(long ai_sessionKey, long ai_initDateTimestamp, int ai_duration, int ai_userId)
        {
            return base.Channel.UpdateEventAsync(ai_sessionKey, ai_initDateTimestamp, ai_duration, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateSessionEventAsync(
                    long ai_sessionKey, 
                    string ai_name, 
                    string ai_password, 
                    long ai_initDateTimestamp, 
                    int ai_duration, 
                    int ai_sessionType, 
                    int ai_sessiontSubtype, 
                    bool ai_mute, 
                    string ai_entryExitTone, 
                    string ai_viewAttendeeList, 
                    int ai_participantLimit, 
                    bool ai_reminderEmail, 
                    int ai_reminderEmailTime, 
                    bool ai_reminderEmail2, 
                    int ai_reminderEmailTime2, 
                    bool ai_greetingEmail, 
                    bool ai_absenteeEmail, 
                    string ai_sessionFamily, 
                    byte[] ai_image, 
                    int ai_imageLength, 
                    int ai_userId)
        {
            return base.Channel.UpdateSessionEventAsync(ai_sessionKey, ai_name, ai_password, ai_initDateTimestamp, ai_duration, ai_sessionType, ai_sessiontSubtype, ai_mute, ai_entryExitTone, ai_viewAttendeeList, ai_participantLimit, ai_reminderEmail, ai_reminderEmailTime, ai_reminderEmail2, ai_reminderEmailTime2, ai_greetingEmail, ai_absenteeEmail, ai_sessionFamily, ai_image, ai_imageLength, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> NotifyUpdateSessionAsync(long LongSessionKey, int IntUserId)
        {
            return base.Channel.NotifyUpdateSessionAsync(LongSessionKey, IntUserId);
        }
        
        public System.Threading.Tasks.Task<int> ResendUpdateSessionAsync(long LongSessionKey, long MemberTimeZoneId, FormacionSvc.Attendee ai_person, int ai_userId)
        {
            return base.Channel.ResendUpdateSessionAsync(LongSessionKey, MemberTimeZoneId, ai_person, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteEventAsync(long LongSessionKey, string StrUserName, string StrUserEmail, int IntUserId)
        {
            return base.Channel.DeleteEventAsync(LongSessionKey, StrUserName, StrUserEmail, IntUserId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteHistoricEventAsync(long LongSessionKey, int IntUserId)
        {
            return base.Channel.DeleteHistoricEventAsync(LongSessionKey, IntUserId);
        }
        
        public System.Threading.Tasks.Task<int> NotifyDeleteSessionAsync(long LongSessionKey, int IntUserId)
        {
            return base.Channel.NotifyDeleteSessionAsync(LongSessionKey, IntUserId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.DeletedEvent>> GetDeletedSessionsAsync(int ai_programId)
        {
            return base.Channel.GetDeletedSessionsAsync(ai_programId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesAsync(long ai_sessionId)
        {
            return base.Channel.GetAttendeesAsync(ai_sessionId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetDeletedAttendeesAsync(long ai_sessionId)
        {
            return base.Channel.GetDeletedAttendeesAsync(ai_sessionId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.ProgramType>> GetTiposCursoAsync()
        {
            return base.Channel.GetTiposCursoAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionType>> GetTiposSesionAsync()
        {
            return base.Channel.GetTiposSesionAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SessionSubtype>> GetSubtiposSesionAsync()
        {
            return base.Channel.GetSubtiposSesionAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Familia>> GetFamiliasSesionAsync()
        {
            return base.Channel.GetFamiliasSesionAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Familia>> GetFamiliasAsync()
        {
            return base.Channel.GetFamiliasAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Origin>> GetAttendeeOriginsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda)
        {
            return base.Channel.GetAttendeeOriginsAsync(IntPagina, IntNumeroRegistros, StrCriterioBusqueda);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Origin>> GetAllAttendeeOriginsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda)
        {
            return base.Channel.GetAllAttendeeOriginsAsync(IntPagina, IntNumeroRegistros, StrCriterioBusqueda);
        }
        
        public System.Threading.Tasks.Task<int> AddAttendeeOriginAsync(string ai_name, int ai_userId)
        {
            return base.Channel.AddAttendeeOriginAsync(ai_name, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateAttendeeOriginAsync(int ai_originId, string ai_name, int ai_userId)
        {
            return base.Channel.UpdateAttendeeOriginAsync(ai_originId, ai_name, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteAttendeeOriginAsync(int ai_originId, int ai_userId)
        {
            return base.Channel.DeleteAttendeeOriginAsync(ai_originId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> CheckAttendeeAsync(string StrEmail, long IntSessionId, int IntUserId)
        {
            return base.Channel.CheckAttendeeAsync(StrEmail, IntSessionId, IntUserId);
        }
        
        public System.Threading.Tasks.Task<int> GetParticipantLimitAsync(long ai_sessionWebexId)
        {
            return base.Channel.GetParticipantLimitAsync(ai_sessionWebexId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSessionsReminderAsync()
        {
            return base.Channel.GetSessionsReminderAsync();
        }
        
        public System.Threading.Tasks.Task<int> SendReminderMailsAsync(long ai_sessionKey, int ai_reminderMail, int ai_reminderTime, int ai_userId)
        {
            return base.Channel.SendReminderMailsAsync(ai_sessionKey, ai_reminderMail, ai_reminderTime, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetGreetingSessionsAsync()
        {
            return base.Channel.GetGreetingSessionsAsync();
        }
        
        public System.Threading.Tasks.Task<int> SendGreetingMailsAsync(long ai_sessionKey, int ai_userId)
        {
            return base.Channel.SendGreetingMailsAsync(ai_sessionKey, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetAbsenteeSessionsAsync()
        {
            return base.Channel.GetAbsenteeSessionsAsync();
        }
        
        public System.Threading.Tasks.Task<int> SendAbsenteeMailsAsync(long ai_sessionKey, int ai_userId)
        {
            return base.Channel.SendAbsenteeMailsAsync(ai_sessionKey, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSessionsAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda)
        {
            return base.Channel.GetSessionsAsync(IntPagina, IntNumeroRegistros, StrCriterioBusqueda);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.Event>> GetSessionAsync(long ai_webexId)
        {
            return base.Channel.GetSessionAsync(ai_webexId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.EventExtended>> GetSessionExtendedAsync(long ai_webexId)
        {
            return base.Channel.GetSessionExtendedAsync(ai_webexId);
        }
        
        public System.Threading.Tasks.Task<int> AddEventTemplateAsync(string ai_name, string ai_password, string ai_description, int ai_sessionType, int ai_sessionSubtype, bool ai_reminderEmail, int ai_reminderEmailTime, bool ai_reminderEmail2, int ai_reminderEmailTime2, bool ai_greetingEmail, bool ai_absenteeEmail, string ai_sessionFamily, byte[] ai_image, int ai_userId)
        {
            return base.Channel.AddEventTemplateAsync(ai_name, ai_password, ai_description, ai_sessionType, ai_sessionSubtype, ai_reminderEmail, ai_reminderEmailTime, ai_reminderEmail2, ai_reminderEmailTime2, ai_greetingEmail, ai_absenteeEmail, ai_sessionFamily, ai_image, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdateEventTemplateAsync(int ai_templateId, string ai_name, string ai_password, string ai_description, int ai_sessionType, int ai_sessionSubtype, bool ai_reminderEmail, int ai_reminderEmailTime, bool ai_reminderEmail2, int ai_reminderEmailTime2, bool ai_greetingEmail, bool ai_absenteeEmail, string ai_sessionFamily, byte[] ai_image, int ai_userId)
        {
            return base.Channel.UpdateEventTemplateAsync(ai_templateId, ai_name, ai_password, ai_description, ai_sessionType, ai_sessionSubtype, ai_reminderEmail, ai_reminderEmailTime, ai_reminderEmail2, ai_reminderEmailTime2, ai_greetingEmail, ai_absenteeEmail, ai_sessionFamily, ai_image, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeleteEventTemplateAsync(int ai_templateId, int ai_userId)
        {
            return base.Channel.DeleteEventTemplateAsync(ai_templateId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.EventTemplate>> GetEventTemplatesAsync(int IntPagina, int IntNumeroRegistros, string StrCriterioBusqueda)
        {
            return base.Channel.GetEventTemplatesAsync(IntPagina, IntNumeroRegistros, StrCriterioBusqueda);
        }
        
        public System.Threading.Tasks.Task<int> UpdateSessionAttendeesAsync(long ai_sessionKey)
        {
            return base.Channel.UpdateSessionAttendeesAsync(ai_sessionKey);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.FamilyProgram>> GetProgramsByFamilyAsync(int ai_familyId)
        {
            return base.Channel.GetProgramsByFamilyAsync(ai_familyId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Attendee>> GetAttendeesByProgramByFamilyAsync(int ai_familyId, int ai_assistInd, int ai_typeGroupingId, string ai_dateFrom, string ai_dateUp, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetAttendeesByProgramByFamilyAsync(ai_familyId, ai_assistInd, ai_typeGroupingId, ai_dateFrom, ai_dateUp, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.AttendeeReport>> GetAttendeeReportAsync(string ai_dateFrom, string ai_dateUp)
        {
            return base.Channel.GetAttendeeReportAsync(ai_dateFrom, ai_dateUp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NotificationSendResult>> GetProgramNotificationResultsAsync(int IntProgramId)
        {
            return base.Channel.GetProgramNotificationResultsAsync(IntProgramId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NotificationSendResult>> GetSessionNotificationResultsAsync(long LongSessionWebexId)
        {
            return base.Channel.GetSessionNotificationResultsAsync(LongSessionWebexId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Newsletter>> GetNewslettersAsync(int ai_pageNumber, int ai_registersPerPage, string ai_dateFrom, string ai_dateUp, string ai_searchCriteria)
        {
            return base.Channel.GetNewslettersAsync(ai_pageNumber, ai_registersPerPage, ai_dateFrom, ai_dateUp, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.SavedNewsletter>> GetSavedNewslettersAsync(int ai_pageNumber, int ai_registersPerPage, string ai_dateFrom, string ai_dateUp, string ai_searchCriteria)
        {
            return base.Channel.GetSavedNewslettersAsync(ai_pageNumber, ai_registersPerPage, ai_dateFrom, ai_dateUp, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<int> CreateNewslettersAsync(string ai_name, string ai_description, string ai_sender, string ai_subject, DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient> ai_recipients, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, string ai_html, int ai_userId)
        {
            return base.Channel.CreateNewslettersAsync(ai_name, ai_description, ai_sender, ai_subject, ai_recipients, ai_events, ai_html, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> CreateNewsletterV2Async(string ai_name, string ai_description, string ai_sender, string ai_subject, DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient> ai_recipients, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, string ai_html, int ai_userId)
        {
            return base.Channel.CreateNewsletterV2Async(ai_name, ai_description, ai_sender, ai_subject, ai_recipients, ai_events, ai_html, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> SaveNewsletterAsync(int id, string ai_name, string ai_description, string ai_sender, string ai_subject, string ai_header, int ai_template, int limitedAssitants, FormacionSvc.ProgramData[] ai_programs, DllWcfUtility.BeanList<FormacionSvc.Event> ai_events, int ai_userId, FormacionSvc.VideoData[] VideoDataList, int NewsletterModifOrCreate)
        {
            return base.Channel.SaveNewsletterAsync(id, ai_name, ai_description, ai_sender, ai_subject, ai_header, ai_template, limitedAssitants, ai_programs, ai_events, ai_userId, VideoDataList, NewsletterModifOrCreate);
        }
        
        public System.Threading.Tasks.Task<int> DeleteSavedNewsletterAsync(int ai_savedNewsletterId, int ai_userId)
        {
            return base.Channel.DeleteSavedNewsletterAsync(ai_savedNewsletterId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria)
        {
            return base.Channel.GetNewsletterRecipientsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientAsync(int ai_newsletterId, int ai_recipientId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria)
        {
            return base.Channel.GetNewsletterRecipientAsync(ai_newsletterId, ai_recipientId, ai_pageNumber, ai_registersPerPage, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSession>> GetNewsletterSessionsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria)
        {
            return base.Channel.GetNewsletterSessionsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.Event>> GetSavedNewsletterSessionsAsync(int ai_newsletterId)
        {
            return base.Channel.GetSavedNewsletterSessionsAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.ProgramData>> GetSavedNewsletterProgramsAsync(int ai_newsletterId)
        {
            return base.Channel.GetSavedNewsletterProgramsAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.VideoData>> GetSavedNewsletterVideosAsync(int ai_newsletterId)
        {
            return base.Channel.GetSavedNewsletterVideosAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSession>> GetNewsletterSessionsByProgramAsync(int ai_newsletterId, int ai_programId, int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria)
        {
            return base.Channel.GetNewsletterSessionsByProgramAsync(ai_newsletterId, ai_programId, ai_pageNumber, ai_registersPerPage, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<FormacionSvc.Newsletter>> GetNewsletterAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<string> GetNewsletterHTMLAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterHTMLAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<string> GetHTMLNewsletterAsync(int templateId, int limitedAssistants, string header, FormacionSvc.Event[] sessionsData, FormacionSvc.ProgramData[] programsData, FormacionSvc.VideoData[] videoData)
        {
            return base.Channel.GetHTMLNewsletterAsync(templateId, limitedAssistants, header, sessionsData, programsData, videoData);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterData>> GetNewsletterDataAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterDataAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterData>> GetNewsletterDataByRecipientAsync(int ai_newsletterId, int ai_recipientId)
        {
            return base.Channel.GetNewsletterDataByRecipientAsync(ai_newsletterId, ai_recipientId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipientProgramData>> GetNewsletterProgramDataByRecipientAsync(int ai_newsletterId, int ai_recipientId)
        {
            return base.Channel.GetNewsletterProgramDataByRecipientAsync(ai_newsletterId, ai_recipientId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.AttendeeNotification>> GetAttendeeNotificationsAsync(int ai_attendeeId)
        {
            return base.Channel.GetAttendeeNotificationsAsync(ai_attendeeId);
        }
        
        public System.Threading.Tasks.Task<int> SetRecipientsCSVFileAsync(string ai_path)
        {
            return base.Channel.SetRecipientsCSVFileAsync(ai_path);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterRecipient>> GetNewsletterRecipientsCSVAsync(int ai_pageNumber, int ai_registersPerPage, string ai_searchCriteria)
        {
            return base.Channel.GetNewsletterRecipientsCSVAsync(ai_pageNumber, ai_registersPerPage, ai_searchCriteria);
        }
        
        public System.Threading.Tasks.Task<int> DeleteNewsletterRecipientsCSVAsync(int ai_newsletterRecipientCSVId, int ai_userId)
        {
            return base.Channel.DeleteNewsletterRecipientsCSVAsync(ai_newsletterRecipientCSVId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterSendingStats>> GetNewsletterSendingStatsAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterSendingStatsAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetNewsletterStatsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterOpenedStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetNewsletterOpenedStatsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterClickStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetNewsletterClickStatsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterStatsErrorAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetNewsletterStatsErrorAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterClicksStats>> GetNewsletterSendingClicksStatsAsync(int ai_newsletterId, string ai_email)
        {
            return base.Channel.GetNewsletterSendingClicksStatsAsync(ai_newsletterId, ai_email);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterEmailStats>> GetNewsletterEmailSendingStatsAsync(int ai_newsletterId, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetNewsletterEmailSendingStatsAsync(ai_newsletterId, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterPlatformStats>> GetNewsletterPlatformStatsAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterPlatformStatsAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterOSStats>> GetNewsletterOperatingSystemStatsAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterOperatingSystemStatsAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.NewsletterAttendee>> GetNewsletterRegistrationAttendeesAsync(int ai_newsletterId)
        {
            return base.Channel.GetNewsletterRegistrationAttendeesAsync(ai_newsletterId);
        }
        
        public System.Threading.Tasks.Task<int> CreatePlanAsync(string ai_name, int ai_familyId, int ai_userId)
        {
            return base.Channel.CreatePlanAsync(ai_name, ai_familyId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeletePlanAsync(int ai_planId, int ai_userId)
        {
            return base.Channel.DeletePlanAsync(ai_planId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> UpdatePlanAsync(int ai_planId, string ai_name, int ai_familyId, int ai_userId)
        {
            return base.Channel.UpdatePlanAsync(ai_planId, ai_name, ai_familyId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> AssociatePlanAsync(int ai_planId, int ai_programId, int ai_userId)
        {
            return base.Channel.AssociatePlanAsync(ai_planId, ai_programId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<int> DeassociatePlanAsync(int ai_associationId, int ai_userId)
        {
            return base.Channel.DeassociatePlanAsync(ai_associationId, ai_userId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCurso>> GetPlanProgramsAsync(int ai_planId, string ai_search, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetPlanProgramsAsync(ai_planId, ai_search, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacion>> GetPlanesAsync(int ai_planId, string ai_search, string ai_orderField, string ai_orderType, int ai_pageNumber, int ai_registersPerPage)
        {
            return base.Channel.GetPlanesAsync(ai_planId, ai_search, ai_orderField, ai_orderType, ai_pageNumber, ai_registersPerPage);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacion>> GetPlanesByNavisionCodeAsync(int ai_navisionId)
        {
            return base.Channel.GetPlanesByNavisionCodeAsync(ai_navisionId);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionEmail>> GetPlanesByEmailAsync(string ai_email)
        {
            return base.Channel.GetPlanesByEmailAsync(ai_email);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionEmail>> GetPlanesByEmailByEntradaAsync(string ai_email, string ai_userIdPro)
        {
            return base.Channel.GetPlanesByEmailByEntradaAsync(ai_email, ai_userIdPro);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCursoEmail>> GetPlanProgramsByEmailAsync(int ai_planId, string ai_email)
        {
            return base.Channel.GetPlanProgramsByEmailAsync(ai_planId, ai_email);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<FormacionSvc.PlanFormacionCursoNavisionCode>> GetPlanProgramsByNavisionCodeAsync(int ai_navisionId, int ai_planId)
        {
            return base.Channel.GetPlanProgramsByNavisionCodeAsync(ai_navisionId, ai_planId);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IWcfFormacion))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IWcfFormacion))
            {
                return new System.ServiceModel.EndpointAddress("http://led-wcfformacion/WcfFormacion.svc");
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return WcfFormacionClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IWcfFormacion);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return WcfFormacionClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IWcfFormacion);
        }
        
        public enum EndpointConfiguration
        {
            
            BasicHttpBinding_IWcfFormacion,
        }
    }
}
