﻿//------------------------------------------------------------------------------
// <generado automáticamente>
//     Este código fue generado por una herramienta.
//     //
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </generado automáticamente>
//------------------------------------------------------------------------------

namespace DocumentacionSvc
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Fichero", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class Fichero : DllWcfUtility.BeanBase
    {
        
        private string _extensionField;
        
        private string _fechaField;
        
        private int _idFicheroField;
        
        private int _idUsuarioField;
        
        private int _indVisibleField;
        
        private string _nombreField;
        
        private string _nombreUsuarioField;
        
        private string _sizeField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _extension
        {
            get
            {
                return this._extensionField;
            }
            set
            {
                this._extensionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _fecha
        {
            get
            {
                return this._fechaField;
            }
            set
            {
                this._fechaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idFichero
        {
            get
            {
                return this._idFicheroField;
            }
            set
            {
                this._idFicheroField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idUsuario
        {
            get
            {
                return this._idUsuarioField;
            }
            set
            {
                this._idUsuarioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _indVisible
        {
            get
            {
                return this._indVisibleField;
            }
            set
            {
                this._indVisibleField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _nombre
        {
            get
            {
                return this._nombreField;
            }
            set
            {
                this._nombreField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _nombreUsuario
        {
            get
            {
                return this._nombreUsuarioField;
            }
            set
            {
                this._nombreUsuarioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _size
        {
            get
            {
                return this._sizeField;
            }
            set
            {
                this._sizeField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="NRef", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class NRef : DllWcfUtility.BeanBase
    {
        
        private int _idNrefField;
        
        private string _nrefField;
        
        private string _tipoField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idNref
        {
            get
            {
                return this._idNrefField;
            }
            set
            {
                this._idNrefField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _nref
        {
            get
            {
                return this._nrefField;
            }
            set
            {
                this._nrefField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _tipo
        {
            get
            {
                return this._tipoField;
            }
            set
            {
                this._tipoField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Estado", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class Estado : DllWcfUtility.BeanBase
    {
        
        private string _descDocumentoEstadoField;
        
        private int _idDocumentoEstadoField;
        
        private int _indResueltaField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descDocumentoEstado
        {
            get
            {
                return this._descDocumentoEstadoField;
            }
            set
            {
                this._descDocumentoEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idDocumentoEstado
        {
            get
            {
                return this._idDocumentoEstadoField;
            }
            set
            {
                this._idDocumentoEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _indResuelta
        {
            get
            {
                return this._indResueltaField;
            }
            set
            {
                this._indResueltaField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TipoDocumento", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class TipoDocumento : DllWcfUtility.BeanBase
    {
        
        private string _descTipoDocumentoField;
        
        private int _idTipoDocumentoField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descTipoDocumento
        {
            get
            {
                return this._descTipoDocumentoField;
            }
            set
            {
                this._descTipoDocumentoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idTipoDocumento
        {
            get
            {
                return this._idTipoDocumentoField;
            }
            set
            {
                this._idTipoDocumentoField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Documento", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class Documento : DllWcfUtility.BeanBase
    {
        
        private string _UsuarioAsignadoField;
        
        private string _apellidosField;
        
        private string _descDocumentoEstadoField;
        
        private string _descDocumentoSubEstadoField;
        
        private string _descTipoDocumentoField;
        
        private string _emailField;
        
        private string _emailColaboradorField;
        
        private string _fechaPeticionField;
        
        private int _idClienteNavField;
        
        private int _idColaboradorField;
        
        private int _idColorField;
        
        private int _idDocumentoField;
        
        private int _idDocumentoEstadoField;
        
        private int _idDocumentoSubEstadoField;
        
        private int _idTipoDocumentoField;
        
        private int _idUsuarioAsignadoField;
        
        private string _idUsuarioProField;
        
        private string _nombreField;
        
        private string _nombreColaboradorField;
        
        private string _observacionesField;
        
        private string _peticionField;
        
        private string _tlfnoField;
        
        private string _usuarioCreadorField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _UsuarioAsignado
        {
            get
            {
                return this._UsuarioAsignadoField;
            }
            set
            {
                this._UsuarioAsignadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _apellidos
        {
            get
            {
                return this._apellidosField;
            }
            set
            {
                this._apellidosField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descDocumentoEstado
        {
            get
            {
                return this._descDocumentoEstadoField;
            }
            set
            {
                this._descDocumentoEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descDocumentoSubEstado
        {
            get
            {
                return this._descDocumentoSubEstadoField;
            }
            set
            {
                this._descDocumentoSubEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descTipoDocumento
        {
            get
            {
                return this._descTipoDocumentoField;
            }
            set
            {
                this._descTipoDocumentoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _email
        {
            get
            {
                return this._emailField;
            }
            set
            {
                this._emailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _emailColaborador
        {
            get
            {
                return this._emailColaboradorField;
            }
            set
            {
                this._emailColaboradorField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _fechaPeticion
        {
            get
            {
                return this._fechaPeticionField;
            }
            set
            {
                this._fechaPeticionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idClienteNav
        {
            get
            {
                return this._idClienteNavField;
            }
            set
            {
                this._idClienteNavField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idColaborador
        {
            get
            {
                return this._idColaboradorField;
            }
            set
            {
                this._idColaboradorField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idColor
        {
            get
            {
                return this._idColorField;
            }
            set
            {
                this._idColorField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idDocumento
        {
            get
            {
                return this._idDocumentoField;
            }
            set
            {
                this._idDocumentoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idDocumentoEstado
        {
            get
            {
                return this._idDocumentoEstadoField;
            }
            set
            {
                this._idDocumentoEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idDocumentoSubEstado
        {
            get
            {
                return this._idDocumentoSubEstadoField;
            }
            set
            {
                this._idDocumentoSubEstadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idTipoDocumento
        {
            get
            {
                return this._idTipoDocumentoField;
            }
            set
            {
                this._idTipoDocumentoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idUsuarioAsignado
        {
            get
            {
                return this._idUsuarioAsignadoField;
            }
            set
            {
                this._idUsuarioAsignadoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _idUsuarioPro
        {
            get
            {
                return this._idUsuarioProField;
            }
            set
            {
                this._idUsuarioProField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _nombre
        {
            get
            {
                return this._nombreField;
            }
            set
            {
                this._nombreField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _nombreColaborador
        {
            get
            {
                return this._nombreColaboradorField;
            }
            set
            {
                this._nombreColaboradorField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _observaciones
        {
            get
            {
                return this._observacionesField;
            }
            set
            {
                this._observacionesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _peticion
        {
            get
            {
                return this._peticionField;
            }
            set
            {
                this._peticionField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _tlfno
        {
            get
            {
                return this._tlfnoField;
            }
            set
            {
                this._tlfnoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _usuarioCreador
        {
            get
            {
                return this._usuarioCreadorField;
            }
            set
            {
                this._usuarioCreadorField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Usuario", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class Usuario : DllWcfUtility.BeanBase
    {
        
        private string _EmailField;
        
        private string _NombreField;
        
        private int _idUsuarioField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _Email
        {
            get
            {
                return this._EmailField;
            }
            set
            {
                this._EmailField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _Nombre
        {
            get
            {
                return this._NombreField;
            }
            set
            {
                this._NombreField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idUsuario
        {
            get
            {
                return this._idUsuarioField;
            }
            set
            {
                this._idUsuarioField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Comentario", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class Comentario : DllWcfUtility.BeanBase
    {
        
        private string _descComentarioField;
        
        private string _descUsuarioAppField;
        
        private string _fechaComentarioField;
        
        private int _idComentarioField;
        
        private int _idDocumentoField;
        
        private int _idUsuarioAppField;
        
        private int _indInternoField;
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descComentario
        {
            get
            {
                return this._descComentarioField;
            }
            set
            {
                this._descComentarioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _descUsuarioApp
        {
            get
            {
                return this._descUsuarioAppField;
            }
            set
            {
                this._descUsuarioAppField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string _fechaComentario
        {
            get
            {
                return this._fechaComentarioField;
            }
            set
            {
                this._fechaComentarioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idComentario
        {
            get
            {
                return this._idComentarioField;
            }
            set
            {
                this._idComentarioField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idDocumento
        {
            get
            {
                return this._idDocumentoField;
            }
            set
            {
                this._idDocumentoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _idUsuarioApp
        {
            get
            {
                return this._idUsuarioAppField;
            }
            set
            {
                this._idUsuarioAppField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int _indInterno
        {
            get
            {
                return this._indInternoField;
            }
            set
            {
                this._indInternoField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TipoJurisdiccion", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class TipoJurisdiccion : object
    {
        
        private string codigoField;
        
        private string descripcionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TipoTribunal", Namespace="http://schemas.datacontract.org/2004/07/BOWcfGestionDocumentacion")]
    public partial class TipoTribunal : object
    {
        
        private string codigoField;
        
        private string descripcionField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string descripcion
        {
            get
            {
                return this.descripcionField;
            }
            set
            {
                this.descripcionField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DocumentacionSvc.IServiceGestionDocumentacion")]
    public interface IServiceGestionDocumentacion
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/InsertarFichero", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/InsertarFicheroResponse")]
        System.Threading.Tasks.Task<int> InsertarFicheroAsync(int iIdDocumento, string strNombre, string strExtension, string strSize, int iIndVisible, int iTipoFichero, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/ModificarFichero", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/ModificarFicheroResponse")]
        System.Threading.Tasks.Task<int> ModificarFicheroAsync(int iIdFichero, int iIndVisible, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/EliminarFichero", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/EliminarFicheroResponse")]
        System.Threading.Tasks.Task<int> EliminarFicheroAsync(int iIdFichero, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarFicheros", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarFicherosResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Fichero>> RecuperarFicherosAsync(int iIdDocumento, int iIndVisible);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/InsertarNref", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/InsertarNrefResponse")]
        System.Threading.Tasks.Task<int> InsertarNrefAsync(int iIdDocumento, string strNref, string strTipo, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/EliminarNRefs", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/EliminarNRefsResponse")]
        System.Threading.Tasks.Task<int> EliminarNRefsAsync(int iIdNRef, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarNRefs", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarNRefsResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.NRef>> RecuperarNRefsAsync(int iIdDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoEstado", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoEstadoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Estado>> RecuperarDocumentoEstadoAsync(int iIdDocumentoEstado, int iIndResueltos);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarTipoDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarTipoDocumentoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoDocumento>> RecuperarTipoDocumentoAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/InsertarDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/InsertarDocumentoResponse")]
        System.Threading.Tasks.Task<int> InsertarDocumentoAsync(string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, int iIdEstado, string strObservaciones, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/ModificarDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/ModificarDocumentoResponse")]
        System.Threading.Tasks.Task<int> ModificarDocumentoAsync(int iIdDocumento, string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, int iIdEstado, int iIdColaborador, string strObservaciones, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoPendientes", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoPendientesRespo" +
            "nse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoPendientesAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iColor, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoResueltos", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarDocumentoResueltosRespon" +
            "se")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoResueltosAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/ModificarDocumentoEstado", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/ModificarDocumentoEstadoResponse")]
        System.Threading.Tasks.Task<int> ModificarDocumentoEstadoAsync(int iIdDocumento, int iIdDocumentoEstado, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarUsuariosByTipo", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarUsuariosByTipoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Usuario>> RecuperarUsuariosByTipoAsync(int iTipoDocumento, int iIndColaboradorExt, int iIndColaboradorRed, int iIndConsultor);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/EliminarComentario", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/EliminarComentarioResponse")]
        System.Threading.Tasks.Task<int> EliminarComentarioAsync(int iIdComentario, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/InsertarComentario", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/InsertarComentarioResponse")]
        System.Threading.Tasks.Task<int> InsertarComentarioAsync(int iIdComentario, string strDescComentario, int iIndInterno, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/ModificarComentario", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/ModificarComentarioResponse")]
        System.Threading.Tasks.Task<int> ModificarComentarioAsync(int iIdComentario, string strDescComentario, int iIndInterno, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarComentario", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarComentarioResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Comentario>> RecuperarComentarioAsync(int iIdDocumento, int iIndExcluirInternos, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/AsignarDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/AsignarDocumentoResponse")]
        System.Threading.Tasks.Task<int> AsignarDocumentoAsync(int iIdDocumento, int iIdUser, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarEstadoDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarEstadoDocumentoResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.Bean<DocumentacionSvc.Estado>> RecuperarEstadoDocumentoAsync(int iIdDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/ModificarEstadoDocumento", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/ModificarEstadoDocumentoResponse")]
        System.Threading.Tasks.Task<int> ModificarEstadoDocumentoAsync(int iIdDocumento, int iIdDocuentoEstado, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RechazarDocumentoMorosidad", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RechazarDocumentoMorosidadRespons" +
            "e")]
        System.Threading.Tasks.Task<int> RechazarDocumentoMorosidadAsync(int iIdDocumento, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/InsertarMotivoRechazo", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/InsertarMotivoRechazoResponse")]
        System.Threading.Tasks.Task<int> InsertarMotivoRechazoAsync(int iIdDocumento, string strMotivoRechazo, int iIdUsuarioApp);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/CheckPuedeCrearDocumentos", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/CheckPuedeCrearDocumentosResponse" +
            "")]
        System.Threading.Tasks.Task<int> CheckPuedeCrearDocumentosAsync(int iCodNavision);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarJurisdiccion", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarJurisdiccionResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoJurisdiccion>> RecuperarJurisdiccionAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceGestionDocumentacion/RecuperarTribunales", ReplyAction="http://tempuri.org/IServiceGestionDocumentacion/RecuperarTribunalesResponse")]
        System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoTribunal>> RecuperarTribunalesAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface IServiceGestionDocumentacionChannel : DocumentacionSvc.IServiceGestionDocumentacion, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class ServiceGestionDocumentacionClient : System.ServiceModel.ClientBase<DocumentacionSvc.IServiceGestionDocumentacion>, DocumentacionSvc.IServiceGestionDocumentacion
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar el punto de conexión de servicio.
    /// </summary>
    /// <param name="serviceEndpoint">El punto de conexión para configurar</param>
    /// <param name="clientCredentials">Credenciales de cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public ServiceGestionDocumentacionClient() : 
                base(ServiceGestionDocumentacionClient.GetDefaultBinding(), ServiceGestionDocumentacionClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IServiceGestionDocumentacion.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServiceGestionDocumentacionClient(EndpointConfiguration endpointConfiguration) : 
                base(ServiceGestionDocumentacionClient.GetBindingForEndpoint(endpointConfiguration), ServiceGestionDocumentacionClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServiceGestionDocumentacionClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(ServiceGestionDocumentacionClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServiceGestionDocumentacionClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(ServiceGestionDocumentacionClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ServiceGestionDocumentacionClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<int> InsertarFicheroAsync(int iIdDocumento, string strNombre, string strExtension, string strSize, int iIndVisible, int iTipoFichero, int iIdUsuarioApp)
        {
            return base.Channel.InsertarFicheroAsync(iIdDocumento, strNombre, strExtension, strSize, iIndVisible, iTipoFichero, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> ModificarFicheroAsync(int iIdFichero, int iIndVisible, int iIdUsuarioApp)
        {
            return base.Channel.ModificarFicheroAsync(iIdFichero, iIndVisible, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> EliminarFicheroAsync(int iIdFichero, int iIdUsuarioApp)
        {
            return base.Channel.EliminarFicheroAsync(iIdFichero, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Fichero>> RecuperarFicherosAsync(int iIdDocumento, int iIndVisible)
        {
            return base.Channel.RecuperarFicherosAsync(iIdDocumento, iIndVisible);
        }
        
        public System.Threading.Tasks.Task<int> InsertarNrefAsync(int iIdDocumento, string strNref, string strTipo, int iIdUsuarioApp)
        {
            return base.Channel.InsertarNrefAsync(iIdDocumento, strNref, strTipo, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> EliminarNRefsAsync(int iIdNRef, int iIdUsuarioApp)
        {
            return base.Channel.EliminarNRefsAsync(iIdNRef, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.NRef>> RecuperarNRefsAsync(int iIdDocumento)
        {
            return base.Channel.RecuperarNRefsAsync(iIdDocumento);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Estado>> RecuperarDocumentoEstadoAsync(int iIdDocumentoEstado, int iIndResueltos)
        {
            return base.Channel.RecuperarDocumentoEstadoAsync(iIdDocumentoEstado, iIndResueltos);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoDocumento>> RecuperarTipoDocumentoAsync()
        {
            return base.Channel.RecuperarTipoDocumentoAsync();
        }
        
        public System.Threading.Tasks.Task<int> InsertarDocumentoAsync(string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, int iIdEstado, string strObservaciones, int iIdUsuarioApp)
        {
            return base.Channel.InsertarDocumentoAsync(strPeticion, iTipoDocumento, iCodNavision, sUsuarioPro, strNombre, strApellidos, strEmail, strTlfno, iIdEstado, strObservaciones, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> ModificarDocumentoAsync(int iIdDocumento, string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, int iIdEstado, int iIdColaborador, string strObservaciones, int iIdUsuarioApp)
        {
            return base.Channel.ModificarDocumentoAsync(iIdDocumento, strPeticion, iTipoDocumento, iCodNavision, sUsuarioPro, strNombre, strApellidos, strEmail, strTlfno, iIdEstado, iIdColaborador, strObservaciones, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp)
        {
            return base.Channel.RecuperarDocumentoAsync(iIdDocumento, iTipoDocumento, iEstadoDocumento, iCodNavision, strBusqueda, strFechaDesde, strFechaHasta, iIdUsuarioAsignado, iPageNumber, iPageSize, strOrderField, strOrderType, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoPendientesAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iColor, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp)
        {
            return base.Channel.RecuperarDocumentoPendientesAsync(iIdDocumento, iTipoDocumento, iEstadoDocumento, iCodNavision, strBusqueda, strFechaDesde, strFechaHasta, iColor, iIdUsuarioAsignado, iPageNumber, iPageSize, strOrderField, strOrderType, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Documento>> RecuperarDocumentoResueltosAsync(int iIdDocumento, int iTipoDocumento, int iEstadoDocumento, int iCodNavision, string strBusqueda, string strFechaDesde, string strFechaHasta, int iIdUsuarioAsignado, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp)
        {
            return base.Channel.RecuperarDocumentoResueltosAsync(iIdDocumento, iTipoDocumento, iEstadoDocumento, iCodNavision, strBusqueda, strFechaDesde, strFechaHasta, iIdUsuarioAsignado, iPageNumber, iPageSize, strOrderField, strOrderType, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> ModificarDocumentoEstadoAsync(int iIdDocumento, int iIdDocumentoEstado, int iIdUsuarioApp)
        {
            return base.Channel.ModificarDocumentoEstadoAsync(iIdDocumento, iIdDocumentoEstado, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Usuario>> RecuperarUsuariosByTipoAsync(int iTipoDocumento, int iIndColaboradorExt, int iIndColaboradorRed, int iIndConsultor)
        {
            return base.Channel.RecuperarUsuariosByTipoAsync(iTipoDocumento, iIndColaboradorExt, iIndColaboradorRed, iIndConsultor);
        }
        
        public System.Threading.Tasks.Task<int> EliminarComentarioAsync(int iIdComentario, int iIdUsuarioApp)
        {
            return base.Channel.EliminarComentarioAsync(iIdComentario, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> InsertarComentarioAsync(int iIdComentario, string strDescComentario, int iIndInterno, int iIdUsuarioApp)
        {
            return base.Channel.InsertarComentarioAsync(iIdComentario, strDescComentario, iIndInterno, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> ModificarComentarioAsync(int iIdComentario, string strDescComentario, int iIndInterno, int iIdUsuarioApp)
        {
            return base.Channel.ModificarComentarioAsync(iIdComentario, strDescComentario, iIndInterno, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.Comentario>> RecuperarComentarioAsync(int iIdDocumento, int iIndExcluirInternos, int iPageNumber, int iPageSize, string strOrderField, string strOrderType, int iIdUsuarioApp)
        {
            return base.Channel.RecuperarComentarioAsync(iIdDocumento, iIndExcluirInternos, iPageNumber, iPageSize, strOrderField, strOrderType, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> AsignarDocumentoAsync(int iIdDocumento, int iIdUser, int iIdUsuarioApp)
        {
            return base.Channel.AsignarDocumentoAsync(iIdDocumento, iIdUser, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.Bean<DocumentacionSvc.Estado>> RecuperarEstadoDocumentoAsync(int iIdDocumento)
        {
            return base.Channel.RecuperarEstadoDocumentoAsync(iIdDocumento);
        }
        
        public System.Threading.Tasks.Task<int> ModificarEstadoDocumentoAsync(int iIdDocumento, int iIdDocuentoEstado, int iIdUsuarioApp)
        {
            return base.Channel.ModificarEstadoDocumentoAsync(iIdDocumento, iIdDocuentoEstado, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> RechazarDocumentoMorosidadAsync(int iIdDocumento, int iIdUsuarioApp)
        {
            return base.Channel.RechazarDocumentoMorosidadAsync(iIdDocumento, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> InsertarMotivoRechazoAsync(int iIdDocumento, string strMotivoRechazo, int iIdUsuarioApp)
        {
            return base.Channel.InsertarMotivoRechazoAsync(iIdDocumento, strMotivoRechazo, iIdUsuarioApp);
        }
        
        public System.Threading.Tasks.Task<int> CheckPuedeCrearDocumentosAsync(int iCodNavision)
        {
            return base.Channel.CheckPuedeCrearDocumentosAsync(iCodNavision);
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoJurisdiccion>> RecuperarJurisdiccionAsync()
        {
            return base.Channel.RecuperarJurisdiccionAsync();
        }
        
        public System.Threading.Tasks.Task<DllWcfUtility.BeanList<DocumentacionSvc.TipoTribunal>> RecuperarTribunalesAsync()
        {
            return base.Channel.RecuperarTribunalesAsync();
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IServiceGestionDocumentacion))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IServiceGestionDocumentacion))
            {
                return new System.ServiceModel.EndpointAddress("http://led-pre-wcfgestiondocumentacion/ServiceGestionDocumentacion.svc");
            }
            throw new System.InvalidOperationException(string.Format("No se pudo encontrar un punto de conexión con el nombre \"{0}\".", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return ServiceGestionDocumentacionClient.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IServiceGestionDocumentacion);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return ServiceGestionDocumentacionClient.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IServiceGestionDocumentacion);
        }
        
        public enum EndpointConfiguration
        {
            
            BasicHttpBinding_IServiceGestionDocumentacion,
        }
    }
}
