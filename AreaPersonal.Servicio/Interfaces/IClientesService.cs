﻿using DllApiBase.Vistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public interface IClientesService<RB>
    {
        RB RecuperarCifClienteNav(string strIdentifier, int IdClienteNav, string IdUsuarioPro);

        RB RecuperarModelo347PDF(string strIdentifier, string strCIF, int intAnyo);

        RB RecuperarProductosClienteNav(string strIdentifier, int IdClienteNav);

        RB RecuperarInformacionCuenta(string strIdentifier, int IdClienteNav, int IdClienteCrm);
    }
}
