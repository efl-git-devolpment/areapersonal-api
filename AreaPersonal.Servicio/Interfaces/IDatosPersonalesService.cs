﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AreaPersonal.Vistas;
using DllApiBase.Vistas;

namespace AreaPersonal.Servicio
{
    public interface IDatosPersonalesService<RB> 
    {

        RB RecuperarDatosPersonales(string strIdentifier, string strUsuarioPro);
        RB ModificarDatosPersonales(string strIdentifier, String strUsuarioPro, String strName, String strFirstName, String strSurName, String strTreatment, String strEmail, String strColor, String strNombreComercial);
        RB RecuperarLogo(string strIdentifier, string strUsuarioPro);
        RB ModificarLogo(string strIdentifier, string strUsuarioPro, byte[] bdata);

        RB ResetearLogo(string strIdentifier, String strUsuarioPro);

        RB ExisteLogo(string strIdentifier, String strUsuarioPro);

        RB RecuperarLogo2(string strIdentifier, string strUsuarioPro);
        RB ModificarLogo2(string strIdentifier, string strUsuarioPro, byte[] bdata);

        RB ResetearLogo2(string strIdentifier, String strUsuarioPro);
       
        RB ExisteLogo2(string strIdentifier, String strUsuarioPro);

        RB ModificarLoginEmail(string strIdentifier, int IdClienteNav, string IdUsuarioPro, string strEmailAnterior, string strEmailNuevo);



    }
}

