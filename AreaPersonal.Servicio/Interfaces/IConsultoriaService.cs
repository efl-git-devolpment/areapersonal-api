﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AreaPersonal.Vistas;

namespace AreaPersonal.Servicio
{
    public interface IConsultoriaService <C,U, M,S, P, T, RS,R, RQ, RE, UP> : IAreaPersonalBaseService <C,U>
    {
        //Task<C> RecuperarCliente(string strIdentifier, int IdClienteNav);
        Task<IEnumerable<M>> RecuperarMaterias(string strIdentifier, int ClienteNav, string CodigoProducto);
        Task<IEnumerable<M>> RecuperarMateriasByUsuario(string strIdentifier, string strUsuarioPro, int indLlamadaExperta);
        Task<IEnumerable<S>> RecuperarSubMaterias(string strIdentifier, int IdMateria);

        Task<IEnumerable<P>> RecuperarProvincias(string strIdentifier);
        Task<IEnumerable<T>> RecuperarTipoConsultasByMateria(string strIdentifier, int IdMateria);
        Task<IEnumerable<T>> RecuperarTipoConsultasCliente(string strIdentifier, int ClienteNav);
        Task<R> ComprobarCrearPeticion(string strIdentifier, int IdClienteNav);
        Task<R> ComprobarCrearPeticionByMateria(string strIdentifier, int IdClienteNav, int IdMateria);
        Task<R> ComprobarEnviarComentarios(string strIdentifier, int IdClienteNav, int IdMateria);

        RS GuardarConsulta(string strIdentifier, String strEmail, String strName, string strSurname, String strDescription, String strPhone, string strTitular, string strCargo, String sNameSugar, String strDisponibilidad, int iEstado = -1, int iIdMatter = -1, int iIdType = -1, int iIdSubMatter = -1, int iIdProvince = -1, int iIdClientNav = -1, int iCodCRM = -1);

        int SendEmailMessageAlta(string strIdentifier, int iCodCRM, string strEmail, string strName, int iIdQuery);

        int ReSendEmailInfo(string strIdentifier, int iCodCRM,  string strEmail, int iIdQuery);

        RQ RecuperarPeticiones(string strIdentifier, int iCodCRM, int iCodNavision, int iType, int iMatter, int iPageNum, int iPageSize);

        RQ RecuperarPeticion(string strIdentifier, int idQuestion);
        RE SendRequest(string strIdentifier, int icodQuestion, int iCodCRM, string strText, int iIdClientNav, Boolean bCopy);

        int SaveFile(string strIdentifier, String sFileName, int iIdQuery, String strSize, byte[] bdata);

        int SaveFile2(String sFileName,byte[] bdata);

    }
}
