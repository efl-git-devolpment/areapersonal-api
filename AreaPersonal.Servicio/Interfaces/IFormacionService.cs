﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public interface IFormacionService<C,U,FP, FC, E, EX, UP> : IAreaPersonalBaseService<C, U>
    {


        //Task<C> RecuperarCliente(string strIdentifier, int IdClienteNav);


        Task<IEnumerable<FP>> RecuperarPlanesByEmail(string strIdentifier, string strUsuarioPro, string strEmail);

        Task<IEnumerable<FC>> RecuperarCursosPlanByEmail(string strIdentifier, int IdPlan, string strEmail);

        Task<IEnumerable<E>> RecuperarSesiones(string strIdentifier, int cursoWebexId, int IdUser);

        Task<EX> RecuperarSesionExtendido(string strIdentifier, int WebexId);

    }
}
