﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public interface IFacturaService<ReF, ReA,RgF,RA>
    {
        Task<ReF> RecuperarFacturas(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate);
        //Task<RgF> RecuperarFacturaPDF(string strIdentifier, int idQueueFactura);
        Task<RgF> RecuperarFacturaPDFById(string strIdentifier, string idFactura, int intCodNavision);


        Task<ReA> RecuperarAbonos(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate);
        //Task<RA> RecuperarAbonoPDF(string strIdentifier, int idQueueAbono);
        Task<RA> RecuperarAbonoPDFById(string strIdentifier, string idAbono, int intCodNavision);

    }
}
