﻿using AreaPersonal.Vistas;
using AreaPersonal.Vistas.External;
using System;
using System.Collections.Generic;

namespace AreaPersonal.Servicio
{
    public interface IExternalService<UP, CP, P, CN, RB, AP, ABP, M,PC, IC, CLB>
    {
        #region Online
        CP RecuperarClienteOnline(int IdClienteNav);
        UP RecuperarUsuarioPro(string sIdUsuarioPro);
        RB RecuperarSuscripcionesUsuarioPro(string IdUsuarioPro);

        RB RecuperarSuscripcionesClienteNav(int IdClienteNav);

        List<P> RecuperarPermisosUsuarioPro(string sIdUsuarioPro);

        #endregion

        #region Navision
        CN RecuperarClienteNavision(int IdClienteNav);

        RB RecuperarFacturas(int IdClienteNav, string strCIF, int IdContactoCrm, string strFechaDesde, string strFechaHasta, int iPagNum, int iPagSize);
        RB RecuperarAbonos(int IdClienteNav, string strCIF, int IdContactoCrm, string strFechaDesde, string strFechaHasta, int iPagNum, int iPagSize);

        AP RecuperarFacturaPdf(string sIdFactura, string strCif);

        ABP RecuperarAbonoPdf(string sIdAbono, string strCif);

        List<PC> RecuperarProductosCliente(int IdClienteNav);

        M RecuperarModelo347Pdf(string strCif, int intAnyo);

        RB RecuperarIndicadorFacturasNuevas(int IdClienteNav, string strCif, DateTime FechaDesde);

        #endregion

        #region Crm

        IC RecuperarInformacionCuenta(int IdClienteNav, int IdClienteCrm);

        #endregion

        #region Claves

        List<string> RecuperarFacturasPagadas(int idClienteNav);

        int InsertarFacturaPagada(InsertarFacturaPagadaPeticion objPeticion);

        RB RecuperarUsuarioProducto(int IdClienteNav, string IdUsuarioPro);

        RB ModificarFechaUltimoAccesoFacturas(int IdClienteNav, int IdUsuarioApp);

        RB RecuperarFechaUltimoAccesoFacturas(int IdClienteNav);

        List<CLB> RecuperarClientesBusqueda(int IdClienteNav, int IdClienteCrm, short iIndTipoBusqueda, int iIdTipoCliente, int iPaginaInicial, int iNumeroDeFilas, int iIdUsuarioApp);

        RB ModificarEmailUsuarioPro(int IdClienteNav, string IdUsuarioPro, string strEmailAnterior, string strEmailNuevo, int IdUsuarioApp);

        #endregion

    }
}
