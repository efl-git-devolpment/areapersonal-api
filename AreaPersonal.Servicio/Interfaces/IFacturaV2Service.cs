﻿using AreaPersonal.Comun.Enums;
using AreaPersonal.Vistas;

namespace AreaPersonal.Servicio
{
    public interface IFacturaV2Service<RB> : IAreaPersonalBaseService
    {
        RB RecuperarFacturas(string strIdentifier, int IdClienteNav, string IdUsuarioPro, int iPagNum, int iPagSize, string strFechaDesde, string strFechaHasta, OrdenarFacturasEnum iOrden);
        RB RecuperarFacturaPDFById(string strIdentifier, string strIdFactura, int IdClienteNav);
        RB RecuperarAbonos(string strIdentifier, int IdClienteNav, string IdUsuarioPro, int iPagNum, int iPagSize, string strFechaDesde, string strFechaHasta);
        RB RecuperarAbonoPDFById(string strIdentifier, string sIdAbono, int IdClienteNav);
        RB ModificarFechaUltimoAccesoFacturas(int IdClienteNav, int IdUsuarioApp);
        RB RecuperarIndicadorFacturasNuevas(string strIdentifier, int IdClienteNav, string IdUsuarioPro);
        int InsertarFacturaPagada(InsertarFacturaPagadaPeticion objPeticion);
    }
}
