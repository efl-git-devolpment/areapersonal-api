﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Servicio
{
    public interface IDocumentacionAdvService
    {

        int AddDocumento(DocumentacionSvc.Documento oDocumento);

        int SendEmailAddDocument(int iCodCRM, string strEmail, string strName, int iIdDocument);

        int AssingUser(int iIdDocumento, int iUserAssigned);

        int SendEmailAssing(int ipIdCodCRM, DocumentacionSvc.Documento objDocument, int iIdDocumento, ClavesSvc.UsuarioApp oUser, String strTitular, int iIdCodNav);

        Boolean Check_NonPayment(int iIdDocumento, int ipIdCodCRM, int iCodNav);
    }
}
