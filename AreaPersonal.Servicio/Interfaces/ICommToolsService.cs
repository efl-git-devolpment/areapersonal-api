﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AreaPersonal.Vistas;

namespace AreaPersonal.Servicio
{
    public interface ICommToolsService<R>
    {
        Task<IEnumerable<R>> RecuperarRevistas(string strIdentifier, string strUsuarioPro);
    }
}
