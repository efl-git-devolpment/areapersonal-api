﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public interface IDocumentacionService<C, U, T,RG> : IBaseService<C, U>
    {
        //Task<C> RecuperarCliente(string strIdentifier, int IdClienteNav, int IdUserApp);
        Task<IEnumerable<T>> RecuperarTipoDocumento(string strIdentifier);

        RG SaveDocumento(string strIdentifier, string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, string strTitular, string sNameSugar, string strTipoDocumento, int iCodCRM = -1);

        int SendEmailMessageAlta(string strIdentifier, int iCodCRM, string strEmail, string strName, int iIdDocument);
    }
}
