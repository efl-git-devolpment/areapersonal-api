﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public interface ICertificadoService<CR, C, CP>
    {
        Task<CR> RecuperarCertificados(string strIdentifier, string strIdUsuarioPro, int intTipoCertificado);
        int RecuperarNumCertificadosDisponibles(string strIdentifier, string strIdUsuarioPro);
        int RecuperarNumCertificadosTotal(string strIdentifier, string strIdUsuarioPro);
    }
}
