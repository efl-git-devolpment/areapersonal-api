﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

namespace AreaPersonal.Servicio
{
    public interface IConsultoriaServiceAdv <C,M>
    {
        bool CanCreateConsultaFicticia(int codNav);
        bool CanCreateConsulta(int iIdQuery, int codNav);
        bool CanCreateFormulario(int iIdQuery, int codNav);
        bool CanCreateExpediente(int iIdQuery, int questionMatter, int codNav);

        int AddQuery(ConsultoriaSvc.Consulta oQuery);

        int SendEmailAssing(int iIdCodCRM, ConsultoriaSvc.Consulta oQuery, int iIdQuery, ClavesSvc.UsuarioApp oUser, String strTitular, int iIdCodNav, int iNivel);

        int AssingUser(int iQueryId, int iUserAssigned);

        Boolean IsNonPaymentQuery(int iIdQuery);

        Boolean Check_NonPayment(int iIdQuery, int ipIdCodCRM, int iCodNav);

        int SendEmailStandBy(int iCodCRM, string strEmail, string strName, int iIdQuery);

        int CheckSendEmailClaveDemo(int iIdCodCRM, int iQueryId);

        int SendEmailAddQuery(int iCodCRM, string strEmail, string strName, int iIdQuery);

        int ReSendEmailInfo(int iCodCRM, string strEmail, int iIdQuery);

        C GetPropertiesQuery(ConsultoriaSvc.Consulta question, List<M> alistMementos);

        int GetNSuscriptionsTotal(int iCodNav, int iIdMatter);

        int GetNSuscriptionsEnabled(int iCodNav, int iIdMatter);

        int AddSubQuery(int iQueryId, string strSubQuery, int iIndInternal, int iIdUser);

        int SendEmailResponse(int iCodCRM, int iIdQuery, string sMessage, int iIdUserApp);
        int SendEmailResponseCopy(int iCodCRM, int iIdQuery, string sMessage, int iIdUserApp);
        int StoreFile(String sFileName, string subFolder, byte[] bdata);
        String CheckStrBloqueos(int idMatter);
        Boolean SuperadoLimiteDescripcion(int idMatter, string strDescripcion);

    }
}
