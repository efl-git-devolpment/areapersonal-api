﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Comun.Configuration;
using DllWcfUtility;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http;
using AreaPersonal.Vistas;


namespace AreaPersonal.Servicio
{
    public enum TypeQuery
    {
        Consulta = 1,
        Expediente = 2,
        Formulario = 3
    }

    public enum GroupBloqueo
    {
        none = 0,
        llamada_experta = 1,
        proteccion_datos = 2,
        especializados = 3,

    }
    public enum TypeStatus
    {
        none = -1,
        open = 1,
        progress = 2,
        stand_by = 3,
        closed_without_resolved = 4,
        pending_info = 5,
        closed_resolved = 6,
        customer_not_located = 7,
        send_level_3 = 8,
        non_payment = 9,
        pending_translate = 10,
        send_level_3_request = 11,
        new_sac = 12,
        closed_not_located = 13,
        pending_info_request = 14,
        pending_info_level3 = 15

    }

    public enum TypeMatter
    {
        none = 0,
        Fiscal = 1, // SF
        Contable = 2,
        Social = 3, // SF
        Civil = 5,
        Administrativo = 6,
        Penal = 7,
        Procesal = 8,
        Inmobiliario_Urbanismo = 9,
        Inmobiliaria = 10,
        Propiedad_Horizontal = 11,
        DerechoFamilia = 12,
        Urbanismo = 13,
        Trafico = 14,
        PropietatHorintalCat = 15,
        Mercantil = 16,
        DerechoLocal = 17, // DL
        ProteccionDatos = 20

    }
    public enum TypeOrigen
    {
        none = 0,
        formulario = 2, // formulario via telefono
        telefonico = 1, // gestor
    }
    public enum TypeEmail
    {
        NewQueryClient = 1,
        NewQueryConsultant = 2,
        NonPayment = 3,
        NonPaymentClient = 4,
        Request = 5,
        RequestLevel3 = 6,
        RequestTranslate = 7,
        RequestCopy = 8,
        PendingStandBy = 9,
        ReSendEmail = 10,
        ReceivedClaveDemo = 11,
        Assign = 12,
        AssignN2 = 13,
        RequestClarification = 14,
        RequestComment = 15

    }
    public enum TypeEmisor
    {
        none = 0,
        gestion = 1,
        landing = 2,
        cobros = 3,

    }
    public enum TypeSuscription
    {
        definitivo = 1,
        demo = 2,
        regalo = 3

    }

    public class ConsultoriaServiceAdv : AreaPersonalBaseService, IConsultoriaServiceAdv<Consulta, Mementos>

    {
        private readonly ConsultoriaSvc.IServiceConsultasJuridicas _svcConsultoria;
        private readonly EmailsSvc.IServicioMailAutomaticoProducto _svcEmails;
        private readonly RepositorySvc.IWcfRepository _svcRepository;

        private readonly TextInfo myTI = new CultureInfo("es-ES", false).TextInfo;

        public ConsultoriaServiceAdv(
                IOptions<ConfigApi> configWeb,
                ILogger<ConsultoriaServiceAdv> logger) : base(configWeb, logger)
        {


            var basicHttpBindingConsultoria = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxReceivedMessageSize = 2147483647,
                MaxBufferSize = 2147483647
            };


            var endpointAddressConsultoria = new EndpointAddress(objConfig.cfgServices.ConsultoriaSvc);
            _svcConsultoria = new ConsultoriaSvc.ServiceConsultasJuridicasClient(basicHttpBindingConsultoria, endpointAddressConsultoria);



            var basicHttpBindingEmails = new BasicHttpBinding();
            var endpointAddressEmails = new EndpointAddress(objConfig.cfgServices.MailsAutomaticoProductoSvc);
            _svcEmails = new EmailsSvc.ServicioMailAutomaticoProductoClient(basicHttpBindingEmails, endpointAddressEmails);

            var basicHttpBindingRepository = new BasicHttpBinding();
            var endpointAddressRepository = new EndpointAddress(objConfig.cfgServices.RepositorySvc);
            _svcRepository = new RepositorySvc.WcfRepositoryClient(basicHttpBindingRepository, endpointAddressRepository);

            //_objVariables = MyConfig.Variables;

        }


        public bool CanCreateConsultaFicticia(int IdClienteNav)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1}", nameof(CanCreateConsultaFicticia), IdClienteNav.ToString());


            try
            {

                Task<BeanList<ConsultoriaSvc.Consulta>> alist = _svcConsultoria.RecuperarConsultasAsync(-1, 1, -1, -1, IdClienteNav, (int)TypeMatter.DerechoLocal, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 100, String.Empty, String.Empty, 1);

                int ncount = 0;

                if ((alist.Result != null) && (alist.Result.beanList != null))
                {
                    ncount = alist.Result.beanList.Where(t => t._idConsultaEstado != (int)TypeStatus.closed_without_resolved && t._idConsultaEstado != (int)TypeStatus.closed_resolved && t._idConsultaEstado != (int)TypeStatus.non_payment).Count();
                }

                objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(CanCreateConsultaFicticia), ncount.ToString());
                return (ncount == 0);

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CanCreateConsultaFicticia), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CanCreateConsultaFicticia), ex.Message);
                return true;
            }
        }

        public bool CanCreateConsulta(int iIdConsulta, int IdClienteNav)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(CanCreateConsulta), iIdConsulta.ToString(), IdClienteNav.ToString());


            try
            {


                Task<BeanList<ConsultoriaSvc.Consulta>> alist = _svcConsultoria.RecuperarConsultasAsync(-1, (int)TypeQuery.Consulta, -1, -1, IdClienteNav, -1, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 100, String.Empty, String.Empty, 1);

                int ncount = 0;
                int indGranCuenta = 0;
                int nSimultenea = this.objConfig.cfgVariables.configConsultoria.PeticionSimultanea;

                if ((alist.Result != null) && (alist.Result.beanList != null))
                {
                    ncount = alist.Result.beanList.Where(t => t._idConsultaEstado != (int)TypeStatus.closed_without_resolved && t._idConsultaEstado != (int)TypeStatus.closed_resolved && t._idConsultaEstado != (int)TypeStatus.non_payment && t._idConsulta != iIdConsulta).Count();


                    ClavesSvc.Cliente account = this.getClienteFromNav(IdClienteNav);

                    if (account != null)
                    {
                        indGranCuenta = account._indGranCuenta;
                    }
                }

                objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(CanCreateConsulta), ncount.ToString() + " indGranCuenta : " + indGranCuenta.ToString());
                return ((ncount == 0) && (indGranCuenta == 0)) || ((ncount < nSimultenea) && (indGranCuenta == 1));

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CanCreateConsulta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CanCreateConsulta), ex.Message);
                return true;
            }
        }

        public bool CanCreateFormulario(int iIdConsulta, int IdClienteNav)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(CanCreateFormulario), iIdConsulta.ToString(), IdClienteNav.ToString());

            try
            {
                if (IdClienteNav > 1000000)
                {
                    objLogger.LogInformation("Resultado {0} : No se puede crear un Expediente para un Cliente Ficticio ", nameof(CanCreateExpediente));
                    return false;
                }

                int ncount = 0;
                int iIdUserApp = this.objConfig.cfgVariables.idUsuarioAreaPersonal;
                Task<BeanList<ConsultoriaSvc.Consulta>> alist = _svcConsultoria.RecuperarConsultasAsync(-1, (int)TypeQuery.Formulario, -1, -1, IdClienteNav, -1, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 100, String.Empty, String.Empty, iIdUserApp);

                if ((alist.Result != null) && (alist.Result.beanList != null))
                {
                    ncount = alist.Result.beanList.Where(t => t._idConsultaEstado != (int)TypeStatus.closed_without_resolved && t._idConsultaEstado != (int)TypeStatus.closed_resolved && t._idConsultaEstado != (int)TypeStatus.non_payment && t._idConsulta != iIdConsulta).Count();
                }

                objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(CanCreateFormulario), ncount.ToString());

                return (ncount == 0);

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CanCreateFormulario), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CanCreateFormulario), ex.Message);
                return true;
            }
        }

        public bool CanCreateExpediente(int iIdConsulta, int intIdMateria, int IdClienteNav)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(CanCreateExpediente), iIdConsulta.ToString(), intIdMateria.ToString(), IdClienteNav.ToString());

            try
            {

                if (IdClienteNav > 1000000) 
                {
                    objLogger.LogInformation("Resultado {0} : No se puede crear un Expediente para un Cliente Ficticio ", nameof(CanCreateExpediente));
                    return false;
                }

                int ncount = 0;
                int nSimultenea = this.objVariables.configConsultoria.PeticionSimultanea;
                int indGranCuenta = 0;

                ClavesSvc.Cliente account = this.getClienteFromNav(IdClienteNav);

                if (account != null)
                {
                    indGranCuenta = account._indGranCuenta;
                }             

                Task<BeanList<ConsultoriaSvc.Consulta>> alist = _svcConsultoria.RecuperarConsultasAsync(-1, (int)TypeQuery.Expediente, -1, -1, IdClienteNav, intIdMateria, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 100, String.Empty, String.Empty, objVariables.idUsuarioAreaPersonal);
            
                if ((alist.Result != null) && (alist.Result.beanList != null))
                {
                    ncount = alist.Result.beanList.Where(t => t._idConsultaEstado != (int)TypeStatus.closed_without_resolved && t._idConsultaEstado != (int)TypeStatus.pending_translate && t._idConsultaEstado != (int)TypeStatus.closed_resolved && t._idConsultaEstado != (int)TypeStatus.non_payment && t._idConsulta != iIdConsulta).Count();
                }

                

                objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(CanCreateExpediente), ncount.ToString() + " indGranCuenta : " + indGranCuenta.ToString());

                return ((ncount == 0) && (indGranCuenta == 0)) || ((ncount < nSimultenea) && (indGranCuenta == 1));

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CanCreateExpediente), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CanCreateExpediente), ex.Message);
                return true;
            }

        }


        public String CheckStrBloqueos(int idMatter)
        {
            String strResult = String.Empty;
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}", nameof(CheckStrBloqueos), idMatter);

                DateTime dtToday = DateTime.Now;

                String strFechaNow = dtToday.Year.ToString() + "/" + dtToday.Month.ToString() + "/" + dtToday.Day.ToString();

                BeanList<ConsultoriaSvc.Bloqueo> alistBloqueos = null;

                if (idMatter == (int)TypeMatter.ProteccionDatos)
                {
                    alistBloqueos = this.GetBloqueos((int)GroupBloqueo.proteccion_datos, strFechaNow, strFechaNow, "FECHA_DESDE", "DESC", 1, 1);
                }
                else
                {
                    if (this.IsLlamadaExperta(idMatter))
                    {
                        if ((dtToday.Hour < 8) || ((dtToday.Hour >= 20) && (dtToday.Minute > 0)) || (dtToday.DayOfWeek == DayOfWeek.Saturday) || (dtToday.DayOfWeek == DayOfWeek.Sunday))
                        {
                            strResult = "Le informamos de que el horario de atención al cliente del Servicio de Llamada Experta es de lunes a viernes de 10 a 17 horas.No obstante, podrá plantear su consulta a través del presente formulario en los días de prestación del Servicio en horario de 8 a 20 horas.";
                            return strResult;
                        }
                        else
                        {
                            alistBloqueos = this.GetBloqueos((int)GroupBloqueo.llamada_experta, strFechaNow, strFechaNow, "FECHA_DESDE", "DESC", 1, 1);
                        }
                    }
                    else
                    {
                        alistBloqueos = this.GetBloqueos((int)GroupBloqueo.especializados, strFechaNow, strFechaNow, "FECHA_DESDE", "DESC", 1, 1);
                    }
                }

                if ((alistBloqueos != null) && (alistBloqueos.beanList != null) && (alistBloqueos.beanList.Count == 1))

                    strResult = alistBloqueos.beanList[0]._comentario;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CheckStrBloqueos), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CheckStrBloqueos), ex.Message);

            }

            return strResult;

        }

        private BeanList<ConsultoriaSvc.Bloqueo> GetBloqueos(int iIdGrupobloqueo, string sFechaDesde, string sFechaHasta, string orderField, string orderType, int numPage, int recsPerPage)
        {
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3},{4}, {5}, {6}", nameof(GetBloqueos), iIdGrupobloqueo.ToString(), sFechaDesde, orderField, orderType, numPage.ToString(), recsPerPage.ToString());


                var lstBloqueos = _svcConsultoria.RecuperarBloqueoAsync(iIdGrupobloqueo, sFechaDesde, sFechaHasta, numPage, recsPerPage, orderField, orderType).Result;

                if ((lstBloqueos != null) && (lstBloqueos.beanList != null))
                {
                    objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(GetBloqueos), lstBloqueos.beanList.ToString());

                }
                else
                {
                    objLogger.LogInformation("Resultado NCount --> {0} {1} ", nameof(GetBloqueos), "Sin bloqueos");
                }

                return lstBloqueos;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CanCreateExpediente), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CanCreateExpediente), ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Adde new query to database
        /// </summary>
        /// <returns></returns>
        public int AddQuery(ConsultoriaSvc.Consulta oQuery)
        {
            try
            {

                objLogger.LogInformation("START --> {0} con parámetros {1}", nameof(AddQuery), oQuery._idConsulta.ToString());

                String strCodLucene = String.Empty;

                int iResult = _svcConsultoria.InsertarConsultaAsync(oQuery._idClienteNav, oQuery._idClienteCRM, oQuery._idUsuarioPro, oQuery._idTipoConsulta, oQuery._idTipoSuscripcion, oQuery._idMateria, oQuery._idSubMateria, oQuery._idConsultaEstado, oQuery._idProvincia,
                                                oQuery._tituloConsulta, oQuery._idVoz, oQuery._descConsulta, oQuery._nombreUsuarioPeticion, oQuery._cargoUsuarioPeticion, oQuery._email,
                                                oQuery._tlf, oQuery._nombreClienteSuscripcion, oQuery._nombreComercial, oQuery._delegacionComercial, oQuery._idSuscripcionNav,
                                                oQuery._idProductoNav, oQuery._descProductoNav, strCodLucene, oQuery._idContactoCrm, oQuery._cargo, oQuery._idColaborador, oQuery._numeroPartes, oQuery._indPrivada, oQuery._facturacionColaborador, oQuery._indFacturacionColaborador, oQuery._referenciaEditorial, (int)TypeOrigen.formulario, 0, 0,oQuery._disponibilidad, objVariables.idUsuarioAreaPersonal).Result;

                objLogger.LogInformation("Resultado Insertar Consulta en BBDD --> {0} {1} ", nameof(AddQuery), iResult.ToString());

                return iResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(AddQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(AddQuery), ex.Message);
                return -1;
            }

        }

        public int AssingUser(int iQueryId, int iUserAssigned)
        {
            try
            {

                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(AssingUser), iQueryId.ToString(), iUserAssigned.ToString());


                var iResult = _svcConsultoria.AsignarConsultaAsync(iQueryId, iUserAssigned, objVariables.idUsuarioAreaPersonal);

                objLogger.LogInformation("Resultado Asinar Consulta  --> {0} {1} ", nameof(AssingUser), iResult.Result.ToString());

                return iResult.Result;
            }

            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                return -1;
            }
        }

        public Boolean IsNonPaymentQuery(int iIdQuery)
        {


            Bean<ConsultoriaSvc.ConsultaEstado> status = _svcConsultoria.RecuperarEstadoConsultaAsync(iIdQuery).Result;

            if (status.bean._idConsultaEstado == (int)TypeStatus.non_payment) return true;
            else return false;

        }

        public Boolean Check_NonPayment(int iIdQuery, int ipIdCodCRM, int iCodNav)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(Check_NonPayment), iIdQuery.ToString(), ipIdCodCRM.ToString(), iCodNav.ToString());

            try
            {


                Bean<ConsultoriaSvc.ConsultaEstado> status = _svcConsultoria.RecuperarEstadoConsultaAsync(iIdQuery).Result;

                int iResult = -1;

                if (status.bean._idConsultaEstado == (int)TypeStatus.non_payment)
                {
                    iResult = this.SendEmailNonPayment(ipIdCodCRM, iCodNav, iIdQuery);
                }
                return (iResult >= 0);
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(Check_NonPayment), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(Check_NonPayment), ex.Message);
                return false;
            }
        }

        private Boolean IsLlamadaExperta(int iMatter)
        {
            return ((iMatter == (int)TypeMatter.Fiscal) ||
                        (iMatter == (int)TypeMatter.Social) ||
                        (iMatter == (int)TypeMatter.ProteccionDatos));
        }

        private Boolean IsDerechoLocal(int iMatter)
        {
            return (iMatter == (int)TypeMatter.DerechoLocal);
            //(iMatter == (int)TypeMatter.Trafico) ||
            //(iMatter == (int)TypeMatter.DerechoFamilia));

        }

        private String GetFooterMatter(int iMatter, string sMatter)
        {
            if (this.IsLlamadaExperta(iMatter))
            {
                return "Servicio de Llamada Experta " + sMatter;
            }
            else
            {
                return "Departamento de Consultoría " + sMatter;
            }

        }
        private String GetMailPattern(int iTypeQuery, TypeEmail ttype, int iMatter)
        {
            String LocalPath = objVariables.path_patters + "\\Consultoria";

            String ExtendedPath = String.Empty;

            if (iTypeQuery == (int)TypeQuery.Consulta)
            {
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient:
                        {
                            if ((iMatter == (int)TypeMatter.DerechoLocal))
                            {
                                ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewConsultaClientVal.html";
                            }
                            else if ((iMatter == (int)TypeMatter.Trafico) || (iMatter == (int)TypeMatter.DerechoFamilia))
                            {
                                ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewConsultaClientDFT.html";
                            }
                            else
                            {
                                ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewConsultaClient.html";
                            }
                        }; break;
                    case TypeEmail.NewQueryConsultant: ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewConsulta.html"; break;
                    case TypeEmail.NonPayment: ExtendedPath = "EnvioImpagos\\plantilla_envio_impago.html"; break;
                    case TypeEmail.NonPaymentClient: ExtendedPath = "EnvioImpagos\\plantilla_envio_impagoCliente.html"; break;
                    case TypeEmail.Request: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta.html"; break;
                    case TypeEmail.RequestLevel3: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_level3.html"; break;
                    case TypeEmail.RequestTranslate: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_traducido.html"; break;
                    case TypeEmail.RequestCopy: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuestaCopia.html"; break;
                    case TypeEmail.PendingStandBy: ExtendedPath = "EnvioNuevaPeticion\\plantilla_EmailPendienteStandBy.html"; break;
                    case TypeEmail.ReSendEmail: ExtendedPath = "EnvioRespuesta\\plantilla_reenvio_Cliente.html"; break;
                    case TypeEmail.ReceivedClaveDemo: ExtendedPath = "EnvioNuevaPeticion\\plantilla_RecibidaDemo.html"; break;
                    case TypeEmail.Assign: ExtendedPath = "Asignar\\plantilla_asignarConsulta.html"; break;
                    case TypeEmail.AssignN2: ExtendedPath = "Asignar\\plantilla_asignarConsultaN2.html"; break;
                    case TypeEmail.RequestClarification: ExtendedPath = "EnvioRespuesta\\plantilla_envio_aclaracion_level3.html"; break;
                    case TypeEmail.RequestComment: ExtendedPath = "EnvioRespuesta\\plantilla_envio_comentario_level3.html"; break;
                    default: break;
                }
            }
            else if (iTypeQuery == (int)TypeQuery.Expediente)
            {
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient: ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewExpedientClient.html"; break;
                    case TypeEmail.NewQueryConsultant: ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewExpedient.html"; break;
                    case TypeEmail.NonPayment: ExtendedPath = "EnvioImpagos\\plantilla_envio_impago.html"; break;
                    case TypeEmail.NonPaymentClient: ExtendedPath = "EnvioImpagos\\plantilla_envio_impagoCliente.html"; break;
                    case TypeEmail.Request: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta.html"; break;
                    case TypeEmail.RequestLevel3: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_level3.html"; break;
                    case TypeEmail.RequestTranslate: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_traducido.html"; break;
                    case TypeEmail.RequestCopy: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuestaCopia.html"; break;
                    case TypeEmail.PendingStandBy: ExtendedPath = "EnvioNuevaPeticion\\plantilla_EmailPendienteStandBy.html"; break;
                    case TypeEmail.ReSendEmail: ExtendedPath = "EnvioRespuesta\\plantilla_reenvio_Cliente.html"; break;
                    case TypeEmail.ReceivedClaveDemo: ExtendedPath = "EnvioNuevaPeticion\\plantilla_RecibidaDemo.html"; break;
                    case TypeEmail.Assign: ExtendedPath = "Asignar\\plantilla_asignarExp.html"; break;
                    case TypeEmail.AssignN2: ExtendedPath = "Asignar\\plantilla_asignarConsultaN2.html"; break;
                    case TypeEmail.RequestClarification: ExtendedPath = "EnvioRespuesta\\plantilla_envio_aclaracion_level3.html"; break;
                    case TypeEmail.RequestComment: ExtendedPath = "EnvioRespuesta\\plantilla_envio_comentario_level3.html"; break;
                    default: break;
                }
            }
            else
            {   // Formulario
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient: ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewFormularioClient.html"; break;
                    case TypeEmail.NewQueryConsultant: ExtendedPath = "EnvioNuevaPeticion\\plantilla_NewFormulario.html"; break;
                    case TypeEmail.NonPayment: ExtendedPath = "EnvioImpagos\\plantilla_envio_impago.html"; break;
                    case TypeEmail.NonPaymentClient: ExtendedPath = "EnvioImpagos\\plantilla_envio_impagoCliente.html"; break;
                    case TypeEmail.Request: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta.html"; break;
                    case TypeEmail.RequestLevel3: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_level3.html"; break;
                    case TypeEmail.RequestTranslate: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuesta_traducido.html"; break;
                    case TypeEmail.RequestCopy: ExtendedPath = "EnvioRespuesta\\plantilla_envio_respuestaCopia.html"; break;
                    case TypeEmail.PendingStandBy: ExtendedPath = "EnvioNuevaPeticion\\plantilla_EmailPendienteStandBy.html"; break;
                    case TypeEmail.ReSendEmail: ExtendedPath = "EnvioRespuesta\\plantilla_reenvio_Cliente.html"; break;
                    case TypeEmail.ReceivedClaveDemo: ExtendedPath = "EnvioNuevaPeticion\\plantilla_RecibidaDemo.html"; break;
                    case TypeEmail.Assign: ExtendedPath = "Asignar\\plantilla_asignarForm.html"; break;
                    case TypeEmail.AssignN2: ExtendedPath = "Asignar\\plantilla_asignarConsultaN2.html"; break;
                    case TypeEmail.RequestClarification: ExtendedPath = "EnvioRespuesta\\plantilla_envio_aclaracion_level3.html"; break;
                    case TypeEmail.RequestComment: ExtendedPath = "EnvioRespuesta\\plantilla_envio_comentario_level3.html"; break;
                    default: break;
                }
            }
            return LocalPath + '\\' + ExtendedPath;
        }

        private String GetAsuntoEmail(TypeEmail ttype, int iTypeQuery)
        {
            String strSubject = "sin asunto";

            if (iTypeQuery == (int)TypeQuery.Consulta)
            {
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient: strSubject = "Su consulta ha sido recibida"; break;
                    case TypeEmail.NewQueryConsultant: strSubject = "Consulta @@NAVISION@@ - @@TITULAR@@"; break;
                    case TypeEmail.NonPayment: strSubject = "Solicitud de cobro por morosidad. Codigo Navision : @@NAVISION@@"; break;
                    case TypeEmail.NonPaymentClient: strSubject = "Resolución de impagos."; break;
                    case TypeEmail.Request: strSubject = "El cliente ha realizado un comentario"; break;
                    case TypeEmail.RequestLevel3: strSubject = "Respuesta Consulta  nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestTranslate: strSubject = "Envío consulta traducido EDF @@NUMERO@@. @@COLABORADOR@@"; break; // No deberia
                    case TypeEmail.RequestCopy: strSubject = "Su información ha sido enviada"; break;
                    case TypeEmail.PendingStandBy: strSubject = "Información sobre su petición"; break;
                    case TypeEmail.ReceivedClaveDemo: strSubject = "Cliente clave demo ha realizado una petición"; break;
                    case TypeEmail.Assign: strSubject = "Asignación Consulta"; break;
                    case TypeEmail.RequestClarification: strSubject = "Petición de aclaración a Consulta nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestComment: strSubject = "Comentario a Consulta nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    default: break;
                }
            }
            else if (iTypeQuery == (int)TypeQuery.Expediente)
            {
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient: strSubject = "Su solicitud de modelo de expediente ha sido procesada"; break;
                    case TypeEmail.NewQueryConsultant: strSubject = "Modelo de expediente @@NAVISION@@ - @@TITULAR@@"; break;
                    case TypeEmail.NonPayment: strSubject = "Solicitud de cobro por morosidad. Codigo Navision : @@NAVISION@@"; break;
                    case TypeEmail.NonPaymentClient: strSubject = "Resolución de impagos."; break;
                    case TypeEmail.Request: strSubject = "El cliente ha realizado un comentario"; break;
                    case TypeEmail.RequestLevel3: strSubject = "Envio Expte. nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestTranslate: strSubject = "Envío modelo de expediente traducido EDF @@NUMERO@@. @@COLABORADOR@@"; break;
                    case TypeEmail.RequestCopy: strSubject = "Su información ha sido enviada"; break;
                    case TypeEmail.PendingStandBy: strSubject = "Información sobre su petición"; break;
                    case TypeEmail.ReceivedClaveDemo: strSubject = "Cliente clave demo ha realizado una petición"; break;
                    case TypeEmail.Assign: strSubject = "Asignación Expediente"; break;
                    case TypeEmail.RequestClarification: strSubject = "Petición de aclaración a Expediente nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestComment: strSubject = "Comentario a Expediente nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    default: break;
                }
            }
            else
            {   // Formulario
                switch (ttype)
                {
                    case TypeEmail.NewQueryClient: strSubject = "Su petición de formulario ha sido recibida"; ; break;
                    case TypeEmail.NewQueryConsultant: strSubject = "Petición de formulario @@NAVISION@@ - @@TITULAR@@"; break;
                    case TypeEmail.NonPayment: strSubject = "Solicitud de cobro por morosidad. Codigo Navision : @@NAVISION@@"; break;
                    case TypeEmail.NonPaymentClient: strSubject = "Resolución de impagos."; break;
                    case TypeEmail.Request: strSubject = "El cliente ha realizado un comentario"; ; break;
                    case TypeEmail.RequestLevel3: strSubject = "Respuesta formulario. nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestTranslate: strSubject = "Envío formulario traducido EDF @@NUMERO@@. @@COLABORADOR@@"; break; // No deberia
                    case TypeEmail.RequestCopy: strSubject = "Su información ha sido enviada"; break;
                    case TypeEmail.PendingStandBy: strSubject = "Información sobre su petición"; break;
                    case TypeEmail.ReceivedClaveDemo: strSubject = "Cliente clave demo ha realizado una petición"; break;
                    case TypeEmail.Assign: strSubject = "Asignación Formulario"; break;
                    case TypeEmail.RequestClarification: strSubject = "Petición de aclaración a Formulario nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    case TypeEmail.RequestComment: strSubject = "Comentario a Formulario nº @@INDICADOR@@ del colaborador @@COLABORADOR@@"; break;
                    default: break;
                }

            }
            return strSubject;
        }
        /// <summary>
        /// Add new subquery to database
        /// </summary>
        /// <returns></returns>
        public int SendEmailAssing(int iIdCodCRM, ConsultoriaSvc.Consulta oQuery, int iIdQuery, ClavesSvc.UsuarioApp oUser, String strTitular, int iIdCodNav, int iNivel)
        {
            try
            {

                //log.Info("SendEmailAssing.Inicio : @iCodCRM: " + iIdCodCRM.ToString() + ", @iIdQuery: " + iIdQuery.ToString());


                int iResult = -1;


                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.NewQueryConsultant, oQuery._idMateria);
                string subject = this.GetAsuntoEmail(TypeEmail.NewQueryConsultant, oQuery._idTipoConsulta);

                if (this.IsLlamadaExperta(oQuery._idMateria))
                {
                    subject = subject.Replace("@@NAVISION@@", iIdCodCRM.ToString());
                }
                else
                {
                    subject = subject.Replace("@@NAVISION@@", iIdCodNav.ToString());
                }
                subject = subject.Replace("@@TITULAR@@", strTitular);

                if (iNivel == 2)
                {
                    subject = "Copia Nivel 2 - " + subject;
                }

                string strBody = String.Empty;


                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                strBody = strBody.Replace("@@SUGAR@@", iIdCodCRM.ToString());
                strBody = strBody.Replace("@@MATERIA@@", oQuery._descMateria);
                strBody = strBody.Replace("@@NOMBRE@@", oQuery._nombreUsuarioPeticion);
                strBody = strBody.Replace("@@TEMA@@", oQuery._descSubMateria);
                strBody = strBody.Replace("@@DESCRIPCION@@", oQuery._descConsulta);
                //strBody = strBody.Replace("@@TELEFONO@@", oQuery._tlf);
                strBody = strBody.Replace("@@EMAIL@@", oQuery._email);
                strBody = strBody.Replace("@@PROVINCIA@@", oQuery._descProvincia);
                strBody = strBody.Replace("@@TITULAR@@", strTitular);
                strBody = strBody.Replace("@@NAVISION@@", iIdCodNav.ToString());

                String sUserApp = oUser._login;
                String sPassword = oUser._pwd;


                string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iIdCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                strBody = strBody.Replace("@@URL@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);


                //strBody = strBody.Replace("@@URL@@", this.GetUrlApplicationGestion() + "LegalConsulting/ShowQuestion?codCRM=" + ipIdCodCRM.ToString() + "&codQuestion=" + iIdQuery.ToString());


                if (oUser != null)
                {
                    string[] lstEmails = new string[1];

                    lstEmails[0] = oUser._email;

                    iResult = this.SendEmailRequest(iIdCodCRM, lstEmails, strBody, subject, oQuery._idMateria, oQuery._indUrgente);

                }

                //log.Info("SendEmailAssing.Fin : @iResult: " + iResult.ToString());

                return iResult;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailAssing), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailAssing), ex.Message);
                return -1;
            }

        }

        private BeanList<ConsultoriaSvc.Consulta> getQuestions(int questionId, int iUserId)
        {
            try
            {
                int iUserOwner = (iUserId > 0) ? iUserId : objVariables.idUsuarioAreaPersonal;


                BeanList<ConsultoriaSvc.Consulta> lstConsulta = _svcConsultoria.RecuperarConsultasAsync(questionId, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 10, String.Empty, String.Empty, iUserOwner).Result;

                return lstConsulta;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(getQuestions), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(getQuestions), ex.Message);
                return null;
            }
        }
        private ConsultoriaSvc.Consulta GetConsulta(int iQuestionid, int iUserId)
        {
            ConsultoriaSvc.Consulta oResult = new ConsultoriaSvc.Consulta();

            try
            {

                if (iUserId == 1) iUserId = objVariables.idUsuarioAreaPersonal;

                BeanList<ConsultoriaSvc.Consulta> oList = this.getQuestions(iQuestionid, iUserId);

                if ((oList != null) && (oList.beanList != null) && (oList.beanList.Count > 0))
                {
                    oResult = oList.beanList.First();
                }
                //else return null;

                return oResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetConsulta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetConsulta), ex.Message);
                return null;
            }
        }
        /// <summary>
        /// SendEmailAddQuery
        /// </summary>
        /// <param name="iCodCRM"></param>
        /// <param name="strEmail"></param>
        /// <param name="strName"></param>
        /// <param name="iIdQuery"></param>
        /// <returns></returns>

        private int GetGrupoId(int iIdMatter)
        {
            try
            {

                string sidGroup = iIdMatter > 0 ? iIdMatter.ToString() : String.Empty;

                switch (iIdMatter)
                {
                    case 1: return objVariables.configConsultoria.mail_idGroup1;
                    case 3: return objVariables.configConsultoria.mail_idGroup3;
                    case 8: return objVariables.configConsultoria.mail_idGroup8;
                    case 10: return objVariables.configConsultoria.mail_idGroup10;
                    case 11: return objVariables.configConsultoria.mail_idGroup11;
                    case 12: return objVariables.configConsultoria.mail_idGroup12;
                    case 13: return objVariables.configConsultoria.mail_idGroup13;
                    case 14: return objVariables.configConsultoria.mail_idGroup14;
                    case 15: return objVariables.configConsultoria.mail_idGroup15;
                    case 16: return objVariables.configConsultoria.mail_idGroup16;
                    case 17: return objVariables.configConsultoria.mail_idGroup17;
                    case 20: return objVariables.configConsultoria.mail_idGroup20;
                    default: return -1;

                }

            }
            catch (Exception ex)
            {
                objLogger.LogError("Error --> {0} {1} ", nameof(GetGrupoId), ex.Message);
                return -1;
            }
        }


        /// <summary>
        /// SendEmailRequest
        /// </summary>
        /// <param name="iCodCRM"></param>
        /// <param name="strEmails"></param>
        /// <param name="strPlantillaHTML"></param>
        /// <param name="strAsunto"></param>
        /// <returns></returns>
        private int SendEmailRequest(int iCodCRM, string[] strEmails, string strPlantillaHTML, string strAsunto, int iIdMatter, int iIndUrgente)
        {
            try
            {
                string strEmailLineOrig = String.Empty;

                if (strEmails != null)
                foreach (String email in strEmails)
                {
                    strEmailLineOrig = strEmailLineOrig + ',' + email;
                }

                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}, {5}", nameof(SendEmailRequest), iCodCRM.ToString(), strEmailLineOrig, strPlantillaHTML.Length.ToString(), strAsunto, iIdMatter.ToString());



                int intIdGrupo = this.GetGrupoId(iIdMatter);
                string strRemitente = "Servicio Consultoría";
                int intPrioridad = 1;
                int intIdUsuario = objVariables.idUsuarioAreaPersonal;

                if (iIndUrgente == 1)
                {
                    strAsunto = "[URGENTE] " + strAsunto;
                }

                if (strEmails.Length > 1)
                {
                    strEmails = this.GetDistinctsEmails(strEmails);
                }

                strPlantillaHTML = this.EncodeHTML(strPlantillaHTML);

                string strEmailLine = String.Empty;

                foreach (String email in strEmails)
                {
                    strEmailLine = strEmailLine + ',' + email;
                }

                //log.Info("SendEmailRequest.CallPeticionEnvioUnicoMails  : @IntIdGrupo: " + intIdGrupo.ToString() + ", @StrRemitente: " + strRemitente + ", @StrAsunto: " + strAsunto +
                //    ", @IntPrioridad: " + intPrioridad.ToString() + ", @IntIdUsuario: " + intIdUsuario.ToString() + ", @Emails : " + strEmailLine);



                var objresult = _svcEmails.PeticionEnvioUnicoMailsAsync(intIdGrupo, strEmails, strPlantillaHTML, strRemitente, strAsunto, intPrioridad, intIdUsuario).Result;
                //PeticionEnvioUnicoMailsResponse request = Emails..PeticionEnvioUnicoMails(newParameters);

                int iResult = objresult;

                objLogger.LogInformation("Resultado PeticionEnvioUnicoMail --> {0} {1} ", nameof(SendEmailRequest), iResult.ToString());

                return iResult;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailRequest), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailRequest), ex.Message);
                return -1;
            }
        }

        private int SendEmailNonPayment(int iCodCRM, int iCodNavision, int iIdQuery)
        {
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(SendEmailNonPayment), iCodCRM.ToString(), iCodNavision.ToString(), iIdQuery.ToString());


                string strBody = String.Empty;

                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, objVariables.idUsuarioAreaPersonal);

                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta {1}", nameof(SendEmailNonPayment), iIdQuery.ToString());


                    return -1;
                }

                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.NonPayment, oQuery._idMateria);
                string strSubject = this.GetAsuntoEmail(TypeEmail.NonPayment, oQuery._idTipoConsulta);

                strSubject = strSubject.Replace("@@NAVISION@@", iCodNavision.ToString());

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                string sDataAllow = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&codQuestion=" + oQuery._idConsulta.ToString() + "&origen=" + Convert.ToString((int)TypeEmisor.cobros) + "&status=" + Convert.ToString((int)TypeStatus.open), "123456asdfghjklqwertyuiop1234567");
                strBody = strBody.Replace("@@URL_ALLOW@@", objVariables.configConsultoria.UrlAppConsultoria + "Question/ResponseNonPayment?sdata=" + sDataAllow);

                string sDataRefuse = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&codQuestion=" + oQuery._idConsulta.ToString() + "&origen=" + Convert.ToString((int)TypeEmisor.cobros) + "&status=" + Convert.ToString((int)TypeStatus.closed_without_resolved), "123456asdfghjklqwertyuiop1234567");
                strBody = strBody.Replace("@@URL_REFUSE@@", objVariables.configConsultoria.UrlAppConsultoria + "Question/ResponseNonPayment?sdata=" + sDataRefuse);

                //strBody = strBody.Replace("@@LINK@@", this.GetUrlApplicationGestion() + "LegalConsulting/ShowQuestion?codCRM=" + iCodCRM.ToString() + "&codQuestion=" + iIdQuery.ToString());

                strBody = strBody.Replace("@@CODNAVISION@@", iCodNavision.ToString());

                string lstEmailsCobros = objVariables.mail_cobros;

                string[] lstEmails = this.GetDistinctsEmails(lstEmailsCobros.Split(';'));


                objLogger.LogInformation("{0} enviado email a {1} ", nameof(SendEmailNonPayment), lstEmails[0]);
                int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, strSubject, oQuery._idMateria, oQuery._indUrgente);

                objLogger.LogInformation("{0} con Resultado {1} ", nameof(SendEmailNonPayment), intResult.ToString());

                return intResult;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailNonPayment), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailNonPayment), ex.Message);
                return -1;
            }

        }


        public int SendEmailStandBy(int iCodCRM, string strEmail, string strName, int iIdQuery)
        {
            try
            {
                //log.Info("SendEmailStandBy.Inicio : @iCodCRM: " + iCodCRM.ToString() + ", @iIdQuery: " + iIdQuery.ToString());

                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}", nameof(SendEmailStandBy), iCodCRM.ToString(), strEmail, strName, iIdQuery.ToString());



                string strBody = String.Empty;



                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, objVariables.idUsuarioAreaPersonal);


                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta {1}", nameof(SendEmailStandBy), iIdQuery.ToString());

                    return -1;
                }


                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.PendingStandBy, oQuery._idMateria);
                string strSubject = this.GetAsuntoEmail(TypeEmail.PendingStandBy, oQuery._idTipoConsulta);

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                strBody = strBody.Replace("@@NAME@@", this.myTI.ToTitleCase(strName.ToLower()));
                strBody = strBody.Replace("@@MATERIA@@", oQuery._descMateria);

                string[] lstEmails = strEmail.Split(';');


                objLogger.LogInformation("{0} enviado email a {1} ", nameof(SendEmailStandBy), lstEmails[0]);
                int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, strSubject, oQuery._idMateria, oQuery._indUrgente);

                objLogger.LogInformation("{0} con Resultado {1} ", nameof(SendEmailStandBy), intResult.ToString());

                return intResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailStandBy), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailStandBy), ex.Message);
                return -1;
            }

        }

        private ClavesSvc.Comercial GetComercial(int iCodCRM, int iUserApp)
        {
            try
            {

                Bean<ClavesSvc.Comercial> oList = _svcClaves.RecuperarComercialAsync(iCodCRM, iUserApp).Result;

                if (oList.bean != null)
                {
                    return oList.bean;
                }

                else return null;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetComercial), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetComercial), ex.Message);
                return null;
            }

        }
        public int CheckSendEmailClaveDemo(int iIdCodCRM, int iQueryId)
        {

            int intResult = -1;
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(CheckSendEmailClaveDemo), iIdCodCRM.ToString(), iQueryId.ToString());



                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iQueryId, objVariables.idUsuarioAreaPersonal);

                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta con id {1}", nameof(CheckSendEmailClaveDemo), iQueryId.ToString());
                    return -1;
                }

                if (oQuery._idTipoSuscripcion == (int)TypeSuscription.demo)
                {

                    ClavesSvc.Comercial oComercial = this.GetComercial(iIdCodCRM, objVariables.idUsuarioAreaPersonal);



                    if (oComercial == null)
                    {
                        objLogger.LogInformation("{0} Resultado: No existe comercial en {1}", nameof(CheckSendEmailClaveDemo), iIdCodCRM.ToString());
                        return -1;
                    }

                    string pathTemplateData = string.Empty;
                    string strBody = String.Empty;
                    string subject = String.Empty;


                    pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.ReceivedClaveDemo, oQuery._idMateria);
                    subject = this.GetAsuntoEmail(TypeEmail.ReceivedClaveDemo, oQuery._idTipoConsulta);



                    using (StreamReader lector = new StreamReader(pathTemplateData))
                    {
                        while (lector.Peek() > -1)
                        {
                            string linea = lector.ReadLine();
                            if (!String.IsNullOrEmpty(linea))
                            {
                                strBody = strBody + linea;
                            }
                        }
                    }

                    String strUserName = oQuery._descUsuarioAsignado;
                    String strMatter = oQuery._descMateria;



                    strBody = strBody.Replace("@@COD_SUGAR@@", iIdCodCRM.ToString());
                    strBody = strBody.Replace("@@NOMBRE_CUENTA@@", oQuery._nombreClienteSuscripcion);
                    strBody = strBody.Replace("@@PRODUCTO@@", oQuery._descProductoNav);
                    strBody = strBody.Replace("@@NOMBRE_CLIENTE@@", oQuery._nombreUsuarioPeticion);

                    strBody = strBody.Replace("@@MATERIA@@", strMatter);

                    string[] lstEmails = new string[2];

                    lstEmails[0] = oComercial._email;
                    lstEmails[1] = oComercial._emailGerente;

                    lstEmails = this.GetDistinctsEmails(lstEmails);

                    objLogger.LogInformation("{0} enviado email a {1} ", nameof(CheckSendEmailClaveDemo), lstEmails[0]);

                    intResult = this.SendEmailRequest(iIdCodCRM, lstEmails, strBody, subject, oQuery._idMateria, oQuery._indUrgente);

                }
                else
                {
                    objLogger.LogInformation("{0} la suscripción no es demo ", nameof(CheckSendEmailClaveDemo));
                    intResult = -2;
                }

                objLogger.LogInformation("{0} con Resultado {1} ", nameof(CheckSendEmailClaveDemo), intResult.ToString());
                return intResult;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CheckSendEmailClaveDemo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CheckSendEmailClaveDemo), ex.Message);
                return -1;
            }


        }

        public int SendEmailAddQuery(int iCodCRM, string strEmail, string strName, int iIdQuery)
        {
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}", nameof(SendEmailAddQuery), iCodCRM.ToString(), strEmail, strName, iIdQuery.ToString());





                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, objVariables.idUsuarioAreaPersonal);

                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta con id {1}", nameof(SendEmailAddQuery), iIdQuery.ToString());
                    return -1;
                }


                string strBody = String.Empty;


                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.NewQueryClient, oQuery._idMateria);
                string subject = this.GetAsuntoEmail(TypeEmail.NewQueryClient, oQuery._idTipoConsulta);



                if ((oQuery != null) && (oQuery._idConsulta > 0))
                {

                    using (StreamReader lector = new StreamReader(pathTemplateData))
                    {
                        while (lector.Peek() > -1)
                        {
                            string linea = lector.ReadLine();
                            if (!String.IsNullOrEmpty(linea))
                            {
                                strBody = strBody + linea;
                            }
                        }
                    }

                    string sData = CommonUI.Models.Login.LoginClaves.Encrypt("icodCRM=" + iCodCRM.ToString() + "&codQuestion=" + iIdQuery.ToString() + "&level3=0&customer=1", "123456asdfghjklqwertyuiop1234567");

                    strBody = strBody.Replace("@@MATERIA@@", oQuery._descMateria);
                    strBody = strBody.Replace("@@NAME@@", this.myTI.ToTitleCase(strName.ToLower()));
                    strBody = strBody.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppConsultoria + "Question/ShowQuestion?data=" + sData);



                    string[] lstEmails = new string[1];

                    lstEmails[0] = strEmail;

                    objLogger.LogInformation("{0} enviado email a {1} ", nameof(SendEmailAddQuery), lstEmails[0]);


                    int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, subject, oQuery._idMateria, oQuery._indUrgente);

                    objLogger.LogInformation("{0} con Resultado {1} ", nameof(SendEmailAddQuery), intResult.ToString());

                    return intResult;
                }
                else
                {

                    objLogger.LogInformation("{0} No existe la consulta :", nameof(SendEmailAddQuery), iIdQuery.ToString());
                    return -1;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailAddQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailAddQuery), ex.Message);
                return -1;
            }

        }

        private BeanList<ConsultoriaSvc.Fichero> GetFilesByQuery(int iIdQuery, int iIdColab, int iVisible)
        {
            try
            {

                BeanList<ConsultoriaSvc.Fichero> alist = _svcConsultoria.RecuperarConsultaFicherosAsync(iIdQuery, iIdColab, iVisible).Result;

                if ((alist != null) && (alist.beanList != null) && (alist.beanList.Count > 0))

                    objLogger.LogInformation("Resultado en {0}, con : @iIdQuery: {1},  @iIdColab: {2}, @iVisible: {3}", nameof(GetFilesByQuery), iIdQuery.ToString(), iIdColab.ToString(), iVisible.ToString());

                return alist;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetFilesByQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetFilesByQuery), ex.Message);
                return null;
            }
        }

        private string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            invalidChars += ".,ºªáäéëíïóöüúñÁÄÉËÍÍÓÖÚÚÑ¿?!¡'çÇ ";

            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        private string GetFile(string sFileName, string sSubFolder)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(GetFile), sFileName, sSubFolder);

            string sResult = String.Empty;

            sFileName = this.MakeValidFileName(Path.GetFileNameWithoutExtension(sFileName)) + Path.GetExtension(sFileName);

            sResult = _svcRepository.getFileAsync(sFileName, objVariables.configConsultoria.IdAreaConsultoria, sSubFolder).Result;

            objLogger.LogInformation("{0} con Resultado {1} ", nameof(GetFile), sResult);


            return sResult;
        }

        private String GetFilesToEmail(int iIdQuery, int iIsActiveCustomer)
        {
            String strFiles = String.Empty;

            BeanList<ConsultoriaSvc.Fichero> arrFiles = this.GetFilesByQuery(iIdQuery, -1, iIsActiveCustomer);

            //List<string> files = new List<string>();
            string sPathRef;

            if ((arrFiles != null) && (arrFiles.beanList != null))
            {
                foreach (ConsultoriaSvc.Fichero itemFile in arrFiles.beanList)
                {
                    sPathRef = this.GetFile(itemFile._nombre, iIdQuery.ToString());
                    //strExt = itemFile._extension.Remove(0, 1).ToUpper();



                    strFiles = strFiles + "<tr>" +
                                             "<td align=\"right\" valign=\"top\">&bull;</td>" +
                                             "<td width=\"15\"></td>" +
                                             "<td align=\"center\" valign=\"top\">" + itemFile._nombre + " </td>" +
                                             "<td width=\"15\"></td>" +
                                             "<td height=\"16\"><a href=\"" + sPathRef + "\"><img src=\"http://content.efl.es/getImage.aspx?image=mailing/comunes/icono-nueva-ventana-24x24.png\" width=\"16\" height=\"16\" alt=\"\" style=\"display:block; height:16px; width:16px;\" border=\"0\" /></a></td>" +
                                           "</tr><tr><td height=\"10\"></td></tr>";

                }
            }

            return strFiles;



        }

        private String GetComentsToEmail(String descUsuarioApp, String descSubConsulta, String fechaSubConsulta, int intIndRespuesta)
        {
            string strComment = String.Empty;

            string strEsRespuesta = String.Empty;

            if (intIndRespuesta == 1)
            {
                strEsRespuesta = "<td align=\"right\" style=\"color:#999999; font-size: 10px;\">(Respuesta)</td>";
            }
            strComment = "<tr>" +
                         "<td align=\"left\" bgcolor=\"#F0EFEF\" height=\"74\"  width=\"100%\" style=\"line-height:normal;font-size:17px; font-family: Lato, arial, Helvetica, sans-serif; padding:40px; border:none;\">" + descSubConsulta +
                          "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                              "<tr>" +
                                "<td height=\"30\"></td>" +
                              "</tr>" +
                          "</table>" +
                              "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
                                "<tr>" +
                                //"<td style=\"color:#999999; font-size: 14px;\">" + descUsuarioApp +

                                //"</td>" +
                                "<td align=\"right\" style=\"color:#999999; font-size: 14px;\">" +
                                fechaSubConsulta +
                                "</td>" + strEsRespuesta +
                               "</tr>" +
                            "</table>" +
                        "</td>" + "</tr>";

            return strComment;

        }

        private BeanList<ConsultoriaSvc.Nref> GetNRefsByQuery(int iIdQuery)
        {
            try
            {


                BeanList<ConsultoriaSvc.Nref> olist = _svcConsultoria.RecuperarConsultaNrefsAsync(iIdQuery).Result;

                if ((olist != null) && (olist.beanList != null) && (olist.beanList.Count > 0))
                {
                    objLogger.LogInformation("Resultado en {0}, con : @iIdQuery: {1},  @Count: {2}", nameof(GetFilesByQuery), iIdQuery.ToString(), olist.beanList.Count.ToString());

                }
                //else
                //{
                //    log.Info("GetNRefsByQuery : @Count: -1");
                //}

                return olist;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetNRefsByQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetNRefsByQuery), ex.Message);
                return null;
            }
        }

        private Dictionary<String, String> GetTitles(String[] oListNRefs)
        {
            string sUrl = objVariables.configConsultoria.urlNrefTitle;

            string usuario = objVariables.onlineUserId;
            string pass = objVariables.onlineUserPass;


            Dictionary<String, String> oTitles = new System.Collections.Generic.Dictionary<string, string>();

            try
            {
                if ((oListNRefs != null) && (oListNRefs.Length > 0))
                {
                    string sParams = String.Empty;
                    foreach (String nRef in oListNRefs)
                    {
                        if (!String.IsNullOrEmpty(nRef))
                        {
                            sParams = sParams + "," + nRef;
                        }
                    }

                    if (sParams.Length > 0) sParams = sParams.TrimStart(',');
                    string strResult = String.Empty;

                    var credentials = new NetworkCredential(usuario, pass);
                    var handler = new HttpClientHandler { Credentials = credentials };

                    using (var client = new HttpClient(handler))
                    {


                        var responseGet = client.GetAsync(sUrl + "nrefs=" + sParams).Result;
                        if (!responseGet.IsSuccessStatusCode)
                        {
                            if (responseGet.Content.ReadAsStreamAsync().Result != null)
                            {
                                strResult = responseGet.Content.ReadAsStringAsync().Result;
                            }
                        }
                        else
                        {
                            objLogger.LogInformation("Error en Conexxion Https--> {0} Sin resultados.", nameof(GetTitles));
                            strResult = responseGet.Content.ReadAsStringAsync().Result;
                        }

                        oTitles = JsonConvert.DeserializeObject<Dictionary<String, String>>(strResult);


                    }

                }

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetTitles), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetTitles), ex.Message);
            }

            return oTitles;
        }


        private String GetNRefsToEmail(int iIdQuery)
        {

            String strNRefs = String.Empty;

            try
            {

                BeanList<ConsultoriaSvc.Nref> arrNRefs = this.GetNRefsByQuery(iIdQuery);

                if ((arrNRefs != null) && (arrNRefs.beanList != null) && (arrNRefs.beanList.Count > 0))
                {
                    string UrlNref = objVariables.configConsultoria.urlNrefDocument + "tipoDocumento={0}&nref={1}";

                    string UrlNRefArt = objVariables.configConsultoria.urlNrefArticle + "nref={0}&seccion={1}";

                    String[] sListNRefs = new String[arrNRefs.beanList.Count];

                    string strAuxNref = string.Empty;
                    for (int i = 0; i < arrNRefs.beanList.Count; i++)
                    {

                        String[] sTitleAux = arrNRefs.beanList[i]._nref.Split('@');

                        if (sTitleAux.Length == 2)
                        {
                            sListNRefs[i] = sTitleAux[0];
                        }
                        else
                        {
                            sListNRefs[i] = this.ParseNRef(arrNRefs.beanList[i]._nref);
                        }
                    }

                    Dictionary<String, String> oListTitles = this.GetTitles(sListNRefs);

                    String sTitle = String.Empty;


                    string url = String.Empty;
                    if ((oListTitles != null) && (oListTitles.Count > 0))
                    {
                        foreach (ConsultoriaSvc.Nref itemNRef in arrNRefs.beanList)
                        {
                            String[] sTitleAux = itemNRef._nref.Split('@');


                            if (sTitleAux.Length == 2)
                            {
                                itemNRef._nref = this.ParseNRef(sTitleAux[0]) + "@" + sTitleAux[1];
                                sTitle = oListTitles[this.ParseNRef(sTitleAux[0])] + " (" + sTitleAux[1] + ")";

                                url = string.Format(UrlNRefArt, itemNRef._nref, sTitleAux[1]);

                            }
                            else
                            {
                                itemNRef._nref = this.ParseNRef(itemNRef._nref);
                                sTitle = oListTitles[itemNRef._nref];
                                url = string.Format(UrlNref, itemNRef._tipo, itemNRef._nref);


                            }

                            // url // No se permite navegar a la referencia.
                            url = String.Empty;

                            strNRefs += "<tr>" +
                                              "<td align=\"right\" valign=\"top\">&bull;</td>" +
                                              "<td width=\"15\"></td>" +
                                              "<td align=\"center\" valign=\"top\">" + sTitle + " </td>" +
                                              "<td width=\"15\"></td>" +
                                              "<td height=\"16\"><a href=\"" + url + "\"><img src=\"http://content.efl.es/getImage.aspx?image=mailing/comunes/icono-nueva-ventana-24x24.png\" width=\"16\" height=\"16\" alt=\"\" style=\"display:block; height:16px; width:16px;\" border=\"0\" /></a></td>" +
                                            "</tr><tr><td height=\"10\"></td></tr>";



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetNRefsToEmail), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetNRefsToEmail), ex.Message);
            }

            return strNRefs;

        }

        private BeanList<ConsultoriaSvc.SubConsulta> getSubqueries(int iIdQuery, int iIdColab, int intIndInternal, int iIdMatter)
        {

            var orderby = "ASC";

            if (this.IsLlamadaExperta(iIdMatter)) // Llamada Experta -> De más actual a más antigua.
            {
                orderby = "DESC";
            }

            return _svcConsultoria.RecuperarSubConsultasAsync(iIdQuery, String.Empty, intIndInternal, iIdColab, 1, 100, "FECHA_SUB_CONSULTA", orderby).Result;
        }

        public int ReSendEmailInfo(int iCodCRM, string strEmail, int iIdQuery)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(ReSendEmailInfo), iCodCRM.ToString(), strEmail, iIdQuery.ToString());
            try
            {


                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, objVariables.idUsuarioAreaPersonal);

                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta con id {1}", nameof(ReSendEmailInfo), iIdQuery.ToString());
                    return -1;
                }


                string strBody = String.Empty;

                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.ReSendEmail, oQuery._idMateria);
                string subject = String.IsNullOrEmpty(oQuery._tituloConsulta) ? "- Sin titulo - " : oQuery._tituloConsulta;

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                BeanList<ConsultoriaSvc.SubConsulta> subquerys = new BeanList<ConsultoriaSvc.SubConsulta>();

                subquerys = this.getSubqueries(iIdQuery, -1, 0, oQuery._idMateria);

                string strComments = String.Empty;


                if ((subquerys.beanList != null) && (subquerys.beanList.Count > 0))
                {
                    int icount = 0;
                    foreach (ConsultoriaSvc.SubConsulta subquery in subquerys.beanList)
                    {

                        strComments = strComments + this.GetComentsToEmail(subquery._descUsuarioApp, subquery._descSubConsulta, subquery._fechaSubConsulta, subquery._indRespuesta);

                        if (icount < subquerys.beanList.Count - 2)
                        {
                            strComments = strComments +
                                "<tr><td width=\"100%\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td height=\"1\"  bgcolor=\"#CCCCCC\" style=\"line-height:1px;font-size:1px; background-color: #CCCCCC; height:1px;\">&nbsp;</td></tr></table></td></tr>";

                        }
                    }
                }
                else
                {
                    strComments = "No existen comentarios.";
                }

                strBody = strBody.Replace("@@COMMENTS@@", strComments);

                String strFiles = this.GetFilesToEmail(iIdQuery, 0);

                String strNrefs = this.GetNRefsToEmail(iIdQuery);

                if ((strFiles.Length > 0) || (strNrefs.Length > 0))
                {
                    strBody = strBody.Replace("@@ATTACHED_FILES@@", strFiles);
                    strBody = strBody.Replace("@@ATTACHED_NREFS@@", strNrefs);
                }
                else
                {
                    strBody = strBody.Replace("@@ATTACHED_FILES@@", "<tr><td>No hay adjuntos.</td></tr>");
                    strBody = strBody.Replace("@@ATTACHED_NREFS@@", "");
                }

                strBody = strBody.Replace("@@TITLE@@", subject);
                strBody = strBody.Replace("@@DESCRIPTION@@", oQuery._descConsulta);
                strBody = strBody.Replace("@@MATERIA@@", oQuery._descMateria);

                strBody = strBody.Replace("@@FOOTER_MATERIA@@", this.GetFooterMatter(oQuery._idMateria, oQuery._descMateria));

                if (!String.IsNullOrEmpty(oQuery._referenciaEditorial))
                {
                    String urlNRef = "";
                    String strReferencia = "<tr><td class=\"content\" colspan=\"2\"><p></p><p class=\"contenText\">REFERENCIA: <a href=\"" + urlNRef + "\">" + oQuery._referenciaEditorial + "</a></p><p></p></td></tr>";


                    strBody = strBody.Replace("@@REFERENCIA@@", strReferencia);
                }
                else
                {
                    strBody = strBody.Replace("@@REFERENCIA@@", "");
                }

                string[] lstEmails = new string[1];

                lstEmails[0] = strEmail;

                int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, subject, oQuery._idMateria, oQuery._indUrgente);

                objLogger.LogInformation("{0} Reenvio al email {1} con resultado {2}", nameof(ReSendEmailInfo), strEmail, intResult.ToString());

                return intResult;


            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                return -1;
            }
        }



        public int GetNSuscriptionsTotal(int iCodNav, int iIdMatter)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(GetNSuscriptionsTotal), iCodNav.ToString(), iIdMatter.ToString());

            try
            {
                int iNumConsultas = 0;

                iNumConsultas = _svcConsultoria.RecuperarNumConsultasContratadasAsync(iCodNav, iIdMatter, this.objVariables.idUsuarioAreaPersonal).Result;
                return iNumConsultas;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                return -1;
            }

        }

        public int GetNSuscriptionsEnabled(int iCodNav, int iIdMatter)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(GetNSuscriptionsEnabled), iCodNav.ToString(), iIdMatter.ToString());

            try
            {
                int iNumConsultas = 0;

                iNumConsultas = _svcConsultoria.RecuperarNumConsultasDisponiblesAsync(iCodNav, iIdMatter, this.objVariables.idUsuarioAreaPersonal).Result;

                return iNumConsultas;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(ReSendEmailInfo), ex.Message);
                return -1;
            }

        }


        private String GetInitials(string sFullName)
        {
            string[] sSplitName = sFullName.Split(' ');

            string sAuxName = String.Empty;

            foreach (String sInitial in sSplitName)
            {
                if (sInitial.Length > 0)
                {
                    sAuxName = sAuxName + sInitial[0];
                    if (sAuxName.Length == 3) break;
                }
                else break;

            }

            return sAuxName;
        }

        private ConsultaPropiedades GetPropertiesSvc(ConsultoriaSvc.Consulta question)
        {
            ConsultaPropiedades oResult = new ConsultaPropiedades();


            oResult.idConsulta = question._idConsulta;
            oResult.idClienteNav = question._idClienteNav;
            oResult.idClienteCRM = question._idClienteCRM;
            oResult.idUsuarioPro = question._idUsuarioPro;
            oResult.idTipoConsulta = question._idTipoConsulta;
            oResult.descTipoConsulta = question._descTipoConsulta;
            oResult.idTipoSuscripcion = question._idTipoSuscripcion;
            oResult.descTipoSuscripcion = question._descTipoSuscripcion;
            oResult.idMateria = question._idMateria;
            oResult.descMateria = question._descMateria;
            oResult.idSubMateria = question._idSubMateria;
            oResult.descSubMateria = question._descSubMateria;
            oResult.idConsultaEstado = question._idConsultaEstado;
            oResult.descConsultaEstado = question._descConsultaEstado;
            oResult.idProvincia = question._idProvincia;
            oResult.descProvincia = question._descProvincia;
            oResult.fechaConsulta = question._fechaConsulta;
            oResult.disponibilidad = question._disponibilidad;
            oResult.fechaConsultaAsignada = question._fechaConsultaAsignada;
            oResult.fechaConsultaCerrada = question._fechaConsultaCerrada;
            oResult.fechaConsultaResuelta = question._fechaConsultaResuelta;
            oResult.fechaAltaSac = question._fechaAltaSac;
            oResult.tituloConsulta = question._tituloConsulta;
            oResult.cargo = question._cargo;
            oResult.idVoz = question._idVoz;
            oResult.descVoz = question._descVoz;
            oResult.descConsulta = question._descConsulta;
            oResult.nombreUsuarioPeticion = question._nombreUsuarioPeticion;
            oResult.cargoUsuarioPeticion = question._cargoUsuarioPeticion;
            oResult.email = question._email;
            oResult.tlf = question._tlf;
            oResult.nombreClienteSuscripcion = question._nombreClienteSuscripcion;
            oResult.nombreComercial = question._nombreComercial;
            oResult.delegacionComercial = question._delegacionComercial;
            oResult.idSuscripcionNav = question._idSuscripcionNav;
            oResult.idProductoNav = question._idProductoNav;
            oResult.descProductoNav = question._descProductoNav;
            oResult.idUsuarioAsignado = question._idUsuarioAsignado;
            oResult.descUsuarioAsignado = question._descUsuarioAsignado;
            oResult.descUsuarioCreador = question._descUsuarioCreador;
            oResult.idContactoCrm = question._idContactoCrm;
            oResult.idColor = question._idColor;
            oResult.idColaborador = question._idColaborador;
            oResult.nombreColaborador = question._nombreColaborador;
            oResult.emailColaborador = question._emailColaborador;
            oResult.numeroPartes = question._numeroPartes;
            oResult.indPrivada = question._indPrivada;
            oResult.facturacionColaborador = question._facturacionColaborador;
            oResult.referenciaEditorial = question._referenciaEditorial;
            oResult.indColaboradorAnulado = question._indColaboradorAnulado;
            oResult.indTraducidoCatalan = question._indTraducidoCatalan;
            oResult.indTraducidoEuskera = question._indTraducidoEuskera;
            oResult.indTraducidoGallego = question._indTraducidoGallego;
            oResult.motivoRechazo = question._motivoRechazo;
            oResult.indFacturacionColaborador = question._indFacturacionColaborador;
            oResult.idTipoOrigen = question._idTipoOrigen;
            oResult.indUrgente = question._indUrgente;
            oResult.indNivel1 = question._indNivel1;
            oResult.indNivel2 = question._indNivel2;
            oResult.indNivel = question._indNivel;
            oResult.timePrepare = question._timePrepare;


            return oResult;
        }

        public Consulta GetPropertiesQuery(ConsultoriaSvc.Consulta question, List<Mementos> alistMementos)
        {

            Consulta oLegalQuery = new Consulta();

            try
            {

                oLegalQuery.sFullNameEtiqueta = this.GetInitials(question._nombreUsuarioPeticion);

                oLegalQuery.sEmail = question._email;

                if (question._idConsultaEstado != (int)TypeStatus.closed_resolved)
                {
                    string strSubMatter = String.IsNullOrEmpty(question._descSubMateria) ? String.Empty : question._descSubMateria;
                    string strVoz = String.IsNullOrEmpty(question._descVoz) ? String.Empty : question._descVoz;
                    string strTitle = String.IsNullOrEmpty(question._tituloConsulta) ? String.Empty : " - " + question._tituloConsulta;

                    question._tituloConsulta = strSubMatter + " - " + strVoz + strTitle;
                }

                string sFormat = (question._fechaConsulta.Length == 19 ? "dd/MM/yyyy HH:mm:ss" : "dd/MM/yyyy h:mm:ss");

                DateTime dateToDisplay = DateTime.ParseExact(question._fechaConsulta, sFormat,
                                       System.Globalization.CultureInfo.InvariantCulture);
                CultureInfo culture = new CultureInfo("es-ES");
                oLegalQuery.sFecha = dateToDisplay.ToString("F", culture);

                oLegalQuery.iStatus = question._idConsultaEstado;
                oLegalQuery.MotivoRechazo = question._motivoRechazo;


                if (question._idUsuarioAsignado > 0)
                {
                    ClavesSvc.UsuarioApp user = this.GetUserApp(question._idUsuarioAsignado);
                    if (user != null)
                    {
                        oLegalQuery.sUserNameAssigned = user._nombre;
                        oLegalQuery.sEmailAssigned = user._email;
                        oLegalQuery.iIdUserAssigned = user._idUsuarioApp;
                    }
                    else oLegalQuery.sUserNameAssigned = "Usuario asignado no encontrado.";
                }
                else oLegalQuery.sUserNameAssigned = "Consulta no asignada.";

                BeanList<ConsultoriaSvc.SubConsulta> alistSubQueries = this.getSubqueries(question._idConsulta, -1, 0, question._idMateria);

                if ((alistSubQueries != null) && (alistSubQueries.beanList != null))
                {
                    oLegalQuery.lstSubQueries = new List<SubConsulta>();

                    foreach (ConsultoriaSvc.SubConsulta item in alistSubQueries.beanList)
                    {
                        SubConsulta newItem = new SubConsulta();
                        newItem.descSubConsulta = item._descSubConsulta;
                        newItem.descUsuarioApp = item._descUsuarioApp;
                        newItem.fechaSubConsulta = item._fechaSubConsulta;

                        if (item._idUsuarioApp == objVariables.idUsuarioAreaPersonal)
                        {
                            newItem.idUsuarioApp =objVariables.configConsultoria.idUserApp;
                        }
                        else
                        { 
                            newItem.idUsuarioApp = item._idUsuarioApp; 
                        }

                        newItem.idSubConsulta = item._idSubConsulta;
                       
                        newItem.indInterno = item._indInterno;

                        oLegalQuery.lstSubQueries.Add(newItem);

                    }

                    if ((oLegalQuery.lstSubQueries != null) && (oLegalQuery.lstSubQueries.Count > 0))
                    {
                        SubConsulta itemLast = oLegalQuery.lstSubQueries.Where(t => t.indRespuesta == 1).FirstOrDefault();

                        if (itemLast == null)
                        {
                            itemLast = oLegalQuery.lstSubQueries.Where(t => t.idUsuarioApp != objVariables.configConsultoria.idUserApp ).FirstOrDefault();
                        }

                        if (itemLast != null)
                        {
                            oLegalQuery.lastRequest = new Comentarios();
                            oLegalQuery.lastRequest.sComment = itemLast.descSubConsulta;
                            oLegalQuery.lastRequest.OwnerUser = false;
                            oLegalQuery.lastRequest.IdSubConsulta = itemLast.idSubConsulta;
                            oLegalQuery.lastRequest.indInterno = itemLast.indInterno;

                            if (this.IsLlamadaExperta(question._idMateria))
                            {
                                oLegalQuery.lastRequest.sName = "Llamada Experta, ";
                            }
                            else
                            {
                                oLegalQuery.lastRequest.sName = "Servicio Consultoría, "; //:itemLast._descUsuarioApp;
                            }

                            oLegalQuery.lastRequest.sLabel = "LED"; // ActionLinkExtensions.GetInitials(itemLast._descUsuarioApp);

                            sFormat = (itemLast.fechaSubConsulta.Length == 19 ? "dd/MM/yyyy HH:mm:ss" : "dd/MM/yyyy h:mm:ss");

                            DateTime sdate = DateTime.ParseExact(itemLast.fechaSubConsulta, sFormat,
                                         System.Globalization.CultureInfo.InvariantCulture);

                            oLegalQuery.lastRequest.sDate = sdate.ToString("D", culture);
                            oLegalQuery.lastRequest.sTime = sdate.ToString("t", culture);
                        }

                        foreach (SubConsulta item in oLegalQuery.lstSubQueries)
                        {
                            sFormat = (item.fechaSubConsulta.Length == 19 ? "dd/MM/yyyy HH:mm:ss" : "dd/MM/yyyy h:mm:ss");
                            DateTime sdate = DateTime.ParseExact(item.fechaSubConsulta, sFormat,
                                              System.Globalization.CultureInfo.InvariantCulture);

                            item.fechaSubConsulta = sdate.ToString("F", culture);

                            if ( objVariables.configConsultoria.idUserApp == item.idUsuarioApp)
                            {
                                item.sName = question._nombreUsuarioPeticion;
                                item.sLabel = oLegalQuery.sFullNameEtiqueta;
                            }
                            else
                            {

                                if ((question._idMateria == (int)TypeMatter.Social) || (question._idMateria == (int)TypeMatter.Fiscal) || (question._idMateria == (int)TypeMatter.ProteccionDatos))
                                {
                                    item.sName = "Llamada Experta"; //subquery._descUsuarioApp;
                                }
                                else
                                {
                                    item.sName = "Servicio Consultoría"; //subquery._descUsuarioApp;
                                }
                                item.sLabel = "LED";
                                
                            }

                        }
                    }

                    DateTime atime = DateTime.Now;

                    BeanList<ConsultoriaSvc.Fichero> arrFiles = this.GetFilesByQuery(question._idConsulta, -1, 0); // 0 - all, 1 - visible, 2 - hidden

                    oLegalQuery.ficheros = new List<string>();
                    string sPathRef;
                    string strExt;

                    if ((arrFiles != null) && (arrFiles.beanList != null))
                    {
                        foreach (ConsultoriaSvc.Fichero itemFile in arrFiles.beanList)
                        {
                            sPathRef = this.GetFile(itemFile._nombre, question._idConsulta.ToString());
                            strExt = itemFile._extension.Remove(0, 1).ToUpper();
                            oLegalQuery.ficheros.Add(itemFile._nombre + "|" + sPathRef + '|' + strExt + '|' + itemFile._indVisible + '|' + itemFile._idFichero + '|' + itemFile._idUsuario);
                        }
                    }


                    BeanList<ConsultoriaSvc.Nref> arrNRefs = this.GetNRefsByQuery(question._idConsulta);

                    oLegalQuery.nrefs = new List<string>();

                    if ((arrNRefs != null) && (arrNRefs.beanList != null) && (arrNRefs.beanList.Count > 0))
                    {
                        String[] sListNRefs = new String[arrNRefs.beanList.Count];

                        string strAuxNref = string.Empty;
                        for (int i = 0; i < arrNRefs.beanList.Count; i++)
                        {

                            if (arrNRefs.beanList[i]._tipo != "N")
                            {
                                String[] sTitleAux = arrNRefs.beanList[i]._nref.Split('@');

                                if (sTitleAux.Length == 2)
                                {
                                    sListNRefs[i] = sTitleAux[0];
                                }
                                else
                                {
                                    sListNRefs[i] = this.ParseNRef(arrNRefs.beanList[i]._nref);
                                }
                            }
                        }

                        Dictionary<String, String> oListTitles = null;

                        if (sListNRefs.Length > 0)
                        {
                            oListTitles = this.GetTitles(sListNRefs);
                        }

                        String sTitle = String.Empty;


                        foreach (ConsultoriaSvc.Nref itemNRef in arrNRefs.beanList)
                        {
                            String[] sTitleAux = itemNRef._nref.Split('@');

                            if (itemNRef._tipo != "N")
                            {
                                if (sTitleAux.Length == 2)
                                {
                                    itemNRef._nref = this.ParseNRef(sTitleAux[0]) + "@" + sTitleAux[1];
                                    sTitle = oListTitles[this.ParseNRef(sTitleAux[0])] + " (" + sTitleAux[1] + ")";
                                }
                                else
                                {
                                    itemNRef._nref = this.ParseNRef(itemNRef._nref);
                                    sTitle = oListTitles[itemNRef._nref];
                                }
                            }
                            else
                            {
                                String sMementoName = alistMementos.Where(t => t.codigoMemento == sTitleAux[0]).FirstOrDefault().descripcion;

                                if (sTitleAux.Length == 2)
                                {
                                    sMementoName = sMementoName + " (" + sTitleAux[1] + ")";
                                }

                                if (!String.IsNullOrEmpty(sMementoName))
                                {
                                    itemNRef._nref = sTitleAux[0] + "@" + sTitleAux[1];
                                    sTitle = sMementoName;
                                }
                            }


                            oLegalQuery.nrefs.Add(itemNRef._idNref.ToString() + "|" + itemNRef._nref + "|" + itemNRef._tipo + '|' + (String.IsNullOrEmpty(sTitle) ? itemNRef._nref : sTitle) + '|' + itemNRef._xml);
                        }

                    }

                }

                oLegalQuery.propiedades = this.GetPropertiesSvc(question);
                //log.Info("-->GetPropertiesQuery.Fin");
                return oLegalQuery;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetPropertiesQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetPropertiesQuery), ex.Message);
                return null;
            }


        }

        private  String ParseNRef(String strNRef)
        {
            String strResult = String.Empty;

            if (!String.IsNullOrEmpty(strNRef))
            {
                String[] sref = strNRef.Split('/');
                if (sref.Length == 2)
                    strResult = Convert.ToInt32(sref[0]).ToString() + "/" + Convert.ToInt32(sref[1]).ToString();
            }
            return strResult;
        }

        /// <summary>
        /// Add new subquery to database
        /// </summary>
        /// <returns></returns>
        public int AddSubQuery(int iQueryId, string strSubQuery, int iIndInternal, int iIdUser)
        {
            int iResult = -1;

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}", nameof(AddSubQuery), iQueryId.ToString(), strSubQuery, iIndInternal.ToString(), iIdUser.ToString());
            try
            {

                iResult = _svcConsultoria.InsertarSubConsultaAsync(iQueryId, strSubQuery, iIndInternal, 0, iIdUser).Result;
                objLogger.LogInformation("{0} Guardado comentario en {1} con resultado {2}", nameof(ReSendEmailInfo), iQueryId.ToString(), iResult.ToString());

                return iResult;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetPropertiesQuery), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetPropertiesQuery), ex.Message);
            }

            return iResult;
        }

        private int GetUserResponsableLlamadaExperta(int iNivel, int iMatter)
        {
            int iUserId = -1;

            try
            {

                if (iMatter == 1)
                {
                    iUserId = objVariables.configConsultoria.mail_n2Responsable_1;
                }
                else
                {
                    iUserId = objVariables.configConsultoria.mail_n2Responsable_3;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetUserResponsableLlamadaExperta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetUserResponsableLlamadaExperta), ex.Message);
            }
            return iUserId;
        }

        private int GetUserGeneralLlamadaExperta(int iNivel, int iMatter)
        {
            int iUserId = -1;
            try
            {
                if (iMatter == 1)
                {
                    if (iNivel == 1)
                    {
                        iUserId = objVariables.configConsultoria.mail_n1General_1;
                    }
                    else
                    {
                        iUserId = objVariables.configConsultoria.mail_n2General_1;
                    }
                }
                else
                {
                    if (iNivel == 1)
                    {
                        iUserId = objVariables.configConsultoria.mail_n1General_3;
                    }
                    else
                    {
                        iUserId = objVariables.configConsultoria.mail_n2General_3;
                    }
                }


            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetUserGeneralLlamadaExperta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetUserGeneralLlamadaExperta), ex.Message);

            }
            return iUserId;
        }

        private int CheckUsuarioAusente(int iIdUsuarioApp)
        {
            int iResult = -1;
            try
            {


                iResult = _svcConsultoria.CheckUsuarioAusenteAsync(iIdUsuarioApp).Result;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(CheckUsuarioAusente), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(CheckUsuarioAusente), ex.Message);
            }

            return iResult;

        }

        private BeanList<ConsultoriaSvc.UsuarioCJ> getUsersByMateria(int IntIdMateria, int IntIdCollaborator, int IntIdNivel)
        {
            try
            {
                return _svcConsultoria.RecuperarUsuariosAppByMateriaAsync(IntIdMateria, IntIdCollaborator, IntIdNivel).Result;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(getUsersByMateria), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(getUsersByMateria), ex.Message);
                return null;
            }
        }


        /// <summary>
        /// SendEmailAddQuery
        /// </summary>
        /// <param name="iCodCRM"></param>
        /// <param name="strEmail"></param>
        /// <param name="strName"></param>
        /// <param name="iIdQuery"></param>
        /// <returns></returns>
        public int SendEmailResponse(int iCodCRM, int iIdQuery, string sMessage,int iIdUserApp)
        {
            int intResult = -1;
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}", nameof(SendEmailResponseCopy), iCodCRM.ToString(), iIdQuery.ToString(), sMessage, iIdUserApp.ToString());

                string strBody = String.Empty;

                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, iIdUserApp);

                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta {1}", nameof(SendEmailResponse), iIdQuery.ToString());

                    return -1;
                }

                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.Request, oQuery._idMateria);
                string subject = this.GetAsuntoEmail(TypeEmail.Request, oQuery._idTipoConsulta);

                if (this.IsLlamadaExperta(oQuery._idMateria))
                {
                    subject = subject + " ( codSugar : " + oQuery._idClienteCRM.ToString() + ", codPeticion : #" + iIdQuery.ToString() + ")";
                }
                else
                {
                    subject = subject + " (" + oQuery._idClienteNav.ToString() + ").";
                }

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                string strNameUser = String.Empty; // "Consulta no asignada";

                string strEmailToSend = "";

                ClavesSvc.UsuarioApp user = null;

                if (oQuery._idUsuarioAsignado > 0)
                {
                    user = this.GetUserApp(oQuery._idUsuarioAsignado);
                    if (user != null)
                    {
                        strNameUser = user._nombre;
                        strEmailToSend = user._email;

                    }
                    else strNameUser = "Usuario asignado no encontrado.";
                }


                strBody = strBody.Replace("@@NAME@@", this.myTI.ToTitleCase(strNameUser.ToLower()));


                strBody = strBody.Replace("@@CLIENTE@@", this.myTI.ToTitleCase(oQuery._nombreClienteSuscripcion));

                if (this.IsLlamadaExperta(oQuery._idMateria))
                {
                    strBody = strBody.Replace("@@CODIGOCLIENTE@@", iCodCRM.ToString());
                    strBody = strBody.Replace("@@CODIGOCLIENTETEXTO@@", "CODIGO KOJAK:");
                    strBody = strBody.Replace("@@NOMBRE@@", oQuery._nombreUsuarioPeticion);
                }
                else
                {
                    strBody = strBody.Replace("@@CODIGOCLIENTE@@", oQuery._idClienteNav.ToString());
                    strBody = strBody.Replace("@@CODIGOCLIENTETEXTO@@", "CODIGO NAVISION:");
                    strBody = strBody.Replace("@@NOMBRE@@", oQuery._nombreClienteSuscripcion);
                }



                strBody = strBody.Replace("@@MATERIA@@", oQuery._descMateria);

                strBody = strBody.Replace("@@TEMA@@", oQuery._descSubMateria);
                strBody = strBody.Replace("@@DESCRIPCION@@", oQuery._descConsulta);
                strBody = strBody.Replace("@@EMAIL@@", oQuery._email);
                strBody = strBody.Replace("@@COMENTARIO@@", sMessage);

                strBody = strBody.Replace("@@FOOTER_MATERIA@@", this.GetFooterMatter(oQuery._idMateria, oQuery._descMateria));



               

                
                //else
                //{
                //    user = this.GetUserApp(iIdUserApp);

                //    if (user != null)
                //    {
                //        strEmailToSend = user._email;
                //    }
                //    else
                //    {
                //        objLogger.LogInformation("{0} Error al no existir el usuario {1} ", nameof(SendEmailResponse), iIdUserApp.ToString());
                //        return -1;
                //    }
                //}
              

               

                if (this.IsLlamadaExperta(oQuery._idMateria))
                {

                    if (user != null)
                    {
                        String sUserApp = user._login;
                        String sPassword = user._pwd;

                        string strBodyCopy = strBody;

                        string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                        strBodyCopy = strBodyCopy.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);

                        string[] lstEmails = new string[1];

                        lstEmails[0] = strEmailToSend;

                        intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBodyCopy, subject, oQuery._idMateria, oQuery._indUrgente);

                        objLogger.LogInformation("{0} Respuesta de envio a {1} es {2}", nameof(SendEmailResponse), user._email, intResult.ToString());

                    }

                    if ((user == null) || ((user != null) && (this.CheckUsuarioAusente(user._idUsuarioApp) == 0))) // Si está ausente se envia al responsable
                    {
                        // Enviarlo a Responsable N2

                        int iIdResponsable = this.GetUserResponsableLlamadaExperta(2, oQuery._idMateria);

                        ClavesSvc.UsuarioApp oUserResponsable = this.GetUserApp(iIdResponsable);

                        if (oUserResponsable != null)
                        {
                            String sUserApp = oUserResponsable._login;
                            String sPassword = oUserResponsable._pwd;

                            string strBodyCopy = strBody;

                            string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                            strBodyCopy = strBodyCopy.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);



                            string[] lstEmails = new string[1];

                            lstEmails[0] = oUserResponsable._email;

                            intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBodyCopy, subject, oQuery._idMateria, oQuery._indUrgente);

                            objLogger.LogInformation("{0} Respuesta de envio a Responsable {1} es {2}", nameof(SendEmailResponse), oUserResponsable._email, intResult.ToString());

                        }
                    }



                    int iIdGeneralN2 = this.GetUserGeneralLlamadaExperta(2, oQuery._idMateria);

                    ClavesSvc.UsuarioApp oUserGeneral2 = this.GetUserApp(iIdGeneralN2);

                    if (oUserGeneral2 != null)
                    {
                        String sUserApp = oUserGeneral2._login;
                        String sPassword = oUserGeneral2._pwd;

                        string strBodyCopy = strBody;

                        string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                        strBodyCopy = strBodyCopy.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);



                        string[] lstEmails = new string[1];

                        lstEmails[0] = oUserGeneral2._email;

                        intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBodyCopy, subject, oQuery._idMateria, oQuery._indUrgente);

                    }

                    if (oQuery._idMateria == (int)TypeMatter.ProteccionDatos) // Respuesta del usuario desde Mis Consultas (Solo para Proteccion de Datos)
                    {
                        String EmailPD = objVariables.configConsultoria.mail_ProteccionDatos;

                        ClavesSvc.UsuarioApp oUserPD = new ClavesSvc.UsuarioApp();

                        string[] lstEmails = new string[1];

                        lstEmails[0] = EmailPD;

                        string strBodyCopy = strBody;
                        string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=-1&password=&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                        strBodyCopy = strBodyCopy.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);

                        int iResultPD = this.SendEmailRequest(iCodCRM, lstEmails, strBodyCopy, subject, oQuery._idMateria, oQuery._indUrgente);
                    }


                }
                else
                {
                    if (user == null)
                    {

                        BeanList<ConsultoriaSvc.UsuarioCJ> alistUsers = this.getUsersByMateria(oQuery._idMateria, 0, 0);

                        if ((alistUsers != null) && (alistUsers.beanList != null))
                        {

                            for (int i = 0; i < alistUsers.beanList.Count; i++)
                            {

                                objLogger.LogInformation("{0}  Encontrado {1} usuarios.", nameof(SendEmailResponse), alistUsers.beanList.Count());

                                string strBodyCopy = strBody;

                                user = this.GetUserApp(alistUsers.beanList[i]._idUsuarioApp);



                                String sUserApp = user._login;
                                String sPassword = user._pwd;

                                string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                                strBodyCopy = strBodyCopy.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);



                                string[] lstEmails = new string[1];


                                lstEmails[0] = user._email;

                                intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBodyCopy, subject, oQuery._idMateria, oQuery._indUrgente);


                            }
                        }
                        else
                        {
                            objLogger.LogInformation("{0}  No hay usuarios para la materia {1}", nameof(SendEmailResponse), oQuery._idMateria.ToString());
                        }

                    }
                    else
                    {

                        objLogger.LogInformation("{0}  Encontrado a {1} como usuario asignado.", nameof(SendEmailResponse), user._nombre);

                        String sUserApp = user._login;
                        String sPassword = user._pwd;

                        string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&isCustomer=0&user=" + sUserApp + "&password=" + sPassword + "&codQuestion=" + iIdQuery.ToString(), "123456asdfghjklqwertyuiop1234567");
                        strBody = strBody.Replace("@@LINK@@", objVariables.configConsultoria.UrlAppGestion + "LegalConsulting?data=" + sData);

                        string[] lstEmails = new string[1];

                        lstEmails[0] = user._email;

                        intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, subject, oQuery._idMateria, oQuery._indUrgente);
                    }
                }
                objLogger.LogInformation("{0}  Resultado {1}", nameof(SendEmailResponse), intResult.ToString());
               

            }

            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailResponse), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailResponse), ex.Message);
            }
            return intResult;

        }

        /// <summary>
        /// SendEmailAddQuery
        /// </summary>
        /// <param name="iCodCRM"></param>
        /// <param name="strEmail"></param>
        /// <param name="strName"></param>
        /// <param name="iIdQuery"></param>
        /// <returns></returns>
        public int SendEmailResponseCopy(int iCodCRM, int iIdQuery, string sMessage, int iIdUserApp)
        {
            int intResult = -1;
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(SendEmailResponseCopy), iCodCRM.ToString(), iIdQuery.ToString(), sMessage);




                string strBody = String.Empty;


                ConsultoriaSvc.Consulta oQuery = this.GetConsulta(iIdQuery, objVariables.idUsuarioAreaPersonal);

                string strEmail = String.Empty;
                string strUserName = String.Empty;


                if ((oQuery == null) || (oQuery._idConsulta <= 0))
                {
                    objLogger.LogInformation("{0} Resultado: No existe consulta {1}", nameof(SendEmailResponseCopy), iIdQuery.ToString());

                    return -1;
                }


                //ClavesSvc.UsuarioApp user = this.GetUserApp(iIdUserApp);

              

                if (!String.IsNullOrEmpty(oQuery._email))
                {

                    strEmail = oQuery._email;
                    strUserName = oQuery._nombreUsuarioPeticion;

                }
                else
                {
                    objLogger.LogInformation("{0} Error al no existir email en Consulta {1} ", nameof(SendEmailResponseCopy), iIdQuery.ToString());
                    return -1;
                }



                string pathTemplateData = this.GetMailPattern(oQuery._idTipoConsulta, TypeEmail.RequestCopy, oQuery._idMateria);
                string strSubject = this.GetAsuntoEmail(TypeEmail.RequestCopy, oQuery._idTipoConsulta);



                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                strBody = strBody.Replace("@@NAME@@", this.myTI.ToTitleCase(strUserName.ToLower()));
                strBody = strBody.Replace("@@COMENTARIO@@", sMessage);



                string[] lstEmails = new string[1];

                lstEmails[0] = strEmail;


                intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, strSubject, oQuery._idMateria, oQuery._indUrgente);

                objLogger.LogInformation("{0}  Resultado {1}", nameof(SendEmailResponseCopy), intResult.ToString());

               
            }

            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailResponseCopy), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailResponseCopy), ex.Message);
            }
            return intResult;
        }

        public int StoreFile(String sFileName, string subFolder, byte[] bdata)
        {
            int iResult = -1;
            try
            {
                objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(StoreFile), sFileName, subFolder, bdata.Length);

                sFileName = this.MakeValidFileName(Path.GetFileNameWithoutExtension(sFileName)) + Path.GetExtension(sFileName);

                iResult =  _svcRepository.uploadFileAsync(bdata, sFileName, objVariables.configConsultoria.IdAreaConsultoria, subFolder,false).Result;


                objLogger.LogInformation("{0}  Resultado {1}", nameof(StoreFile), iResult.ToString());
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailResponseCopy), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailResponseCopy), ex.Message);
            }

            return iResult;

        }

        public Boolean SuperadoLimiteDescripcion(int idMatter, string strDescripcion)
        {
            Boolean bSeSupera = false;
            if ((this.IsLlamadaExperta(idMatter)) && (strDescripcion.Length > 1500))
                bSeSupera = true;
            else
                bSeSupera = (strDescripcion.Length > 3000);

            return bSeSupera;

        }
    }
}
