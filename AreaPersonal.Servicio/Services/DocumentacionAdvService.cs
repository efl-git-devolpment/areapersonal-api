﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Comun.Configuration;
using DllWcfUtility;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Globalization;

namespace AreaPersonal.Servicio
{
    public enum TypeStatusDoc
    {
        open = 1,
        progress = 2,
        send_colab_redaccion = 3,
        send_colab_ee = 4,

        send_pending_review = 5,
        send_pending_info = 6,

        closed_res_sac = 7,
        closed_res_ee = 8,
        closed_res_redaccion = 9,
        closed_without_resolved = 10,

        stand_by = 11,
        send_CENDOJ = 12,

        send_pending_Cendoj = 13,
        send_colaborador = 14,
        send_colaborador_ok = 15,
        send_colab_redaccion_ok = 16,
        send_colab_ee_ok = 17

    }

    public enum TypeEmailDoc
    {
        NewDocumentClient = 1,
        RequestResolved = 2,
        NonPayment = 3,
        NonPaymentClient = 4,
        RequestCliente = 5,
        RequestNewCommentClient = 6,
        ReSendEmail,
        NewDocument = 8,
        RequestNewComment = 9,

        //RequestSAC = ,
        //RequestTranslate = 7,
        //RequestCopy = 8,
        //PendingStandBy = 9,
        //ReSendEmail = 10,
        //ReceivedClaveDemo = 11
    }

    public enum TypeDocumento
    {
        ConveniosColectivos = 1,
        DoctrinaAdministrativa = 2,
        Formularios = 3,
        Jurisprudencia = 4,
        Legislación = 5,
        Otra = 6
    }

    public class DocumentacionAdvService : BaseService, IDocumentacionAdvService
    {

        private readonly DocumentacionSvc.IServiceGestionDocumentacion _svcDocumentacion;
        //private readonly ClavesSvc.IServiceClaves _svcClaves;
            private readonly EmailsSvc.IServicioMailAutomaticoProducto _svcEmails;
            //private readonly ILogger<DocumentacionAdvService> _logger;
            //private readonly ObjVariables _objVariables;

            private readonly TextInfo myTI = new CultureInfo("es-ES", false).TextInfo;

            public DocumentacionAdvService(
                    IOptions<OptConfig> configWeb,
                    ILogger<DocumentacionAdvService> logger) : base(configWeb, logger)
            {
                //_logger = logger;


                var basicHttpBindingDocumentacion = new BasicHttpBinding(BasicHttpSecurityMode.None)
                {
                    MaxReceivedMessageSize = 2147483647,
                    MaxBufferSize = 2147483647
                };


                var endpointAddressDocumentacion = new EndpointAddress(MyConfig.Services.DocumentacionSvc);
                _svcDocumentacion = new DocumentacionSvc.ServiceGestionDocumentacionClient(basicHttpBindingDocumentacion, endpointAddressDocumentacion);

                //var basicHttpBindingClaves = new BasicHttpBinding();
                //var endpointAddressClaves = new EndpointAddress(MyConfig.Services.ClavesSvc);
                //_svcClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);

                var basicHttpBindingEmails = new BasicHttpBinding();
                var endpointAddressEmails = new EndpointAddress(MyConfig.Services.EmailsSvc);
                _svcEmails = new EmailsSvc.ServicioMailAutomaticoProductoClient(basicHttpBindingEmails, endpointAddressEmails);

                //_objVariables = MyConfig.Variables;

            }


        /// <summary>
        /// AddDocumento new documento to database
        /// </summary>
        /// <returns></returns>
        public int AddDocumento(DocumentacionSvc.Documento oDocumento)
        {
            int iResult = -1;

            _logger.LogInformation("START --> {0} con parámetros {1}, {2]", nameof(AddDocumento), oDocumento._idDocumento.ToString());

            try
            {


                iResult = _svcDocumentacion.InsertarDocumentoAsync(oDocumento._peticion, oDocumento._idTipoDocumento, oDocumento._idClienteNav,
                    oDocumento._idUsuarioPro, oDocumento._nombre, oDocumento._apellidos, oDocumento._email, oDocumento._tlfno, oDocumento._idDocumentoEstado,
                    oDocumento._observaciones, _objVariables.idUsuarioAreaPersonal).Result;

                _logger.LogInformation("Resultado Insertar Documento en BBDD --> {0} {1} ", nameof(AddDocumento), iResult.ToString());

                return iResult;
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(AddDocumento), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(AddDocumento), ex.Message);
                return -1;
            }

        }

        public int AssingUser(int iIdDocumento, int iUserAssigned)
        {
            try
            {
                _logger.LogInformation("START --> {0} con parámetros {1}, {2], {3}", nameof(AssingUser), iIdDocumento.ToString(), iUserAssigned.ToString());


                var iResult = _svcDocumentacion.AsignarDocumentoAsync(iIdDocumento, iUserAssigned, _objVariables.idUsuarioAreaPersonal);

                _logger.LogInformation("Resultado Asinar Documento  --> {0} {1} ", nameof(AssingUser), iResult.Result.ToString());

                return iResult.Result;
            }

            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Add new document to database
        /// </summary>
        /// <returns></returns>
        public int SendEmailAssing(int ipIdCodCRM, DocumentacionSvc.Documento objDocument, int iIdDocumento,ClavesSvc.UsuarioApp oUser, String strTitular, int iIdCodNav)
        {
            try
            {

                _logger.LogInformation("START --> {0} con parámetros {1}, {2], {3},{4}", nameof(SendEmailAssing), ipIdCodCRM, objDocument._idDocumento.ToString(), iIdCodNav.ToString());

                int iResult = -1;

                string pathTemplateData = this.GetMailPattern(TypeEmailDoc.NewDocument);
                string subject = this.GetAsuntoEmail(TypeEmailDoc.NewDocument);

                if (ipIdCodCRM > 0)
                {
                    ClavesSvc.Cliente oCliente = this.getClienteFromCRM(ipIdCodCRM, objDocument._idClienteNav);

                    if (oCliente != null)
                    {

                        subject = subject + " (Cod Kojak : " + ipIdCodCRM.ToString() + ", Titular Cuenta : " + oCliente._razonSocial + ")";
                    }
                }

                string strBody = String.Empty;

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }

                strBody = strBody.Replace("@@CONSULTOR@@", this.myTI.ToTitleCase(oUser._nombre.ToLower()));
                strBody = strBody.Replace("@@SUGAR@@", ipIdCodCRM.ToString());
                strBody = strBody.Replace("@@TIPO@@", objDocument._descTipoDocumento);
                strBody = strBody.Replace("@@NOMBRE@@", objDocument._nombre);
                strBody = strBody.Replace("@@DESCRIPCION@@", objDocument._peticion);
                strBody = strBody.Replace("@@TELEFONO@@", objDocument._tlfno);
                strBody = strBody.Replace("@@EMAIL@@", objDocument._email);
                strBody = strBody.Replace("@@TITULAR@@", strTitular);
                strBody = strBody.Replace("@@NAVISION@@", iIdCodNav.ToString());

                String sUserApp = oUser._login;
                String sPassword = oUser._pwd;

                string sData = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + ipIdCodCRM.ToString() + "&sparam=0&user=" + sUserApp + "&password=" + sPassword + "&codDocumento=" + iIdDocumento.ToString(), "123456asdfghjklqwertyuiop1234567");

                strBody = strBody.Replace("@@URL@@", _objVariables.configDocumentacion.UrlAppGestion + "GestionDocumentacion?data=" + sData);


                if (oUser != null)
                {
                    string[] lstEmails = new string[1];

                    lstEmails[0] = oUser._email;

                    iResult = this.SendEmailRequest(ipIdCodCRM, lstEmails, strBody, subject);

                }

                _logger.LogInformation("{0} Resultado: {1}", nameof(SendEmailNonPayment), iResult.ToString());

                return iResult;

            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(SendEmailAssing), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(SendEmailAssing), ex.Message);
                return -1;
            }

        }


        public int SendEmailAddDocument(int iCodCRM, string strEmail, string strName, int iIdDocument)
        {
            try
            {
                _logger.LogInformation("START --> {0} con parámetros {1}, {2], {3}, {4}", nameof(SendEmailAddDocument), iCodCRM.ToString(), strEmail, strName, iIdDocument.ToString());

                string strBody = String.Empty;

                DocumentacionSvc.Documento oDocument = this.GetDocumento(iIdDocument, _objVariables.idUsuarioAreaPersonal);

                if ((oDocument == null) || (oDocument._idDocumento <= 0))
                {
                    _logger.LogInformation("{0} Resultado: No existe documento {1}", nameof(SendEmailNonPayment), iIdDocument.ToString());
                    return -1;
                }

                string pathTemplateData = this.GetMailPattern(TypeEmailDoc.NewDocumentClient);
                string subject = this.GetAsuntoEmail(TypeEmailDoc.NewDocumentClient);

                if ((oDocument != null) && (oDocument._idDocumento > 0))
                {

                    using (StreamReader lector = new StreamReader(pathTemplateData))
                    {
                        while (lector.Peek() > -1)
                        {
                            string linea = lector.ReadLine();
                            if (!String.IsNullOrEmpty(linea))
                            {
                                strBody = strBody + linea;
                            }
                        }
                    }

                    string sData = CommonUI.Models.Login.LoginClaves.Encrypt("icodCRM=" + iCodCRM.ToString() + "&icodDocumento=" + iIdDocument.ToString() + "&bcustomer=1&iColab=-1&iIdUser=" + _objVariables.idUsuarioAreaPersonal.ToString(), "123456asdfghjklqwertyuiop1234567");

                    strBody = strBody.Replace("@@NAME@@", this.myTI.ToTitleCase(strName.ToLower()));
                    strBody = strBody.Replace("@@LINK@@", _objVariables.configDocumentacion.UrlAppDocumentacion + "Documentacion/ShowDocumento?data=" + sData);

                    string[] lstEmails = new string[1];

                    lstEmails[0] = strEmail;

                    _logger.LogInformation("{0} enviado email a {1} ", nameof(SendEmailAddDocument), lstEmails[0]);
                    int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, subject);

                    _logger.LogInformation("{0} con Resultado {1} ", nameof(SendEmailAddDocument), intResult.ToString());

                    return intResult;


                }
                else
                {
                    _logger.LogInformation("{0} Resultado: No existe documento {1}", nameof(SendEmailNonPayment), iIdDocument.ToString());
                    return -1;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                return -1;
            }
        }

        public Boolean Check_NonPayment(int iIdDocumento, int ipIdCodCRM, int iCodNav)
        {
            try
            {

                Bean<DocumentacionSvc.Estado> status = _svcDocumentacion.RecuperarEstadoDocumentoAsync(iIdDocumento).Result;

                int iResult = -1;

                if (status.bean._idDocumentoEstado == (int)TypeStatusDoc.send_pending_review)
                {
                    iResult = this.SendEmailNonPayment(ipIdCodCRM, iCodNav, iIdDocumento);
                }
                return (iResult >= 0);
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                return false;
            }
        }

        private int SendEmailNonPayment(int iCodCRM, int iCodNavision, int iIdDocumento)
        {
            try
            {

                _logger.LogInformation("START --> {0} con parámetros {1}, {2], {3}", nameof(SendEmailNonPayment), iCodCRM.ToString(), iCodNavision.ToString(), iIdDocumento.ToString());


                string strBody = String.Empty;

                DocumentacionSvc.Documento objDocumento = this.GetDocumento(iIdDocumento, _objVariables.idUsuarioAreaPersonal);

                if ((objDocumento == null) || (objDocumento._idDocumento <= 0))
                {
                    _logger.LogInformation("{0} Resultado: No existe documento {1}", nameof(SendEmailNonPayment), iIdDocumento.ToString());
                    return -1;
                }

                string pathTemplateData = this.GetMailPattern(TypeEmailDoc.NonPayment);
                string strSubject = this.GetAsuntoEmail(TypeEmailDoc.NonPayment);

                strSubject = strSubject.Replace("@@NAVISION@@", iCodNavision.ToString());

                using (StreamReader lector = new StreamReader(pathTemplateData))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            strBody = strBody + linea;
                        }
                    }
                }


                string sDataAllow = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&codDocumento=" + iIdDocumento.ToString() + "&status=" + Convert.ToString((int)TypeStatus.open), "123456asdfghjklqwertyuiop1234567");
                strBody = strBody.Replace("@@URL_ALLOW@@", _objVariables.configDocumentacion.UrlAppDocumentacion + "Documentacion/UpdateStateQuery?sdata=" + sDataAllow);

                string sDataRefuse = CommonUI.Models.Login.LoginClaves.Encrypt("codCRM=" + iCodCRM.ToString() + "&codDocumento=" + iIdDocumento.ToString(), "123456asdfghjklqwertyuiop1234567");
                strBody = strBody.Replace("@@URL_REFUSE@@", _objVariables.configDocumentacion.UrlAppDocumentacion + "Documentacion/RefuseQuery?sdata=" + sDataRefuse);

                //strBody = strBody.Replace("@@LINK@@", this.GetUrlApplicationGestion() + "LegalConsulting/ShowQuestion?codCRM=" + iCodCRM.ToString() + "&codQuestion=" + iIdQuery.ToString());

                strBody = strBody.Replace("@@CODNAVISION@@", iCodNavision.ToString());

                string lstEmailsCobros = _objVariables.mail_cobros;

                string[] lstEmails = this.GetDistinctsEmails(lstEmailsCobros.Split(';'));


                _logger.LogInformation("{0} enviado email a {1} ", nameof(SendEmailNonPayment), lstEmails[0]);
                int intResult = this.SendEmailRequest(iCodCRM, lstEmails, strBody, strSubject);

                _logger.LogInformation("{0} con Resultado {1} ", nameof(SendEmailNonPayment), intResult.ToString());

                return intResult;


            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(AssingUser), ex.Message);
                return -1;
            }

        }


        private String GetMailPattern(TypeEmailDoc ttype)
        {
            String LocalPath = _objVariables.path_patters + "\\Documentacion";
            String ExtendedPath = String.Empty;

            switch (ttype)
            {
                case TypeEmailDoc.NewDocumentClient: ExtendedPath = "1_plantilla_NuevaPeticionCliente.html"; break;
                case TypeEmailDoc.NonPayment: ExtendedPath = "4_1_plantilla_envio_impago.html"; break;
                case TypeEmailDoc.NonPaymentClient: ExtendedPath = "4_2_plantilla_envio_impagoCliente.html"; break;
                case TypeEmailDoc.RequestCliente: ExtendedPath = "5_Plantilla_Reenvio_Cliente.html"; break;
                case TypeEmailDoc.RequestNewCommentClient: ExtendedPath = "6_Plantilla_NuevoCommentCliente.html"; break;
                case TypeEmailDoc.ReSendEmail: ExtendedPath = "7_plantilla_reenvio_Cliente.html"; break;
                case TypeEmailDoc.NewDocument: ExtendedPath = "8_plantilla_NuevaPeticion.html"; break;
                case TypeEmailDoc.RequestNewComment: ExtendedPath = "9_plantilla_NuevoComment.html"; break;
                default: break;
            }

            return LocalPath + '\\' + ExtendedPath;
        }

        private String GetAsuntoEmail(TypeEmailDoc ttype)
        {
            String strSubject = "sin asunto";

            switch (ttype)
            {
                case TypeEmailDoc.NewDocumentClient: strSubject = "Tu petición ha sido recibida"; break;

                case TypeEmailDoc.NonPayment: strSubject = "Solicitud de cobro por morosidad. Codigo Navision : @@NAVISION@@"; break;
                case TypeEmailDoc.NonPaymentClient: strSubject = "Resolución de impagos."; break;
                case TypeEmailDoc.RequestResolved: strSubject = "Tu petición ha sido resuelta"; break;
                case TypeEmailDoc.RequestCliente: strSubject = "Tu información ha sido enviada"; break;
                case TypeEmailDoc.RequestNewCommentClient: strSubject = "Tienes una información en relación con tu petición"; break;
                case TypeEmailDoc.ReSendEmail: strSubject = "Reenvio de información"; break;
                case TypeEmailDoc.NewDocument: strSubject = "Petición Servicio de Documentación"; break;
                case TypeEmailDoc.RequestNewComment: strSubject = "Comentario del cliente"; break;
                default: break;
            }

            return strSubject;
        }

        /// <summary>
        /// SendEmailRequest
        /// </summary>
        /// <param name="iCodCRM"></param>
        /// <param name="strEmails"></param>
        /// <param name="strPlantillaHTML"></param>
        /// <param name="strAsunto"></param>
        /// <returns></returns>
        private int SendEmailRequest(int iCodCRM, string[] strEmails, string strPlantillaHTML, string strAsunto)
        {
            try
            {

                _logger.LogInformation("START --> {0} con parámetros {1}, {2], {3}", nameof(SendEmailRequest), iCodCRM.ToString(), strEmails.Length.ToString(), strAsunto);


                int intIdGrupo = _objVariables.configDocumentacion.iGroupMail;
                string strRemitente = "Servicio Documentación";
                int intPrioridad = 1;
                int intIdUsuario = _objVariables.idUsuarioAreaPersonal;

             
                if (strEmails.Length > 1)
                {
                    strEmails = this.GetDistinctsEmails(strEmails);
                }

                strPlantillaHTML = this.EncodeHTML(strPlantillaHTML);

                string strEmailLine = String.Empty;

                foreach (String email in strEmails)
                {
                    strEmailLine = strEmailLine + ',' + email;
                }

                //log.Info("SendEmailRequest.CallPeticionEnvioUnicoMails  : @IntIdGrupo: " + intIdGrupo.ToString() + ", @StrRemitente: " + strRemitente + ", @StrAsunto: " + strAsunto +
                //    ", @IntPrioridad: " + intPrioridad.ToString() + ", @IntIdUsuario: " + intIdUsuario.ToString() + ", @Emails : " + strEmailLine);



                var objresult = _svcEmails.PeticionEnvioUnicoMailsAsync(intIdGrupo, strEmails, strPlantillaHTML, strRemitente, strAsunto, intPrioridad, intIdUsuario).Result;
                //PeticionEnvioUnicoMailsResponse request = Emails..PeticionEnvioUnicoMails(newParameters);

                int iResult = objresult;

                _logger.LogInformation("Resultado Envio Email --> {0} {1} ", nameof(SendEmailRequest), iResult.ToString());


                return iResult;

            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(SendEmailRequest), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(SendEmailRequest), ex.Message);
                return -1;
            }
        }

      
        private DocumentacionSvc.Documento GetDocumento(int iIdDocument, int iUserId)
        {
            DocumentacionSvc.Documento oResult = new DocumentacionSvc.Documento();

            try
            {

                int iUserOwner = (iUserId > 0) ? iUserId : _objVariables.idUsuarioAreaPersonal;

                BeanList<DocumentacionSvc.Documento> oList = _svcDocumentacion.RecuperarDocumentoAsync(iIdDocument, -1, -1, -1, String.Empty, String.Empty, String.Empty, -1, 1, 10000, String.Empty, String.Empty, iUserOwner).Result;

                if ((oList != null) && (oList.beanList != null) && (oList.beanList.Count > 0))
                {
                    oResult = oList.beanList.First();
                }
                //else return null;

                return oResult;
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(GetDocumento), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(GetDocumento), ex.Message);
                return null;
            }
        }
    }
}
