﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using AreaPersonal.Vistas.External.Claves;
using AreaPersonal.Vistas.External.Crm;
using AutoMapper.Configuration.Conventions;
using DllApiBase.Servicio;
using DllApiBase.Servicio.PeticionesHttp;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves.Usuario;
using DllApiBase.Vistas.Cliente;
using DllApiBase.Vistas.Permisos;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Navision.Vistas.Cliente;
using Navision.Vistas.Facturas;
using Navision.Vistas.Productos;
using Online.Servicio.Services;
using System;
using System.Collections.Generic;

namespace AreaPersonal.Servicio
{
    public class ExternalService : ServicioApiBase<ExternalService>, IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda>
    {
        private OnlineProductoBaseService<ExternalService> objServicioOnline;
        private readonly PeticionesHttpApi<ExternalService> objPeticionesHttpApi;

        internal ConfigApi objConfig { get; private set; }
        internal static ILogger<ExternalService> objLogger { get; private set; }

        public ExternalService(
            IOptions<ConfigApi> objConfigBase,
            ILogger<ExternalService> objLoggerBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            objServicioOnline = new Online.Servicio.Services.OnlineProductoBaseService<ExternalService>(objConfig.cfgUrlProducto, objLoggerBase);
            objPeticionesHttpApi = new PeticionesHttpApi<ExternalService>(objLoggerBase);


        }


        #region Online
        public ClientePro RecuperarClienteOnline(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarClienteOnline), IdClienteNav.ToString());

            DllApiBase.Vistas.Cliente.ClientePro objResultado = new DllApiBase.Vistas.Cliente.ClientePro();

            try
            {
                var objCliente = (ResultadoOperacion<DllApiBase.Vistas.Cliente.ClientePro>)objServicioOnline.RecuperarCliente(IdClienteNav);

                if ((objCliente != null) && (objCliente.eResultado == enumResultado.Ok))
                {

                    objResultado = objCliente.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarClienteOnline), "Recuperado Cliente Id " + IdClienteNav.ToString());

                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarClienteNavision), "No se ha recuperado ningún clienteNav");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarClienteOnline), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarClienteOnline), ex.Message);
            }

            return objResultado;
        }

        public UsuarioPro RecuperarUsuarioPro(string sIdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarUsuarioPro), sIdUsuarioPro);
            UsuarioPro objResultado = new UsuarioPro();

            try
            {
                var objUsuario = (ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>)objServicioOnline.BuscarUsuario(sIdUsuarioPro);

                if ((objUsuario != null) && (objUsuario.eResultado == enumResultado.Ok_Recuperar))
                {
                    objResultado = objUsuario.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarUsuarioPro), "Recuperado UsuarioPro " + sIdUsuarioPro);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarUsuarioPro), "No se ha recuperado ningún UsuarioPro");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarUsuarioPro), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarUsuarioPro), ex.Message);
            }

            return objResultado;
        }
        public ResultadoBase RecuperarSuscripcionesClienteNav(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarSuscripcionesClienteNav), IdClienteNav);

            ResultadoLista<Online.Vistas.Suscripciones.Suscripcion> objResultadoApi = new ResultadoLista<Online.Vistas.Suscripciones.Suscripcion>();

            try
            {
                string urlBaseApi = this.objConfig.cfgUrlsApi.OnlineApi;
                string strPrams = $"IdClienteNav={IdClienteNav}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoLista<Online.Vistas.Suscripciones.Suscripcion>>(urlBaseApi, "/Suscripciones//RecuperarSuscripcionesPorCliente", string.Empty, lstQueryParams);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarSuscripcionesClienteNav), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarSuscripcionesClienteNav), ex.Message);

                objResultadoApi = null;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarSuscripcionesClienteNav), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }
        public ResultadoBase RecuperarSuscripcionesUsuarioPro(string IdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarUsuarioProducto), IdUsuarioPro);

            ResultadoLista<Online.Vistas.Suscripciones.Suscripcion> objResultadoApi = new ResultadoLista<Online.Vistas.Suscripciones.Suscripcion>();

            try
            {
                string urlBaseApi = this.objConfig.cfgUrlsApi.OnlineApi;
                string strPrams = $"IdUsuarioPro={IdUsuarioPro}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoLista<Online.Vistas.Suscripciones.Suscripcion>>(urlBaseApi, "/Suscripciones/RecuperarSuscripcionesPorEntrada", string.Empty, lstQueryParams);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarSuscripcionesUsuarioPro), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarSuscripcionesUsuarioPro), ex.Message);

                objResultadoApi = null;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarSuscripcionesUsuarioPro), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }
        public List<DllApiBase.Vistas.Permisos.PermisosAsociado> RecuperarPermisosUsuarioPro(string sIdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarPermisosUsuarioPro), sIdUsuarioPro);

            List<DllApiBase.Vistas.Permisos.PermisosAsociado> objResultado = new List<DllApiBase.Vistas.Permisos.PermisosAsociado>();

            try
            {
                var objResultadoOnline = (ResultadoLista<DllApiBase.Vistas.Permisos.PermisosAsociado>)objServicioOnline.RecuperarPermisosUsuario(sIdUsuarioPro);

                if ((objResultadoOnline != null) && (objResultadoOnline.eResultado == enumResultado.Ok))
                {
                    objResultado = objResultadoOnline.lstResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarPermisosUsuarioPro), "Recuperado permisos para " + sIdUsuarioPro);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarPermisosUsuarioPro), "No se ha recuperado ningún permiso");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarPermisosUsuarioPro), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarPermisosUsuarioPro), ex.Message);
            }

            return objResultado;
        }
        #endregion

        #region Navision
        public Navision.Vistas.Cliente.ClienteNavision RecuperarClienteNavision(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarClienteNavision), IdClienteNav.ToString());
            Navision.Vistas.Cliente.ClienteNavision objResultado = new Navision.Vistas.Cliente.ClienteNavision();

            try
            {
                string urlNavisionApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"IdClienteNav={IdClienteNav}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<Navision.Vistas.Cliente.ClienteNavision>>(urlNavisionApi, "/Clientes/RecuperarClienteNav", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.oResultado != null))
                {
                    objResultado = objResultadoNavision.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarClienteNavision), "Recuperado Cliente Id " + IdClienteNav.ToString());
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarClienteNavision), "No se ha recuperado ningún clienteNav");
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(RecuperarClienteNavision), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarClienteNavision), ex.Message);
                objResultado = null;
            }

            return objResultado;

        }

        public ResultadoBase RecuperarFacturas(int IdClienteNav, string strCIF, int IdContactoCrm, string strFechaDesde, string strFechaHasta, int iPagNum, int iPagSize)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4,},{5},{6},{7}", nameof(RecuperarFacturas), IdClienteNav.ToString(), strCIF, IdContactoCrm.ToString(), strFechaDesde, strFechaHasta, iPagNum.ToString(), iPagSize.ToString());

            ResultadoLista<Navision.Vistas.Facturas.Factura> objResultado = new();

            try
            {
                string urlNavisionApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"IdClienteNav={IdClienteNav}&iPagNum={iPagNum}&iPagSize={iPagSize}&strCif={strCIF}&IdContactoCrm={IdContactoCrm}&strFechadesde={strFechaDesde}&strFechaHasta={strFechaHasta}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoLista<Navision.Vistas.Facturas.Factura>>(urlNavisionApi, "/Facturas/RecuperarFacturas", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.lstResultado != null))
                {
                    objResultado = objResultadoNavision;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarFacturas), "Recuperado " + objResultado.lstResultado.Count.ToString() + " factura(s)");
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarFacturas), "No se ha recuperado ninguna factura");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarFacturas), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarFacturas), ex.Message);
            }

            return objResultado;
        }
        public ResultadoBase RecuperarAbonos(int IdClienteNav, string strCIF, int IdContactoCrm, string strFechaDesde, string strFechaHasta, int iPagNum, int iPagSize)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4,},{5},{6},{7}", nameof(RecuperarAbonos), IdClienteNav.ToString(), strCIF, IdContactoCrm.ToString(), strFechaDesde, strFechaHasta, iPagNum.ToString(), iPagSize.ToString());

            ResultadoLista<Navision.Vistas.Facturas.Abono> objResultado = new();

            try
            {
                string urlNavisionApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"IdClienteNav={IdClienteNav}&iPagNum={iPagNum}&iPagSize={iPagSize}&strCif={strCIF}&IdContactoCrm={IdContactoCrm}&strFechadesde={strFechaDesde}&strFechaHasta={strFechaHasta}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoLista<Navision.Vistas.Facturas.Abono>>(urlNavisionApi, "/Facturas/RecuperarAbonos", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.lstResultado != null))
                {
                    objResultado = objResultadoNavision;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarAbonos), "Recuperado " + objResultado.lstResultado.ToString() + " abono(s)");
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarAbonos), "No se ha recuperado ninguna abono");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarAbonos), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarAbonos), ex.Message);
            }

            return objResultado;
        }
        public Navision.Vistas.Facturas.FacturaPDF RecuperarFacturaPdf(string sIdFactura, string strCif)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarFacturaPdf), sIdFactura, strCif);

            FacturaPDF objResultado = new FacturaPDF();

            try
            {
                string urlOnlineApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"strCIF={strCif}&strIdFactura={sIdFactura}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<FacturaPDF>>(urlOnlineApi, "/Facturas/RecuperarFacturaPDF", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.oResultado != null))
                {
                    objResultado = objResultadoNavision.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarFacturaPdf), "Recuperado factura en PDF: " + objResultado.strFacturaFileContent);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarFacturaPdf), "No se ha recuperado ninguna factura en PDF");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarFacturaPdf), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarFacturaPdf), ex.Message);
            }

            return objResultado;
        }
        public Navision.Vistas.Facturas.AbonoPDF RecuperarAbonoPdf(string sIdAbono, string strCif)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarAbonoPdf), sIdAbono, strCif);

            AbonoPDF objResultado = new AbonoPDF();

            try
            {
                string urlNavisionApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"strCIF={strCif}&strIdAbono={sIdAbono}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<AbonoPDF>>(urlNavisionApi, "/Facturas/RecuperarAbonoPDF", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.oResultado != null))
                {
                    objResultado = objResultadoNavision.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarAbonoPdf), "Recuperado abono en PDF: " + objResultado.strAbonoFileContent);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarAbonoPdf), "No se ha recuperado ningun abono en PDF");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarAbonoPdf), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarAbonoPdf), ex.Message);
            }

            return objResultado;
        }
        /// <summary>
        /// Recupera los productos en Navision por ClienteNav
        /// </summary>
        /// <param name="IdClienteNav"></param>
        /// <returns></returns>
        public List<Navision.Vistas.Productos.ProductoCliente> RecuperarProductosCliente(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarProductosCliente), IdClienteNav.ToString());

            List<ProductoCliente> objResultado = new List<ProductoCliente>();

            try
            {
                string urlNavisionApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strParams = $"IdClienteNav={IdClienteNav}";

                string[] lstQueryParams = new string[] { (string)strParams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoLista<ProductoCliente>>(urlNavisionApi, "/Productos/RecuperarProductosCliente", string.Empty, lstQueryParams);

                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.lstResultado != null))
                {
                    
                    objResultado = objResultadoNavision.lstResultado;

                    //foreach(ProductoCliente objItem in objResultadoNavision.lstResultado)
                    //{
                    //    if (!objItem.bRenovable) objItem.strFechaFIN = string.Empty;
                    //}
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarProductosCliente), "Recuperado productos de Navision: " + objResultadoNavision.sDescripcion);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarProductosCliente), "No se ha recuperado ningun producto de Navision");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error -->{0} Mensaje-->{1}", nameof(RecuperarProductosCliente), ex.Message);
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarProductosCliente), ex.Message);
            }

            return objResultado;
        }

        /// <summary>
        /// Método que recupera de Api Navision un modelo 347 por Cif y Año
        /// </summary>
        /// <param name="strCif">Cif del ClienteNav</param>
        /// <param name="intAnyo">Año del modelo 347</param>
        /// <returns></returns>
        public Navision.Vistas.Cliente.Modelo347PDF RecuperarModelo347Pdf(string strCif, int intAnyo)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarModelo347Pdf), strCif, intAnyo);

            Modelo347PDF objResultado = new Modelo347PDF();

            try
            {
                string urlBaseApi = this.objConfig.cfgUrlsApi.NavisionApi;
                string strPrams = $"strCif={strCif}&intAnyo={intAnyo}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                var objResultadoNavision = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<Modelo347PDF>>(urlBaseApi, "/Clientes/RecuperarModelo347", string.Empty, lstQueryParams);


                if ((objResultadoNavision != null) && (objResultadoNavision.eResultado == enumResultado.Ok) && (objResultadoNavision.oResultado != null))
                {
                    objResultado = objResultadoNavision.oResultado;
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarModelo347Pdf), "Recuperado modelo 347 en PDF: " + objResultado.strModelo347FileName);
                }
                else
                {
                    objLogger.LogDebug("END --> Resultado {0} --> {1}", nameof(RecuperarModelo347Pdf), "No se ha recuperado ningun modelo 347 en PDF");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarModelo347Pdf), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarModelo347Pdf), ex.Message);
                objResultado = null;
            }
            return objResultado;
        }

        public ResultadoBase RecuperarIndicadorFacturasNuevas(int IdClienteNav, string strCif, DateTime FechaDesde)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}, {3}", nameof(RecuperarIndicadorFacturasNuevas), IdClienteNav, strCif, FechaDesde);
            var objResultadoApi = new ResultadoBooleano();
            try
            {
                var strParams = $"IdClienteNav={IdClienteNav}&strCIF={strCif}&fechaDesde={FechaDesde:yyyy/MM/dd HH:mm:ss}";
                var lstQueryParams = new string[] { strParams };
                objResultadoApi = objPeticionesHttpApi.GetApi<ResultadoBooleano>(objConfig.cfgUrlsApi.NavisionApi,
                    "/Facturas/ComprobarFacturasNuevas", string.Empty, lstQueryParams);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarIndicadorFacturasNuevas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarIndicadorFacturasNuevas), ex.Message);
                objResultadoApi.sDescripcion = ex.Message;
                objResultadoApi.eResultado = enumResultado.Error_Excepcion;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarIndicadorFacturasNuevas), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        #endregion

        #region Crm
        /// <summary>
        /// Recuperar informacion de la cuenta desde la parte de Online
        /// </summary>
        /// <param name="IdClienteNav"></param>
        /// <param name="IdClienteCrm"></param>
        /// <returns></returns>
        public InfoCuenta RecuperarInformacionCuenta(int IdClienteNav, int IdClienteCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros IdClienteNav:  {1},  IdClienteCrm: {2}", nameof(RecuperarInformacionCuenta), IdClienteNav.ToString(), IdClienteCrm.ToString());

            InfoCuenta objResultado = new InfoCuenta();
            string strDescripcion = String.Empty;
            try
            {

                string urlBaseApi = this.objConfig.cfgUrlsApi.CrmApi;
                string strParams = "IdClienteNav=" + IdClienteNav.ToString() + "&IdClienteCrm=" + IdClienteCrm.ToString();

                ResultadoOperacion<InfoCuenta> objResultadoApi = new ResultadoOperacion<InfoCuenta>();

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<InfoCuenta>>(urlBaseApi, "/Cuentas/RecuperarInformacionCuenta", null, strParams);

                if (((objResultadoApi != null) && (objResultadoApi.eResultado == enumResultado.Ok_Recuperar) && (objResultadoApi.oResultado != null)))
                {
                    objResultado = objResultadoApi.oResultado;
                    strDescripcion = "Recuperado InfoCuenta con IdClienteNav " + objResultado.cod_navision;
                }
                else
                {
                    objResultado = null;
                    strDescripcion = "No se ha recuperado la información de la cuenta.";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInformacionCuenta), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInformacionCuenta), ex);

                strDescripcion = ex.Message;

            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInformacionCuenta), strDescripcion);
            return objResultado;
        }

        #endregion

        #region Claves

        /// <summary>
        /// Recupera las facturas ya pagadas por cliente de Claves
        /// </summary>
        /// <param name="idClienteNav">Id del cliente</param>
        /// <returns></returns>
        public List<string> RecuperarFacturasPagadas(int idClienteNav)
        {
            var objRespuesta = objPeticionesHttpApi.GetApi<ResultadoLista<string>>(objConfig.cfgUrlsApi.ClavesApi, "/Facturas/RecuperarFacturasPagadas",
                null, $"IdClienteNav={idClienteNav}");
            if (objRespuesta.eResultado != enumResultado.Ok_Recuperar)
            {
                throw new Exception($"La petición a /Facturas/RecuperarFacturasPagadas de Claves con el IdClienteNav = {idClienteNav}, no ha sido correcta. " +
                    $"Mensaje --> {objRespuesta.sDescripcion}");
            }
            return objRespuesta.lstResultado;
        }
        public List<ClienteBusqueda> RecuperarClientesBusqueda(int IdClienteNav, int IdClienteCrm, short iIndTipoBusqueda, int iIdTipoCliente, int iPaginaInicial, int iNumeroDeFilas, int iIdUsuarioApp)
        {
            objLogger.LogDebug($"START --> {nameof(RecuperarClientesBusqueda)} con parámetros {IdClienteNav},{IdClienteCrm},{iIndTipoBusqueda},{iIdTipoCliente},{iPaginaInicial},{iNumeroDeFilas},{iIdUsuarioApp}");

            List<ClienteBusqueda> objResultado = new List<ClienteBusqueda>();
            string strDescripcion = String.Empty;

            try
            {

                string urlBaseApi = this.objConfig.cfgUrlsApi.ClavesApi;

                RecuperarClientesPeticion objPeticion = new RecuperarClientesPeticion();
                objPeticion.IdClienteNav = IdClienteNav;
                objPeticion.IdClienteCrm = IdClienteCrm;
                objPeticion.iIndTipoBusqueda = iIndTipoBusqueda;

                objPeticion.IdUsuarioPro = string.Empty;
                objPeticion.strNombreCompleto = string.Empty;
                objPeticion.strRazonSocial = string.Empty;
                objPeticion.strTelefono = string.Empty;
                objPeticion.strCifNif = string.Empty;
                objPeticion.strEmail = string.Empty;
                objPeticion.strLogin = string.Empty;
                objPeticion.strIp = string.Empty;
                objPeticion.strCampoOrdenar = string.Empty;

                objPeticion.bIndOrdenarAsc = true;

                objPeticion.iPaginaInicial = iPaginaInicial; // la primera pagina es la 0
                objPeticion.iNumeroDeFilas = iNumeroDeFilas;
                objPeticion.strIdUsuarioAsignadoCrm = string.Empty;
                objPeticion.iIdUsuarioApp = iIdUsuarioApp;


                string[] lstQueryParams = null;

                var objResultadoApi = (ResultadoLista<ClienteBusqueda>)this.objPeticionesHttpApi.PostApi<ResultadoLista<ClienteBusqueda>>(urlBaseApi, "/Clientes/RecuperarClientes", string.Empty, objPeticion, lstQueryParams);

                if ((objResultadoApi != null) && (objResultadoApi.eResultado == enumResultado.Ok_Recuperar))
                {
                    objResultado = objResultadoApi.lstResultado;
                    strDescripcion = objResultadoApi.sDescripcion;
                }
                else
                {
                    strDescripcion = "Error al recuperar clientes en Claves";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarClientesBusqueda), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarClientesBusqueda), ex.Message);

                strDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarClientesBusqueda), strDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Inserta facturas pagadas en el api de Claves
        /// </summary>
        /// <param name="strIdFacturaNav">Id de la factura de Nav</param>
        /// <param name="IdClienteNav">Id del cliente de Nav</param>
        /// <param name="IdUsuarioApp">Id del usuario</param>
        /// <returns></returns>
        public int InsertarFacturaPagada(InsertarFacturaPagadaPeticion objPeticion)
        {
            var objRespuesta = objPeticionesHttpApi.PostApi<ResultadoEntero>(objConfig.cfgUrlsApi.ClavesApi, "/Facturas/InsertarFacturaPagada", null, objPeticion, null);
            if (objRespuesta.eResultado != enumResultado.Ok_Insertado)
            {
                throw new Exception($"La petición a /Facturas/InsertarFacturaPagada de Claves no ha sido correcta. Resultado --> " +
                    $"{objRespuesta?.iResultado}. Mensaje --> {objRespuesta.sDescripcion}");
            }
            return objRespuesta.iResultado;
        }

        /// <summary>
        /// Este método llama a la API de Claves, al método de Facturas/ModificarUltimaFechaAccesoFacturas.
        /// </summary>
        /// <param name="IdClienteNav">Id cliente Navision</param>
        /// <returns>Consigue un ResultadoFecha que nos indica la última fecha de acceso a las facturas de un usuario.</returns>
        public ResultadoBase RecuperarFechaUltimoAccesoFacturas(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarFechaUltimoAccesoFacturas), IdClienteNav);
            var objResultadoApi = new ResultadoFecha();
            try
            {
                var strParams = $"IdClienteNav={IdClienteNav}";
                var lstQueryParams = new string[] { strParams };
                objResultadoApi = objPeticionesHttpApi.GetApi<ResultadoFecha>(objConfig.cfgUrlsApi.ClavesApi,
                    "/Facturas/RecuperarUltimaFechaAccesoFacturas", string.Empty, lstQueryParams);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarFechaUltimoAccesoFacturas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarFechaUltimoAccesoFacturas), ex.Message);
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarFechaUltimoAccesoFacturas), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        /// <summary>
        /// Este método llama a la API de Claves, al método de Facturas/ModificarUltimaFechaAccesoFacturas.
        /// </summary>
        /// <param name="IdClienteNav">Id cliente Navision</param>
        /// <param name="IdUsuarioApp">Id usuario App</param>
        /// <returns>Consigue un ResultadoEntero que nos dice si la modificación se ha realizado con éxito</returns>
        public ResultadoBase ModificarFechaUltimoAccesoFacturas(int IdClienteNav, int IdUsuarioApp)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}", nameof(ModificarFechaUltimoAccesoFacturas), IdClienteNav, IdUsuarioApp);
            var objResultadoApi = new ResultadoEntero();
            try
            {
                var strParams = $"IdClienteNav={IdClienteNav}&IdUsuarioApp={IdUsuarioApp}";
                var lstQueryParams = new string[] { strParams };
                objResultadoApi = objPeticionesHttpApi.PutApi<ResultadoEntero>(objConfig.cfgUrlsApi.ClavesApi,
                    "/Facturas/ModificarUltimaFechaAccesoFacturas", string.Empty, null, lstQueryParams);
                if (objResultadoApi == null || objResultadoApi.eResultado != enumResultado.Ok_Insertado)
                    throw new Exception(objResultadoApi.sDescripcion ?? "Ha ocurrido un error al hacer la petición a claves");
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarFechaUltimoAccesoFacturas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarFechaUltimoAccesoFacturas), ex.Message);
                objResultadoApi.sDescripcion = ex.Message;
                objResultadoApi.eResultado = enumResultado.Error_Excepcion;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ModificarFechaUltimoAccesoFacturas), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        public ResultadoBase RecuperarUsuarioProducto(int IdClienteNav, string IdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarUsuarioProducto), IdClienteNav.ToString(), IdUsuarioPro);

            ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro> objResultadoApi = new ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>();

            try
            {
                string urlBaseApi = this.objConfig.cfgUrlsApi.ClavesApi;
                string strPrams = $"IdClienteNav={IdClienteNav}&IdUsuarioPro={IdUsuarioPro}";

                string[] lstQueryParams = new string[] { (string)strPrams };

                objResultadoApi = this.objPeticionesHttpApi.GetApi<ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>>(urlBaseApi, "/UsuariosProducto/RecuperarUsuarioProducto", string.Empty, lstQueryParams);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarUsuarioProducto), ex.Message);

                objResultadoApi = null;
            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarUsuarioProducto), objResultadoApi.sDescripcion);
            return objResultadoApi;
        }

        /// <summary>
        /// Inserta facturas pagadas en el api de Claves
        /// </summary>
        /// <param name="strIdFacturaNav">Id de la factura de Nav</param>
        /// <param name="IdClienteNav">Id del cliente de Nav</param>
        /// <param name="IdUsuarioApp">Id del usuario</param>
        /// <returns></returns>
        public ResultadoBase ModificarEmailUsuarioPro(int IdClienteNav, string IdUsuarioPro, string strEmailAnterior, string strEmailNuevo, int IdUsuarioApp)
        {
            objLogger.LogDebug($"START --> {nameof(ModificarEmailUsuarioPro)} con parámetros {IdClienteNav},{IdUsuarioPro},{strEmailAnterior},{strEmailNuevo},{IdUsuarioApp}");

            ResultadoEntero objResultado = new ResultadoEntero();
            string strDescripcion = String.Empty;

            try
            {

                string urlBaseApi = this.objConfig.cfgUrlsApi.ClavesApi;

                ModificarEmailPeticion objPeticion = new ModificarEmailPeticion();
                objPeticion.IdClienteNav = IdClienteNav;
                objPeticion.strEmailAnterior = strEmailAnterior;
                objPeticion.strEmailNuevo = strEmailNuevo;
                objPeticion.IdUsuarioPro = IdUsuarioPro;
                objPeticion.IdUsuarioApp = IdUsuarioApp;

                string[] lstQueryParams = null;

                var objResultadoApi = (ResultadoEntero)this.objPeticionesHttpApi.PutApi<ResultadoEntero>(urlBaseApi, "/UsuariosProducto/ModificarEmailUsuarioPro", string.Empty, objPeticion, lstQueryParams);

                if ((objResultadoApi != null) && (objResultadoApi.eResultado == enumResultado.Ok_Modificado))
                {
                    objResultado = objResultadoApi;
                    strDescripcion = objResultadoApi.sDescripcion;
                }
                else
                {
                    strDescripcion = "Error al modificar email de usuario";
                    objResultado = new ResultadoEntero();
                    objResultado.iResultado = -2;
                    objResultado.sDescripcion = objResultadoApi !=null? objResultadoApi.sDescripcion:strDescripcion;
                    objResultado.eResultado = enumResultado.Error_Validacion;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(ModificarEmailUsuarioPro), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(ModificarEmailUsuarioPro), ex.Message);

                strDescripcion = ex.Message;
            }
            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(ModificarEmailUsuarioPro), strDescripcion);
            return objResultado;
        }
        #endregion
    }
}
