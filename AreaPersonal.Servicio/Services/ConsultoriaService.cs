﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Linq;
using System.Globalization;
using System.IO;
using DllApiBase.Vistas.Claves.Usuario;

namespace AreaPersonal.Servicio
{
    public class ConsultoriaService : AreaPersonalBaseService, IConsultoriaService<Cliente, UsuarioProducto, Materia, SubMateria, Provincia, TipoConsulta, ResultadoGuardar, ResultadoOperacion, ResultadoConsultas, ResultadoEnviar, UsuarioPro>

    {
        private readonly ConsultoriaSvc.IServiceConsultasJuridicas _svcConsultoria;

        private readonly NavisionSvc.IServiceNavision _svcNavision;

        private readonly ExtraMementosSvc.IServiceExtraMementos _svcExtraMementos;

        private readonly IConsultoriaServiceAdv<Consulta, Mementos> _model;

        //private readonly ClavesSvc.IServiceClaves _svcClaves;
        //private readonly ILogger<ConsultoriaService> objLogger;

        public ConsultoriaService(
            IOptions<ConfigApi> configWeb,
            IConsultoriaServiceAdv<Consulta, Mementos> model,
            ILogger<ConsultoriaService> logger) : base(configWeb, logger)
        {
            //objLogger = logger;
            _model = model;

            var basicHttpBindingConsultoria = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxReceivedMessageSize = int.MaxValue,
                MaxBufferSize = int.MaxValue
            };


            var endpointAddressConsultoria = new EndpointAddress(objConfig.cfgServices.ConsultoriaSvc);
            _svcConsultoria = new ConsultoriaSvc.ServiceConsultasJuridicasClient(basicHttpBindingConsultoria, endpointAddressConsultoria);

            //var basicHttpBindingClaves = new BasicHttpBinding();
            //var endpointAddressClaves = new EndpointAddress(objConfigBase.cfgServices.ClavesSvc);
            //_svcClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);

            var basicHttpBindingNavision = new BasicHttpBinding();
            var endpointAddressNavision = new EndpointAddress(objConfigBase.cfgServices.NavisionSvc);
            _svcNavision = new NavisionSvc.ServiceNavisionClient(basicHttpBindingNavision, endpointAddressNavision);


            var basicHttpBindingExtraMementos = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxReceivedMessageSize = int.MaxValue,
                MaxBufferSize = int.MaxValue
            };

            var endpointAddressExtraMementos = new EndpointAddress(objConfigBase.cfgServices.ExtraMementosSvc);
            _svcExtraMementos = new ExtraMementosSvc.ServiceExtraMementosClient(basicHttpBindingExtraMementos, endpointAddressExtraMementos);



        }


        //public async Task<Cliente> RecuperarCliente(string strIdentifier, int IdClienteNav)
        //{
        //    int IdUserApp = _objVariables.idUsuarioAreaPersonal;
        //    objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCliente), strIdentifier, IdClienteNav.ToString(), IdUserApp.ToString());

        //    try
        //    {


        //        if (IsValidIdentifier(strIdentifier))
        //        {
        //            int IntNumTdcSugar = 0;
        //            int IntCodCuentaNavision = IdClienteNav;
        //            string StrIdUsuarioPro = String.Empty;
        //            string StrNombreCompleto = String.Empty;
        //            string StrRazonSocial = String.Empty;
        //            string StrPhoneOffice = String.Empty;
        //            string StrCifNif = String.Empty;
        //            string StrEmail = String.Empty;
        //            string StrLogin = String.Empty;
        //            string StrIp = String.Empty;
        //            int IntIndTipoBusqueda = IdClienteNav > 1000000 ? 3 : 1;
        //            int IntIdTipoCliente = 0;
        //            string StrCampoOrdenar = String.Empty;
        //            int IntIndOrdenarAsc = 0;
        //            int IntPaginaInicial = 0;
        //            int IntNumeroDeFilas = 1;

        //            int IntIdUsuarioApp = IdUserApp;

        //            var lstClientes = await _svcClaves.RecuperarClientesTempAsync(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
        //                StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
        //                IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
        //                IntNumeroDeFilas, IntIdUsuarioApp);

        //            if ((lstClientes != null) && (lstClientes.beanList != null) && (lstClientes.beanList.Count > 0))
        //            {
        //                //log.Info("getCliente.Fin : Encontrado " + oList.beanList.Count.ToString());
        //                ClavesSvc.Cliente oCliente = lstClientes.beanList[0];

        //                var objJSon = new Cliente();

        //                objJSon.cifNif = oCliente._cifNif;
        //                objJSon.indGranCuenta = oCliente._indGranCuenta;
        //                objJSon.codCuentaNavision = oCliente._codCuentaNavision;
        //                objJSon.nombreCompleto = oCliente._nombreCompleto;
        //                objJSon.numTdcSugar = oCliente._numTdcSugar;
        //                objJSon.provincia = oCliente._provincia;
        //                objJSon.razonSocial = oCliente._razonSocial;
        //                objJSon.telefono = oCliente._telefono;

        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), objJSon.codCuentaNavision.ToString());
        //                return objJSon;

        //            }
        //            else
        //            {
        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), "No hay cliente");
        //                return null;
        //            }

        //        }
        //        else
        //        {
        //            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), "Identificador no válido");
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarCliente), ex.Message);
        //        objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarCliente), ex.Message);
        //        return null;
        //    }

        //}
        public async Task<IEnumerable<Materia>> RecuperarMaterias(string strIdentifier, int ClienteNav, string CodigoProducto)
        {
            int IdUserApp = objVariables.idUsuarioAreaPersonal;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarMaterias), strIdentifier, ClienteNav.ToString(), CodigoProducto, IdUserApp.ToString());

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstMatter = await _svcConsultoria.RecuperarMateriasAsync(ClienteNav, CodigoProducto, IdUserApp, 1);

                    var listaJson = new List<Materia>();
                    foreach (var item in lstMatter.beanList)
                    {
                        listaJson.Add(new Materia
                        {
                            idMateria = item._idMateria,
                            descMateria = item._descMateria,
                            //indProducto = item._indProducto
                        });
                    }

                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarMaterias), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarMaterias), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarMaterias), ex.Message);
                return null;
            }

        }

        public async Task<IEnumerable<Materia>> RecuperarMateriasByUsuario(string strIdentifier, string strUsuarioPro, int indLlamadaExperta)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(RecuperarMaterias), strIdentifier, strUsuarioPro);

            try
            {
                var listaJson = new List<Materia>();

                if (IsValidIdentifier(strIdentifier))
                {
                    if (indLlamadaExperta == -1)
                    {
                        var lstMatterLlExp = await _svcConsultoria.RecuperarMateriasPattonAsync(strUsuarioPro, 1);
                        var lstMatterNOLlExp = await _svcConsultoria.RecuperarMateriasPattonAsync(strUsuarioPro, 0);


                        if ((lstMatterLlExp != null) && (lstMatterLlExp.beanList != null))
                            foreach (var item in lstMatterLlExp.beanList)
                            {
                                listaJson.Add(new Materia
                                {
                                    idMateria = item._idMateria,
                                    descMateria = item._descMateria,

                                });
                            }

                        if ((lstMatterNOLlExp != null) && (lstMatterNOLlExp.beanList != null))
                            foreach (var item in lstMatterNOLlExp.beanList)
                            {
                                listaJson.Add(new Materia
                                {
                                    idMateria = item._idMateria,
                                    descMateria = item._descMateria,

                                });
                            }


                    }
                    else
                    {


                        var lstMatter = await _svcConsultoria.RecuperarMateriasPattonAsync(strUsuarioPro, indLlamadaExperta);

                        listaJson = new List<Materia>();

                        if ((lstMatter != null) && (lstMatter.beanList != null))
                            foreach (var item in lstMatter.beanList)
                            {
                                listaJson.Add(new Materia
                                {
                                    idMateria = item._idMateria,
                                    descMateria = item._descMateria,

                                });
                            }
                    }

                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarMaterias), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarMaterias), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarMaterias), ex.Message);
                return null;
            }

        }

        public async Task<IEnumerable<SubMateria>> RecuperarSubMaterias(string strIdentifier, int IdMateria)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(RecuperarSubMaterias), strIdentifier, IdMateria.ToString());

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstSubMatter = await _svcConsultoria.RecuperarSubMateriasAsync(IdMateria);

                    var listaJson = new List<SubMateria>();
                    foreach (var item in lstSubMatter.beanList)
                    {
                        listaJson.Add(new SubMateria
                        {
                            idSubMateria = item._idSubMateria,
                            descSubMateria = item._descSubMateria
                        });
                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSubMaterias), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSubMaterias), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarSubMaterias), ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Provincia>> RecuperarProvincias(string strIdentifier)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}", nameof(RecuperarProvincias), strIdentifier);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstProvinces = await _svcConsultoria.RecuperarProvinciasAsync();

                    var listaJson = new List<Provincia>();
                    foreach (var item in lstProvinces.beanList)
                    {
                        listaJson.Add(new Provincia
                        {
                            idProvincia = item._idProvincia,
                            descProvincia = item._descProvincia,
                            //codigoProvincia = item._codigoProvincia
                        });
                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarProvincias), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarProvincias), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarProvincias), ex.Message);
                return null;
            }

        }

        public async Task<IEnumerable<TipoConsulta>> RecuperarTipoConsultasByMateria(string strIdentifier, int IdMateria)
        {

            int IdUsuarioApp = objVariables.idUsuarioAreaPersonal;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarTipoConsultasByMateria), strIdentifier, IdMateria.ToString(), IdUsuarioApp.ToString());

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lsttypes = await _svcConsultoria.RecuperarTipoConsultaPattonAsync(IdMateria);

                    var listaJson = new List<TipoConsulta>();
                    foreach (var item in lsttypes.beanList)
                    {
                        listaJson.Add(new TipoConsulta
                        {
                            idTipoConsulta = item._idTipoConsulta,
                            descTipoConsulta = item._descTipoConsulta,
                            //indSuscripcionDemo = item._indSuscripcionDemo
                        });
                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoConsultasByMateria), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoConsultasByMateria), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarTipoConsultasByMateria), ex.Message);
                return null;
            }



        }

        public async Task<IEnumerable<TipoConsulta>> RecuperarTipoConsultasCliente(string strIdentifier, int ClienteNav)
        {

            int IdUsuarioApp = objVariables.idUsuarioAreaPersonal;

            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarTipoConsultasCliente), strIdentifier, ClienteNav.ToString(), IdUsuarioApp.ToString());

            try
            {
               
                if (IsValidIdentifier(strIdentifier))
                {
                    var lsttypes = await _svcConsultoria.RecuperarTipoConsultasClienteAsync(ClienteNav, null, null, 1, IdUsuarioApp);

                    var listaJson = new List<TipoConsulta>();
                    foreach (var item in lsttypes.beanList)
                    {
                        listaJson.Add(new TipoConsulta
                        {
                            idTipoConsulta = item._idTipoConsulta,
                            descTipoConsulta = item._descTipoConsulta,
                            //indSuscripcionDemo = item._indSuscripcionDemo
                        });
                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoConsultasCliente), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoConsultasCliente), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarTipoConsultasCliente), ex.Message);
                return null;
            }


        }
        public async Task<ResultadoOperacion> ComprobarCrearPeticion(string strIdentifier, int IdClienteNav)
        {

            int IdUsuarioApp = objVariables.idUsuarioAreaPersonal;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(ComprobarCrearPeticion), strIdentifier, IdClienteNav.ToString(), IdUsuarioApp.ToString());

            ResultadoOperacion objResultado = new ResultadoOperacion();

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {

                    var iCheck = await _svcConsultoria.CheckPuedeCrearConsultaAsync(IdClienteNav, String.Empty, IdUsuarioApp);

                    switch (iCheck)
                    {
                        case 0:
                            {
                                var iResultMorosidad = _svcNavision.CheckClienteMorosoAsync(IdClienteNav);

                                if ((iResultMorosidad != null) && (iResultMorosidad.Result == 0))
                                {
                                    objResultado.iResultadoOperacion = 0;
                                    objResultado.descResultadoOperacion = "Ok";
                                }
                                else
                                {
                                    objResultado.iResultadoOperacion = 4;
                                    objResultado.descResultadoOperacion = "Cliente Moroso";
                                }
                            }; break;
                        case 1:
                            {
                                objResultado.iResultadoOperacion = 1;
                                objResultado.descResultadoOperacion = "No existen suscripciones activas asociadas para las materias del cliente.";
                            }; break;
                        case 2:
                            {
                                objResultado.iResultadoOperacion = 2;
                                objResultado.descResultadoOperacion = "El Número de consultas asociadas con el producto esta agotado.";
                            }; break;
                        case 3:
                            {
                                objResultado.iResultadoOperacion = 3;
                                objResultado.descResultadoOperacion = "No existe suscripciones permitidas con clave DEMO.";
                            }; break;
                        case 4:
                            {
                                objResultado.iResultadoOperacion = 4;
                                objResultado.descResultadoOperacion = "Solo es posible tener tres consultas abiertas de manera simultánea, tan pronto se resuelva una de ellas podrá hacer uso del servicio y plantear una nueva consulta.";
                            }; break;
                        default:
                            {
                                objResultado.iResultadoOperacion = 5;
                                objResultado.descResultadoOperacion = "El cliente " + IdClienteNav.ToString() + " no es válido.";
                            };
                            break;
                    }
                }
                else
                {
                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "El identificador no es válido.";

                }

                objLogger.LogInformation("Resultado {0} --> {1}", nameof(ComprobarCrearPeticion), objResultado.iResultadoOperacion.ToString());
                return objResultado;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ComprobarCrearPeticion), ex.Message);
                objResultado.iResultadoOperacion = -2;
                objResultado.descResultadoOperacion = "Error interno ->" + ex.Message;
                return objResultado;

            }

        }

        public async Task<ResultadoOperacion> ComprobarCrearPeticionByMateria(string strIdentifier, int IdClienteNav, int IdMateria)
        {

            int IdUsuarioApp = objVariables.idUsuarioAreaPersonal;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3},{4}", nameof(ComprobarCrearPeticionByMateria), strIdentifier, IdClienteNav.ToString(), IdUsuarioApp.ToString(), IdMateria.ToString());

            ResultadoOperacion objResultado = new ResultadoOperacion();

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var strCheckBloqueo = _model.CheckStrBloqueos(IdMateria);

                    if (!String.IsNullOrEmpty(strCheckBloqueo))
                    {
                        objResultado.iResultadoOperacion = 6;
                        objResultado.descResultadoOperacion = strCheckBloqueo;
                    }
                    else
                    {

                        var iCheck = await _svcConsultoria.CheckPuedeCrearConsultaByMateriaAsync(IdClienteNav, IdMateria);

                        switch (iCheck)
                        {
                            case 0:
                                {
                                    var iResultMorosidad = _svcNavision.CheckClienteMorosoAsync(IdClienteNav);

                                    if ((iResultMorosidad != null) && (iResultMorosidad.Result == 0))
                                    {
                                        objResultado.iResultadoOperacion = 0;
                                        objResultado.descResultadoOperacion = "Ok";
                                    }
                                    else
                                    {
                                        objResultado.iResultadoOperacion = 4;
                                        objResultado.descResultadoOperacion = "Cliente Moroso";
                                    }
                                }; break;
                            case 1:
                                {
                                    objResultado.iResultadoOperacion = 1;
                                    objResultado.descResultadoOperacion = "No existen suscripciones activas asociadas para las materias del cliente.";
                                }; break;
                            case 2:
                                {
                                    objResultado.iResultadoOperacion = 2;
                                    objResultado.descResultadoOperacion = "El Número de consultas asociadas con el producto esta agotado.";
                                }; break;
                            //case 3:
                            //    {
                            //        objResultado.iResultadoOperacion = 3;
                            //        objResultado.descResultadoOperacion = "No existe suscripciones permitidas con clave DEMO.";
                            //    }; break;
                            case 3:
                                {
                                    objResultado.iResultadoOperacion = 3;
                                    objResultado.descResultadoOperacion = "Solo es posible tener tres consultas abiertas de manera simultánea, tan pronto se resuelva una de ellas podrá hacer uso del servicio y plantear una nueva consulta.";
                                }; break;
                            default:
                                {
                                    objResultado.iResultadoOperacion = 5;
                                    objResultado.descResultadoOperacion = "El cliente " + IdClienteNav.ToString() + " no es válido.";
                                };
                                break;
                        }
                    }
                }
                else
                {
                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "El identificador no es válido.";

                }

                objLogger.LogInformation("Resultado {0} --> {1}", nameof(ComprobarCrearPeticionByMateria), objResultado.iResultadoOperacion.ToString());
                return objResultado;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ComprobarCrearPeticionByMateria), ex.Message);
                objResultado.iResultadoOperacion = -2;
                objResultado.descResultadoOperacion = "Error interno ->" + ex.Message;
                return objResultado;

            }

        }

        public async Task<ResultadoOperacion> ComprobarEnviarComentarios(string strIdentifier, int IdClienteNav, int IdMateria)
        {

            int IdUsuarioApp = objVariables.idUsuarioAreaPersonal;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3},{4}", nameof(ComprobarEnviarComentarios), strIdentifier, IdClienteNav.ToString(), IdUsuarioApp.ToString(), IdMateria.ToString());

            ResultadoOperacion objResultado = new ResultadoOperacion();

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var strCheckBloqueo = _model.CheckStrBloqueos(IdMateria);

                    if (!String.IsNullOrEmpty(strCheckBloqueo))
                    {
                        objResultado.iResultadoOperacion = 6;
                        objResultado.descResultadoOperacion = strCheckBloqueo;
                    }
                    else
                    {
                        objResultado.iResultadoOperacion = 0;
                        objResultado.descResultadoOperacion = "Ok";
                    }
                }
                else
                {
                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "El identificador no es válido.";

                }

                objLogger.LogInformation("Resultado {0} --> {1}", nameof(ComprobarEnviarComentarios), objResultado.iResultadoOperacion.ToString());
                return objResultado;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ComprobarEnviarComentarios), ex.Message);
                objResultado.iResultadoOperacion = -2;
                objResultado.descResultadoOperacion = "Error interno ->" + ex.Message;
                return objResultado;

            }

        }

        //private ClavesSvc.UsuarioApp GetUserApp(int iUserApp)
        //{
        //    try
        //    {

        //        Bean<ClavesSvc.UsuarioApp> users = _svcClaves.RecuperarUsuarioAppByIdAsync(iUserApp).Result;

        //        if (users.bean != null)
        //        {
        //            return users.bean;
        //        }

        //        else return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogInformation("Error --> {0} {1} ", nameof(GetUserApp), ex.Message);
        //        objLogger.LogError("Error --> {0} {1} ", nameof(GetUserApp), ex.Message);
        //        return null;
        //    }

        //}

        public BeanList<ConsultoriaSvc.UsuarioCJ> getUsersByMateria(int IntIdMateria, int IntIdCollaborator, int IntIdNivel)
        {
            try
            {
                Task<BeanList<ConsultoriaSvc.UsuarioCJ>> objResult = null;
                objResult = _svcConsultoria.RecuperarUsuariosAppByMateriaAsync(IntIdMateria, IntIdCollaborator, IntIdNivel);

                return objResult.Result;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(getUsersByMateria), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(getUsersByMateria), ex.Message);
                return null;
            }
        }


        public ResultadoGuardar GuardarConsulta(string strIdentifier, string strEmail, String strName, string strSurname, String strDescription, String strPhone, string strTitular, string strCargo, String sNameSugar, string strDisponibilidad, int iEstado = -1, int iIdMatter = -1, int iIdType = -1, int iIdSubMatter = -1, int iIdProvince = -1, int iIdClientNav = -1, int iCodCRM = -1)
        {
            ResultadoGuardar objResultado = new ResultadoGuardar();

            try
            {

                if (!IsValidIdentifier(strIdentifier))
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(GuardarConsulta), "Identificador no válido");

                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "Identificador no válido";

                    objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);


                    return objResultado;
                }

                objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(GuardarConsulta), strIdentifier, "@iCodCRM: " + iCodCRM.ToString() + ", @iIdClientNav: " + iIdClientNav.ToString() + ", @iIdMatter: " + iIdMatter.ToString() +
                ", @iIdType: " + iIdType.ToString() + ", @iIdMatter: " + iIdMatter.ToString() + ", @iIdSubMatter: " + iIdSubMatter.ToString() + ", @iIdProvince: " + iIdProvince.ToString() + ", @strDisponibilidad: " + strDisponibilidad +
                ", @iEstado: " + iEstado.ToString() + ", @sNameSugar: " + sNameSugar + ", @strCargo: " + strCargo +
                ", @strPhone: " + strPhone + ", @strDescription (length): " + strDescription.Length.ToString() +
                ", @strSurname: " + strSurname + ", @strNameUser: " + strName + ", @strEmail: " + strEmail);


                string sDescription = String.IsNullOrEmpty(strDescription) ? String.Empty : strDescription;

                if (_model.SuperadoLimiteDescripcion(iIdMatter, strDescription))
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(GuardarConsulta), "Se ha superado el límite de caracteres");

                    objResultado.iResultadoOperacion = -6;
                    objResultado.descResultadoOperacion = "Se ha superado el límite de caracteres";

                    objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);


                    return objResultado;
                }

                int iIdQuery = -1;
                bool bSendEmail = true;
                int iResult = -1;

                bool CanCreateExpediente = true;
                bool CanCreateFormulario = true;
                bool CanCreateConsulta = true;

                bool IsStandBy = false;

                if ((iIdMatter == (int)TypeMatter.DerechoLocal) && (iIdClientNav > 1000000))
                {
                    if (iIdType == (int)TypeQuery.Consulta)
                    {
                        if (!_model.CanCreateConsultaFicticia(iIdClientNav))
                        {
                            objResultado.bEnviarEmail = false;
                            objResultado.iIdConsulta = -1;
                            objResultado.iResultadoOperacion = -5;
                            objResultado.descResultadoOperacion = "Solo se pueden crear una Consulta DEMO para Derecho Local";

                            objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);

                            return objResultado;
                        }
                    }

                    else
                    {
                        objResultado.bEnviarEmail = false;
                        objResultado.iIdConsulta = -1;
                        objResultado.iResultadoOperacion = -5;
                        objResultado.descResultadoOperacion = "No se pueden crear expedientes o formularios en Cliente DEMO para Derecho Local";

                        objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);

                        return objResultado;
                    }
                }

                if ((iIdType == (int)TypeQuery.Consulta) && (iIdMatter == (int)TypeMatter.DerechoLocal))
                {
                    if ((iEstado != (int)TypeStatus.closed_without_resolved) && (iEstado != (int)TypeStatus.closed_resolved))
                    {
                        CanCreateConsulta = _model.CanCreateConsulta(iIdQuery, iIdClientNav);
                    }
                }



                if ((iIdType == (int)TypeQuery.Expediente) && ((iIdMatter == (int)TypeMatter.DerechoLocal)))
                {

                    CanCreateExpediente = _model.CanCreateExpediente(-1, iIdMatter, iIdClientNav);
                }

                if ((iIdType == (int)TypeQuery.Formulario) && ((iIdMatter == (int)TypeMatter.DerechoLocal)))
                {
                    CanCreateFormulario = _model.CanCreateFormulario(iIdQuery, iIdClientNav);
                }


                ConsultoriaSvc.Consulta newQuery = new ConsultoriaSvc.Consulta();

                int _IdTipoSuscripcion = -1;
                string _IdProductNav = String.Empty;
                string _DescProductNav = String.Empty;


                // Fill data of query
                if ((!CanCreateExpediente) || (!CanCreateFormulario) || (!CanCreateConsulta))
                {
                    newQuery._idConsultaEstado = (int)TypeStatus.stand_by;

                    IsStandBy = true;

                    objLogger.LogInformation("{0} : Consulta en StandBy ", nameof(GuardarConsulta));

                }
                else
                {
                    newQuery._idConsultaEstado = iEstado;
                    bSendEmail = true;
                }

                newQuery._idMateria = iIdMatter;
                newQuery._idSubMateria = iIdSubMatter;
                newQuery._idTipoSuscripcion = _IdTipoSuscripcion;
                newQuery._idSuscripcionNav = -1;
                newQuery._descConsulta = strDescription.Replace("&nbsp;", " ");
                newQuery._email = strEmail;
                newQuery._tituloConsulta = String.Empty;
                newQuery._idTipoConsulta = iIdType;
                newQuery._idProvincia = iIdProvince;
                //newQuery._descProvincia = strDescProvince;
                newQuery._tlf = strPhone;
                newQuery._idClienteNav = iIdClientNav;
                newQuery._idClienteCRM = iCodCRM;
                newQuery._idProductoNav = _IdProductNav;
                newQuery._descProductoNav = _DescProductNav;
                //newQuery._descMateria = strDescMatter;
                //newQuery._descSubMateria = strDescSubMatter;
                newQuery._idColaborador = -1;
                newQuery._referenciaEditorial = String.Empty;
                newQuery._facturacionColaborador = String.Empty;
                newQuery._indFacturacionColaborador = -1;

                newQuery._cargo = (iIdMatter == (int)TypeMatter.DerechoLocal) ? strCargo : String.Empty;
                newQuery._disponibilidad = ((iIdMatter == (int)TypeMatter.Fiscal) || (iIdMatter == (int)TypeMatter.Social)) ? strDisponibilidad : String.Empty;

                newQuery._idVoz = -1;

                newQuery._indPrivada = 0;
                newQuery._numeroPartes = -1;

                newQuery._nombreUsuarioPeticion = strName + (!String.IsNullOrEmpty(strSurname) ? " " + strSurname : String.Empty);
                newQuery._nombreClienteSuscripcion = sNameSugar;

                if (String.IsNullOrEmpty(strTitular))
                {
                    strTitular = sNameSugar;
                }
                else
                {
                    strTitular = strTitular + " / " + sNameSugar;
                }

                iResult = _model.AddQuery(newQuery);

                if (iResult < 0)
                {
                    // Return -1 por defecto.
                    // return error.
                    objResultado.bEnviarEmail = false;
                    objResultado.iIdConsulta = -1;
                    objResultado.iResultadoOperacion = iResult;
                    objResultado.descResultadoOperacion = "Error en el guardado de la petición.";
                    objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);

                    return objResultado;
                }

                iIdQuery = iResult > 0 ? iResult : iIdQuery;

                iResult = 1;

                if ((iIdQuery > 0) && (_model.Check_NonPayment(iIdQuery, iCodCRM, iIdClientNav)))
                {
                    objLogger.LogInformation("{0} : Consulta nueva {1} es morosa.", nameof(GuardarConsulta), iIdQuery.ToString());
                    bSendEmail = false;
                }

                if (iIdQuery > 0)
                {
                    int iUserApp = -1;
                   
                    int iResultAssign = -1;


                    if (!IsStandBy)
                    {
                        if ((iIdMatter == (int)TypeMatter.Fiscal) || (iIdMatter == (int)TypeMatter.Social)) // Llamada Experta y trabajo con niveles.
                        {



                            if (bSendEmail)
                            {

                                int iIdGeneralN2 = iIdMatter == (int)TypeMatter.Fiscal ? objVariables.configConsultoria.mail_n2General_1 : objVariables.configConsultoria.mail_n2General_3;

                                ClavesSvc.UsuarioApp oUser2 = this.GetUserApp(iIdGeneralN2);

                                if (oUser2 != null)
                                {
                                    int iResultAssign2 = _model.SendEmailAssing(iCodCRM, newQuery, iIdQuery, oUser2, strTitular, iIdClientNav, 2);
                                    objLogger.LogInformation("Info --> {0} Enviado correo de asignación  a {1} con resultado {2}", nameof(GuardarConsulta), oUser2._email, iResultAssign2.ToString());

                                }
                            }
                            else
                            {
                                objLogger.LogInformation("Info --> {0} : No enviado correo a Usuario asignado por esta Pendiente de Cobros", nameof(GuardarConsulta));
                                iResult = 7;
                            }



                        }

                        else
                        {
                            BeanList<ConsultoriaSvc.UsuarioCJ> alistUsers = this.getUsersByMateria(iIdMatter, 0, 0);

                            if ((alistUsers != null) && (alistUsers.beanList != null))
                            {
                                //log.Info("Question.SaveQuestion: Encontrados " + alistUsers.beanList.Count.ToString() + " consultores");
                                objLogger.LogInformation("Info --> {0} : Encontrados {1} consultores", nameof(GuardarConsulta), alistUsers.beanList.Count.ToString());


                                if (alistUsers.beanList.Count == 1)
                                {
                                    iUserApp = alistUsers.beanList[0]._idUsuarioApp;

                                    iResultAssign = _model.AssingUser(iIdQuery, iUserApp);



                                    if (iResultAssign >= 0)
                                    {
                                        if (bSendEmail)
                                        {

                                            iResult = 2;

                                            int iResultSendEmail = -1;

                                            ClavesSvc.UsuarioApp oUser = this.GetUserApp(iUserApp);
                                            if (oUser != null)
                                            {

                                                iResultSendEmail = _model.SendEmailAssing(iCodCRM, newQuery, iIdQuery, oUser, strTitular, iIdClientNav, 1);
                                                objLogger.LogInformation("Info --> {0} Enviado correo de asignación  a {1} con resultado {2}", nameof(GuardarConsulta), oUser._email, iResultSendEmail.ToString());
                                                if (iResultSendEmail >= 0) iResult = 3;
                                            }
                                            else
                                            {
                                                objLogger.LogInformation("Error --> {0} No existe usuario {1}", nameof(GuardarConsulta), iUserApp.ToString());
                                                iResult = 6;
                                            }

                                        }
                                        else
                                        {
                                            //log.Info("Question.SaveQuestion: No enviado correo a Usuario asignado por esta Pendiente de Cobros");
                                            objLogger.LogInformation("Info --> {0} : No enviado correo a Usuario asignado por esta Pendiente de Cobros", nameof(GuardarConsulta));
                                            iResult = 7;

                                        }
                                    }
                                    else
                                    {
                                        objLogger.LogInformation("Error --> {0} : No se ha podido asignar la petion al usuario {1} ", nameof(GuardarConsulta), iUserApp.ToString());

                                    }

                                }
                                else
                                {

                                    if (bSendEmail)
                                    {
                                        iUserApp = -1;
                                        ClavesSvc.UsuarioApp oUser = null;

                                        for (int i = 0; i < alistUsers.beanList.Count; i++)
                                        {
                                            iUserApp = alistUsers.beanList[i]._idUsuarioApp;

                                            oUser = this.GetUserApp(iUserApp);

                                            if (oUser != null)
                                            {

                                                int iResultAsignacion2 = _model.SendEmailAssing(iCodCRM, newQuery, iIdQuery, oUser, strTitular, iIdClientNav, 1);
                                                objLogger.LogInformation("Info --> {0} Enviado correo de asignación  a {1} con resultado {2}", nameof(GuardarConsulta), oUser._email, iResultAsignacion2.ToString());

                                            }

                                        }
                                        iResult = 8; // Enviado a varios gestores por materia
                                    }
                                    else
                                    {
                                        objLogger.LogInformation("Info --> {0} : No se envian correos al resto de gestores por esta Pendiente de Cobros", nameof(GuardarConsulta));
                                        iResult = 7;
                                    }

                                }
                            }
                            else
                            {
                                objLogger.LogInformation("{0} : Encontrados 0 consultores", nameof(GuardarConsulta));
                            }
                        }

                    }
                    else
                    {
                        // Peticion en StandBy

                        if (bSendEmail)
                        {
                            int iResultStandBy = _model.SendEmailStandBy(iCodCRM, strEmail, newQuery._nombreUsuarioPeticion, iIdQuery);

                            objLogger.LogInformation("{0} : Resultado de SendEmailStandBy --> {1} ", nameof(GuardarConsulta), iResultStandBy.ToString());
                        }
                        else
                            objLogger.LogInformation("{0} : No se envia notificación de StandBy por ser Moroso");
                    }

                    int iCheckDemo = _model.CheckSendEmailClaveDemo(iCodCRM, iIdQuery);

                }
               
                //log.Info("Question.SaveQuestion : @iResult: " + iResult.ToString());
                objLogger.LogInformation("{0} Resultado de Guardar --> {1} ", nameof(GuardarConsulta), iResult.ToString());

                objResultado.iResultadoOperacion = iResult;
                objResultado.iIdConsulta = iIdQuery;
                objResultado.bEnviarEmail = bSendEmail;
                objResultado.descResultadoOperacion = "Petición guardada en el sistema.";

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GuardarConsulta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GuardarConsulta), ex.Message);

                objResultado.iResultadoOperacion = -2;
                objResultado.iIdConsulta = -1;
                objResultado.bEnviarEmail = false;
                objResultado.descResultadoOperacion = "Error " + ex.Message;
            }

            objLogger.LogInformation("END --> {0} Resultado : {1}, descripcion : {2} ", nameof(GuardarConsulta), objResultado.iResultadoOperacion.ToString(), objResultado.descResultadoOperacion);
            return objResultado;
        }

        public int SendEmailMessageAlta(string strIdentifier, int iCodCRM, string strEmail, string strName, int iIdQuery)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(SendEmailMessageAlta), strIdentifier, iCodCRM.ToString(), strEmail, strName, iIdQuery.ToString());
            int intResult = -1;
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {


                    intResult = _model.SendEmailAddQuery(iCodCRM, strEmail, strName, iIdQuery);
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoConsultasByMateria), "Identificador no válido");
                    return intResult;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendEmailMessageAlta), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendEmailMessageAlta), ex.Message);

            }
            objLogger.LogInformation("{0} con Resultado ", nameof(SendEmailMessageAlta), intResult.ToString());
            return intResult;
        }

        public int ReSendEmailInfo(string strIdentifier, int iCodCRM, string strEmail, int iIdQuery)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3},{4}", nameof(ReSendEmailInfo), strIdentifier, iCodCRM.ToString(), strEmail, iIdQuery.ToString());

            int intResult = -1;
            try
            {

                if (IsValidIdentifier(strIdentifier))
                {


                    intResult = _model.ReSendEmailInfo(iCodCRM, strEmail, iIdQuery);

                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(ReSendEmailInfo), "Identificador no válido");
                    return intResult;
                }




            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(ComprobarCrearPeticionByMateria), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(ComprobarCrearPeticionByMateria), ex.Message);

            }
            objLogger.LogInformation("{0} con Resultado ", nameof(SendEmailMessageAlta), intResult.ToString());
            return intResult;
        }

        private ConsultoriaSvc.Consulta GetQuestion(int idQuestion)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}", nameof(GetQuestion), idQuestion.ToString());


            try
            {
                BeanList<ConsultoriaSvc.Consulta> list = _svcConsultoria.RecuperarConsultasAsync(idQuestion, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, 1, 1, String.Empty, String.Empty, this.objVariables.idUsuarioAreaPersonal).Result;

                if ((list != null) && (list.beanList != null) && (list.beanList.Count > 0))
                    return list.beanList[0];
                else return null;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetAllQuestions), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetAllQuestions), ex.Message);
                return null;
            }
        }

        private BeanList<ConsultoriaSvc.Consulta> GetAllQuestions(int codCrm, int codNav, int iType, int iMatter, int iPageNum, int iPageSize)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}, {5}", nameof(GetAllQuestions), codNav.ToString(), iType.ToString(), iMatter.ToString(), iPageNum.ToString(), iPageSize.ToString());


            try
            {

                string strOrderField = "ID_CONSULTA";
                string strOrderType = "DESC";

                BeanList<ConsultoriaSvc.Consulta> list = _svcConsultoria.RecuperarConsultasAsync(-1, iType, -1, codCrm, codNav, iMatter, -1, -1, -1, -1, 2, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, -1, -1, iPageNum, iPageSize, strOrderField, strOrderType, this.objVariables.idUsuarioAreaPersonal).Result;

                return list;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(GetAllQuestions), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetAllQuestions), ex.Message);
                return null;
            }
        }


        // Obtener extra mementos de las consultas
        private List<Mementos> getmementosAsync()
        {
            List<Mementos> aResult = new List<Mementos>();

            try
            {

                BeanList<ExtraMementosSvc.Memento> list = _svcExtraMementos.RecuperarMementosAsync(-1, -1, -1, string.Empty, string.Empty, -1, -1, -1, 1, 100000, "DESC_MATERIAS", "ASC").Result;

                foreach (ExtraMementosSvc.Memento item in list.beanList)
                {
                    Mementos newItem = new Mementos();

                    newItem.codigoMemento = item._codigoMemento;
                    newItem.descripcion = item._descripcion;

                    aResult.Add(newItem);
                }

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(getmementosAsync), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(getmementosAsync), ex.Message);
                return null;
            }

            return aResult;
        }

        public ResultadoConsultas RecuperarPeticion(string strIdentifier, int iQuestionId)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(RecuperarPeticion), strIdentifier, iQuestionId.ToString());

            ResultadoConsultas oResult = new ResultadoConsultas();
            List<Consulta> lResultConsultas = new List<Consulta>();
            List<ConsultaMateria> lResultMateria = new List<ConsultaMateria>();

            oResult.iResultado = -1;
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {

                    ConsultoriaSvc.Consulta objConsulta = this.GetQuestion(iQuestionId);

                    List<Mementos> alisMementos = this.getmementosAsync();

                    if (objConsulta != null)
                    {
                        oResult.iTotalConsultas = 1;

                        Consulta newQuery = _model.GetPropertiesQuery(objConsulta, alisMementos);

                        if (newQuery != null)
                        {
                            lResultConsultas.Add(newQuery);

                            if (lResultMateria.Where(t => t.IdMatter == objConsulta._idMateria).Count() == 0)
                            {
                                int inumTotal = _model.GetNSuscriptionsTotal(objConsulta._idClienteNav, objConsulta._idMateria);
                                int inumEnabled = _model.GetNSuscriptionsEnabled(objConsulta._idClienteNav, objConsulta._idMateria);

                                lResultMateria.Add(new ConsultaMateria(objConsulta._idMateria, objConsulta._descMateria, inumTotal, inumEnabled));
                            }
                        }
                    }
                }

                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarPeticion), "Identificador no válido");

                }

                oResult.iResultado = lResultConsultas.Count();
                oResult.listadoConsultas = lResultConsultas;
                oResult.listadoMaterias = lResultMateria;
                return oResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarPeticion), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarPeticion), ex.Message);
                return oResult;
            }
        }

        public ResultadoConsultas RecuperarPeticiones(string strIdentifier, int iCodCRM, int iCodNavision, int iType, int iMatter, int iPageNum, int iPageSize)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}, {5},{6}", nameof(GetAllQuestions), strIdentifier, iCodCRM.ToString(), iCodNavision.ToString(), iType.ToString(), iMatter.ToString(), iPageNum.ToString(), iPageSize.ToString());

            ResultadoConsultas oResult = new ResultadoConsultas();
            List<Consulta> lResultConsultas = new List<Consulta>();
            List<ConsultaMateria> lResultMateria = new List<ConsultaMateria>();

            oResult.iResultado = -1;
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {

                    BeanList<ConsultoriaSvc.Consulta> listConsultas = this.GetAllQuestions(iCodCRM, iCodNavision, iType, iMatter, iPageNum, iPageSize);

                    List<Mementos> alisMementos = this.getmementosAsync();

                    if ((listConsultas != null) && (listConsultas.beanList != null))
                    {
                        oResult.iTotalConsultas = listConsultas.ObjResult.IntTotalRecs;
                        foreach (ConsultoriaSvc.Consulta itemQuestion in listConsultas.beanList)
                        {
                            Consulta newQuery = _model.GetPropertiesQuery(itemQuestion, alisMementos);

                            if (newQuery != null)
                            {
                                lResultConsultas.Add(newQuery);

                                if (lResultMateria.Where(t => t.IdMatter == itemQuestion._idMateria).Count() == 0)
                                {
                                    int inumTotal = _model.GetNSuscriptionsTotal(iCodNavision, itemQuestion._idMateria);
                                    int inumEnabled = _model.GetNSuscriptionsEnabled(iCodNavision, itemQuestion._idMateria);

                                    lResultMateria.Add(new ConsultaMateria(itemQuestion._idMateria, itemQuestion._descMateria, inumTotal, inumEnabled));
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarPeticiones), "Identificador no válido");

                }

                oResult.iResultado = lResultConsultas.Count();
                oResult.listadoConsultas = lResultConsultas;
                oResult.listadoMaterias = lResultMateria;
                return oResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarPeticiones), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarPeticiones), ex.Message);
                return oResult;
            }

        }

        public ResultadoEnviar SendRequest(string strIdentifier, int icodQuestion, int iCodCRM, string strText, int iIdClientNav, Boolean bCopy)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}, {5}, {6}", nameof(SendRequest), strIdentifier, icodQuestion.ToString(), iCodCRM.ToString(), iIdClientNav.ToString(), strText,  bCopy.ToString());

            ResultadoEnviar oResult = new ResultadoEnviar();
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {

                    int intResultEmail = -1;
                    int intResultEmailCopy = -1;
                    int intResult = -1;
                    int iSubQueryId = -1;
                    if (iCodCRM <= 0) iCodCRM = 1;

                    int iIdUserApp = objVariables.idUsuarioAreaPersonal;

                    iSubQueryId = _model.AddSubQuery(icodQuestion, strText, 0, iIdUserApp);

                    if (iSubQueryId >= 0)
                    {
                        oResult.iResultadoOperacion = 1;
                        oResult.iIdComentario = iSubQueryId;
                        oResult.descResultadoOperacion = "Guardado comentario";
                        intResultEmail = _model.SendEmailResponse(iCodCRM, icodQuestion, strText, iIdUserApp);

                        if (intResultEmail >= 0)
                        {
                            oResult.iResultadoOperacion++;
                            oResult.descResultadoOperacion = oResult.descResultadoOperacion + " - Enviado Comentario ";

                            if (bCopy)
                            {
                                intResultEmailCopy = _model.SendEmailResponseCopy(iCodCRM, icodQuestion, strText, iIdUserApp);

                                if (intResultEmailCopy >= 0)
                                {
                                    oResult.iResultadoOperacion++;
                                    oResult.descResultadoOperacion = oResult.descResultadoOperacion + " - Enviado copia ";
                                }
                            }
                        }

                    }
                    else intResult = -1;

                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(SendRequest), intResult.ToString());
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(SendRequest), "Identificador no válido");

                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SendRequest), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SendRequest), ex.Message);
                return oResult;
            }

            return oResult;
        }


        public int SaveFile(string strIdentifier, String sFileName, int iIdQuery, String strSize, byte[] bdata)
        {
            int iResult = -1;
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}, {4}, {5}", nameof(SaveFile), strIdentifier, sFileName, iIdQuery.ToString(), strSize.ToString(), bdata.Length);

                    int iIdUserApp = objVariables.idUsuarioAreaPersonal;

                    string sExtension = Path.GetExtension(sFileName);

                    iResult = _svcConsultoria.InsertarFicheroAsync(iIdQuery, sFileName, sExtension, strSize, 0, iIdUserApp).Result;

                    if (iResult > 0)
                    {

                        int iResultStored = _model.StoreFile(sFileName, iIdQuery.ToString(), bdata);

                        objLogger.LogInformation("START --> {0} -> llamar a StoreFile con resultado {1}", nameof(SaveFile), iResultStored);

                        if (iResultStored < 0)
                        {
                            iResultStored = _svcConsultoria.EliminarFicheroAsync(iResult, iIdUserApp).Result;

                            objLogger.LogInformation("START --> {0} -> llamar a StoreFile con resultado {1}", nameof(SaveFile), iResultStored);

                            if (iResultStored == 0) iResult = -3;
                            else iResult = -4;
                        }
                    }
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(SaveFile), "Identificador no válido");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SaveFile), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SaveFile), ex.Message);
               
            }

            objLogger.LogInformation("{0} con Resultado {1}", nameof(SaveFile), iResult.ToString());
            return iResult;

        }

        public int SaveFile2(String sFileName, byte[] bdata)
        {
            int iResult = -1;
            try
            {
              
                int iIdUserApp = objVariables.idUsuarioAreaPersonal;

                string sExtension = Path.GetExtension(sFileName);

                string path = objVariables.path_patters + "\\Consultoria";
                string originalFile = System.IO.Path.Combine(path, sFileName);

                File.WriteAllBytes(originalFile, bdata);
                return 0;

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(SaveFile2), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(SaveFile2), ex.Message);

            }

            objLogger.LogInformation("{0} con Resultado {1}", nameof(SaveFile2), iResult.ToString());
            return iResult;

        }

    }

    
}
