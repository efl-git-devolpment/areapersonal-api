﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public class CertificadoService : AreaPersonalBaseService, ICertificadoService<CertificadoResult,Certificado, CertificadoPropiedad>

    {


        public CertificadoService(
            IOptions<ConfigApi> configWeb,
            ILogger<CertificadoService> logger) : base(configWeb, logger)
        {
            //objLogger = logger;

            //var basicHttpBindingClaves = new BasicHttpBinding();
            //var endpointAddressClaves = new EndpointAddress(objConfigBase.cfgServices.ClavesSvc);
            //_svcClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);

            //_objVariables = MyConfig.Variables;
        }

        /// <summary>
        /// Recuperar los certificados de un acceso
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strIdUsuarioPro"></param>
        /// <param name="intTipoCertificado"></param>
        /// <returns></returns>
        public async Task<CertificadoResult> RecuperarCertificados(string strIdentifier, string strIdUsuarioPro, int intTipoCertificado)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCertificados), strIdentifier, strIdUsuarioPro, intTipoCertificado);

            CertificadoResult oResult = null;
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var oCertificadoResult = await _svcClaves.RecuperarCertificadosSafeStamperAsync(strIdUsuarioPro, intTipoCertificado);

                    oResult = new CertificadoResult();

                    oResult.count = oCertificadoResult._count;
                    oResult.pageSize = oCertificadoResult._pageSize;
                    oResult.pageTotal = oCertificadoResult._pageTotal;
                    oResult.list = new List<Certificado>();

                    if (oCertificadoResult._list != null)
                    {

                        foreach (var item in oCertificadoResult._list)
                        {

                            CertificadoPropiedad oObjCertificadoPropiedad = new CertificadoPropiedad();

                            if (item._properties != null)
                            {
                                oObjCertificadoPropiedad.accuracy = item._properties.accuracy;
                                oObjCertificadoPropiedad.address = item._properties.address;
                                oObjCertificadoPropiedad.filename = item._properties.filename;
                                oObjCertificadoPropiedad.latitude = item._properties.latitude;
                                oObjCertificadoPropiedad.longitude = item._properties.longitude;
                                oObjCertificadoPropiedad.md5 = item._properties.md5;
                                oObjCertificadoPropiedad.sha1 = item._properties.sha1;
                                oObjCertificadoPropiedad.sha256 = item._properties.sha256;
                                oObjCertificadoPropiedad.sha512 = item._properties.sha512;
                                oObjCertificadoPropiedad.size = item._properties.size;
                                oObjCertificadoPropiedad.subject = item._properties.subject;
                                oObjCertificadoPropiedad.toAddresses = item._properties.toAddresses;
                                oObjCertificadoPropiedad.url = item._properties.url;

                            }

                            oResult.list.Add(new Certificado
                            {
                                code = item._code,
                                entryDate = item._entryDate,
                                recipientMail = item._recipientMail,
                                recipientName = item._recipientName,
                                state = item._state,
                                subject = item._subject,
                                thumbnailUrl = item._thumbnailUrl,
                                url = item._url,
                                property = oObjCertificadoPropiedad
                            });

                        }
                    }
                    return oResult;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCertificados), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarCertificados), ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Recuperar el número de certificados disponibles
        /// </summary>
        /// <returns></returns>
        public int RecuperarNumCertificadosDisponibles(string strIdentifier, string strIdUsuarioPro)
        {
            int iResult = -1;

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(RecuperarNumCertificadosDisponibles), strIdentifier, strIdUsuarioPro);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {


                    iResult = _svcClaves.RecuperarCertificadosDisponiblesAsync(strIdUsuarioPro).Result;

                    objLogger.LogInformation("Resultado Certificados Disponibles --> {0} {1} ", nameof(RecuperarNumCertificadosDisponibles), iResult.ToString());
                }

                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCertificados), "Identificador no válido");
                }

                return iResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarNumCertificadosDisponibles), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarNumCertificadosDisponibles), ex.Message);
                return -1;
            }

        }

        /// <summary>
        /// Recuperar el número de certificados total
        /// </summary>
        /// <returns></returns>
        public int RecuperarNumCertificadosTotal(string strIdentifier, string strIdUsuarioPro)
        {
            int iResult = -1;

            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(RecuperarNumCertificadosTotal), strIdentifier, strIdUsuarioPro);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {


                    iResult = _svcClaves.RecuperarNumCertificadosSafeStamperAsync(strIdUsuarioPro).Result;

                    objLogger.LogInformation("Resultado Certificados Total --> {0} {1} ", nameof(RecuperarNumCertificadosTotal), iResult.ToString());
                }

                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarNumCertificadosTotal), "Identificador no válido");
                }

                return iResult;
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarNumCertificadosTotal), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarNumCertificadosTotal), ex.Message);
                return -1;
            }

        }

    }
}