﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using AreaPersonal.Vistas.External.Claves;
using AreaPersonal.Vistas.External.Crm;
using AutoMapper;
using DllApiBase.Comun.Configuracion;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves.Usuario;
using DllApiBase.Vistas.Cliente;
using DllApiBase.Vistas.Permisos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Navision.Vistas.Cliente;
using Navision.Vistas.Facturas;
using Navision.Vistas.Productos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public class ClientesService : AreaPersonalBaseService, IClientesService<ResultadoBase>
    {

        private readonly IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objExternalService;
        public ClientesService(
            ILogger<ClientesService> objLoggerBase,
            IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objExternalServiceBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase, objLoggerBase)
        {

            objExternalService = objExternalServiceBase;
        }

        /// <summary>
        /// Recuperar datospersonales
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param> 

        /// <returns></returns>
        public ResultadoBase RecuperarCifClienteNav(string strIdentifier, int IdClienteNav, string IdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCifClienteNav), strIdentifier, IdClienteNav.ToString(), IdUsuarioPro);

            ResultadoCadena objResultado = new ResultadoCadena();

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    if (IdClienteNav <= 0)
                    {

                        var objUsuario = objExternalService.RecuperarUsuarioPro(IdUsuarioPro);

                        if (objUsuario != null)
                        {

                            IdClienteNav = objUsuario.idClienteNav;
                        }
                        else
                        {
                            IdClienteNav = -1;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Usuario no encontrado";
                        }
                    }

                    if (IdClienteNav > 0)
                    {
                        var objCliente = objExternalService.RecuperarClienteNavision((int)IdClienteNav);

                        if ((objCliente != null) && (objCliente.IdClienteNav >0))
                        {
                            objResultado.sDescripcion = "Cliente Nav " + IdClienteNav.ToString() + " encontrado con CIF " + objCliente.sCifNif;
                            objResultado.sResultado = objCliente.sCifNif;
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Cliente no encontrado";
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Cliente no encontrado";
                    }

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objLogger.LogDebug("Error --> {0} {1} ", nameof(RecuperarCifClienteNav), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarCifClienteNav), ex.Message);
                objResultado.sDescripcion = ex.Message;
            }

            objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarCifClienteNav), objResultado.sDescripcion);
            return objResultado;

        }

        public ResultadoBase RecuperarModelo347PDF(string strIdentifier, string strCIF, int intAnyo)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarModelo347PDF), strIdentifier, strCIF, intAnyo);
            ResultadoOperacion<Modelo347PDFNav> objResultado = new ResultadoOperacion<Modelo347PDFNav>();

            try
            {

                if (IsValidoIdentificador(strIdentifier))
                {


                    var objResultadoNav = (Navision.Vistas.Cliente.Modelo347PDF)objExternalService.RecuperarModelo347Pdf(strCIF, intAnyo);


                    if ((objResultadoNav != null) && !String.IsNullOrWhiteSpace(objResultadoNav.strModelo347FileName))
                    {
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Recuperado modelo 347 con nombre " + objResultadoNav.strModelo347FileName;

                        objResultado.oResultado = new Modelo347PDFNav();
                        objResultado.oResultado.strNombreArchivo = objResultadoNav.strModelo347FileName;
                        objResultado.oResultado.strArchivoBase64 = objResultadoNav.strModelo347FileContent;

                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Error al recuperar el modelo 347 con cif " + strCIF + " y año " + intAnyo.ToString();
                    }

                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarModelo347PDF), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarModelo347PDF), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarModelo347PDF), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }

            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarModelo347PDF), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;
        }

        public ResultadoBase RecuperarProductosClienteNav(string strIdentifier, int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarProductosClienteNav), strIdentifier, IdClienteNav.ToString());
            ResultadoLista<ProductoCliente> objResultado = new ResultadoLista<ProductoCliente>();

            try
            {

                if (IsValidoIdentificador(strIdentifier))
                {


                    var objResultadoNav = (List<ProductoCliente>)objExternalService.RecuperarProductosCliente(IdClienteNav);


                    if (objResultadoNav != null)
                    {
                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Recuperado " + objResultadoNav.Count().ToString() + " producto(s).";
                        objResultado.lstResultado = objResultadoNav;
                        objResultado.iContador = objResultadoNav.Count();
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Error al recuperar productos con ClientNav " + IdClienteNav.ToString();
                    }

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarModelo347PDF), "Identificador no válido");
                    objResultado.sDescripcion = "Identificador no válido";
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarProductosClienteNav), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarProductosClienteNav), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }

            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarProductosClienteNav), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;
        }


        public ResultadoBase RecuperarInformacionCuenta(string strIdentifier, int IdClienteNav, int IdClienteCrm)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarInformacionCuenta), IdClienteNav.ToString(), IdClienteCrm.ToString());

            ResultadoOperacion<Cliente> objResultado = new ResultadoOperacion<Cliente>();

            try
            {

                if (IsValidoIdentificador(strIdentifier))
                {
                    int iTipoBusqueda = IdClienteNav > 1000000 ? 3 : 1;

                    if (iTipoBusqueda == 1)
                    {

                        var objCliente = objExternalService.RecuperarInformacionCuenta(IdClienteNav, -1);
                        if (objCliente != null)
                        {
                            objResultado.sDescripcion = "Recuperado Cliente con IdClienteNav " + objCliente.cod_navision;
                            objResultado.eResultado = enumResultado.Ok_Recuperar;

                            Cliente objResultadoCliente = new Cliente();


                            objResultadoCliente.cifNif = objCliente.documento;
                            objResultadoCliente.indGranCuenta = String.IsNullOrEmpty(objCliente.gran_cuenta) ? 0 : Convert.ToInt32(objCliente.gran_cuenta);
                            objResultadoCliente.codCuentaNavision = String.IsNullOrEmpty(objCliente.cod_navision) ? 0 : Convert.ToInt32(objCliente.cod_navision);
                            objResultadoCliente.nombreCompleto = objCliente.nombre;
                            objResultadoCliente.numTdcSugar = objCliente.num_kojak;
                            objResultadoCliente.email = objCliente.emails != null ? string.Join(";", objCliente.emails) : string.Empty;
                            objResultadoCliente.provincia = objCliente.provincia;
                            objResultadoCliente.razonSocial = objCliente.razon_social;
                            objResultadoCliente.telefono = objCliente.telefono_ppal;
                            objResultado.oResultado = objResultadoCliente;
                        }
                        else
                        {
                            objResultado.sDescripcion = "No se ha recuperado ningún Cliente";
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                    }
                    else
                    {
                        var objListadoClientes = (List<ClienteBusqueda>)objExternalService.RecuperarClientesBusqueda(IdClienteNav, -1, (short)iTipoBusqueda, -1, 0, 1, objConfig.cfgIdUsuarioApi);
                        if ((objListadoClientes != null) && (objListadoClientes.Count > 0))
                        {
                            ClienteBusqueda objClienteBusqueda = objListadoClientes[0];
                            objResultado.sDescripcion = "Recuperado Cliente con IdClienteNav " + objClienteBusqueda.CodCuentaNavision;
                            objResultado.eResultado = enumResultado.Ok_Recuperar;

                            Cliente objResultadoCliente = new Cliente();

                            objResultadoCliente.cifNif = objClienteBusqueda.CifNif;
                            objResultadoCliente.indGranCuenta = objClienteBusqueda.IndGranCuenta.HasValue ? objClienteBusqueda.IndGranCuenta.Value : 0;
                            objResultadoCliente.codCuentaNavision = objClienteBusqueda.CodCuentaNavision.HasValue ? objClienteBusqueda.CodCuentaNavision.Value : 0;
                            objResultadoCliente.nombreCompleto = objClienteBusqueda.NombreCompleto;
                            objResultadoCliente.numTdcSugar = objClienteBusqueda.NumTdcSugar.HasValue ? objClienteBusqueda.NumTdcSugar.Value : 0;
                            objResultadoCliente.razonSocial = objClienteBusqueda.RazonSocial;
                            objResultadoCliente.telefono = objClienteBusqueda.Telefono;
                            objResultadoCliente.email = objClienteBusqueda.Email;
                            objResultadoCliente.provincia = objClienteBusqueda.Provincia;
                            objResultado.oResultado = objResultadoCliente;
                        }
                        else
                        {
                            objResultado.sDescripcion = "No se ha recuperado ningún Cliente";
                            objResultado.eResultado = enumResultado.Error_Validacion;
                        }
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarInformacionCuenta), "Identificador no válido");
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarInformacionCuenta), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarInformacionCuenta), ex.Message);

                objResultado.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("END -> Resultado {0} --> {1}", nameof(RecuperarInformacionCuenta), objResultado.sDescripcion);
            return objResultado;

        }

      
    }
}
