﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Xml.Serialization;

namespace AreaPersonal.Servicio
{
    public class CommToolsService : BaseService, ICommToolsService<Revista>

    {
     
        public CommToolsService(
            IOptions<OptConfig> configWeb,
            ILogger<CommToolsService> logger) : base(configWeb, logger)
        {
          
        }

        private Revista GetRevista(int idRevista)
        {
            Revista newItem = null;

            string requestURL = string.Format(_objVariables.urlRevistas, idRevista.ToString());

            string responseResult;
            Stream newStream;
            WebResponse responseweb;
            StreamReader reader;
            WebRequest request = WebRequest.Create(requestURL);
            request.Method = "GET";
            request.ContentType = "application/xml";
            responseweb = request.GetResponse();
            newStream = responseweb.GetResponseStream();

            reader = new StreamReader(newStream);
            responseResult = reader.ReadToEnd();
            //responseResult = responseResult.Remove(0, responseResult.IndexOf("![CDATA") + 2);
            XmlSerializer serializer = new XmlSerializer(typeof(Revista));
            //responseResult = "<pymes>" + responseResult + "</pymes>";

            StringReader stringReader = new StringReader(responseResult);

            newItem = (Revista)serializer.Deserialize(stringReader);
            reader.Close();
            stringReader.Close();

            return newItem;
        }

        /// <summary>
        /// Metodo para recuperar las revistas de un Usuario Pro
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Revista>> RecuperarRevistas(string strIdentifier, string strUsuarioPro)
        {
            _logger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(RecuperarRevistas), strIdentifier, strUsuarioPro);

            
            try
            {
                if (IsValidIdentifier(strIdentifier))
                {

                    List<Revista> objListResult = new List<Revista>();
                    List<UsuarioAreas> objResult = new List<UsuarioAreas>();
                        // Otener la url de login en producto del appSettings

                        string urlBase = MyConfig.Services.CommToolsApi + "/Areas/GetUsuariosProAreas?idUsuarioPro={0}";

                        string url= String.Format(urlBase, strUsuarioPro);

                        HttpClient client = new HttpClient();

                        HttpResponseMessage response = client.GetAsync(url).Result;
                        if (response.IsSuccessStatusCode)
                        {

                            objResult = JsonConvert.DeserializeObject<List<UsuarioAreas>>(response.Content.ReadAsStringAsync().Result.ToString());
                        }

                    if ((objResult != null) && (objResult.Count > 0))
                    {
                        _logger.LogInformation("START --> {0} Recuperados {1} revistas", nameof(RecuperarRevistas), strIdentifier, objResult.Count.ToString());
                        foreach (UsuarioAreas item in objResult)
                        {
                            if ((item.IdExterno != null) && (item.IdExterno>0))
                            {
                                Revista itemNew = this.GetRevista(item.IdExterno.Value);

                                if (itemNew != null)
                                {
                                    objListResult.Add(itemNew);
                                }
                            }
                        }
                    }


                    return objListResult;
                }
                else
                {
                    _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarRevistas), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(RecuperarRevistas), ex.Message);
                return null;
            }
        }

       

    }
}
