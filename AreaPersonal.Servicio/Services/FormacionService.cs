﻿
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;
using DllApiBase.Vistas.Claves.Usuario;

namespace AreaPersonal.Servicio
{
    public class FormacionService : AreaPersonalBaseService, IFormacionService<Cliente,UsuarioProducto, PlanFormacionEmail, PlanFormacionCursoEmail, Evento, EventoExtendido, UsuarioPro>

    {
        private readonly FormacionSvc.IWcfFormacion _svcFormacion;
        //private readonly ClavesSvc.IServiceClaves _svcClaves;
        //private readonly ILogger<FormacionService> objLogger;
        //private readonly ObjVariables _objVariables;


        public FormacionService(
            IOptions<ConfigApi> configWeb,
            ILogger<FormacionService> logger) : base(configWeb, logger)
        {
            //objLogger = logger;


            var basicHttpBindingFormacion = new BasicHttpBinding();
            var endpointAddressFormacion = new EndpointAddress(objConfigBase.cfgServices.FormacionSvc);
            _svcFormacion = new FormacionSvc.WcfFormacionClient(basicHttpBindingFormacion, endpointAddressFormacion);

            //var basicHttpBindingClaves = new BasicHttpBinding();
            //var endpointAddressClaves = new EndpointAddress(objConfigBase.cfgServices.ClavesSvc);
            //_svcClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);

            //_objVariables = MyConfig.Variables;
        }


    



        public async Task<IEnumerable<PlanFormacionEmail>> RecuperarPlanesByEmail(string strIdentifier, string strUsuarioPro, string strEmail)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarPlanesByEmail), strIdentifier, strUsuarioPro, strEmail);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstPlanes = await _svcFormacion.GetPlanesByEmailByEntradaAsync(strEmail, strUsuarioPro);

                    var listaJson = new List<PlanFormacionEmail>();
                    foreach (var item in lstPlanes.beanList)
                    {
                        listaJson.Add(new PlanFormacionEmail
                        {
                            AssistProgramsNumber = item.AssistProgramsNumber,
                            Desc_familia = item.Desc_familia,
                            Id = item.Id,
                            Id_familia = item.Id_familia,
                            Name = item.Name,
                            PastRegisterProgramsNumber = item.PastRegisterProgramsNumber,
                            RegisterProgramsNumber = item.RegisterProgramsNumber,
                            TotalPrograms = item.TotalPrograms

                        });
                    }

                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarPlanesByEmail), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarPlanesByEmail), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarPlanesByEmail), ex.Message);
                return null;
            }

        }



        public async Task<IEnumerable<PlanFormacionCursoEmail>> RecuperarCursosPlanByEmail(string strIdentifier, int IdPlan, string strEmail)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCursosPlanByEmail), strIdentifier, IdPlan, strEmail);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstCursos = await _svcFormacion.GetPlanProgramsByEmailAsync(IdPlan, strEmail);

                    var listaJson = new List<PlanFormacionCursoEmail>();
                    foreach (var item in lstCursos.beanList)
                    {
                        listaJson.Add(new PlanFormacionCursoEmail
                        {
                            AssistDate = item.AssistDate,
                            CursoWebexId = item.CursoWebexId,
                            PastSessionDate = item.PastSessionDate,
                            ProgramId = item.ProgramId,
                            ProgramName = item.ProgramName,
                            ProgramType = item.ProgramType,
                            ProgramTypeDesc = item.ProgramTypeDesc,
                            RegisterProgramInd = item.RegisterProgramInd,
                            SessionDate = item.SessionDate,
                            WebexId = item.WebexId
                        });


                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCursosPlanByEmail), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCursosPlanByEmail), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarCursosPlanByEmail), ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Evento>> RecuperarSesiones(string strIdentifier, int cursoWebexId, int IdUser)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarSesiones), strIdentifier, cursoWebexId.ToString(), IdUser.ToString());

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstSessions = await _svcFormacion.GetProgramEventsAsync(cursoWebexId, IdUser);

                    var listaJson = new List<Evento>();
                    foreach (var item in lstSessions.beanList)
                    {
                        listaJson.Add(new Evento
                        {
                            AbsenteeEmail = item.AbsenteeEmail,
                            AbsenteeEmailSended = item.AbsenteeEmailSended,
                            AssistInd = item.AssistInd,
                            ProgramId = item.ProgramId,
                            ProgramName = item.ProgramName,
                            AttendeeCount = item.AttendeeCount,
                            Description = item.Description,
                            Duration = item.Duration,
                            EndDate = item.EndDate,
                            EndDateProxy = item.EndDateProxy,

                            EntryExitTone = item.EntryExitTone,
                            Family = item.Family,
                            FamilyDescription = item.FamilyDescription,
                            GreetingEmail = item.GreetingEmail,
                            GreetingEmailSended = item.GreetingEmailSended,
                            Id = item.Id,
                            ImagePath = item.ImagePath,
                            ListStatus = item.ListStatus,
                            Mute = item.Mute,
                            Name = item.Name,

                            ParticipantLimit = item.ParticipantLimit,
                            Password = item.Password,
                            ProgramWebExId = item.ProgramWebExId,
                            ReminderEmail = item.ReminderEmail,
                            ReminderEmail2 = item.ReminderEmail2,
                            ReminderEmailSended = item.ReminderEmailSended,
                            ReminderEmailSended2 = item.ReminderEmailSended2,
                            ReminderEmailTime = item.ReminderEmailTime,
                            ReminderEmailTime2 = item.ReminderEmailTime2,
                            SessionSubtypeDescription = item.SessionSubtypeDescription,

                            SessionSubtypeId = item.SessionSubtypeId,
                            SessionTypeDescription = item.SessionTypeDescription,
                            SessionTypeId = item.SessionTypeId,
                            StartDate = item.StartDate,
                            StartDateProxy = item.StartDateProxy,
                            Status = item.Status,
                            TimeZoneId = item.TimeZoneId,
                            Type = item.Type,
                            Vacancy = item.Vacancy,
                            ViewAttendeeList = item.ViewAttendeeList,
                            WebExHostId = item.WebExHostId,
                            WebExId = item.WebExId



                        });
                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSesiones), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSesiones), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarSesiones), ex.Message);
                return null;
            }

        }

        public async Task<EventoExtendido> RecuperarSesionExtendido(string strIdentifier, int WebexId)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}{2}", nameof(RecuperarSesionExtendido), strIdentifier, WebexId);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var objSession = await _svcFormacion.GetSessionExtendedAsync(WebexId);
                    var objJSon = new EventoExtendido();

                    if ((objSession != null) && (objSession.bean != null))
                    {
                     

                        objJSon.valor = objSession.bean.valor;
                        objJSon.titulo = objSession.bean.titulo;
                        objJSon.resumencontenido = objSession.bean.resumencontenido;
                        objJSon.modalidad = objSession.bean.modalidad;
                        objJSon.precio = objSession.bean.precio;
                        objJSon.nivel = objSession.bean.nivel;

                        objJSon.recomendadopara = objSession.bean.recomendadopara;
                        objJSon.observaciones = objSession.bean.observaciones;
                        objJSon.requisitostecnicos = objSession.bean.requisitostecnicos;
                        objJSon.enlaceayuda = objSession.bean.enlaceayuda;
                        objJSon.imagen = objSession.bean.imagen;
                        objJSon.WebExId = objSession.bean.WebExId;

                        objJSon.Name = objSession.bean.Name;
                        objJSon.Description = objSession.bean.Description;
                        objJSon.Duration = objSession.bean.Duration;
                        objJSon.StartDate = objSession.bean.StartDate;
                        objJSon.StartDateProxy = objSession.bean.StartDateProxy;

                        objJSon.EndDate = objSession.bean.EndDate;
                        objJSon.impartidopor = objSession.bean.impartidopor;
                        objJSon.textoinscripcion = objSession.bean.textoinscripcion;
                        objJSon.numeroplazasvisibles = objSession.bean.numeroplazasvisibles;
                        objJSon.numeroplazasreales = objSession.bean.numeroplazasreales;
                        objJSon.ultimasplazas = objSession.bean.ultimasplazas;

                    }
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSesionExtendido), "Identificador no válido");

                    return objJSon;


                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarSesionExtendido), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarSesionExtendido), ex.Message);
                return null;
            }
        }

    }
}
