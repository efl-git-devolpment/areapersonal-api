﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;


namespace AreaPersonal.Servicio
{
    public class DocumentacionService : BaseService, IDocumentacionService<Cliente, UsuarioProducto, TipoDocumento, ResultadoGuardar>

    {
        private readonly DocumentacionSvc.IServiceGestionDocumentacion _svcDocumentacion;
        //private readonly ClavesSvc.IServiceClaves _svcClaves;
        //private readonly ILogger<DocumentacionService> _logger;
        private readonly IDocumentacionAdvService _model;

        public DocumentacionService(
            IOptions<OptConfig> configWeb,
            IDocumentacionAdvService model,
            ILogger<DocumentacionService> logger) : base(configWeb, logger)
        {
            //_logger = logger;

            _model = model;

            //var basicHttpBindingClaves = new BasicHttpBinding();
            //var endpointAddressClaves = new EndpointAddress(MyConfig.Services.ClavesSvc);
            //_svcClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);

            var basicHttpBindingDocumentacion = new BasicHttpBinding();
            var endpointAddressDocumentacion = new EndpointAddress(MyConfig.Services.DocumentacionSvc);
            _svcDocumentacion = new DocumentacionSvc.ServiceGestionDocumentacionClient(basicHttpBindingDocumentacion, endpointAddressDocumentacion);

        }


        //public async Task<Cliente> RecuperarCliente(string strIdentifier, int IdClienteNav, int IdUserApp)
        //{

        //    _logger.LogInformation("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCliente), strIdentifier, IdClienteNav.ToString(), IdUserApp.ToString());

        //    try
        //    {


        //        if (IsValidIdentifier(strIdentifier))
        //        {
        //            int IntNumTdcSugar = 0;
        //            int IntCodCuentaNavision = IdClienteNav;
        //            string StrIdUsuarioPro = String.Empty;
        //            string StrNombreCompleto = String.Empty;
        //            string StrRazonSocial = String.Empty;
        //            string StrPhoneOffice = String.Empty;
        //            string StrCifNif = String.Empty;
        //            string StrEmail = String.Empty;
        //            string StrLogin = String.Empty;
        //            string StrIp = String.Empty;
        //            int IntIndTipoBusqueda = IdClienteNav > 1000000 ? 3 : 1;
        //            int IntIdTipoCliente = 0;
        //            string StrCampoOrdenar = String.Empty;
        //            int IntIndOrdenarAsc = 0;
        //            int IntPaginaInicial = 0;
        //            int IntNumeroDeFilas = 1;

        //            int IntIdUsuarioApp = IdUserApp;

        //            var lstClientes = await _svcClaves.RecuperarClientesTempAsync(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
        //                StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
        //                IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
        //                IntNumeroDeFilas, IntIdUsuarioApp);

        //            if ((lstClientes != null) && (lstClientes.beanList != null) && (lstClientes.beanList.Count > 0))
        //            {
        //                //log.Info("getCliente.Fin : Encontrado " + oList.beanList.Count.ToString());
        //                ClavesSvc.Cliente oCliente = lstClientes.beanList[0];

        //                var objJSon = new Cliente();

        //                objJSon.cifNif = oCliente._cifNif;
        //                objJSon.indGranCuenta = oCliente._indGranCuenta;
        //                objJSon.codCuentaNavision = oCliente._codCuentaNavision;
        //                objJSon.nombreCompleto = oCliente._nombreCompleto;
        //                objJSon.numTdcSugar = oCliente._numTdcSugar;
        //                objJSon.provincia = oCliente._provincia;
        //                objJSon.razonSocial = oCliente._razonSocial;
        //                objJSon.telefono = oCliente._telefono;

        //                _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), objJSon.codCuentaNavision.ToString());
        //                return objJSon;

        //            }
        //            else
        //            {
        //                _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), "No hay cliente");
        //                return null;
        //            }

        //        }
        //        else
        //        {
        //            _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarCliente), "Identificador no válido");
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogInformation("Error --> {0} {1} ", nameof(RecuperarCliente), ex.Message);
        //        return null;
        //    }



        //}
        
        public async Task<IEnumerable<TipoDocumento>> RecuperarTipoDocumento(string strIdentifier)
        {
            _logger.LogInformation("START --> {0} con parámetros {1}", nameof(RecuperarTipoDocumento), strIdentifier);

            try
            {
                if (IsValidIdentifier(strIdentifier))
                {
                    var lstProvinces = await _svcDocumentacion.RecuperarTipoDocumentoAsync();

                    var listaJson = new List<TipoDocumento>();
                    foreach (var item in lstProvinces.beanList)
                    {
                        listaJson.Add(new TipoDocumento
                        {
                            idTipoDocumento = item._idTipoDocumento,
                            descTipoDocumento = item._descTipoDocumento,
                            //codigoProvincia = item._codigoProvincia
                        });
                    }
                    _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoDocumento), listaJson.Count.ToString());
                    return listaJson;
                }
                else
                {
                    _logger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarTipoDocumento), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(RecuperarTipoDocumento), ex.Message);
                return null;
            }

        }


        private BeanList<DocumentacionSvc.Usuario> getUserByTipo(int iIdType, int iIndColaboradorExt, int iIndColaboradorRed, int iIndConsultor)
        {

            try
            {
                Task<BeanList<DocumentacionSvc.Usuario>> objResult = null;
                objResult = _svcDocumentacion.RecuperarUsuariosByTipoAsync(iIdType, iIndColaboradorExt, iIndColaboradorRed, iIndConsultor);

                return objResult.Result;
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(getUserByTipo), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(getUserByTipo), ex.Message);
                return null;
            }

           
        }

        public ResultadoGuardar SaveDocumento(string strIdentifier, string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTlfno, string strTitular, string sNameSugar, string strTipoDocumento,  int iCodCRM = -1)
        {
            ResultadoGuardar objResultado = new ResultadoGuardar();

            try
            {
                if (!IsValidIdentifier(strIdentifier))
                {
                    _logger.LogInformation("Resultado {0} --> {1}", nameof(SaveDocumento), "Identificador no válido");

                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "Identificador no válido";
                    return objResultado;
                }

                _logger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(SaveDocumento), strIdentifier, "@iCodCRM: " + iCodCRM.ToString() + ", @iCodNavision: " + iCodNavision.ToString() + ", @iTipoDocumento: " + iTipoDocumento.ToString() +
                    "@strTlfno: " + strTlfno + ", @strPeticion (length): " + strPeticion.Length.ToString() + ", @strTipoDocumento: " + strTipoDocumento  + ", @sNameSugar: " + sNameSugar +
                    "@strNombre: " + strNombre + ", @strApellidos: " + strApellidos + ", @strEmail: " + strEmail + ", @sUsuarioPro: " + sUsuarioPro);


                string sDescription = String.IsNullOrEmpty(strPeticion) ? String.Empty : strPeticion;

                int iIdDocumento = -1;
                bool bSendEmail = false;
                int iResult = -1;
                int iIdEstado = 1;

                DocumentacionSvc.Documento oDocument = new DocumentacionSvc.Documento();

                oDocument._idDocumentoEstado = iIdEstado;


                oDocument._apellidos = strApellidos;
                oDocument._email = strEmail;
                oDocument._fechaPeticion = Convert.ToString(DateTime.Now);
                oDocument._idDocumentoSubEstado = -1;
                oDocument._idTipoDocumento = iTipoDocumento;
                oDocument._idUsuarioPro = sUsuarioPro;
                oDocument._nombre = strNombre;
                oDocument._peticion = strPeticion;
                oDocument._tlfno = strTlfno;
                oDocument._idClienteNav = iCodNavision;
                oDocument._descTipoDocumento = strTipoDocumento;
                oDocument._observaciones = String.Empty;
                oDocument._idColaborador = -1;

                int iUserApp = -1;

                iResult = _model.AddDocumento(oDocument);

                if (iResult < 0)
                {
                    // Return -1 por defecto.
                    // return error.
                    objResultado.bEnviarEmail = false;
                    objResultado.iIdConsulta = -1;
                    objResultado.iResultadoOperacion = -1;
                    objResultado.descResultadoOperacion = "Error en el guardado del documento.";

                    return objResultado;
                }
                else
                {
                    bSendEmail = true;

                    iIdDocumento = iResult > 0 ? iResult : iIdDocumento;

                    iResult = 1;

                    if ((iIdDocumento > 0) && (bSendEmail))
                    {

                        BeanList<DocumentacionSvc.Usuario> alistUsers = this.getUserByTipo(iTipoDocumento, -1, -1, 1);

                        if ((alistUsers != null) && (alistUsers.beanList != null))
                        {
                            //log.Info("DocumentacionController.SaveDocumento: Encontrados " + alistUsers.beanList.Count.ToString() + " consultores");

                            if (alistUsers.beanList.Count == 1)
                            {

                                iUserApp = alistUsers.beanList[0]._idUsuario;



                                int iResultAssign = _model.AssingUser(iIdDocumento, iUserApp);

                                if (iResultAssign >= 0)
                                {
                                    iResult = 2;


                                    int iResultSendEmail = -1;

                                    ClavesSvc.UsuarioApp oUser = this.GetUserApp(iUserApp);

                                    if (oUser != null)
                                    {
                                        iResultSendEmail = _model.SendEmailAssing(iCodCRM, oDocument, iIdDocumento, oUser, strTitular, iCodNavision);
                                        if (iResultSendEmail >= 0) iResult = 3;
                                    }
                                    else
                                    {
                                        iResult = 6;
                                    }

                                }
                               
                            }

                        }
                    }
                    else
                    {
                        _logger.LogInformation("{0} No se han encontado consultores ", nameof(SaveDocumento));

                    }

                    if (_model.Check_NonPayment(iIdDocumento, iCodCRM, iCodNavision))
                    {
                        bSendEmail = false;
                    }
                }




                //log.Info("Question.SaveQuestion : @iResult: " + iResult.ToString());
                _logger.LogInformation("{0} Resultado de Guardar --> {1} ", nameof(SaveDocumento), iResult.ToString());

                objResultado.iResultadoOperacion = iResult;
                objResultado.iIdConsulta = iIdDocumento;
                objResultado.bEnviarEmail = bSendEmail;
                objResultado.descResultadoOperacion = "Petición guardada en el sistema.";
            }
           
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(SaveDocumento), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(SaveDocumento), ex.Message);

                objResultado.iResultadoOperacion = -2;
                objResultado.iIdConsulta = -1;
                objResultado.bEnviarEmail = false;
                objResultado.descResultadoOperacion = "Error " + ex.Message;
            }

            return objResultado;
        }

       

        public int SendEmailMessageAlta(string strIdentifier, int iCodCRM, string strEmail, string strName, int iIdDocument)
        {
            int intResult = -1;
            try
            {
                _logger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(SendEmailMessageAlta), strIdentifier, iCodCRM.ToString(), strEmail, strName, iIdDocument.ToString());



                intResult = _model.SendEmailAddDocument(iCodCRM, strEmail, strName, iIdDocument);



            }
            catch (Exception ex)
            {
                _logger.LogInformation("Error --> {0} {1} ", nameof(SendEmailMessageAlta), ex.Message);
                _logger.LogError("Error --> {0} {1} ", nameof(SendEmailMessageAlta), ex.Message);

            }
            _logger.LogInformation("{0} con Resultado ", nameof(SendEmailMessageAlta), intResult.ToString());
            return intResult;
        }
    }
}