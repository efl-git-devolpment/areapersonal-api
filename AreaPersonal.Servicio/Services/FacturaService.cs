﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public class FacturaService : AreaPersonalBaseService, IFacturaService<ResultadoFacturas, ResultadoAbonos, ResultadoGenerarFactura, ResultadoGenerarAbono>

    {


        private readonly NavisionSvc.IServiceNavision _svcNavision;


        public FacturaService(
            IOptions<ConfigApi> configWeb,
            ILogger<FacturaService> logger) : base(configWeb, logger)
        {
            //objLogger = logger;

            var basicHttpBindingNavision = new BasicHttpBinding();
            var endpointAddressNavision = new EndpointAddress(objConfigBase.cfgServices.NavisionSvc);
            _svcNavision = new NavisionSvc.ServiceNavisionClient(basicHttpBindingNavision, endpointAddressNavision);
            //_objVariables = MyConfig.Variables;
        }

        /// <summary>
        /// Recuperar las facturas de un acceso
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="intCodNavision"></param> 
        /// <param name="strIdUsuarioPro"></param>
        /// <param name="numPage"></param>
        /// <param name="recsPerPage"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        public async Task<ResultadoFacturas> RecuperarFacturas(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7}", nameof(RecuperarFacturas), strIdentifier, intCodNavision, strIdUsuarioPro, numPage, recsPerPage, startdate, enddate);

            ResultadoFacturas oResult = new ResultadoFacturas();

            oResult.iResultado = -1;
            oResult.regTotal = -1;
            oResult.numPage = numPage;
            oResult.recsPerPage = recsPerPage;
            oResult.list = new List<Factura>();

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    var lstFacturas = await objServiceClaves.RecuperarUsuarioFacturasAsync(intCodNavision, strIdUsuarioPro, startdate, enddate, numPage, recsPerPage);



                    if ((lstFacturas.ObjResult != null) && (lstFacturas.beanList != null) && (lstFacturas.ObjResult.IntResult >= 0))
                    {
                        oResult.regTotal = lstFacturas.ObjResult.IntTotalRecs;
                        oResult.iResultado = lstFacturas.ObjResult.IntResult;

                        var listaFacturas = new List<Factura>();
                        foreach (var item in lstFacturas.beanList)
                        {
                            listaFacturas.Add(new Factura
                            {
                                idFacturaNav = item._idFacturaNav,
                                idClienteFacturacionNav = item._idClienteFacturacionNav,
                                idClienteEnvioNav = item._idClienteEnvioNav,
                                importe = item._importe,
                                fecha = item._fecha,
                                fechaVencimiento = item._fechaVencimiento
                            });
                        }
                        objLogger.LogInformation("Resultado {0} --> Total facturas {1}", nameof(RecuperarFacturas), listaFacturas.Count.ToString());
                        oResult.descResultado = "OK";
                        oResult.list = listaFacturas;
                    }
                    else
                    {
                        oResult.iResultado = lstFacturas.ObjResult != null ? lstFacturas.ObjResult.IntResult : -1;
                        oResult.descResultado = lstFacturas.ObjResult != null ? lstFacturas.ObjResult.StrDescription: "Error al recuperar Facturas";
                    }
                }
                else
                {
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturas), "Identificador no válido");
                    oResult.descResultado = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                oResult.descResultado = ex.Message;
            }

            return oResult;

        }

        /// <summary>
        /// Recuperar identicador de descarga de una factura
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="idFactura"></param>
        /// <returns></returns>
        private async Task<int> RecuperarFacturaInfo(string idFactura, int intCodNavision)
        {
            int iResult = -1;
            objLogger.LogInformation("START --> {0} con parámetros {1},{2}", nameof(RecuperarFacturaInfo), idFactura, intCodNavision);

            try
            {

                var cliente = getClienteFromNav(intCodNavision);
                string strVatNumber = String.Empty;

                if (cliente != null)
                {
                    strVatNumber = cliente._cifNif;

                    if (String.IsNullOrEmpty(strVatNumber))
                    {
                        iResult = -20;
                    }
                    else
                    {
                        iResult = await _svcNavision.GenerarFacturaVatAsync(idFactura, strVatNumber);
                    }
                }
                else
                {
                    iResult = -3;
                }

            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);

            }

            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaInfo), iResult.ToString());
            return iResult;

        }

        ///// <summary>
        ///// Recuperar PDF de la Factura
        ///// </summary>
        ///// <param name="strIdentifier"></param>
        ///// <param name="idQueueFactura"></param>
        ///// <returns></returns>
        //public async Task<ResultadoGenerarFactura> RecuperarFacturaPDF(string strIdentifier, int idQueueFactura)
        //{
        //    objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(RecuperarFacturaInfo), strIdentifier, idQueueFactura);

        //    ResultadoGenerarFactura oResult = new ResultadoGenerarFactura();
        //    try
        //    {
        //        if (IsValidoIdentificador(strIdentifier))
        //        {

        //            var oPathFactura = await _svcNavision.RecuperarPathFacturaAsync(idQueueFactura);

        //            if ((oPathFactura != null) && (oPathFactura.bean != null))
        //            {
        //                oResult.estado = oPathFactura.bean._estado;
        //                oResult.idColaFactura = oPathFactura.bean._idColaFactura;
        //                oResult.mensajeError = oPathFactura.bean._mensajeError;
        //                oResult.idFacturaNav = oPathFactura.bean._idFacturaNav;
        //                oResult.path = oPathFactura.bean._path;

        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDF), "Encontrado Factura " + oResult.idFacturaNav);

        //            }
        //            else
        //            {
        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDF), "La Factura no existe");
        //            }
        //        }
        //        else
        //        {
        //            oResult.idFacturaNav = String.Empty;
        //            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaInfo), "Identificador no válido");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        oResult.idFacturaNav = "-1";
        //        objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);
        //        objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);
        //    }
        //    return oResult;
        //}

        /// <summary>
        /// Recuperar PDF de la Factura
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="idQueueFactura"></param>
        /// <param name="intCodNavision"></param>
        /// <returns></returns>
        public async Task<ResultadoGenerarFactura> RecuperarFacturaPDFById(string strIdentifier, string idFactura, int intCodNavision)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(RecuperarFacturaPDFById), strIdentifier, idFactura, intCodNavision);


            ResultadoGenerarFactura oResult = new ResultadoGenerarFactura();
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                { 
                
                    int idQueueFactura = await this.RecuperarFacturaInfo(idFactura, intCodNavision);

                    if (idQueueFactura > 0)
                    {
                        var oPathFactura = await _svcNavision.RecuperarPathFacturaAsync(idQueueFactura);

                        if ((oPathFactura != null) && (oPathFactura.bean != null))
                        {
                            oResult.estado = oPathFactura.bean._estado;
                            oResult.idColaFactura = oPathFactura.bean._idColaFactura;
                            oResult.mensajeError = oPathFactura.bean._mensajeError;
                            oResult.idFacturaNav = oPathFactura.bean._idFacturaNav;
                            oResult.path = oPathFactura.bean._path;

                            oResult.iResultadoOperacion = 1;
                            oResult.descResultadoOperacion = "Factura encontrada";

                            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDFById), "Encontrado Factura " + oResult.idFacturaNav);

                        }
                        else
                        {
                            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDFById), "La Factura no existe");
                            oResult.iResultadoOperacion = -1;
                            oResult.descResultadoOperacion = "Factura no encontrada";
                        }
                    }
                    else
                    {
                        oResult.iResultadoOperacion = -1;

                        switch (idQueueFactura)
                        {
                            case -3: oResult.descResultadoOperacion = "Cliente no existe : " + intCodNavision.ToString(); break;
                            case -10: oResult.descResultadoOperacion = "Parametro Factura en blanco o vacio o null"; break;
                            case -20: oResult.descResultadoOperacion = "Parametro de VAT en blanco o vacio o null"; break;
                            case -30: oResult.descResultadoOperacion = "No se encuentra la referencia de Factura en la BBDD"; break;
                            case -40: oResult.descResultadoOperacion = "Factura no encontrada para el cliente " + intCodNavision.ToString(); break;
                            default: oResult.descResultadoOperacion = "Factura no encontrada"; ; break;
                        }
                    
                        
                        objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDFById), "La Factura no existe");
                    }
                }
                else
                {
                    oResult.iResultadoOperacion = -2;
                    oResult.descResultadoOperacion = "Identificador no válido";

                    oResult.idFacturaNav = String.Empty;
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaPDFById), "Identificador no válido");
                }

            }
            catch (Exception ex)
            {
                oResult.idFacturaNav = "-1";

                oResult.iResultadoOperacion = -3;
                oResult.descResultadoOperacion = ex.Message;

                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarFacturaPDFById), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarFacturaPDFById), ex.Message);
            }
            return oResult;
        }

        /// <summary>
        /// Recuperar los abonos de un acceso
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="intCodNavision"></param> 
        /// <param name="strIdUsuarioPro"></param>
        /// <param name="numPage"></param>
        /// <param name="recsPerPage"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        public async Task<ResultadoAbonos> RecuperarAbonos(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate)
        {

            objLogger.LogInformation("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7}", nameof(RecuperarAbonos), strIdentifier, intCodNavision, strIdUsuarioPro, numPage, recsPerPage, startdate, enddate);

            ResultadoAbonos oResult = new ResultadoAbonos();

            oResult.iResultado = -1;
            oResult.regTotal = -1;
            oResult.numPage = numPage;
            oResult.recsPerPage = recsPerPage;
            oResult.list = new List<Abono>();


            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var lstAbonos = await objServiceClaves.RecuperarUsuarioAbonosAsync(intCodNavision, strIdUsuarioPro, startdate, enddate, numPage, recsPerPage);

                    if ((lstAbonos.ObjResult != null) && (lstAbonos.beanList != null) && (lstAbonos.ObjResult.IntResult >= 0))
                    {
                        oResult.regTotal = lstAbonos.ObjResult.IntTotalRecs;
                        oResult.iResultado = lstAbonos.ObjResult.IntResult;



                        var listaAbonos = new List<Abono>();
                        foreach (var item in lstAbonos.beanList)
                        {
                            listaAbonos.Add(new Abono
                            {
                                idAbonoNav = item._idAbonoNav,
                                idClienteAbonoNav = item._idClienteAbonoNav,
                                idClienteEnvioNav = item._idClienteEnvioNav,
                                importe = item._importe,
                                fecha = item._fecha
                            });
                        }
                        objLogger.LogInformation("Resultado {0} --> Total abonos {1}", nameof(RecuperarFacturas), listaAbonos.Count.ToString());
                        oResult.descResultado = "OK";
                        oResult.list = listaAbonos;
                    }
                    else
                    {
                        oResult.iResultado = lstAbonos.ObjResult != null ? lstAbonos.ObjResult.IntResult : -1;
                        oResult.descResultado = lstAbonos.ObjResult != null ? lstAbonos.ObjResult.StrDescription : "Error al recuperar Facturas";
                    }
                }

                else
                {
                   

                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonos), "Identificador no válido");
                    oResult.descResultado = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarAbonos), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarAbonos), ex.Message);
                oResult.descResultado = ex.Message;
            }

            return oResult;

        }

        /// <summary>
        /// Recuperar identicador de descarga de un abono
        /// </summary>
        /// <param name="idAbono"></param>
        /// <param name="intCodNavision"></param>
        /// <returns></returns>
        private async Task<int> RecuperarAbono(string idAbono, int intCodNavision)
        {
            int iResult = -1;
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(RecuperarAbono), idAbono, intCodNavision);

            try
            {
                var cliente = getClienteFromNav(intCodNavision);
                string strVatNumber = String.Empty;

                if (cliente != null)
                {
                    strVatNumber = cliente._cifNif;
                }
                iResult = await _svcNavision.GenerarAbonoVatAsync(idAbono, strVatNumber);
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbono), iResult.ToString());
                 objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbono), "Identificador no válido");
                
            }
            catch (Exception ex)
            {
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarAbono), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarAbono), ex.Message);

            }
            return iResult;

        }

        ///// <summary>
        ///// Recuperar PDF de la Abono
        ///// </summary>
        ///// <param name="strIdentifier"></param>
        ///// <param name="idQueueAbono"></param>
        ///// <returns></returns>
        //public async Task<ResultadoGenerarAbono> RecuperarAbonoPDF(string strIdentifier, int idQueueAbono)
        //{
        //    objLogger.LogInformation("START --> {0} con parámetros {1}, {2}", nameof(RecuperarAbonoPDF), strIdentifier, idQueueAbono);

        //    ResultadoGenerarAbono oResult = new ResultadoGenerarAbono();
        //    try
        //    {
        //        if (IsValidoIdentificador(strIdentifier))
        //        {

        //            var oPathAbono = await _svcNavision.RecuperarPathAbonoAsync(idQueueAbono);

        //            if ((oPathAbono != null) && (oPathAbono.bean != null))
        //            {
        //                oResult.estado = oPathAbono.bean._estado;
        //                oResult.idColaAbono = oPathAbono.bean._idColaAbono;
        //                oResult.mensajeError = oPathAbono.bean._mensajeError;
        //                oResult.idAbonoNav = oPathAbono.bean._idAbonoNav;
        //                oResult.path = oPathAbono.bean._path;

        //                oResult.iResultadoOperacion = 1;
        //                oResult.descResultadoOperacion = "Abono encontrado";

        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDF), "Encontrado Abono " + oResult.idAbonoNav);

        //            }
        //            else
        //            {

        //                oResult.iResultadoOperacion = -1;
        //                oResult.descResultadoOperacion = "Abono no encontrado";

        //                objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDF), "El Abono no existe");
        //            }
        //        }
        //        else
        //        {
        //            oResult.iResultadoOperacion = -2;
        //            oResult.descResultadoOperacion = "Identificador no válido";

        //            oResult.idAbonoNav = String.Empty;
        //            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDF), "Identificador no válido");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        oResult.iResultadoOperacion = -3;
        //        oResult.descResultadoOperacion = ex.Message;

        //        oResult.idAbonoNav = "-1";
        //        objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarAbonoPDF), ex.Message);
        //        objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarAbonoPDF), ex.Message);
        //    }
        //    return oResult;
        //}


        /// <summary>
        /// Recuperar PDF de la Abono
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="idAbono"></param>
        /// <param name="intCodNavision"></param>
        /// <returns></returns>
        public async Task<ResultadoGenerarAbono> RecuperarAbonoPDFById(string strIdentifier, string idAbono, int intCodNavision)
        {
            objLogger.LogInformation("START --> {0} con parámetros {1}, {2}, {3}", nameof(RecuperarAbonoPDFById), strIdentifier, idAbono, intCodNavision);


            ResultadoGenerarAbono oResult = new ResultadoGenerarAbono();
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    int idQueueAbono = await this.RecuperarAbono(idAbono, intCodNavision);

                    if (idQueueAbono > 0)
                    {
                        var oPathAbono = await _svcNavision.RecuperarPathAbonoAsync(idQueueAbono);

                        if ((oPathAbono != null) && (oPathAbono.bean != null))
                        {
                            oResult.estado = oPathAbono.bean._estado;
                            oResult.idColaAbono = oPathAbono.bean._idColaAbono;
                            oResult.mensajeError = oPathAbono.bean._mensajeError;
                            oResult.idAbonoNav = oPathAbono.bean._idAbonoNav;
                            oResult.path = oPathAbono.bean._path;

                            oResult.iResultadoOperacion = 1;
                            oResult.descResultadoOperacion = "Abono encontrado";


                            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDFById), "Encontrado Abono " + oResult.idAbonoNav);

                        }
                        else
                        {
                            oResult.iResultadoOperacion = -1;
                            oResult.descResultadoOperacion = "Abono no encontrado";

                            objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDFById), "El Abono no existe");
                        }
                    }
                    else
                    {
                        if (idQueueAbono == -40)
                        {
                            oResult.descResultadoOperacion = "Abono no encontrado para el cliente " + intCodNavision.ToString();
                        }
                        else
                        {
                            oResult.descResultadoOperacion = "Abono no encontrado";
                        }
                        oResult.iResultadoOperacion = -1;

                        objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarAbonoPDFById), "El Abono no existe");
                    }
                }
                else
                {
                    oResult.iResultadoOperacion = -2;
                    oResult.descResultadoOperacion = "Identificador no válido";

                    oResult.idAbonoNav = String.Empty;
                    objLogger.LogInformation("Resultado {0} --> {1}", nameof(RecuperarFacturaInfo), "Identificador no válido");
                }

            }
            catch (Exception ex)
            {
                oResult.iResultadoOperacion = -3;
                oResult.descResultadoOperacion = ex.Message;

                oResult.idAbonoNav = "-1";
                objLogger.LogInformation("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarFacturaInfo), ex.Message);
            }
            return oResult;
        }


    }
}
