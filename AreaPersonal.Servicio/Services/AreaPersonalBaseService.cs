﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllApiBase.Servicio;
using DllWcfUtility;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace AreaPersonal.Servicio
{
    public class AreaPersonalBaseService : ServicioApiBase<AreaPersonalBaseService>, IAreaPersonalBaseService
    {
        internal readonly ClavesSvc.IServiceClaves objServiceClaves;
        //internal readonly ILogger<AreaPersonalBaseService> objLogger;
        internal readonly ObjVariables objVariables;

        internal ConfigApi objConfig { get; private set; }
        internal readonly ILogger<AreaPersonalBaseService> objLogger;

        public AreaPersonalBaseService(
            IOptions<ConfigApi> objConfigBase,
            ILogger<AreaPersonalBaseService> objLoggerBase) : base(objConfigBase, objLoggerBase)
        {
            objLogger = objLoggerBase;
            objConfig = objConfigBase.Value;
            //objVariables = objConfigBase.Value.cfgVariables;
            var basicHttpBindingClaves = new BasicHttpBinding();
            var endpointAddressClaves = new EndpointAddress(objConfig.cfgServices.ClavesSvc);
            objServiceClaves = new ClavesSvc.ServiceClavesClient(basicHttpBindingClaves, endpointAddressClaves);
        }

        public string EncodeHTML(string strHTML)
        {
            string strReturn = strHTML;

            strReturn = strReturn.Replace("€", "&#8364;");

            strReturn = strReturn.Replace("“", "&#8220;");
            strReturn = strReturn.Replace("”", "&#8221;");
            strReturn = strReturn.Replace("‘", "&#8216;");
            strReturn = strReturn.Replace("’", "&#8217;");

            strReturn = strReturn.Replace("&nbsp;", " ");

            return strReturn;
        }

        public string[] GetDistinctsEmails(string[] strEmails)
        {
            List<string> listEmails = new List<string>();

            foreach (String strEmail in strEmails)
            {
                if (!listEmails.Contains(strEmail))
                    listEmails.Add(strEmail);
            }

            return listEmails.ToArray();
        }

        public bool IsValidoIdentificador(String strIdentifier)
        {
            try
            {
                int iAcceso = objServiceClaves.ChequearAccesoWsAsync(strIdentifier).Result;
                return iAcceso == 1;
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(IsValidoIdentificador), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(IsValidoIdentificador), ex.Message);
                return false;
            }
        }

        internal ClavesSvc.Cliente getClienteFromNav(int codNavision)
        {
            try
            {
                objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(getClienteFromNav), codNavision.ToString());

                Task<BeanList<ClavesSvc.Cliente>> oList = null;

                // Parametros
                int IntNumTdcSugar = 0;
                int IntCodCuentaNavision = codNavision;
                string StrIdUsuarioPro = String.Empty;
                string StrNombreCompleto = String.Empty;
                string StrRazonSocial = String.Empty;
                string StrPhoneOffice = String.Empty;
                string StrCifNif = String.Empty;
                string StrEmail = String.Empty;
                string StrLogin = String.Empty;
                string StrIp = String.Empty;
                int IntIndTipoBusqueda = codNavision > 1000000 ? 3 : 1;
                int IntIdTipoCliente = 0;
                string StrCampoOrdenar = String.Empty;
                int IntIndOrdenarAsc = 0;
                int IntPaginaInicial = 0;
                int IntNumeroDeFilas = 1;

                int IntIdUsuarioApp = objVariables.idUsuarioAreaPersonal;

                oList = objServiceClaves.RecuperarClientesTempAsync(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
                    StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
                    IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
                    IntNumeroDeFilas, IntIdUsuarioApp);

                if ((oList != null) && (oList.Result != null) && (oList.Result.beanList != null) && (oList.Result.beanList.Count > 0))
                {
                    objLogger.LogDebug("End --> {0} Encontrado {1} resultados", nameof(getClienteFromNav), oList.Result.beanList.Count.ToString());

                    return oList.Result.beanList[0];
                }
                else
                {
                    objLogger.LogDebug("End --> {0} Encontrado : No se han encontrado ningun cliente", nameof(getClienteFromNav));
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(getClienteFromNav), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(getClienteFromNav), ex.Message);
                return null;
            }
        }

        internal ClavesSvc.Cliente getClienteFromCRM(int iCodCRM, int codNavision)
        {
            try
            {
                objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(getClienteFromCRM), iCodCRM.ToString(), codNavision.ToString());

                BeanList<ClavesSvc.Cliente> oList = null;

                // Parametros
                int IntNumTdcSugar = iCodCRM;
                int IntCodCuentaNavision = 0; // codNavision;
                string StrIdUsuarioPro = String.Empty;
                string StrNombreCompleto = String.Empty;
                string StrRazonSocial = String.Empty;
                string StrPhoneOffice = String.Empty;
                string StrCifNif = String.Empty;
                string StrEmail = String.Empty;
                string StrLogin = String.Empty;
                string StrIp = String.Empty;
                int IntIndTipoBusqueda = 1;
                if (iCodCRM <= 0)
                {
                    IntIndTipoBusqueda = codNavision > 1000000 ? 3 : 1;
                }

                int IntIdTipoCliente = 1;
                string StrCampoOrdenar = String.Empty;
                int IntIndOrdenarAsc = 1;
                int IntPaginaInicial = 0;
                int IntNumeroDeFilas = 1;

                int IntIdUsuarioApp = objVariables.idUsuarioAreaPersonal;

                //oList = service.RecuperarClientes(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
                //    StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
                //    IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
                //    IntNumeroDeFilas, IntIdUsuarioApp);

                oList = objServiceClaves.RecuperarClientesTempAsync(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
                   StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
                   IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
                   IntNumeroDeFilas, IntIdUsuarioApp).Result;

                if ((oList != null) && (oList.beanList != null) && (oList.beanList.Count > 0))
                {
                    objLogger.LogDebug("End --> {0} Encontrado {1} resultados", nameof(getClienteFromCRM), oList.beanList.Count.ToString());

                    return oList.beanList[0];
                }
                else
                {
                    objLogger.LogDebug("End --> {0} Encontrado : No se han encontrado ningun cliente", nameof(getClienteFromCRM));
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(getClienteFromCRM), ex.Message);
                return null;
            }
        }

        public async Task<Cliente> RecuperarCliente(string strIdentifier, int IdClienteNav)
        {
            int IdUserApp = objVariables.idUsuarioAreaPersonal;

            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarCliente), strIdentifier, IdClienteNav.ToString(), IdUserApp.ToString());

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    int IntNumTdcSugar = 0;
                    int IntCodCuentaNavision = IdClienteNav;
                    string StrIdUsuarioPro = String.Empty;
                    string StrNombreCompleto = String.Empty;
                    string StrRazonSocial = String.Empty;
                    string StrPhoneOffice = String.Empty;
                    string StrCifNif = String.Empty;
                    string StrEmail = String.Empty;
                    string StrLogin = String.Empty;
                    string StrIp = String.Empty;
                    int IntIndTipoBusqueda = IdClienteNav > 1000000 ? 3 : 1;
                    int IntIdTipoCliente = 0;
                    string StrCampoOrdenar = String.Empty;
                    int IntIndOrdenarAsc = 0;
                    int IntPaginaInicial = 0;
                    int IntNumeroDeFilas = 1;

                    int IntIdUsuarioApp = IdUserApp;

                    var lstClientes = await objServiceClaves.RecuperarClientesTempAsync(IntNumTdcSugar, IntCodCuentaNavision, StrIdUsuarioPro, StrNombreCompleto,
                        StrRazonSocial, StrPhoneOffice, StrCifNif, StrEmail, StrLogin, StrIp,
                        IntIndTipoBusqueda, IntIdTipoCliente, StrCampoOrdenar, IntIndOrdenarAsc, IntPaginaInicial,
                        IntNumeroDeFilas, IntIdUsuarioApp);

                    if ((lstClientes != null) && (lstClientes.beanList != null) && (lstClientes.beanList.Count > 0))
                    {
                        //log.Info("getCliente.Fin : Encontrado " + oList.beanList.Count.ToString());
                        ClavesSvc.Cliente oCliente = lstClientes.beanList[0];

                        var objJSon = new Cliente();

                        objJSon.cifNif = oCliente._cifNif;
                        objJSon.indGranCuenta = oCliente._indGranCuenta;
                        objJSon.codCuentaNavision = oCliente._codCuentaNavision;
                        objJSon.nombreCompleto = oCliente._nombreCompleto;
                        objJSon.numTdcSugar = oCliente._numTdcSugar;
                        objJSon.provincia = oCliente._provincia;
                        objJSon.razonSocial = oCliente._razonSocial;
                        objJSon.telefono = oCliente._telefono;

                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarCliente), objJSon.codCuentaNavision.ToString());
                        return objJSon;
                    }
                    else
                    {
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarCliente), "No hay cliente");
                        return null;
                    }
                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarCliente), "Identificador no válido");
                    return null;
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(RecuperarCliente), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarCliente), ex.Message);
                return null;
            }
        }

        internal ClavesSvc.UsuarioApp GetUserApp(int iUserApp)
        {
            try
            {
                Bean<ClavesSvc.UsuarioApp> users = objServiceClaves.RecuperarUsuarioAppByIdAsync(iUserApp).Result;

                if (users.bean != null)
                {
                    return users.bean;
                }

                else return null;
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("Error --> {0} {1} ", nameof(GetUserApp), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(GetUserApp), ex.Message);
                return null;
            }
        }

        //public async Task<UsuarioProducto> RecuperarUsuario(string strIdentifier, string strUsuarioPro)
        //{
        //    objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarUsuario), strIdentifier, strUsuarioPro);

        //    try
        //    {
        //        if (IsValidoIdentificador(strIdentifier))
        //        {
        //            var itemUsuario = await objServiceClaves.BuscarUsuarioAsync(strUsuarioPro);

        //            if (itemUsuario != null)
        //            {
        //                //log.Info("getCliente.Fin : Encontrado " + oList.beanList.Count.ToString());
        //                ClavesSvc.UsuarioProducto oCliente = itemUsuario;

        //                var objJSon = new UsuarioProducto();

        //                objJSon.estado = oCliente._estado;
        //                objJSon.fechaCreacionFormateada = oCliente._fechaCreacionFormateada;
        //                objJSon.idCliente = oCliente._idCliente;
        //                objJSon.idClienteCrm = oCliente._idClienteCrm;
        //                objJSon.idClienteNav = oCliente._idClienteNav;
        //                objJSon.idEntrada = oCliente._idEntrada;
        //                objJSon.ip = oCliente._ip;
        //                objJSon.login = oCliente._login;

        //                objJSon.email = oCliente._email;
        //                objJSon.nombre = oCliente._nombre;
        //                objJSon.numConcurrencias = oCliente._numConcurrencias;
        //                objJSon.numNavision = oCliente._numNavision;
        //                objJSon.primerApellido = oCliente._primerApellido;

        //                objJSon.pwd = oCliente._pwd;
        //                objJSon.segundoApellido = oCliente._segundoApellido;
        //                objJSon.tipoEntrada = oCliente._tipoEntrada;

        //                return objJSon;
        //            }
        //            else
        //            {
        //                objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarUsuario), "No hay Usuario");
        //                return null;
        //            }
        //        }
        //        else
        //        {
        //            objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarUsuario), "Identificador no válido");
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogDebug("Error --> {0} {1} ", nameof(RecuperarUsuario), ex.Message);
        //        return null;
        //    }
        //}
    }
}