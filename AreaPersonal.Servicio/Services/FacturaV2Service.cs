﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Comun.Enums;
using AreaPersonal.Vistas;
using AreaPersonal.Vistas.External.Claves;
using AreaPersonal.Vistas.External.Crm;
using AutoMapper;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves.Usuario;
using DllApiBase.Vistas.Cliente;
using DllApiBase.Vistas.Permisos;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Navision.Vistas.Cliente;
using Navision.Vistas.Facturas;
using Navision.Vistas.Productos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AreaPersonal.Servicio
{
    public class FacturaV2Service : AreaPersonalBaseService, IFacturaV2Service<ResultadoBase>
    {
        private readonly IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objExternalService;
        private readonly IMapper objMapper;

        public FacturaV2Service(
            IOptions<ConfigApi> objConfigBase,
            ILogger<FacturaV2Service> objLoggerBase,
            IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objExternalServiceBase,
            IMapper objMapperBase
            ) : base(objConfigBase, objLoggerBase)
        {
            objExternalService = objExternalServiceBase;
            objMapper = objMapperBase;
        }

        /// <summary>
        /// Comprobar si tiene suscripciones activas.
        /// </summary>
        /// <param name="IdUsuarioPro"></param>
        /// <returns></returns>
        private Boolean CheckSuscripcionesActivas(int IdClienteNav, string IdUsuarioPro)
        {
            return true;
            //objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(CheckSuscripcionesActivas), IdClienteNav.ToString(), IdUsuarioPro);

            //bool bResultado = true ;

            //ResultadoLista<Online.Vistas.Suscripciones.Suscripcion> lstSuscripciones = (ResultadoLista<Online.Vistas.Suscripciones.Suscripcion>)objExternalService.RecuperarSuscripcionesClienteNav(IdClienteNav);

            //if ((lstSuscripciones != null) && (lstSuscripciones.lstResultado != null))
            //    foreach (Online.Vistas.Suscripciones.Suscripcion objItem in lstSuscripciones.lstResultado)
            //    {

            //        if ((objItem.IdTipoSuscripcion == 1) || (objItem.IdTipoSuscripcion == 5))
            //        {
            //            if ((DateTime.Now.CompareTo(objItem.FechaDesde) >= 0) && (DateTime.Now.CompareTo(objItem.FechaHasta) <= 0))
            //            {
            //                objLogger.LogDebug("Continue --> {0} encontrado suscripción activa para {1} en IdSuscripcion {2}", nameof(CheckSuscripcionesActivas), IdUsuarioPro, objItem.IdSuscripcionNav.ToString());
            //                bResultado = true;
            //                break;
            //            }
            //        }
            //    }

            //objLogger.LogDebug("END --> {0} con parámetros {1}", nameof(CheckSuscripcionesActivas), bResultado.ToString());

            //return bResultado;

        }

        /// <summary>
        /// Comprueba si el cliente tiene permisos para ver las facturas.
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="IdClienteNav"></param>
        /// <param name="IdUsuarioPro"></param>
        /// <returns></returns>
        private (bool tienePermisosFacturas, string descripcionResultado, enumResultado? tipoResultado, string strCifNifBusqueda, int iIdContactoCrmBusqueda) RecuperarPermisosFacturas(string strIdentifier, int IdClienteNav, string IdUsuarioPro)
        {
            var strCifNifBusqueda = string.Empty;
            var iIdContactoCrmBusqueda = 0;

            if (!IsValidoIdentificador(strIdentifier))
            {
                objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarPermisosFacturas), "Identificador no válido");
                return (false, "Identificador no válido", enumResultado.Error_Validacion, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var objPermisos = objExternalService.RecuperarPermisosUsuarioPro(IdUsuarioPro);

            if (objPermisos == null)
            {
                objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarPermisosFacturas), "No se han encontrado permisos");
                return (false, "No se han encontrado permisos", enumResultado.Error_Validacion, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var objClienteNav = objExternalService.RecuperarClienteNavision(IdClienteNav);

            if ((objClienteNav == null) || (objClienteNav.IdClienteNav <= 0))
            {
                objLogger.LogDebug("CONTINUE --> {0} no existe Cliente Id {1}", nameof(RecuperarPermisosFacturas), IdClienteNav.ToString());
                return (false, $"No existe cliente con ID {IdClienteNav}", enumResultado.Error_Validacion, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var oPermisoAdminEC = objPermisos.Where(p => p.valor == "gestor-area-personal").FirstOrDefault();

            if (oPermisoAdminEC != null)
            {
                objLogger.LogDebug("CONTINUE --> {0} con permiso AdminEC", nameof(RecuperarPermisosFacturas));
                return (true, $"El usuario {IdUsuarioPro} tiene permisos por AdminEC", null, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var oPermisoCif = objPermisos.Where(p => p.valor == "facturas-por-cif").FirstOrDefault();

            if (oPermisoCif != null)
            {
                strCifNifBusqueda = objClienteNav.sCifNif;
                objLogger.LogDebug("CONTINUE --> {0} con permiso Cif", nameof(RecuperarPermisosFacturas));
                return (true, $"El usuario {IdUsuarioPro} tiene permisos por CIF", null, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var oPermisoCuenta = objPermisos.Where(p => p.valor == "facturas-por-cuenta").FirstOrDefault();

            if (oPermisoCuenta != null)
            {
                objLogger.LogDebug("CONTINUE --> {0} con permiso por Cuenta", nameof(RecuperarPermisosFacturas));
                return (true, $"El usuario {IdUsuarioPro} tiene permisos por cuenta", null, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var oPermisoContacto = objPermisos.Where(p => p.valor == "facturas-por-contacto").FirstOrDefault();

            if (oPermisoContacto == null)
            {
                objLogger.LogDebug("CONTINUE --> {0} el cliente {1}, no tiene permisos por contacto", nameof(RecuperarPermisosFacturas), IdClienteNav.ToString());
                return (false, $"El usuario {IdUsuarioPro} no tiene permisos por contacto", enumResultado.Error_Validacion, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            var objResultadoUsuario = (ResultadoOperacion<UsuarioPro>)objExternalService.RecuperarUsuarioProducto(IdClienteNav, IdUsuarioPro);

            if ((objResultadoUsuario != null) && (objResultadoUsuario.eResultado == enumResultado.Ok_Recuperar))
            {
                iIdContactoCrmBusqueda = objResultadoUsuario.oResultado.idContactoCrm;
                objLogger.LogDebug("CONTINUE --> {0} con permiso por Contacto", nameof(RecuperarPermisosFacturas));
                return (true, $"El usuario {IdUsuarioPro} tiene permisos por contacto", null, strCifNifBusqueda, iIdContactoCrmBusqueda);
            }

            objLogger.LogDebug("CONTINUE --> {0} No se ha encontrado usuarioPro {1}", nameof(RecuperarFacturas), IdUsuarioPro);
            return (false, $"El usuario {IdUsuarioPro} no tiene permisos", enumResultado.Error_Validacion, strCifNifBusqueda, iIdContactoCrmBusqueda);
        }

        public ResultadoBase RecuperarFacturas(string strIdentifier, int IdClienteNav, string IdUsuarioPro, int iPagNum, int iPagSize, string strFechaDesde, string strFechaHasta, OrdenarFacturasEnum iOrden)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7} ", nameof(RecuperarFacturas), strIdentifier,
                IdClienteNav, IdUsuarioPro, iPagNum, iPagSize, strFechaDesde, strFechaHasta);

            var objResultado = new ResultadoLista<Vistas.Factura>();

            try
            {
                var permisosFacturas = RecuperarPermisosFacturas(strIdentifier, IdClienteNav, IdUsuarioPro);

                if (!permisosFacturas.tienePermisosFacturas)
                {
                    objResultado.sDescripcion = permisosFacturas.descripcionResultado;
                    objResultado.eResultado = permisosFacturas.tipoResultado ?? enumResultado.Error_Validacion;
                    return objResultado;
                }

                if (!CheckSuscripcionesActivas(IdClienteNav, IdUsuarioPro))
                {
                    objResultado.sDescripcion = $"El cliente {IdClienteNav}, no tiene suscripciones activas";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    return objResultado;
                }

                var objListaFacturas = (ResultadoLista<Navision.Vistas.Facturas.Factura>)objExternalService.RecuperarFacturas(IdClienteNav, permisosFacturas.strCifNifBusqueda,
                    permisosFacturas.iIdContactoCrmBusqueda, strFechaDesde, strFechaHasta, iPagNum, iPagSize);

                if (objListaFacturas == null || objListaFacturas.lstResultado == null)
                {
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    objResultado.sDescripcion = "Error al recuperar las facturas.";
                    return objResultado;
                }

                objResultado.lstResultado = objMapper.Map<List<Vistas.Factura>>(objListaFacturas.lstResultado);
                objResultado.iContador = objListaFacturas.iContador;

                var listaFacturasPagadas = objExternalService.RecuperarFacturasPagadas(IdClienteNav);
                if (listaFacturasPagadas == null)
                {
                    objResultado.lstResultado = OrdenarListadoFacturas(objResultado.lstResultado, iOrden);
                    objResultado.eResultado = enumResultado.Ok_Recuperar;
                    objResultado.sDescripcion = "Se han recuperado un total de " + objResultado.lstResultado.Count.ToString() + " factura(s)";
                    return objResultado;
                }

                for (var i = 0; i < objResultado.lstResultado.Count; i++)
                {
                    if (objResultado.lstResultado[i].IEstadoPagada == EstadoFacturasEnum.Pendiente && listaFacturasPagadas.Contains(objResultado.lstResultado[i].idFacturaNav))
                    {
                        objResultado.lstResultado[i].IEstadoPagada = EstadoFacturasEnum.EnProceso;
                        objResultado.lstResultado[i].importeImpago = "0";
                    }
                }

                objResultado.lstResultado = OrdenarListadoFacturas(objResultado.lstResultado, iOrden);
                objResultado.eResultado = enumResultado.Ok_Recuperar;
                objResultado.sDescripcion = "Se han recuperado un total de " + objResultado.lstResultado.Count.ToString() + " factura(s)";
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }

            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarFacturas), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;
        }

        private List<Vistas.Factura> OrdenarListadoFacturas (List<Vistas.Factura> lstFacturas, OrdenarFacturasEnum iOrden)
        {
            return iOrden switch
            {
                OrdenarFacturasEnum.FechaCreciente => lstFacturas.OrderBy(x => x.fecha).ToList(),
                OrdenarFacturasEnum.FechaDecreciente => lstFacturas.OrderByDescending(x => x.fecha).ToList(),
                OrdenarFacturasEnum.EstadoCreciente => lstFacturas.OrderBy(x => x.IEstadoPagada).ToList(),
                OrdenarFacturasEnum.EstadoDecreciente => lstFacturas.OrderByDescending(x => x.IEstadoPagada).ToList(),
                _ => lstFacturas
            };
        }


        ///// <summary>
        ///// Recuperar las facturas de un UsuarioPro
        ///// </summary>
        ///// <param name="IdClienteNav"></param> 
        ///// <param name="IdUsuarioPro"></param>
        ///// <param name="iPagNum"></param>
        ///// <param name="iPagSize"></param>
        ///// <param name="strFechaDesde"></param>
        ///// <param name="strFechaHasta"></param>
        ///// <returns></returns>
        //public ResultadoBase RecuperarFacturas(string strIdentifier, int IdClienteNav, string IdUsuarioPro, int iPagNum, int iPagSize, string strFechaDesde, string strFechaHasta)
        //{
        //    objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7} ", nameof(RecuperarFacturas), strIdentifier, IdClienteNav, IdUsuarioPro, iPagNum, iPagSize, strFechaDesde, strFechaHasta);



        //    ResultadoLista<AreaPersonal.Vistas.Factura> objResultado = new ResultadoLista<AreaPersonal.Vistas.Factura>();


        //    ResultadoLista<Navision.Vistas.Facturas.Factura> objListaFacturas = new ResultadoLista<Navision.Vistas.Facturas.Factura>();

        //    string strCifNifBusqueda = String.Empty;
        //    int iIdClienteNavBusqueda = 0;
        //    int iIdContactoCrmBusqueda = 0;
        //    bool bTienePermisoFacturas = false;

        //    try
        //    {
        //        if (IsValidoIdentificador(strIdentifier))
        //        {

        //            List<PermisosAsociado> objPermisos = objExternalService.RecuperarPermisosUsuarioPro(IdUsuarioPro);

        //            if (objPermisos != null)
        //            {



        //                ClienteNavision objClienteNav = objExternalService.RecuperarClienteNavision(IdClienteNav);

        //                if ((objClienteNav != null) && (objClienteNav.IdClienteNav > 0))
        //                {

        //                    var oPermisoAdminEC = objPermisos.Where(p => p.valor == "gestor-area-personal").FirstOrDefault();

        //                    if (oPermisoAdminEC != null)
        //                    {
        //                        iIdClienteNavBusqueda = IdClienteNav;
        //                        bTienePermisoFacturas = true;
        //                        objLogger.LogDebug("CONTINUE --> {0} con permiso AdminEC", nameof(RecuperarFacturas));
        //                    }
        //                    else
        //                    {
        //                        var oPermisoCif = objPermisos.Where(p => p.valor == "facturas-por-cif").FirstOrDefault();
        //                        if (oPermisoCif != null)
        //                        {
        //                            strCifNifBusqueda = objClienteNav.sCifNif;
        //                            bTienePermisoFacturas = true;
        //                            objLogger.LogDebug("CONTINUE --> {0} con permiso Cif", nameof(RecuperarFacturas));

        //                        }
        //                        else
        //                        {
        //                            var oPermisoCuenta = objPermisos.Where(p => p.valor == "facturas-por-cuenta").FirstOrDefault();
        //                            if (oPermisoCuenta != null)
        //                            {
        //                                iIdClienteNavBusqueda = IdClienteNav;
        //                                bTienePermisoFacturas = true;
        //                                objLogger.LogDebug("CONTINUE --> {0} con permiso por Cuenta", nameof(RecuperarFacturas));
        //                            }
        //                            else // permiso por contacto
        //                            {
        //                                var oPermisoContacto = objPermisos.Where(p => p.valor == "facturas-por-contacto").FirstOrDefault();
        //                                if (oPermisoContacto != null)
        //                                {

        //                                    var objResultadoUsuario = (ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>)objExternalService.RecuperarUsuarioProducto(IdClienteNav, IdUsuarioPro);

        //                                    if ((objResultadoUsuario != null) && (objResultadoUsuario.eResultado == enumResultado.Ok_Recuperar))
        //                                    {
        //                                        iIdContactoCrmBusqueda = objResultadoUsuario.oResultado.idContactoCrm;
        //                                        bTienePermisoFacturas = true;
        //                                        objLogger.LogDebug("CONTINUE --> {0} con permiso por Contacto", nameof(RecuperarFacturas));
        //                                    }
        //                                    else
        //                                    {
        //                                        objLogger.LogDebug("CONTINUE --> {0} No se ha encontrado usuarioPro {1}", nameof(RecuperarFacturas), IdUsuarioPro);
        //                                    }
        //                                }
        //                            }

        //                        }
        //                    }
        //                    if (bTienePermisoFacturas)
        //                    {
        //                        if (this.CheckSuscripcionesActivas(IdClienteNav, IdUsuarioPro))
        //                        {
        //                            objListaFacturas = (ResultadoLista<Navision.Vistas.Facturas.Factura>)objExternalService.RecuperarFacturas(IdClienteNav, strCifNifBusqueda, iIdContactoCrmBusqueda, strFechaDesde, strFechaHasta, iPagNum, iPagSize);
        //                            if ((objListaFacturas != null) && (objListaFacturas.lstResultado != null))
        //                            {
        //                                objResultado.lstResultado = new List<Vistas.Factura>();
        //                                objResultado.iContador = objListaFacturas.iContador;
        //                                foreach (var item in objListaFacturas.lstResultado)
        //                                {
        //                                    objResultado.lstResultado.Add(new AreaPersonal.Vistas.Factura
        //                                    {
        //                                        idFacturaNav = item.IdFacturaNav,
        //                                        idClienteFacturacionNav = item.IdClienteFacturacionNav,
        //                                        idClienteEnvioNav = item.IdClienteEnvioNav,
        //                                        importe = item.strImporteIVA,
        //                                        importeImpago = item.strImpago,
        //                                        fecha = item.strFechaRegistro,
        //                                        fechaVencimiento = item.strFechaVencimiento
        //                                    });
        //                                }

        //                                objResultado.eResultado = enumResultado.Ok_Recuperar;
        //                                objResultado.sDescripcion = "Se han recuperado un total de " + objResultado.lstResultado.Count.ToString() + " factura(s)";
        //                            }
        //                            else
        //                            {
        //                                objResultado.eResultado = enumResultado.Error_Validacion;
        //                                objResultado.sDescripcion = "Error al recuperar las facturas.";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            objResultado.eResultado = enumResultado.Error_Validacion;
        //                            objResultado.sDescripcion = "El usuario " + IdUsuarioPro + " no puede acceder a facturas por no tener suscripciones activas";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        objResultado.eResultado = enumResultado.Error_Validacion;
        //                        objResultado.sDescripcion = "No tienen permiso de facturas " + IdUsuarioPro;
        //                        objLogger.LogDebug("CONTINUE --> {0} No tienen permiso de facturas {1}", nameof(RecuperarFacturas), IdUsuarioPro);
        //                    }

        //                }
        //                else
        //                {

        //                    objResultado.sDescripcion = "No existe Cliente Id " + IdClienteNav.ToString();
        //                    objResultado.eResultado = enumResultado.Error_Validacion;
        //                    objLogger.LogDebug("CONTINUE --> {0} no existe Cliente Id {1}", nameof(RecuperarFacturas), IdClienteNav.ToString());
        //                }
        //            }

        //        }

        //        else
        //        {
        //            objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarFacturas), "Identificador no válido");
        //            objResultado.sDescripcion = "Identificador no válido";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
        //        objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
        //        objResultado.sDescripcion = ex.Message;
        //        objResultado.eResultado = enumResultado.Error_Excepcion;
        //    }

        //    objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarFacturas), objResultado.eResultado.ToString(), objResultado.sDescripcion);
        //    return objResultado;

        //}

        /// <summary>
        /// Actualiza la fecha de ultimo acceso a facturas del usuario.
        /// </summary>
        /// <param name="IdClienteNav">Id del usuario de Navision</param>
        /// <param name="IdUsuarioPro">Id del usuario</param>
        /// <returns></returns>
        public ResultadoBase ModificarFechaUltimoAccesoFacturas(int IdClienteNav, int IdUsuarioApp)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}", nameof(ModificarFechaUltimoAccesoFacturas), IdClienteNav, IdUsuarioApp);
            return objExternalService.ModificarFechaUltimoAccesoFacturas(IdClienteNav, IdUsuarioApp);
        }

        /// <summary>
        /// Recupera un booleano que indica si el usuario tiene facturas nuevas.
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="IdClienteNav"></param>
        /// <param name="IdUsuarioPro"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarIndicadorFacturasNuevas(string strIdentifier, int IdClienteNav, string IdUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarIndicadorFacturasNuevas), IdClienteNav, IdClienteNav, IdUsuarioPro);
            var objResultado = new ResultadoBooleano();
            try
            {
                var permisosFacturas = RecuperarPermisosFacturas(strIdentifier, IdClienteNav, IdUsuarioPro);

                if (!permisosFacturas.tienePermisosFacturas)
                {
                    objResultado.sDescripcion = permisosFacturas.descripcionResultado;
                    objResultado.eResultado = permisosFacturas.tipoResultado ?? enumResultado.Error_Validacion;
                    return objResultado;
                }

                if (!CheckSuscripcionesActivas(IdClienteNav, IdUsuarioPro))
                {
                    objResultado.sDescripcion = $"El cliente {IdClienteNav}, no tiene suscripciones activas";
                    objResultado.eResultado = enumResultado.Error_Validacion;
                    return objResultado;
                }

                var resultadoFechaUltimoAccesoFacturas = (ResultadoFecha)RecuperarFechaUltimoAccesoFacturas(IdClienteNav);
                if (resultadoFechaUltimoAccesoFacturas.eResultado != enumResultado.Ok_Recuperar || resultadoFechaUltimoAccesoFacturas.dtResultado == null)
                    throw new Exception(resultadoFechaUltimoAccesoFacturas.sDescripcion);
                objResultado = (ResultadoBooleano)RecuperarIndicadorFacturasNuevasBD(IdClienteNav, permisosFacturas.strCifNifBusqueda, (DateTime)resultadoFechaUltimoAccesoFacturas.dtResultado);
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarIndicadorFacturasNuevas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarIndicadorFacturasNuevas), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }
            return objResultado;
        }

        /// <summary>
        /// Recupera el número de facturas nuevas desde una fecha 
        /// </summary>
        /// <param name="IdClienteNav"></param>
        /// <param name="Cif"></param>
        /// <param name="FechaUltimoAcceso"></param>
        /// <returns></returns>
        private ResultadoBase RecuperarIndicadorFacturasNuevasBD(int IdClienteNav, string strCif, DateTime FechaUltimoAcceso)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}, {3}", nameof(RecuperarIndicadorFacturasNuevasBD), IdClienteNav, strCif, FechaUltimoAcceso);
            return objExternalService.RecuperarIndicadorFacturasNuevas(IdClienteNav, strCif, FechaUltimoAcceso);
        }

        /// <summary>
        /// Recupera la fecha de último acceso a las facturas de un usuario dese Claves.
        /// </summary>
        /// <param name="IdClienteNav"></param>
        /// <returns></returns>
        private ResultadoBase RecuperarFechaUltimoAccesoFacturas(int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1}", nameof(RecuperarFechaUltimoAccesoFacturas), IdClienteNav);
            return objExternalService.RecuperarFechaUltimoAccesoFacturas(IdClienteNav);
        }

        /// <summary>
        /// Recupera la factura en PDF (base 64)
        /// </summary>
        /// <param name="strIdFactura"></param>
        /// <param name="IdClienteNav"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarFacturaPDFById(string strIdentifier, string strIdFactura, int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarFacturaPDFById), strIdentifier, strIdFactura, IdClienteNav);
            ResultadoOperacion<FacturaPDFNav> objResultado = new ResultadoOperacion<FacturaPDFNav>();




            string strCifNifBusqueda = String.Empty;

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    ClienteNavision objClienteNav = objExternalService.RecuperarClienteNavision(IdClienteNav);

                    if ((objClienteNav != null) && (objClienteNav.IdClienteNav > 0) && !String.IsNullOrWhiteSpace(objClienteNav.sCifNif))
                    {

                        strCifNifBusqueda = objClienteNav.sCifNif;
                        objLogger.LogDebug("CONTINUE --> {0} recuperado ClienteNav {1} con Cif {2}", nameof(RecuperarFacturaPDFById), IdClienteNav, strCifNifBusqueda);

                        var objResultadoNav = (Navision.Vistas.Facturas.FacturaPDF)objExternalService.RecuperarFacturaPdf(strIdFactura, strCifNifBusqueda);


                        if ((objResultadoNav != null) && !String.IsNullOrWhiteSpace(objResultadoNav.strFacturaFileName))
                        {
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.sDescripcion = "Recuperado factura con nombre " + objResultadoNav.strFacturaFileName;

                            objResultado.oResultado = new FacturaPDFNav();
                            objResultado.oResultado.strNombreArchivo = objResultadoNav.strFacturaFileName;
                            objResultado.oResultado.strArchivoBase64 = objResultadoNav.strFacturaFileContent;

                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Error al recuperar la factura con Id " + strIdFactura;
                        }
                    }
                }
                else
                {


                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarFacturaPDFById), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }

            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarFacturas), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }


            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarFacturaPDFById), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;
        }

        /// <summary>
        /// Recuperar los abonos de un UsuarioPro
        /// </summary>
        /// <param name="IdClienteNav"></param> 
        /// <param name="IdUsuarioPro"></param>
        /// <param name="iPagNum"></param>
        /// <param name="iPagSize"></param>
        /// <param name="strFechaDesde"></param>
        /// <param name="strFechaHasta"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarAbonos(string strIdentifier, int IdClienteNav, string IdUsuarioPro, int iPagNum, int iPagSize, string strFechaDesde, string strFechaHasta)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7}", nameof(RecuperarAbonos), strIdentifier, IdClienteNav, IdUsuarioPro, iPagNum, iPagSize, strFechaDesde, strFechaHasta);

            ResultadoLista<AreaPersonal.Vistas.Abono> objResultado = new ResultadoLista<AreaPersonal.Vistas.Abono>();


            ResultadoLista<Navision.Vistas.Facturas.Abono> objListaAbonos = new ResultadoLista<Navision.Vistas.Facturas.Abono>();


            string strCifNifBusqueda = String.Empty;
            int iIdClienteNavBusqueda = 0;
            int iIdContactoCrmBusqueda = 0;
            bool bTienePermisoFacturas = false;

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    List<PermisosAsociado> objPermisos = objExternalService.RecuperarPermisosUsuarioPro(IdUsuarioPro);

                    if ((objPermisos != null) && (objPermisos != null))
                    {





                        ClienteNavision objClienteNav = objExternalService.RecuperarClienteNavision(IdClienteNav);

                        if ((objClienteNav != null) && (objClienteNav.IdClienteNav > 0))
                        {

                            var oPermisoAdminEC = objPermisos.Where(p => p.valor == "gestor-area-personal").FirstOrDefault();

                            if (oPermisoAdminEC != null)
                            {
                                iIdClienteNavBusqueda = IdClienteNav;
                                bTienePermisoFacturas = true;
                                objLogger.LogDebug("CONTINUE --> {0} con permiso AdminEC", nameof(RecuperarFacturas));
                            }
                            else
                            {
                                var oPermisoCif = objPermisos.Where(p => p.valor == "facturas-por-cif").FirstOrDefault();
                                if (oPermisoCif != null)
                                {
                                    bTienePermisoFacturas = true;
                                    strCifNifBusqueda = objClienteNav.sCifNif;
                                    objLogger.LogDebug("CONTINUE --> {0} con permiso Cif", nameof(RecuperarAbonos));

                                }
                                else
                                {
                                    var oPermisoCuenta = objPermisos.Where(p => p.valor == "facturas-por-cuenta").FirstOrDefault();
                                    if (oPermisoCuenta != null)
                                    {
                                        bTienePermisoFacturas = true;
                                        iIdClienteNavBusqueda = IdClienteNav;
                                        objLogger.LogDebug("CONTINUE --> {0} con permiso por Cuenta", nameof(RecuperarAbonos));
                                    }
                                    else // permiso por contacto
                                    {
                                        var oPermisoContacto = objPermisos.Where(p => p.valor == "facturas-por-contacto").FirstOrDefault();
                                        if (oPermisoContacto != null)
                                        {
                                            var objResultadoUsuario = (ResultadoOperacion<DllApiBase.Vistas.Claves.Usuario.UsuarioPro>)objExternalService.RecuperarUsuarioProducto(IdClienteNav, IdUsuarioPro);

                                            if ((objResultadoUsuario != null) && (objResultadoUsuario.eResultado == enumResultado.Ok_Recuperar))
                                            {
                                                iIdContactoCrmBusqueda = objResultadoUsuario.oResultado.idContactoCrm;
                                                bTienePermisoFacturas = true;
                                                objLogger.LogDebug("CONTINUE --> {0} con permiso por Contacto", nameof(RecuperarAbonos));
                                            }
                                            else
                                            {
                                                objLogger.LogDebug("CONTINUE --> {0} No se ha encontrado usuarioPro {1}", nameof(RecuperarAbonos), IdUsuarioPro);
                                            }
                                        }
                                    }

                                }
                            }
                            if (bTienePermisoFacturas)
                            {
                                if (this.CheckSuscripcionesActivas(IdClienteNav, IdUsuarioPro))
                                {
                                    objListaAbonos = (ResultadoLista<Navision.Vistas.Facturas.Abono>)objExternalService.RecuperarAbonos(IdClienteNav, strCifNifBusqueda, iIdContactoCrmBusqueda, strFechaDesde, strFechaHasta, iPagNum, iPagSize);
                                    if ((objListaAbonos != null) && (objListaAbonos.lstResultado != null))
                                    {
                                        objResultado.lstResultado = new List<Vistas.Abono>();
                                        objResultado.iContador = objListaAbonos.iContador;
                                        foreach (var item in objListaAbonos.lstResultado)
                                        {
                                            objResultado.lstResultado.Add(new AreaPersonal.Vistas.Abono
                                            {
                                                idAbonoNav = item.IdAbonoNav,
                                                idClienteFacturacionNav = item.IdClienteFacturacionNav,
                                                idClienteEnvioNav = item.IdClienteEnvioNav,
                                                importe = item.strImporteIVA,
                                                fecha = item.strFechaRegistro,
                                                fechaVencimiento = item.strFechaVencimiento
                                            });
                                        }

                                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                                        objResultado.sDescripcion = "Se han recuperado un total de " + objResultado.lstResultado.Count.ToString() + " abono(s)";
                                    }
                                }
                                else
                                {
                                    objResultado.eResultado = enumResultado.Error_Validacion;
                                    objResultado.sDescripcion = "El usuario " + IdUsuarioPro + " no puede acceder a abonos por no tener suscripciones activas";
                                }
                            }
                            else
                            {
                                objResultado.eResultado = enumResultado.Error_Validacion;
                                objResultado.sDescripcion = "No tienen permiso de facturas " + IdUsuarioPro;
                                objLogger.LogDebug("CONTINUE --> {0} No tienen permiso de facturas {1}", nameof(RecuperarAbonos), IdUsuarioPro);
                            }

                        }
                        else
                        {
                            objResultado.sDescripcion = "No existe Cliente Id " + IdClienteNav.ToString();
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objLogger.LogDebug("CONTINUE --> {0} no existe Cliente Id {1}", nameof(RecuperarAbonos), IdClienteNav.ToString());
                        }
                    }




                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarAbonos), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }


            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarAbonos), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarAbonos), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }


            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarAbonos), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;

        }

        /// <summary>
        /// Recupera la abono en PDF (base 64)
        /// </summary>
        /// <param name="strIdAbono"></param>
        /// <param name="IdClienteNav"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarAbonoPDFById(string strIdentifier, string strIdAbono, int IdClienteNav)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(RecuperarAbonoPDFById), strIdentifier, strIdAbono, IdClienteNav);
            ResultadoOperacion<AbonoPDFNav> objResultado = new ResultadoOperacion<AbonoPDFNav>();




            string strCifNifBusqueda = String.Empty;

            try
            {

                if (IsValidoIdentificador(strIdentifier))
                {
                    ClienteNavision objClienteNav = objExternalService.RecuperarClienteNavision(IdClienteNav);

                    if ((objClienteNav != null) && (objClienteNav.IdClienteNav > 0) && !String.IsNullOrWhiteSpace(objClienteNav.sCifNif))
                    {

                        strCifNifBusqueda = objClienteNav.sCifNif;
                        objLogger.LogDebug("CONTINUE --> {0} recuperado ClienteNav {1} con Cif {2}", nameof(RecuperarAbonoPDFById), IdClienteNav, strCifNifBusqueda);

                        var objResultadoNav = (Navision.Vistas.Facturas.AbonoPDF)objExternalService.RecuperarAbonoPdf(strIdAbono, strCifNifBusqueda);


                        if ((objResultadoNav != null) && !String.IsNullOrWhiteSpace(objResultadoNav.strAbonoFileName))
                        {
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.sDescripcion = "Recuperado abono con nombre " + objResultadoNav.strAbonoFileName;

                            objResultado.oResultado = new AbonoPDFNav();
                            objResultado.oResultado.strNombreArchivo = objResultadoNav.strAbonoFileName;
                            objResultado.oResultado.strArchivoBase64 = objResultadoNav.strAbonoFileContent;

                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Error al recuperar el abono con Id " + strIdAbono;
                        }
                    }
                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarAbonoPDFById), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogDebug("ERROR --> {0} {1} ", nameof(RecuperarAbonoPDFById), ex.Message);
                objLogger.LogError("ERROR --> {0} {1} ", nameof(RecuperarAbonoPDFById), ex.Message);
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
            }


            objLogger.LogDebug("END --> {0} Resultado {1} con descripción {2}", nameof(RecuperarAbonoPDFById), objResultado.eResultado.ToString(), objResultado.sDescripcion);
            return objResultado;
        }

        public int InsertarFacturaPagada(InsertarFacturaPagadaPeticion objPeticion)
        {
            return objExternalService.InsertarFacturaPagada(objPeticion);
        }

    }
}
