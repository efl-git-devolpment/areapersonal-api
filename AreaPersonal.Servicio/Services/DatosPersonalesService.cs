﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Servicio;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Vistas;
using DllWcfUtility;
using System.ServiceModel;
using System.Threading.Tasks;
using DllApiBase.Servicio;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves;
using DllApiBase.Vistas.Claves.Usuario;
using System.IO;
using ClavesSvc;
using DllApiBase.Vistas.Permisos;
using Navision.Vistas.Cliente;
using Navision.Vistas.Facturas;
using DllApiBase.Vistas.Cliente;
using Navision.Vistas.Productos;
using AreaPersonal.Vistas.External.Claves;
using AreaPersonal.Vistas.External.Crm;

namespace AreaPersonal.Servicio
{

    public static class TiposDatosPersonales
    {
        public const string USER_ACCESS_TYPE_USER_PASS = "usuario"; // 1
        public const string USER_ACCESS_TYPE_USER_PASS_IP = "usuario_ip"; // 2,
        public const string USER_ACCESS_TYPE_LOGIN_IP = "login_ip"; //3,
        public const string USER_ACCESS_TYPE_IP = "ip"; //4,
        public const string USER_ACCESS_EMAIL = "correo"; //5


    }

    public class DatosPersonalesService : AreaPersonalBaseService, IDatosPersonalesService<ResultadoBase>

    {
        internal readonly RepositorySvc.IWcfRepository objServicioWcfRepository;
        private readonly IExternalService<UsuarioPro,ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objServicioExternal;

        //private readonly Online.Servicio.Services.OnlineProductoBaseService<AreaPersonalBaseService> objServicioOnline;
        public DatosPersonalesService(
            ILogger<DatosPersonalesService> objLoggerBase,
            IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta, ClienteBusqueda> objServicioExternalBase,
            IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase, objLoggerBase)
        {
            var basicHttpBindingRepository = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxReceivedMessageSize = int.MaxValue,
                MaxBufferSize = int.MaxValue
            };

            var endpointAddressRepository = new EndpointAddress(objConfig.cfgServices.RepositorySvc);
            objServicioWcfRepository = new RepositorySvc.WcfRepositoryClient(basicHttpBindingRepository, endpointAddressRepository);

            //objServicioOnline = new Online.Servicio.Services.OnlineProductoBaseService<AreaPersonalBaseService>(objConfig.cfgUrlProducto, objLoggerBase);
            objServicioExternal = objServicioExternalBase;
        }


       


       

        /// <summary>
        /// Recuperar datospersonales
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param> 

        /// <returns></returns>
        public ResultadoBase RecuperarDatosPersonales(string strIdentifier, string strUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarDatosPersonales), strIdentifier, strUsuarioPro);

            ResultadoOperacion<UsuarioPersonal> objResultado = new ResultadoOperacion<UsuarioPersonal>();

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {
                        UsuarioPersonal objResultadoUsuario = new UsuarioPersonal();

                        objResultadoUsuario.email = objUsuario.email;
                        objResultadoUsuario.estado = objUsuario.estado;
                        objResultadoUsuario.fechaCreacionFormateada = objUsuario.fechaCreacionFormateada;
                        objResultadoUsuario.idCliente = objUsuario.idCliente;

                        objResultadoUsuario.idClienteCrm = objUsuario.idContactoCrm;
                        objResultadoUsuario.idClienteNav = objUsuario.idClienteNav;
                        objResultadoUsuario.idEntrada = objUsuario.idEntrada;
                        objResultadoUsuario.ip = objUsuario.ip;

                        objResultadoUsuario.login = "*****"; //  objUsuario.login;
                        objResultadoUsuario.nombre = objUsuario.nombre;
                        objResultadoUsuario.numConcurrencias = objUsuario.numConcurrencias;
                        objResultadoUsuario.numNavision = objUsuario.numNavision;

                        objResultadoUsuario.primerApellido = objUsuario.primerApellido;
                        objResultadoUsuario.pwd = "*****"; //objUsuario.pwd;
                        objResultadoUsuario.segundoApellido = objUsuario.segundoApellido;
                        objResultadoUsuario.tipoEntrada = objUsuario.tipoEntrada;
                        objResultadoUsuario.tratamiento = objUsuario.tratamiento;
                        //objResultadoUsuario.color = objUsuario.color;
                        //objResultadoUsuario.nombreComercial = objUsuario.nombreComercial;
                        objResultado.oResultado = objResultadoUsuario;

                        var strLogo = String.Empty;

                        var sResult = objServicioWcfRepository.getLogoUsuarioProAsync(strUsuarioPro, objUsuario.idClienteNav).Result;

                        if (!String.IsNullOrEmpty(sResult))
                            strLogo = sResult;


                        objResultadoUsuario.pathLogo = strLogo;

                        string strExtension = objConfig.cfgExtensionLogo2; // .Path.GetExtension(strFileName);
                        string strFile = objUsuario.idClienteNav + "." + strExtension;


                        objResultadoUsuario.pathLogo2 = objServicioWcfRepository.getFileAsync(strFile, objConfig.cfgAreaLogo2, objUsuario.idClienteNav.ToString()).Result;

                        // Recuperar color y nombre comercial de Cliente

                        var objCliente = (ClientePro)objServicioExternal.RecuperarClienteOnline(objUsuario.idClienteNav);

                        if (objCliente != null)
                        {
                            objResultadoUsuario.color = objCliente.color;
                            objResultadoUsuario.nombreComercial = objCliente.nombreComercial;
                        }

                        objResultado.eResultado = enumResultado.Ok_Recuperar;
                        objResultado.sDescripcion = "Datos Usuario Encontrados";

                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarDatosPersonales), "Datos Usuario Encontrados");
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarDatosPersonales), "Usuario no encontrado");
                    }
                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarDatosPersonales), "Identificador no válido");

                }
            }
            catch (Exception ex)
            {
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objLogger.LogDebug("Error --> {0} {1} ", nameof(RecuperarDatosPersonales), ex.Message);
                objLogger.LogError("Error --> {0} {1} ", nameof(RecuperarDatosPersonales), ex.Message);
                objResultado.sDescripcion = ex.Message;
            }

            return objResultado;

        }


        /// <summary>
        /// Recuperar path del Logo del usuario
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarLogo(string strIdentifier, string strUsuarioPro)
        {

            ResultadoCadena objResultado = new ResultadoCadena();
            objResultado.sResultado = String.Empty;

            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}", nameof(RecuperarLogo), strIdentifier, strUsuarioPro);

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    
                        {

                        var sResult = objServicioWcfRepository.getLogoUsuarioProAsync(strUsuarioPro, objUsuario.idClienteNav).Result;

                        if (String.IsNullOrEmpty(sResult))
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "No se ha recuperado ningún Logo";
                        }
                        else
                        {
                            objResultado.sResultado = sResult;
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.sDescripcion = "Se ha recuperado el logo de " + strUsuarioPro;
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo), "Usuario no encontrado");
                    }
                 
                }

                else
                {
           
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";

                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo), "Identificador no válido");
                }


            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarLogo), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo), objResultado.sResultado);
            return objResultado;

        }

        /// <summary>
        ///Modificar datos personales
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param>
        /// <param name="strName"></param>
        /// <param name="strFirstName"></param>
        /// <param name="strSurName"></param>
        /// <param name="strTreatment"></param>
        /// <param name="strEmail"></param>
        /// <param name="strPassword"></param>
        /// <param name="strColor"></param>
        /// <param name="strNombreComercial"></param>
        /// <returns></returns>
        public  ResultadoBase ModificarDatosPersonales(string strIdentifier, String strUsuarioPro, String strName, String strFirstName, String strSurName, String strTreatment, String strEmail,   String strColor, String strNombreComercial)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5},{6},{7},{8},{9}", nameof(ModificarDatosPersonales), strIdentifier, strUsuarioPro, strName, strFirstName, strSurName, strTreatment, strEmail, strColor, strNombreComercial);

            ResultadoEntero objResultado = new ResultadoEntero();
            objResultado.iResultado = -1;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                   

                    var objUsuario =  objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {

                        int iNumConcurr = 0; // String.IsNullOrEmpty(oUser.) ? 0 : Convert.ToInt32(oUser._numConcurrencias);

                     
                       
                            int iResult = -1;
                            switch (objUsuario.tipoEntrada)
                            {
                                case (TiposDatosPersonales.USER_ACCESS_TYPE_USER_PASS):
                                    { 
                                        iResult = objServiceClaves.ModificarUsuarioAsync(objUsuario.idClienteNav, strUsuarioPro, objUsuario.login, objUsuario.pwd, objUsuario.estado, strName, strFirstName, strSurName, strTreatment, strEmail, iNumConcurr, 1).Result;
                                       
                                    }; break;
                                case (TiposDatosPersonales.USER_ACCESS_TYPE_IP):
                                    {
                                        String strIp = String.Empty;
                                     iResult =  objServiceClaves.ModificarIpAsync(objUsuario.idClienteNav, strUsuarioPro, objUsuario.ip, objUsuario.estado, strName, strFirstName, strSurName, strTreatment, strEmail, iNumConcurr, 1).Result;
                                    }; break;
                                case (TiposDatosPersonales.USER_ACCESS_TYPE_LOGIN_IP):
                                    {
                                        iResult =  objServiceClaves.ModificarLoginIpAsync(objUsuario.idClienteNav, strUsuarioPro, objUsuario.login, objUsuario.ip, objUsuario.estado, strName, strFirstName, strSurName, strTreatment, strEmail, iNumConcurr, 1).Result;
                                    }; break;
                                case (TiposDatosPersonales.USER_ACCESS_TYPE_USER_PASS_IP):
                                    {
                                        iResult = objServiceClaves.ModificarUsuarioIpAsync(objUsuario.idClienteNav, strUsuarioPro, objUsuario.login, objUsuario.pwd, objUsuario.ip, objUsuario.estado, strName, strFirstName, strSurName, strTreatment, strEmail, iNumConcurr, 1).Result;

                                    }; break;
                                case (TiposDatosPersonales.USER_ACCESS_EMAIL):
                                    {
                                        iResult =  objServiceClaves.ModificarCorreoAsync(objUsuario.idClienteNav, strUsuarioPro, objUsuario.estado, strName, strFirstName, strSurName, strTreatment, strEmail, iNumConcurr, 1).Result;
                                    }; break;
                            }

                        if (iResult >= 0)
                        {
                            // Actualizar los campos de Cliente

                            iResult =  objServiceClaves.ModificarClientePersonalizacionAsync(objUsuario.idClienteNav, strNombreComercial, strColor,1).Result;
                        }
                            objResultado.iResultado = iResult;
                            objResultado.sDescripcion = "Resultado Modificacion";

                        
                        if (iResult == -2)
                        {
                            objResultado.sDescripcion = "Contraseña no válida";
                            objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarDatosPersonales), "Identificador no válido");
                        }

                    }
                    else
                    {
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarDatosPersonales), "Usuario no encontrado");
                    }

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";

                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarDatosPersonales), "Identificador no válido");

                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ModificarDatosPersonales), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("Resultado {0} --> {1},{2}", nameof(ModificarDatosPersonales), objResultado.iResultado.ToString(), objResultado.sDescripcion);
            return objResultado;

        }


        public ResultadoBase ModificarLogo(string strIdentifier, String strUsuarioPro, byte[] bdata)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(ModificarLogo), strIdentifier, strUsuarioPro, bdata.Length.ToString());

            ResultadoEntero objResultado = new ResultadoEntero();
            objResultado.iResultado = -1;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    string strExtension = objConfig.cfgExtensionLogo;
                    string strFile = strUsuarioPro + "." + strExtension;

                    objResultado.iResultado = objServicioWcfRepository.uploadFileAsync(bdata, strFile, objConfig.cfgAreaLogo, String.Empty, true).Result;


                    //var iResult = _svcClaves.EstablecerPersonalizacion_LogoAsync(strUsuarioPro, bdata).Result;

                    

                    objResultado.sDescripcion = "Resultado Modificación";
                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarLogo), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarLogo), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;

                objResultado.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("Resultado {0} --> {1},{2}", nameof(ModificarLogo), objResultado.iResultado.ToString(), objResultado.sDescripcion);
            return objResultado;


        }

        public ResultadoBase ResetearLogo(string strIdentifier, String strUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(ResetearLogo), strIdentifier, strUsuarioPro);

            ResultadoEntero objResultado = new ResultadoEntero();
            objResultado.iResultado = -1;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {

                    //var iResult = _svcClaves.EliminarPersonalizacion_LogoAsync(strUsuarioPro).Result;

                    var bResult = objServicioWcfRepository.resetLogoUsuarioProAsync(strUsuarioPro).Result;

                    if (bResult)
                    {
                        objResultado.iResultado = 1;
                        objResultado.sDescripcion = "Resultado Reseteo OK";
                    }
                    else
                    {
                        objResultado.iResultado = -1;
                        objResultado.sDescripcion = "Resultado Reseteo KO";
                    }

                }
                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ResetearLogo), "Identificador no válido");

                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarLogo), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;

                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1},{2}", nameof(ResetearLogo), objResultado.iResultado.ToString(), objResultado.sDescripcion);
            return objResultado;

        }

        public ResultadoBase ExisteLogo(string strIdentifier, String strUsuarioPro)
        {
            ResultadoBooleano objResultado = new ResultadoBooleano();
            objResultado.bResultado = false;

            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}", nameof(ExisteLogo), strIdentifier, strUsuarioPro);

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {

                        var sResult = objServicioWcfRepository.getExtensionLogoUsuarioProAsync(objConfig.cfgAreaLogo,strUsuarioPro, objUsuario.idClienteNav,String.Empty).Result;

                        if (String.IsNullOrEmpty(sResult))
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.bResultado = false;
                            objResultado.sDescripcion = "No se ha encontrado ningún Logo";
                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.bResultado = true;
                            objResultado.sDescripcion = "Se ha encontrado el logo de " + strUsuarioPro + ". Es un archivo " + sResult;


                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objResultado.bResultado = false;
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo), "Usuario no encontrado");
                    }

                }

                else
                {

                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";

                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo), "Identificador no válido");
                }


            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ExisteLogo), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo), objResultado.bResultado.ToString());
            return objResultado;


        }

        /// <summary>
        /// Recuperar path del Logo del usuario
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <param name="strUsuarioPro"></param>
        /// <returns></returns>
        public ResultadoBase RecuperarLogo2(string strIdentifier, string strUsuarioPro)
        {

            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(RecuperarLogo2), strIdentifier, strUsuarioPro);

            ResultadoCadena objResultado = new ResultadoCadena();
            objResultado.sResultado = String.Empty;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {
                        string strExtension = objConfig.cfgExtensionLogo2; // .Path.GetExtension(strFileName);
                        string strFile = objUsuario.idClienteNav + "." + strExtension;


                        var strResult = objServicioWcfRepository.getFileAsync(strFile, objConfig.cfgAreaLogo2, objUsuario.idClienteNav.ToString()).Result;


                        if (String.IsNullOrEmpty(strResult))
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "No se ha recuperado ningún Logo";
                        }
                        else
                        {
                            objResultado.sResultado = strResult;
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.sDescripcion = "Se ha recuperado el logo de " + strUsuarioPro;
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo2), "Usuario no encontrado");
                    }
                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo2), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(RecuperarLogo2), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1}", nameof(RecuperarLogo2), objResultado.sResultado);
            return objResultado;

        }

        public ResultadoBase ModificarLogo2(string strIdentifier, String strUsuarioPro, byte[] bdata)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3}", nameof(ModificarLogo), strIdentifier, strUsuarioPro, bdata.Length.ToString());

            ResultadoEntero objResultado = new ResultadoEntero();
            objResultado.iResultado = -1;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {
                        string strExtension = objConfig.cfgExtensionLogo2; // .Path.GetExtension(strFileName);
                        string strFile = objUsuario.idClienteNav + "." + strExtension;

                        objResultado.iResultado = objServicioWcfRepository.uploadFileAsync(bdata, strFile, objConfig.cfgAreaLogo2, objUsuario.idClienteNav.ToString(), true).Result;

                        if (objResultado.iResultado >= 0)
                        {
                            objResultado.eResultado = enumResultado.Ok_Modificado;
                            objResultado.sDescripcion = "Resultado Modificacion";
                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Resultado incorrecto en la Modificacion";
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objResultado.iResultado = -1;
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarLogo2), "Usuario no encontrado");
                    }
                }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarLogo2), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ModificarLogo2), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;

                objResultado.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("Resultado {0} --> {1},{2}", nameof(ModificarLogo2), objResultado.iResultado.ToString(), objResultado.sDescripcion);
            return objResultado;

        }

        public ResultadoBase ResetearLogo2(string strIdentifier, String strUsuarioPro)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2}", nameof(ResetearLogo2), strIdentifier, strUsuarioPro);

            ResultadoEntero objResultado = new ResultadoEntero();
            objResultado.iResultado = -1;
            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {
                        string strFileName = objUsuario.idClienteNav + ".png";

                        var iResult = objServicioWcfRepository.removeFileAsync(strFileName, objConfig.cfgAreaLogo2, objUsuario.idClienteNav.ToString()).Result;


                        if (iResult >= 0)
                        {
                            objResultado.iResultado = 1;
                            objResultado.eResultado = enumResultado.Ok_Modificado;
                            objResultado.sDescripcion = "Resultado Reseteo";
                        }
                        else
                        {
                            objResultado.iResultado = 0;
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.sDescripcion = "Resultado incorrecto en la Modificacion";
                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objResultado.iResultado = -1;
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(ResetearLogo2), "Usuario no encontrado");
                    }
            }
                else
                {
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ResetearLogo2), "Identificador no válido");
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ResetearLogo2), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;

                objResultado.sDescripcion = ex.Message;

            }

            objLogger.LogDebug("Resultado {0} --> {1},{2}", nameof(ResetearLogo2), objResultado.iResultado.ToString(), objResultado.sDescripcion);
            return objResultado;

        }

        public ResultadoBase ExisteLogo2(string strIdentifier, String strUsuarioPro)
        {
            ResultadoBooleano objResultado = new ResultadoBooleano();
            objResultado.bResultado = false;

            objLogger.LogDebug("START --> {0} con parámetros {1}, {2}", nameof(ExisteLogo2), strIdentifier, strUsuarioPro);

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    var objUsuario = objServicioExternal.RecuperarUsuarioPro(strUsuarioPro);

                    if ((objUsuario != null) && (objUsuario.idClienteNav > 0))
                    {

                        var sResult = objServicioWcfRepository.getExtensionLogoUsuarioProAsync(objConfig.cfgAreaLogo2, strUsuarioPro, objUsuario.idClienteNav, objUsuario.idClienteNav.ToString()).Result;

                        if (String.IsNullOrEmpty(sResult))
                        {
                            objResultado.eResultado = enumResultado.Error_Validacion;
                            objResultado.bResultado = false;
                            objResultado.sDescripcion = "No se ha encontrado ningún Logo (tandem)";
                        }
                        else
                        {
                            objResultado.eResultado = enumResultado.Ok_Recuperar;
                            objResultado.bResultado = true;
                            objResultado.sDescripcion = "Se ha encontrado el logo2 (tandem) de " + strUsuarioPro + ". Es un archivo "+ sResult;


                        }
                    }
                    else
                    {
                        objResultado.eResultado = enumResultado.Error_Validacion;
                        objResultado.sDescripcion = "Usuario no encontrado";
                        objResultado.bResultado = false;
                        objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo2), "Usuario no encontrado");
                    }

                }

                else
                {

                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";

                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo2), "Identificador no válido");
                }


            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ExisteLogo2), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo2), objResultado.bResultado.ToString());
            return objResultado;


        }

        public ResultadoBase ModificarLoginEmail(string strIdentifier, int IdClienteNav, string IdUsuarioPro, string strEmailAnterior, string strEmailNuevo)
        {
            objLogger.LogDebug("START --> {0} con parámetros {1},{2},{3},{4},{5}", nameof(ModificarLoginEmail), strIdentifier, IdClienteNav.ToString(), IdUsuarioPro, strEmailAnterior, strEmailNuevo);
            
            ResultadoEntero objResultado = new ResultadoEntero();

            try
            {
                if (IsValidoIdentificador(strIdentifier))
                {
                    return (ResultadoEntero)objServicioExternal.ModificarEmailUsuarioPro(IdClienteNav, IdUsuarioPro, strEmailAnterior, strEmailNuevo, objConfig.cfgIdUsuarioApi);
                }

                else
                {
                    objResultado.eResultado = enumResultado.Error_IdentificadorNoValido;
                    objResultado.sDescripcion = "Identificador no válido";
                    objLogger.LogDebug("Resultado {0} --> {1}", nameof(ExisteLogo2), "Identificador no válido");
                }
            }
            catch (Exception ex)
            {
                objLogger.LogError("Error -->{0} Mensaje-->{1}", nameof(ModificarLoginEmail), ex.Message);
                objResultado.eResultado = enumResultado.Error_Excepcion;
                objResultado.sDescripcion = ex.Message;

            }
            objLogger.LogDebug("Resultado {0} --> {1}", nameof(ModificarLoginEmail), objResultado.sDescripcion);
            return objResultado;


        }


    }
}

