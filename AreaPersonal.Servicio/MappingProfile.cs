﻿using AreaPersonal.Comun.Enums;
using AutoMapper;
using System.Collections.Generic;



namespace AreaPersonal.Servicio
{
    /// <summary>
    /// Configuración del mapeo de AUTOMAPPER que es cargado por DI en el método del proyecto llamante
    /// </summary>
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //CreateMap<ClaseModelo, ClaseVista>()
            //    .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.IdOrigen))
            //    .ForMember(dest => dest.url, opt => opt.MapFrom(src => src.UrlOrigen))
            //    .ForMember(dest => dest.descripcion, opt => opt.MapFrom(src => src.DescOrigen))
            //    .ReverseMap();
            CreateMap<Navision.Vistas.Facturas.Factura, Vistas.Factura>()
                .ForMember(dest => dest.idFacturaNav, opt => opt.MapFrom(src => src.IdFacturaNav))
                .ForMember(dest => dest.idClienteFacturacionNav, opt => opt.MapFrom(src => src.IdClienteFacturacionNav))
                .ForMember(dest => dest.idClienteEnvioNav, opt => opt.MapFrom(src => src.IdClienteEnvioNav))
                .ForMember(dest => dest.importe, opt => opt.MapFrom(src => src.strImporteIVA))
                .ForMember(dest => dest.importeImpago, opt => opt.MapFrom(src => src.strImpago))
                .ForMember(dest => dest.fecha, opt => opt.MapFrom(src => src.strFechaRegistro))
                .ForMember(dest => dest.fechaVencimiento, opt => opt.MapFrom(src => src.strFechaVencimiento))
                .ForMember(dest => dest.IEstadoPagada, opt => opt.MapFrom(src => src.strImpago == "0" ? EstadoFacturasEnum.Pagada : EstadoFacturasEnum.Pendiente));
        }
    }
}
