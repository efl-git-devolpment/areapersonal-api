﻿namespace AreaPersonal.Comun.Enums
{
    public enum OrdenarFacturasEnum
    {
        SinOrden = 0,
        FechaCreciente,
        FechaDecreciente,
        EstadoCreciente,
        EstadoDecreciente
    }
}
