﻿namespace AreaPersonal.Comun.Enums
{
    public enum EstadoFacturasEnum
    {
        Pagada = 1,
        Pendiente,
        EnProceso,
    }
}
