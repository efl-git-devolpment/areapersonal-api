﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AreaPersonal.Comun.Extensions
{
    public static class Helpers
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
            this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector)
        {
            var knownKeys = new HashSet<TKey>();
            return source.Where(element => knownKeys.Add(keySelector(element)));
        }
    }
}
