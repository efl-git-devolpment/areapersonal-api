﻿using System;

namespace AreaPersonal.Comun
{
    /// <summary>
    /// Excepción para controlar el guardado de URL
    /// </summary>
    public class FormatoUrlException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public FormatoUrlException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public FormatoUrlException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public FormatoUrlException(string messsage, Exception inner) : base(messsage, inner)
        { }

    }
}