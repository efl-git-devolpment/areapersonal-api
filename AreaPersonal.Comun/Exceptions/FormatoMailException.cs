﻿using System;

namespace AreaPersonal.Comun
{
    /// <summary>
    /// Excepción para controlar el guardado de direcciones de correo
    /// </summary>
    public class FormatoMailException : Exception
    {
        /// <summary>
        /// Constructor básico
        /// </summary>
        public FormatoMailException()
        { }

        /// <summary>
        /// Constructor con mensaje de error
        /// </summary>
        /// <param name="messsage"></param>
        public FormatoMailException(string messsage) : base(messsage)
        { }

        /// <summary>
        /// Constructor con mensaje de error y manteniendo datos de la excepción original
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="inner"></param>
        public FormatoMailException(string messsage, Exception inner) : base(messsage, inner)
        { }

    }
}