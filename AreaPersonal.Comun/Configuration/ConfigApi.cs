﻿using DllApiBase.Comun.Configuracion;
using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Comun.Configuration
{
    public class ConfigApi : ConfigApiBase
    { /// <summary>
        /// <summary>
        /// Configuración de variables
        /// </summary>
        //public ObjVariables cfgVariables { get; set; } // Migrado al Api de Servicios

        /// <summary>
        /// Configuracion de los valores de acceso a los servicio de Producto
        /// </summary>
        public DllApiBase.Comun.Configuracion.ConfigUrlProducto cfgUrlProducto { get; set; }

        public int cfgAreaLogo { get; set; }
        public int cfgAreaLogo2 { get; set; }

        public string cfgExtensionLogo { get; set; }
        public string cfgExtensionLogo2 { get; set; }

    }
}
