﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Comun.Configuration
{
    public class ObjVarDocumentacion
    {
        public string mail_documentacion { get; set; }

        public string UrlAppGestion { get; set; }

        public string UrlAppDocumentacion { get; set; }

        public int iGroupMail { get; set; }
    }
}
