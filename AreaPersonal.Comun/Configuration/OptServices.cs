﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Comun.Configuration
{
    public class OptServices
    {
        public string ConsultoriaSvc { get; set; }

        public string ClavesSvc { get; set; }

        public string FormacionSvc { get; set; }

        public string DocumentacionSvc { get; set; }

        public string EmailsSvc { get; set; }

        public string NavisionSvc { get; set; }

        public string OnlineSvc { get; set; }

        public string CommToolsApi { get; set; }

        public string RepositorySvc { get; set; }

        public string ExtraMementosSvc { get; set; }
    }
}
