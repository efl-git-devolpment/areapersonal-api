﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AreaPersonal.Comun.Configuration
{
    /// <summary>
    /// Variables de Consultoria
    /// </summary>
    public class ObjVarConsultoria
    {
        public int PeticionSimultanea { get; set; }
        public int IdAreaConsultoria { get; set; }
        public string UrlAppGestion { get; set; }

        public string UrlAppConsultoria { get; set; }

        public int mail_idGroup1 { get; set; }
        public int mail_idGroup3 { get; set; }
        public int mail_idGroup8 { get; set; }
        public int mail_idGroup10 { get; set; }
        public int mail_idGroup11 { get; set; }
        public int mail_idGroup12 { get; set; }
        public int mail_idGroup13 { get; set; }
        public int mail_idGroup14 { get; set; }
        public int mail_idGroup15 { get; set; }
        public int mail_idGroup16 { get; set; }
        public int mail_idGroup17 { get; set; }
        public int mail_idGroup20 { get; set; }
        public int mail_idGroup21 { get; set; }
        public int mail_idGroup22 { get; set; }
        public int mail_idGroup23 { get; set; }
        public int mail_idGroup24 { get; set; }
        public int mail_idGroup25 { get; set; }
        public int mail_idGroup26 { get; set; }
        public int mail_idGroup27 { get; set; }


        public int mail_n1General_1 { get; set; }
        public int mail_n1General_3 { get; set; }
        public int mail_n2General_1 { get; set; }
        public int mail_n2General_3 { get; set; }

        public int mail_n2Responsable_1 { get; set; }
        public int mail_n2Responsable_3 { get; set; }
        public string mail_ProteccionDatos { get; set; }

        public string urlNrefDocument { get; set; }
        public string urlNrefMarginal { get; set; }

        public string urlNrefArticle { get; set; }

        public string urlNrefTitle { get; set; }
      
        public int idUserApp { get; set; }

        public int idUsuarioAreaPersonal { get; set; }
        public int idUsuarioNeo { get; set; }
        public int idUsuarioEspacioLefebre { get; set; }
        public int IdUsuarioSercicioExperto { get; set; }
    }
}
