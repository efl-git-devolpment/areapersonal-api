﻿namespace Patton.Comun.Configuration
{
    /// <summary>
    /// Contenedor de configuraciones de servicios externos
    /// </summary>
    public class OptUrls
    {
        /// <summary>
        /// Servicio de LeXon
        /// </summary>
        public Svc Lexon { get; set; }

        /// <summary>
        /// Servicio de LED
        /// </summary>
        public SvcLed Led { get; set; }

        /// <summary>
        /// Servicio de Evolution
        /// </summary>
        public SvcEvolution Evolution { get; set; }

        /// <summary>
        /// Servicio de Jira
        /// </summary>
        public SvcJira Jira { get; set; }

    }
}