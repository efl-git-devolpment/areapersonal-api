﻿using AreaPersonal.Api.Extensions;
using AreaPersonal.Comun.Configuration;
using DllApiBase.Vistas;
using DllApiBase.WebApi.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO;
using System.Linq;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((ctx, lc) => lc
.ReadFrom.Configuration(ctx.Configuration));

builder.Services.RegisterServices();
builder.Services.AddOptions();
builder.Services.Configure<ConfigApi>(builder.Configuration);
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddSession(opt =>
{
    opt.IdleTimeout = TimeSpan.FromMinutes(60);
});
builder.Services.AddMvc();
builder.Services.Configure<ApiBehaviorOptions>(apiBehaviorOptions => apiBehaviorOptions.InvalidModelStateResponseFactory = actionContext =>
{
    return new BadRequestObjectResult(new
    {
        eResultado = enumResultado.Error_Parametros,
        sDescripcion = $"Error en los parámetros: {string.Join('/', actionContext.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage))}"
    });
});
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v3", new OpenApiInfo
    {
        Version = "v3.03.03",
        Title = "API AreaPersonal - Mi Cuenta",
        Description = "API que nos permite operar con servicios de Area Personal",
        TermsOfService = new Uri("http://www.lefebvre.es"),
        Contact = new OpenApiContact
        {
            Name = "Lefebvre.es",
            Url = new Uri("http://www.lefebvre.es"),
        },
        License = new OpenApiLicense
        {
            Name = "Propiedad de Lefevbre - El Derecho",
            Url = new Uri("http://www.lefebvre.es")
        }
    });
    var basePath = AppContext.BaseDirectory;
    var xmlPath = Path.Combine(basePath, "AreaPersonal.xml");
    c.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

app.UseSession();
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v3/swagger.json", "API AreaPersonal V3.03.04");
    c.RoutePrefix = @"api";
    c.DocExpansion(DocExpansion.List);
});
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.UseRequestLogging();

app.Run();