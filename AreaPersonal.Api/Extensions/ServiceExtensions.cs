﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DllApiBase.Vistas;
using DllApiBase.Vistas.Claves.Usuario;
using Navision.Vistas.Cliente;
using DllApiBase.Vistas.Permisos;
using ClavesSvc;
using DllApiBase.Vistas.Cliente;
using Navision.Vistas.Facturas;
using Navision.Vistas.Productos;
using AreaPersonal.Vistas.External.Claves;
using AreaPersonal.Vistas.External.Crm;

namespace AreaPersonal.Api.Extensions
{
    /// <summary>
    ///   Extensiones para tratar la inyección de servicios en Asp core
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        ///   Nos permite mantener ConfigureServices más limpio y mantenible https://www.infoworld.com/article/3232636/application-development/how-to-use-dependency-injection-in-aspnet-core.html
        ///   1. SINGLETON: solo se crea una instancia que es compartida por todos los clientes.
        ///   2. SCOPED: Una instancia por cada petición.
        ///   3. TRANSIENT: No será compartidos y se crea cada vez que se requiera.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {

            //services.AddScoped<IFacturaService<ResultadoFacturas, ResultadoAbonos, ResultadoGenerarFactura, ResultadoGenerarAbono>, FacturaService>();
            services.AddScoped<IFacturaV2Service<ResultadoBase>, FacturaV2Service>();
            services.AddScoped<IExternalService<UsuarioPro, ClientePro, PermisosAsociado, ClienteNavision, ResultadoBase, FacturaPDF, AbonoPDF, Modelo347PDF, ProductoCliente, InfoCuenta,ClienteBusqueda>, ExternalService>();
            services.AddScoped<IDatosPersonalesService<ResultadoBase>, DatosPersonalesService>();
            services.AddScoped<IClientesService<ResultadoBase>, ClientesService>();

            return services;
        }

       
    }
}
