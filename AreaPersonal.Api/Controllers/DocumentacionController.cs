﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que gestiona las llamadas y Peticiones a Documentación
    /// </summary>
    [Route("Documentacion/")]
    public class DocumentacionController : BaseController<DocumentacionController>
    {

        private readonly IDocumentacionService<Cliente, UsuarioProducto,TipoDocumento, ResultadoGuardar> _svc;
        private readonly ObjVariables _objVariables;
        /// <summary>
        ///  Constructor del controlador de Documentación
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_svcDocumentacion"></param>
        /// <param name="configWeb"></param>
        public DocumentacionController(
         ILogger<DocumentacionController> logger,
         IDocumentacionService<Cliente, UsuarioProducto, TipoDocumento, ResultadoGuardar> _svcDocumentacion,
         IOptions<OptConfig> configWeb
            ) : base(configWeb, logger)
        {
            _svc = _svcDocumentacion;
            _objVariables = MyConfig.Variables;
        }

        /// <summary>
        /// Método que devuelve datos del Cliente
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Código de Cliente Navision</param>
        [Route("Cliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(Cliente), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarCliente(string strIdentifier, int IdClienteNav)
        {
            if ((IdClienteNav <= 0) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var cliente = await _svc.RecuperarCliente(strIdentifier, IdClienteNav);

            return cliente == null ? (ActionResult)NotFound() : new ObjectResult(cliente);
        }

        /// <summary>
        /// Método que devuelve datos del usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>

        [Route("Usuario/")]
        [HttpGet]
        [ProducesResponseType(typeof(UsuarioProducto), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarUsuario(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var usuario = await _svc.RecuperarUsuario(strIdentifier, strUsuarioPro);

            return usuario == null ? (ActionResult)NotFound() : new ObjectResult(usuario);
        }

        /// <summary>
        /// Metodo que devuelve los tipos de documentos
        /// </summary>
        /// <param name="strIdentifier"></param>
        /// <returns></returns>
        [Route("TipoDocumentos/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TipoDocumento>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarTipos(string strIdentifier)
        {
            if (String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstTipos = await _svc.RecuperarTipoDocumento(strIdentifier);

            return lstTipos == null ? (ActionResult)NotFound() : new ObjectResult(lstTipos);
        }

        /// <summary>
        /// Método para guardar una petición.
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strEmail">Email del usuario</param>
        /// <param name="strNombre">Nombre del usuario</param>
        /// <param name="strApellidos">Apellido del usuario</param>
        /// <param name="strPeticion">Planteamiento de la petición</param>
        /// <param name="strTelefono">Teléfono del usuario</param>
        /// <param name="strTipoDocumento">Descripción del tipo de documento</param>
        /// <param name="strTitular">Nombre del titular de la cuenta</param>
        /// <param name="iTipoDocumento">Identificador del tipo</param>
        /// <param name="sUsuarioPro">Usuario de la petición</param> 
        /// <param name="iCodNavision">Identificador del cliente Navision</param>
        /// <param name="iCodCRM">Identificador del cliente Kojak</param>
        ///  /// <param name="sNombreCliente">Nombre del Cliente</param>
        /// <returns></returns>
        [Route("GuardarDocumento/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoGuardar), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SaveDocumento(string strIdentifier, string strPeticion, int iTipoDocumento, int iCodNavision, string sUsuarioPro, string strNombre, string strApellidos, string strEmail, string strTelefono, 
            string strTitular, string sNombreCliente, string strTipoDocumento, int iCodCRM)
        {


            if ((iCodNavision <= 0) || (iTipoDocumento <= 0) || (strNombre.Length <= 0)|| (strTelefono.Length <= 0) || (strEmail.Length <= 0) 
                || (strPeticion.Length <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            int iEstado = 1;

            ResultadoGuardar objResult = _svc.SaveDocumento(strIdentifier, strPeticion, iTipoDocumento, iCodNavision, sUsuarioPro, strNombre,
                strApellidos, strEmail, strTelefono, strTitular, sNombreCliente, strTipoDocumento, iCodCRM);


            if ((objResult.bEnviarEmail) && (objResult.iIdConsulta > 0))
            {
                _svc.SendEmailMessageAlta(strIdentifier, iCodCRM, strEmail, strNombre, objResult.iIdConsulta);
            }
            return new ObjectResult(objResult);
        }
    }
}

