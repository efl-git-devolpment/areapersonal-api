﻿using System;
using System.Collections.Generic;
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using AreaPersonal.Vistas.DatosPersonales;
using DllApiBase.WebApi;
using DllApiBase.Vistas;
using DllApiBase.Servicio;

namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que gestiona las llamadas y Peticiones a Consultoria
    /// </summary>
    [Route("DatosPersonales/")]
    public class DatosPersonalesController : ControllerApiBase
    {
        private readonly ILogger<DatosPersonalesController> objLogger;
        private readonly IDatosPersonalesService<ResultadoBase> objServicioDatosPersonales;
        private readonly ObjVariables objVariables;
        /// <summary>
        ///  Constructor del controlador de Consultoria
        /// </summary>
        /// <param name="objServicioDatosPersonalesBase"></param>
        /// <param name="objLoggerBase">servicio de log</param>
        /// <param name="cfgConfigApi"></param>
        public DatosPersonalesController(
         ILogger<DatosPersonalesController> objLoggerBase,
         IDatosPersonalesService<ResultadoBase> objServicioDatosPersonalesBase,
         IOptions<ConfigApi> cfgConfigApi
            ) : base(cfgConfigApi)
        {
            objLogger = objLoggerBase;
            objServicioDatosPersonales = objServicioDatosPersonalesBase;
            //objVariables = cfgConfigApi.Value.cfgVariables;
        }

        /// <summary>
        /// Método que devuelve datos personales de un Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        [Route("RecuperarDatosPersonales/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<UsuarioPersonal>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarDatosPersonales(string strIdentifier, string strIdUsuarioPro)
        {

            ResultadoOperacion<UsuarioPersonal> objResultado = new ResultadoOperacion<UsuarioPersonal>();
            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoOperacion<UsuarioPersonal>)objServicioDatosPersonales.RecuperarDatosPersonales(strIdentifier, strIdUsuarioPro);

            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

          
        }

        /// <summary>
        /// Método que devuelve datos del usuario
        /// </summary>
        /// <param name="objRequest">Objeto contener de los campos del Usuario a Modificar</param> 
        ///// <param name="strIdUsuarioPro">Usuario de producto</param>
        ///// <param name="strName">Nombre</param>
        ///// <param name="strFirstName">Primer apellido</param>
        ///// <param name="strSurName">Segundo apellido</param>
        ///// <param name="strTreatment">Tratamiento</param>
        ///// <param name="strEmail">Email</param>
        ///// <param name="strColor">Color en Hexadesimal</param>
        ///// <param name="strNombreComercial">Nombre Comercial</param>
        [Route("ModificarDatosPersonales/")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ModificarDatosPersonales([FromBody]ModificarUsuarioRequest objRequest)
        {

            ResultadoEntero objResultado = new ResultadoEntero();
            if (objRequest != null)
            {
                if (string.IsNullOrEmpty(objRequest.strIdentifier))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
                }
                else if (String.IsNullOrEmpty(objRequest.strUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(objRequest.strUsuarioPro)))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
                }
                else
                {
                    objResultado = (ResultadoEntero)objServicioDatosPersonales.ModificarDatosPersonales(objRequest.strIdentifier, objRequest.strUsuarioPro, objRequest.strName, objRequest.strFirstName, objRequest.strSurName, objRequest.strTreatment, objRequest.strEmail, objRequest.strColor, objRequest.strNombreComercial);
                }
            }
            else
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

    
        }

        /// <summary>
        /// Método que devuelve la ruta de acceso a Logo de un usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        [Route("RecuperarLogo/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarLogo(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoCadena objResultado = new ResultadoCadena();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoCadena)objServicioDatosPersonales.RecuperarLogo(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Método Post para modificar el Logo de un Usuario
        /// </summary>
        /// <param name="objRequest">Objeto contener de los parámetros</param> 
        /// <returns></returns>
        [Route("ModificarLogo/")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ModificarLogo([FromBody] ModificarLogoRequest objRequest)
        {
            ResultadoEntero objResultado = new ResultadoEntero();
            if (objRequest != null)
            {
                if (string.IsNullOrEmpty(objRequest.strIdentifier))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
                }
                else if (String.IsNullOrEmpty(objRequest.strUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(objRequest.strUsuarioPro)))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetro no válido: strUsuarioPro debe estar relleno y formato correcto";
                }
                else
                {
                    String strIdUsuarioPro = objRequest.strUsuarioPro;
                    Byte[] bdata = objRequest.bdata;
                    objResultado = (ResultadoEntero)objServicioDatosPersonales.ModificarLogo(objRequest.strIdentifier, strIdUsuarioPro, bdata);
                  
                }
            }
            else
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para resetar el Logo de un Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        /// <returns></returns>
        [Route("ResetearLogo/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ResetearLogo(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoEntero objResultado = new ResultadoEntero();
            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoEntero)objServicioDatosPersonales.ResetearLogo(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

          
        }

        /// <summary>
        /// Método para conocer si existe el Logo de un Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        /// <returns></returns>
        [Route("ExisteLogo/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoBooleano), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ExisteLogo(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoBooleano objResultado = new ResultadoBooleano();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoBooleano)objServicioDatosPersonales.ExisteLogo(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Método que devuelve la ruta de acceso a Logo2 en formato PNG (tandem) de un usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        [Route("RecuperarLogo2/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarLogo2(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoCadena objResultado = new ResultadoCadena();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoCadena)objServicioDatosPersonales.RecuperarLogo2(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Método para modificar el Logo2 en formato PNG (tandem) de un Usuario
        /// </summary>
        /// <param name="objRequest">Objeto contener de los parámetros</param> 
        /// <returns></returns>
        [Route("ModificarLogo2/")]
        [HttpPost]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ModificarLogo2([FromBody] ModificarLogoRequest objRequest)
        {
            ResultadoEntero objResultado = new ResultadoEntero();
            if (objRequest != null)
            {
                if (string.IsNullOrEmpty(objRequest.strIdentifier))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
                }
                else if (String.IsNullOrEmpty(objRequest.strUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(objRequest.strUsuarioPro)))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetro no válido: strUsuarioPro debe estar relleno y formato correcto";
                }
                else
                {
                    String strIdUsuarioPro = objRequest.strUsuarioPro;
                    Byte[] bdata = objRequest.bdata;
                    objResultado = (ResultadoEntero)objServicioDatosPersonales.ModificarLogo2(objRequest.strIdentifier, strIdUsuarioPro, bdata);

                }
            }
            else
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método para resetar el Logo2 (tandem) de un Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        /// <returns></returns>
        [Route("ResetearLogo2/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoCadena), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ResetearLogo2(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoEntero objResultado = new ResultadoEntero();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoEntero)objServicioDatosPersonales.ResetearLogo2(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Método para conocer si exixte  el Logo2 (tandem) de un Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strIdUsuarioPro">Usuario de producto</param>
        /// <returns></returns>
        [Route("ExisteLogo2/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoBooleano), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ExisteLogo2(string strIdentifier, string strIdUsuarioPro)
        {
            ResultadoBooleano objResultado = new ResultadoBooleano();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(strIdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(strIdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: strIdUsuarioPro debe estar relleno y formato correcto";
            }
            else
            {
                objResultado = (ResultadoBooleano)objServicioDatosPersonales.ExisteLogo2(strIdentifier, strIdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Método para modificar el email/login de un usuario de producto
        /// </summary>
        /// <param name="objPeticion">Objeto contener de los parámetros</param> 
        /// <returns></returns>
        [Route("ModificarLoginEmail/")]
        [HttpPut]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ModificarLoginEmail([FromBody] ModificarEmailPeticion objPeticion)
        {
            ResultadoEntero objResultado = new ResultadoEntero();
            if (objPeticion != null)
            {
                objLogger.LogDebug("START -->{0} Parámetros-->{1}", nameof(ModificarLoginEmail), JsonConvert.SerializeObject(objPeticion));


                if (string.IsNullOrEmpty(objPeticion.strIdentifier))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
                }
                else if (objPeticion.IdClienteNav <= 0)
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: IdClienteNav debe ser mayor que cero.";
                }
                else if (String.IsNullOrEmpty(objPeticion.IdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(objPeticion.IdUsuarioPro)))
                {
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Parámetro no válido: IdUsuarioPro debe estar relleno y formato correcto";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmailAnterior))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailAnterior no esta relleno o no tiene formato correcto.";
                }
                else if (!ServicioHelpers.ValidarEmail(objPeticion.strEmailNuevo))
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailAnterior no esta relleno o no tiene formato correcto.";
                }
                else if (objPeticion.strEmailNuevo.ToLower() == objPeticion.strEmailAnterior.ToLower())
                {
                    objResultado.iResultado = -1;
                    objResultado.eResultado = enumResultado.Error_Parametros;
                    objResultado.sDescripcion = "Error en parámetros: objPeticion.strEmailAnterior no puede se igual a objPeticion.strEmailNuevo.";
                }
                else
                {

                    objResultado = (ResultadoEntero)objServicioDatosPersonales.ModificarLoginEmail(objPeticion.strIdentifier,objPeticion.IdClienteNav,objPeticion.IdUsuarioPro,objPeticion.strEmailAnterior,objPeticion.strEmailNuevo);

                }
            }
            else
            {
                objLogger.LogDebug("START -->{0} Parámetros -->{1}", nameof(ModificarLoginEmail), " sin parámetros");

                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no valido: El objeto esta vacio.";
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

    }
}
