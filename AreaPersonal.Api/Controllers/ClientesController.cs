﻿using DllApiBase.Comun.Configuracion;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AreaPersonal.Vistas;
using System.Threading.Tasks;
using AreaPersonal.Servicio;
using System;
using Navision.Vistas.Productos;


namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que recupera los Clientes
    /// </summary>

    [Route("Clientes")]
    public class ClientesController : ControllerApiBase
    {
        private readonly ILogger<ClientesController> objLogger;
        private readonly IClientesService<ResultadoBase> objServiceClientes;

        /// <summary>
        /// Constructor al que se inyectan los servicios aplicables
        /// </summary>
        /// <param name="objServiceClientesBase">servicio para tratar clientes</param>
        /// <param name="objlogger">servicio de log</param>
        /// <param name="configAccessor"></param>
        public ClientesController(
            IClientesService<ResultadoBase> objServiceClientesBase,
            ILogger<ClientesController> objlogger,
            IOptions<ConfigApiBase> configAccessor
            ) : base(configAccessor)
        {
            objServiceClientes = objServiceClientesBase;
            objLogger = objlogger;
        }

        /// <summary>
        /// Método que recupera el Cif de un Cliente de Navision / BC
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdUsuarioPro">Usuario de producto</param>
        /// <param name="IdClienteNav">Identificador cliente Navision</param>
        [Route("RecuperarCif/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<UsuarioPersonal>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarCif([FromQuery] string strIdentifier, [FromQuery] int IdClienteNav, [FromQuery] string IdUsuarioPro)
        {

            ResultadoCadena objResultado = new ResultadoCadena();
            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(IdUsuarioPro) && (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: IdUsuarioPro debe estar relleno o IdClienteNav mayor que cero";
            }
            else if (!String.IsNullOrEmpty(IdUsuarioPro) && (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(IdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdUsuarioPro si esta relleno necesita formato correcto";
            }
            else
            {
                objResultado = (ResultadoCadena)objServiceClientes.RecuperarCifClienteNav(strIdentifier, IdClienteNav, IdUsuarioPro);

            }
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);


        }

        /// <summary>
        /// Recupera el documento Modelo 347 de un Cliente Navision / BC
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strCif">CIF del Cliente</param>
        /// <param name="intAnyo">Año del modelo</param>
        /// <response code="200">Ok. Se ha devuelto resultado</response>              
        /// <response code="404">Error. No se devuelve resultado</response>    
        /// <returns>Devuelve el modelo 347 de un Cliente.</returns>
        [Route("RecuperarModelo347/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<Modelo347PDFNav>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarModelo347([FromQuery] string strIdentifier, [FromQuery] string strCif, [FromQuery] int intAnyo)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2},{3}", nameof(RecuperarModelo347), strIdentifier, strCif, intAnyo.ToString());

            ResultadoOperacion<Modelo347PDFNav> objResultado = new ResultadoOperacion<Modelo347PDFNav>();

            if (intAnyo < 1)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo intAnyo";
            }
            else if (string.IsNullOrWhiteSpace(strCif))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Debe rellenar el campo CIF";
            }
            else
            {
                objResultado = (ResultadoOperacion<Modelo347PDFNav>)objServiceClientes.RecuperarModelo347PDF(strIdentifier,strCif, intAnyo);
            }

            objLogger.LogDebug("Resultado -->{0} Mensaje-->{1}", nameof(RecuperarModelo347), objResultado.sDescripcion);
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        /// <summary>
        /// Recupera productos por Cliente desde Navision / BC
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>re
        /// <param name="IdClienteNav">Identificador Cliente Nav</param>
        /// <response code="200">Ok. Se ha devuelto resultado</response>              
        /// <response code="404">Error. No se devuelve resultado</response>    
        /// <returns>Devuelve el modelo 347 de un Cliente.</returns>
        [Route("RecuperarProductosCliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<ProductoCliente>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult RecuperarProductosCliente([FromQuery] string strIdentifier, [FromQuery] int IdClienteNav)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2}", nameof(RecuperarProductosCliente), strIdentifier, IdClienteNav.ToString());

            ResultadoLista<ProductoCliente> objResultado = new ResultadoLista<ProductoCliente>();

            if (IdClienteNav < 1)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdClienteNav debe mayor que cero";
            }
            else
            {
                objResultado = (ResultadoLista<ProductoCliente>)objServiceClientes.RecuperarProductosClienteNav(strIdentifier, IdClienteNav);
            }

            objLogger.LogDebug("Resultado -->{0} Mensaje-->{1}", nameof(RecuperarProductosCliente), objResultado.sDescripcion);
            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);

        }

        //Recuperar Información del Cliente
        /// <summary>
        /// Método que devuelve datos del Cliente
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Código de Cliente Navision</param>
        [Route("RecuperarCliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<Cliente>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public ActionResult RecuperarCliente([FromQuery] string strIdentifier, [FromQuery] int IdClienteNav)
        {
            ResultadoOperacion<Cliente> objResultado = new ResultadoOperacion<Cliente>();

            if ((IdClienteNav <= 0) || String.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos";
            }
            else
            {
              
                    objResultado = (ResultadoOperacion<Cliente>)objServiceClientes.RecuperarInformacionCuenta(strIdentifier, IdClienteNav, -1);
                

            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

    }
}

