﻿
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que gestiona los Mementos
    /// </summary>
    [Route("Revistas/")]
    public class RevistasController : BaseController<RevistasController>
    {

        private readonly ICommToolsService<Revista> _svc;
        private readonly ObjVariables _objVariables;
        /// <summary>
        ///  Constructor del controlador de Memento
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_svcCommTools"></param>
        /// <param name="configWeb"></param>
        public RevistasController(
         ILogger<RevistasController> logger,
         ICommToolsService<Revista> _svcCommTools,
         IOptions<OptConfig> configWeb
            ) : base(configWeb, logger)
        {
            _svc = _svcCommTools;
            _objVariables = MyConfig.Variables;
        }

        /// <summary>
        /// Método que devuelve el histórico de Mementos
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Usuario de producto</param>
        [Route("RecuperarRevistasPorUsuario/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Revista>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarRevistasPorUsuario(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstRevistas = await _svc.RecuperarRevistas(strIdentifier, strUsuarioPro);

            return lstRevistas == null ? (ActionResult)NotFound() : new ObjectResult(lstRevistas);
        }  

    }
}
