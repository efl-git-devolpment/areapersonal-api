﻿
using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que gestiona los Certificados
    /// </summary>
    [Route("Certificados/")]
    public class CertificadosController : BaseController<CertificadosController>
    {

        private readonly ICertificadoService<CertificadoResult,Certificado, CertificadoPropiedad> _svc;
        private readonly ObjVariables _objVariables;
        /// <summary>
        ///  Constructor del controlador de Certificados
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_svcCertificados"></param>
        /// <param name="configWeb"></param>
        public CertificadosController(
         ILogger<CertificadosController> logger,
         ICertificadoService<CertificadoResult, Certificado, CertificadoPropiedad> _svcCertificados,
         IOptions<ConfigApi> configWeb
            ) : base(configWeb, logger)
        {
            _svc = _svcCertificados;
            _objVariables = configWeb.Value.cfgVariables;
        }

        /// <summary>
        /// Método que devuelve los Certificados de un acceso y tipo
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Usuario de producto</param>
        /// <param name="intTipoCertificado">Tipo de Certificado</param>
        [Route("CertificadosList/")]
        [HttpGet]
        [ProducesResponseType(typeof(CertificadoResult), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarCertificados(string strIdentifier, string strUsuarioPro, int intTipoCertificado)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstCertificates = await _svc.RecuperarCertificados(strIdentifier, strUsuarioPro, intTipoCertificado);

            return lstCertificates == null ? (ActionResult)NotFound() : new ObjectResult(lstCertificates);
        }


        /// <summary>
        ///Método que devuelve el número de Certificados disponibles de un usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Cñodigo de usuario</param>
        /// <returns></returns>
        [Route("NumCertificadosDisponibles/")]
        [HttpGet]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarNumCertificadosDisponibles(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var iCount =  _svc.RecuperarNumCertificadosDisponibles(strIdentifier, strUsuarioPro);

            return  new ObjectResult(iCount);
        }

        /// <summary>
        /// Método que devuelve el número total de Certificados de un usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Código de usuario</param>
        /// <returns></returns>
        [Route("NumCertificadosTotal/")]
        [HttpGet]
        [ProducesResponseType(typeof(int), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public ActionResult RecuperarCertificadosTotal(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var iCount = _svc.RecuperarNumCertificadosTotal(strIdentifier, strUsuarioPro);

            return new ObjectResult(iCount);
        }

    }
}


