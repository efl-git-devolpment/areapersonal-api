﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using AreaPersonal.Comun.Enums;
using DllApiBase.Vistas;
using DllApiBase.WebApi;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.ComponentModel.DataAnnotations;

namespace AreaPersonal.WebApi.Controllers
{

    /// <summary>
    /// Controlador que gestiona las facturas y abonos
    /// </summary>
    [Route("Facturas/")]
    [ApiController]
    public class FacturasController : ControllerApiBase
    {

        private readonly IFacturaV2Service<ResultadoBase> objFacturasV2Service;
        private readonly ObjVariables objVariables;
        private readonly ILogger<FacturasController> objLogger;
        private readonly IOptions<ConfigApi> objConfig;

        /// <summary>
        ///  Constructor del controlador de Facturas
        /// </summary>
        /// <param name="objLoggerBase"></param>

        /// <param name="objFacturasV2ServiceBase"></param>
        /// <param name="objConfigBase"></param>
        public FacturasController(
         ILogger<FacturasController> objLoggerBase,
         //IFacturaService<ResultadoFacturas, ResultadoAbonos, ResultadoGenerarFactura, ResultadoGenerarAbono> objFacturasServiceBase,
         IFacturaV2Service<ResultadoBase> objFacturasV2ServiceBase,
         IOptions<ConfigApi> objConfigBase
            ) : base(objConfigBase)
        {
            objConfig = objConfigBase;
            objLogger = objLoggerBase;
            objFacturasV2Service = objFacturasV2ServiceBase;

        }

        ///// <summary>
        ///// Método que devuelve facturas por Usuario
        ///// </summary>
        ///// <param name="strIdentifier">Identificador del Origen</param>
        ///// <param name="intCodNavision">Codigo Navision del cliente</param>
        ///// <param name="strIdUsuarioPro">Usuario de producto</param>
        ///// <param name="numPage">Nº de página</param>
        ///// <param name="recsPerPage">Nº registros por página</param>
        ///// <param name="startdate">Fecha de inicio</param>
        ///// <param name="enddate">Fecha de fin</param>
        //[Route("RecuperarListadoFacturas/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ResultadoFacturas), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public async Task<IActionResult> RecuperarFacturas(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate)
        //{

        //    if (String.IsNullOrEmpty(strIdUsuarioPro) || String.IsNullOrEmpty(strIdentifier) || (intCodNavision <= 0))
        //        return BadRequest();

        //    var lstFacturas = await objFacturasService.RecuperarFacturas(strIdentifier, intCodNavision, strIdUsuarioPro, numPage, recsPerPage, startdate, enddate);

        //    return lstFacturas == null ? (ActionResult)NotFound() : new ObjectResult(lstFacturas);
        //}


        ///// <summary>
        ///// Método que devuelve la información de una factura
        ///// </summary>
        ///// <param name="strIdentifier">Identificador del Origen</param>
        ///// <param name="idFactura">Identicador de la Factura</param>
        ///// <param name="intCodNavision">Codigo Navision del cliente</param>
        ///// <returns></returns>
        //[Route("RecuperarFactura/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ResultadoGenerarFactura), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public ActionResult RecuperarFacturaPdf_Obsoleto(string strIdentifier, string idFactura, int intCodNavision)
        //{

        //    if ((String.IsNullOrEmpty(idFactura)) || String.IsNullOrEmpty(strIdentifier) || (intCodNavision <= 0))
        //        return BadRequest();

        //    var objResult = objFacturasService.RecuperarFacturaPDFById(strIdentifier, idFactura, intCodNavision).Result;

        //    return objResult == null ? (ActionResult)NotFound() : new ObjectResult(objResult);
        //}


        ///// <summary>
        ///// Método que devuelve abonos por Usuario
        ///// </summary>
        ///// <param name="strIdentifier">Identificador del Origen</param>
        ///// <param name="intCodNavision">Codigo Navision del cliente</param>
        ///// <param name="strIdUsuarioPro">Usuario de producto</param>
        ///// <param name="numPage">Nº de página</param>
        ///// <param name="recsPerPage">Nº registros por página</param>
        ///// <param name="startdate">Fecha de inicio</param>
        ///// <param name="enddate">Fecha de fin</param>
        //[Route("RecuperarListadoAbonos/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ResultadoAbonos), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public async Task<IActionResult> RecuperarAbonos(string strIdentifier, int intCodNavision, string strIdUsuarioPro, int numPage, int recsPerPage, string startdate, string enddate)
        //{

        //    if (String.IsNullOrEmpty(strIdUsuarioPro) || String.IsNullOrEmpty(strIdentifier) || (intCodNavision <= 0))
        //        return BadRequest();

        //    var lstFacturas = await objFacturasService.RecuperarAbonos(strIdentifier, intCodNavision, strIdUsuarioPro, numPage, recsPerPage, startdate, enddate);

        //    return lstFacturas == null ? (ActionResult)NotFound() : new ObjectResult(lstFacturas);
        //}

        ///// <summary>
        ///// Método que devuelve la información de un Abono
        ///// </summary>
        ///// <param name="strIdentifier">Identificador del Origen</param>
        ///// <param name="idAbono">Identicador de un Abono</param>
        ///// <param name="intCodNavision">Codigo Navision del cliente</param>
        ///// <returns></returns>
        //[Route("RecuperarAbono/")]
        //[HttpGet]
        //[ProducesResponseType(typeof(ResultadoGenerarFactura), 200)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public ActionResult RecuperarAbonoPdf(string strIdentifier, string idAbono, int intCodNavision)
        //{

        //    if ((String.IsNullOrEmpty(idAbono)) || String.IsNullOrEmpty(strIdentifier))
        //        return BadRequest();

        //    var objResult = objFacturasService.RecuperarAbonoPDFById(strIdentifier, idAbono, intCodNavision).Result;

        //    return objResult == null ? (ActionResult)NotFound() : new ObjectResult(objResult);
        //}


        /// <summary>
        /// Método que devuelve facturas por Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Codigo Navision del cliente</param>
        /// <param name="IdUsuarioPro">Usuario de producto</param>
        /// <param name="strFechaDesde">Fecha de inicio. Formato dd/MM/aaaa</param>
        /// <param name="strFechaHasta">Fecha de fin. Formato dd/MM/aaaa</param>
        /// <param name="iPagNum">Número de página</param>
        /// <param name="iPagSize">Tamaño de página</param>
        /// <param name="iOrden">Orden que se va a usar para la respuesta. Por defecto 0 (sin orden), 1 (fechaCreciente), 2 (fechaDecreciente), 3 (estadoCreciente), 4 (estadoDecreciente)</param>
        [HttpGet("RecuperarFacturas/")]
        [ProducesResponseType(typeof(ResultadoLista<Factura>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarFacturas(string strIdentifier, [FromQuery] int IdClienteNav, [FromQuery] string IdUsuarioPro, 
            [FromQuery][RegularExpression("^(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/([0-9]{4})$", ErrorMessage = "strFechaDesde debe tener el formato dd/MM/aaaa")] string strFechaDesde, 
            [FromQuery][RegularExpression("^(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/([0-9]{4})$", ErrorMessage = "strFechaHasta debe tener el formato dd/MM/aaaa")] string strFechaHasta, 
            [FromQuery] int iPagNum, [FromQuery] int iPagSize, [FromQuery] OrdenarFacturasEnum iOrden = OrdenarFacturasEnum.SinOrden)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2},{3},{4,},{5},{6}", nameof(RecuperarFacturas), IdClienteNav.ToString(), IdUsuarioPro, strFechaDesde, strFechaHasta, iPagNum.ToString(), iPagSize.ToString());

            ResultadoLista<Factura> objResultado = new ResultadoLista<Factura>();

            if (string.IsNullOrEmpty(IdUsuarioPro) || string.IsNullOrEmpty(strIdentifier) || (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos. Debe indicarse IdClienteNav, IdUsuarioPro y el strIdentifier";
            }
            else if ((iPagNum <= 0) || (iPagSize <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos.Los valores de iPagNum o iPagSize no son válidos";
            }
            else
            {

                objResultado = (ResultadoLista<Factura>)objFacturasV2Service.RecuperarFacturas(strIdentifier, IdClienteNav, IdUsuarioPro, iPagNum, iPagSize, strFechaDesde, strFechaHasta, iOrden);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que devuelve si hay facturas nuevas para el Usuario.
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Codigo Navision del cliente</param>
        /// <param name="IdUsuarioPro">Usuario de producto</param>
        [HttpGet("ComprobarFacturasNuevas/")]
        [ProducesResponseType(typeof(ResultadoBooleano), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ComprobarFacturasNuevas(string strIdentifier, [FromQuery] int IdClienteNav, [FromQuery] string IdUsuarioPro)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2},{3}", nameof(ComprobarFacturasNuevas), strIdentifier, IdClienteNav.ToString(), IdUsuarioPro);

            var objResultado = new ResultadoBooleano();

            if (string.IsNullOrEmpty(strIdentifier))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos: Se debe indicar strIdentifier";
            }
            else if (String.IsNullOrEmpty(IdUsuarioPro) || (!DllApiBase.Servicio.ServicioHelpers.ValidarUsuarioPro(IdUsuarioPro)))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válido: IdUsuarioPro debe estar relleno y formato correcto";
            }
            else if (IdClienteNav <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetro no válidos. IdClienteNav deben mayor que cero";
            }
            else
            {
                objResultado = (ResultadoBooleano)objFacturasV2Service.RecuperarIndicadorFacturasNuevas(strIdentifier, IdClienteNav, IdUsuarioPro);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que actualiza la fecha de última revisión de facturas de un usuario.
        /// </summary>
        /// <param name="IdClienteNav">Id cliente Navision</param>
        /// <param name="IdUsuarioApp">Usuario de app</param>
        [HttpPut("ModificarFechaUltimoAccesoFacturas/")]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public IActionResult ModificarFechaUltimoAccesoFacturas([FromQuery] int IdClienteNav, [FromQuery] int IdUsuarioApp)
        {
            objLogger.LogDebug("START -->{0} Parámetros --> {1},{2}", nameof(ModificarFechaUltimoAccesoFacturas), IdClienteNav, IdUsuarioApp);

            var objResultado = new ResultadoEntero();

            if (IdClienteNav <= 0 || IdUsuarioApp <= 0)
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos. IdClienteNav e IdUsuarioApp deben ser superiores a 0";
            }
            else
            {

                objResultado = (ResultadoEntero)objFacturasV2Service.ModificarFechaUltimoAccesoFacturas(IdClienteNav, IdUsuarioApp);
            }

            return objResultado == null ? NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que devuelve la información de una factura en base64
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="iIdFactura">Identicador de la Factura</param>
        /// <param name="IdClienteNav">Código Navision del cliente</param>
        /// <returns></returns>
        [Route("RecuperarFacturaPDF/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<FacturaPDFNav>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public ActionResult RecuperarFacturaPdf(string strIdentifier, string iIdFactura, int IdClienteNav)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2}", nameof(RecuperarFacturaPdf), iIdFactura, IdClienteNav.ToString());

            ResultadoOperacion<FacturaPDFNav> objResultado = new ResultadoOperacion<FacturaPDFNav>();

            if (String.IsNullOrEmpty(iIdFactura) || String.IsNullOrEmpty(strIdentifier) || (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos. Debe indicarse IdClienteNav, iIdFactura y el strIdentifier";
            }
            else
            {

                objResultado = (ResultadoOperacion<FacturaPDFNav>)objFacturasV2Service.RecuperarFacturaPDFById(strIdentifier, iIdFactura, IdClienteNav);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que devuelve abonos por Usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Codigo Navision del cliente</param>
        /// <param name="IdUsuarioPro">Usuario de producto</param>
        /// <param name="strFechaDesde">Fecha de inicio</param>
        /// <param name="strFechaHasta">Fecha de fin</param>
        /// <param name="iPagNum">Número de página</param>
        /// <param name="iPagSize">Tamaño de página</param>
        [Route("RecuperarAbonos/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoLista<Abono>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarAbonos(string strIdentifier, [FromQuery] int IdClienteNav, [FromQuery] string IdUsuarioPro, [FromQuery] string strFechaDesde, [FromQuery] string strFechaHasta, [FromQuery] int iPagNum, [FromQuery] int iPagSize)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2},{3},{4,},{5},{6}", nameof(RecuperarAbonos), IdClienteNav.ToString(), IdUsuarioPro, strFechaDesde, strFechaHasta, iPagNum.ToString(), iPagSize.ToString());

            ResultadoLista<Abono> objResultado = new ResultadoLista<Abono>();

            if (String.IsNullOrEmpty(IdUsuarioPro) || String.IsNullOrEmpty(strIdentifier) || (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos. Debe indicarse IdClienteNav, IdUsuarioPro y el strIdentifier";
            }
            else if ((iPagNum <= 0) || (iPagSize <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos.Los valores de iPagNum o iPagSize no son válidos";
            }
            else
            {

                objResultado = (ResultadoLista<Abono>)objFacturasV2Service.RecuperarAbonos(strIdentifier, IdClienteNav, IdUsuarioPro, iPagNum, iPagSize, strFechaDesde, strFechaHasta);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Método que devuelve la información de un abono en base64
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="iIdAbono">Identicador del abono</param>
        /// <param name="IdClienteNav">Código Navision del cliente</param>
        /// <returns></returns>
        [Route("RecuperarAbonoPDF/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion<AbonoPDFNav>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public ActionResult RecuperarAbonoPDF(string strIdentifier, string iIdAbono, int IdClienteNav)
        {
            objLogger.LogDebug("START -->{0} Parámetros -->{1},{2}", nameof(RecuperarAbonoPDF), iIdAbono, IdClienteNav.ToString());

            ResultadoOperacion<AbonoPDFNav> objResultado = new ResultadoOperacion<AbonoPDFNav>();

            if (String.IsNullOrEmpty(iIdAbono) || String.IsNullOrEmpty(strIdentifier) || (IdClienteNav <= 0))
            {
                objResultado.eResultado = enumResultado.Error_Parametros;
                objResultado.sDescripcion = "Parámetros no válidos. Debe indicarse IdClienteNav, iIdAbono y el strIdentifier";
            }
            else
            {

                objResultado = (ResultadoOperacion<AbonoPDFNav>)objFacturasV2Service.RecuperarAbonoPDFById(strIdentifier, iIdAbono, IdClienteNav);
            }

            return objResultado == null ? (ActionResult)NotFound() : new ObjectResult(objResultado);
        }

        /// <summary>
        /// Inserta una factura como pagada en Claves
        /// </summary>
        /// <param name="objPeticion">objeto Necesario para la ínserción de la factura</param>
        /// <returns></returns>
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResultadoEntero), StatusCodes.Status400BadRequest)]
        [HttpPost("InsertarFacturaPagada")]
        public IActionResult InsertarFacturaPagada([FromBody][Required(ErrorMessage = "objPeticion es necesario y no puede ser nulo.")] InsertarFacturaPagadaPeticion? objPeticion)
        {
            var objResultado = new ResultadoEntero();
            try
            {
                objResultado.iResultado = objFacturasV2Service.InsertarFacturaPagada(objPeticion);
                objResultado.eResultado = enumResultado.Ok_Insertado;
                objResultado.sDescripcion = "Se ha insertado la factura pagada con éxito";
                return Ok(objResultado);
            }
            catch (Exception ex)
            {
                objLogger.LogError($"Error --> {nameof(InsertarFacturaPagada)}. Resultado --> {ex}");
                objResultado.sDescripcion = ex.Message;
                objResultado.eResultado = enumResultado.Error_Excepcion;
                return BadRequest(objResultado);
            }
        }

    }
}