﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador de Formación
    /// </summary>
    [Route("Formacion/")]
    public class FormacionController : BaseController<FormacionController>
    {


        private readonly IFormacionService<Cliente, UsuarioProducto, PlanFormacionEmail, PlanFormacionCursoEmail, Evento,EventoExtendido> _svc;
        /// <summary>
        ///  Constructor del controlador de Formacion
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_svcFormacion"></param>
        /// <param name="configWeb"></param>
        public FormacionController(
         ILogger<FormacionController> logger,
         IFormacionService<Cliente, UsuarioProducto, PlanFormacionEmail, PlanFormacionCursoEmail, Evento, EventoExtendido> _svcFormacion,
         IOptions<OptConfig> configWeb
            ) : base(configWeb, logger)
        {
            _svc = _svcFormacion;
        }

        /// <summary>
        /// Método que devuelve datos del Cliente
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Código de Cliente Navision</param>
        [Route("Cliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(Cliente), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarCliente(string strIdentifier, int IdClienteNav)
        {
            int IdUserApp = 1;
            if ((IdClienteNav <= 0) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var cliente = await _svc.RecuperarCliente(strIdentifier, IdClienteNav);

            return cliente == null ? (ActionResult)NotFound() : new ObjectResult(cliente);
        }

        /// <summary>
        /// Método que devuelve datos del usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>
       
        [Route("Usuario/")]
        [HttpGet]
        [ProducesResponseType(typeof(UsuarioProducto), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarUsuario(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var usuario = await _svc.RecuperarUsuario(strIdentifier, strUsuarioPro);

            return usuario == null ? (ActionResult)NotFound() : new ObjectResult(usuario);
        }

        /// <summary>
        /// Método que devuelve los planes de formacion de un usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>
        /// <returns></returns>
        [Route("Planes de Formacion/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PlanFormacionEmail>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarPlanesByEmail(string strIdentifier, string strUsuarioPro)
        {
            string strEmail = "prueba";
            if (String.IsNullOrEmpty(strUsuarioPro)|| String.IsNullOrEmpty(strEmail))
                return BadRequest();


            var lstPlanes = await _svc.RecuperarPlanesByEmail(strIdentifier, strUsuarioPro, strEmail);

            return lstPlanes == null ? (ActionResult)NotFound() : new ObjectResult(lstPlanes);
        }

        /// <summary>
        /// Método que devuelve los cursos por plan
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdPlan">Identificador del plan</param>
        /// <param name="strEmail">Email del usuario</param>
        /// <returns></returns>
        [Route("Cursos por Plan/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PlanFormacionCursoEmail>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarCursosPlanByEmail(string strIdentifier, int IdPlan, string strEmail)
        {
            if ((IdPlan <= 0) || String.IsNullOrEmpty(strIdentifier) || String.IsNullOrEmpty(strEmail))
                return BadRequest();


            var lstCursos = await _svc.RecuperarCursosPlanByEmail(strIdentifier, IdPlan, strEmail);

            return lstCursos == null ? (ActionResult)NotFound() : new ObjectResult(lstCursos);
        }

        /// <summary>
        /// Método que devuelve el listado de las provincias
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="cursoWebexId">Identificador del curso en Webex</param>
        /// <returns></returns>
        [Route("Sesiones por Curso/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Evento>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarSesiones(string strIdentifier, int cursoWebexId)
        {
            int IdUser = 1;
            if (String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstSesiones = await _svc.RecuperarSesiones(strIdentifier, cursoWebexId, IdUser);

            return lstSesiones == null ? (ActionResult)NotFound() : new ObjectResult(lstSesiones);
        }

        /// <summary>
        /// Método que devuelve el listado de las provincias
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="WebexId">Identificador del curso en Webex</param>
        /// <returns></returns>
        [Route("Info de sesion/")]
        [HttpGet]
        [ProducesResponseType(typeof(EventoExtendido), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarInfoSession(string strIdentifier, int WebexId)
        {
            if (String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstSesiones = await _svc.RecuperarSesionExtendido(strIdentifier, WebexId);

            return lstSesiones == null ? (ActionResult)NotFound() : new ObjectResult(lstSesiones);
        }


    }
}
