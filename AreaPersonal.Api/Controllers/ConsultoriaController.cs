﻿using AreaPersonal.Comun.Configuration;
using AreaPersonal.Servicio;
using AreaPersonal.Vistas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using DllApiBase.Vistas.Claves.Usuario;

namespace AreaPersonal.WebApi.Controllers
{
    /// <summary>
    /// Controlador que gestiona las llamadas y Peticiones a Consultoria
    /// </summary>
    [Route("Consultoria/")]
    public class ConsultoriaController : BaseController<ConsultoriaController>
    {

        private readonly IConsultoriaService<Cliente, UsuarioProducto,Materia, SubMateria, Provincia, TipoConsulta,ResultadoGuardar, ResultadoOperacion, ResultadoConsultas,ResultadoEnviar, UsuarioPro> _svc;
        /// <summary>
        ///  Constructor del controlador de Consultoria
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="_svcConsultoria"></param>
        /// <param name="configWeb"></param>
        public ConsultoriaController(
         ILogger<ConsultoriaController> logger,
         IConsultoriaService<Cliente, UsuarioProducto, Materia, SubMateria, Provincia, TipoConsulta, ResultadoGuardar, ResultadoOperacion, ResultadoConsultas, ResultadoEnviar, UsuarioPro> _svcConsultoria,
         IOptions<ConfigApi> configWeb
            ) : base(configWeb, logger)
        {
            _svc = _svcConsultoria;
        }

        /// <summary>
        /// Método que devuelve datos del Cliente
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Código de Cliente Navision</param>
        [Route("Cliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(Cliente), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarCliente(string strIdentifier, int IdClienteNav)
        {

            if ((IdClienteNav <= 0) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var cliente = await _svc.RecuperarCliente(strIdentifier, IdClienteNav);

            return cliente == null ? (ActionResult)NotFound() : new ObjectResult(cliente);
        }

        /// <summary>
        /// Método que devuelve datos del usuario
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>

        [Route("Usuario/")]
        [HttpGet]
        [ProducesResponseType(typeof(UsuarioProducto), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarUsuario(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strUsuarioPro) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var usuario = await _svc.RecuperarUsuario(strIdentifier, strUsuarioPro);

            return usuario == null ? (ActionResult)NotFound() : new ObjectResult(usuario);
        }

        /// <summary>
        /// Método que devuelve las materias configuradas para un Cliente y Producto
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>
        /// <returns></returns>
        [Route("Materias_LlamadaExperta/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Materia>), 200)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecuperarMateriasExperta(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strIdentifier) || (String.IsNullOrEmpty(strUsuarioPro)))
                return BadRequest();


            var lstMatters = await _svc.RecuperarMateriasByUsuario(strIdentifier, strUsuarioPro, 1);

            return lstMatters == null ? (ActionResult)NotFound() : new ObjectResult(lstMatters);
        }

        /// <summary>
        /// Método que devuelve las materias configuradas para un Cliente y Producto
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strUsuarioPro">Entrada del Usuario</param>
        /// <returns></returns>
        [Route("Materias_Consultoria/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Materia>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarMateriasConsultoria(string strIdentifier, string strUsuarioPro)
        {

            if (String.IsNullOrEmpty(strIdentifier) || (String.IsNullOrEmpty(strUsuarioPro)))
                return BadRequest();


            var lstMatters = await _svc.RecuperarMateriasByUsuario(strIdentifier, strUsuarioPro, 0);

            return lstMatters == null ? (ActionResult)NotFound() : new ObjectResult(lstMatters);
        }

        /// <summary>
        /// Método que devuelve las submaterias configuradas para una materia
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdMateria">Código de Materia</param>
        /// <returns></returns>
        [Route("SubMaterias/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<SubMateria>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarSubMaterias(string strIdentifier, int IdMateria)
        {
            if ((IdMateria <= 0) || String.IsNullOrEmpty(strIdentifier))
                return BadRequest();


            var lstSubMatters = await _svc.RecuperarSubMaterias(strIdentifier, IdMateria);

            return lstSubMatters == null ? (ActionResult)NotFound() : new ObjectResult(lstSubMatters);
        }

        /// <summary>
        /// Método que devuelve el listado de las provincias
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <returns></returns>
        [Route("Provincias/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Provincia>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarProvincias(string strIdentifier)
        {
            if (String.IsNullOrEmpty(strIdentifier))
                return BadRequest();

            var lstProvincias = await _svc.RecuperarProvincias(strIdentifier);

            return lstProvincias == null ? (ActionResult)NotFound() : new ObjectResult(lstProvincias);
        }

        /// <summary>
        /// Método que devuelve los tipos de peticiones por Materia
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdMateria">Código de Materia</param>
        /// <returns></returns>
        [Route("TiposConsultasPorMateria/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TipoConsulta>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarTiposByMateria(string strIdentifier, int IdMateria)
        {

            if ((IdMateria <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var lstTypes = await _svc.RecuperarTipoConsultasByMateria(strIdentifier, IdMateria);

            return lstTypes == null ? (ActionResult)NotFound() : new ObjectResult(lstTypes);
        }


        /// <summary>
        /// Método que devuelve los tipos de peticiones por Cliente
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="IdClienteNav">Código de Cliente Navision</param>
        /// <returns></returns>
        [Route("TiposConsultasPorCliente/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TipoConsulta>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RecuperarTiposByCliente(string strIdentifier, int IdClienteNav)
        {

            if ((IdClienteNav <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var lstTypes = await _svc.RecuperarTipoConsultasCliente(strIdentifier, IdClienteNav);

            return lstTypes == null ? (ActionResult)NotFound() : new ObjectResult(lstTypes);
        }

        /// <summary>
        /// Método que devuelve objeto con el resultado del chequeo
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="ClienteNav">Código de Cliente Navision</param>
        /// <param name="IdMateria">Código de Materia</param>
        /// <returns></returns>
        [Route("ComprobarCrearPeticionByMateria/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ComprobarCrearPeticionByMateria(string strIdentifier, int ClienteNav, int IdMateria)
        {

            if ((ClienteNav <= 0) || (IdMateria <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            ResultadoOperacion objResult = await _svc.ComprobarCrearPeticionByMateria(strIdentifier, ClienteNav, IdMateria);

            return new ObjectResult(objResult);
        }

        /// <summary>
        /// Método que devuelve objeto con el resultado del chequeo
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="ClienteNav">Código de Cliente Navision</param>
        /// <param name="IdMateria">Código de Materia</param>
        /// <returns></returns>
        [Route("ComprobarEnviarComentarios/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoOperacion), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ComprobarEnviarComentarios(string strIdentifier, int ClienteNav, int IdMateria)
        {

            if ((ClienteNav <= 0) || (IdMateria <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            ResultadoOperacion objResult = await _svc.ComprobarEnviarComentarios(strIdentifier, ClienteNav, IdMateria);

            return new ObjectResult(objResult);
        }

        /// <summary>
        /// Método para guardar una petición.
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="strEmail">Email del usuario</param>
        /// <param name="strNombre">Nombre del usuario</param>
        /// <param name="strApellido">Apellido del usuario</param>
        /// <param name="strDescripcion">Planteamiento de la petición</param>
        /// <param name="strTelefono">Teléfono del usuario</param>
        /// <param name="strTitular">Nombre del titular de la cuenta</param>
        /// <param name="strCargo">Cargo (solo Derecho Local)</param>
        /// <param name="strDisponibilidad">strDisponibilidad (solo Derecho Local)</param> 
        /// <param name="sNombreCliente">Nombre del Cliente</param>
        /// <param name="iIdMateria">Identificador de la materia</param>
        /// <param name="iIdTipoPeticion">Tipo de la petición.</param>
        /// <param name="iIdSubMateria">Identificador de la submateria</param>
        /// <param name="iIdProvincia">Identificador de la Provincia</param>
        /// <param name="iIdClientNav">Identificador del cliente Navision</param>
        /// <param name="iCodCRM">Identificador del cliente Kojak</param>
        /// <returns></returns>
        [Route("GuardarConsulta/")]
        [HttpGet]
        [ProducesResponseType(typeof(ResultadoGuardar), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult SaveQuestion(string strIdentifier, String strEmail, String strNombre, string strApellido, String strDescripcion,
            String strTelefono, string strTitular, string strCargo, String sNombreCliente, String strDisponibilidad,
            int iIdMateria = -1, int iIdTipoPeticion = -1, int iIdSubMateria = -1, int iIdProvincia = -1, int iIdClientNav = -1, int iCodCRM = -1)
        {


            if ((iIdClientNav <= 0) || (iIdMateria <= 0) || (iIdSubMateria <= 0) || (iIdTipoPeticion <= 0) || (iIdProvincia <= 0) || (strNombre.Length <= 0)
                || (strEmail.Length <= 0) || (strDescripcion.Length <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            int iEstado = 1;

            ResultadoGuardar objResult = _svc.GuardarConsulta(strIdentifier, strEmail, strNombre, strApellido, strDescripcion, strTelefono,
                strTitular, strCargo, sNombreCliente, strDisponibilidad, iEstado, iIdMateria, iIdTipoPeticion,
                iIdSubMateria, iIdProvincia, iIdClientNav, iCodCRM);


            if ((objResult.bEnviarEmail) && (objResult.iIdConsulta > 0))
            {
                _svc.SendEmailMessageAlta(strIdentifier, iCodCRM, strEmail, strNombre, objResult.iIdConsulta);
            }
            return new ObjectResult(objResult);
        }

        /// <summary>
        /// Método Post para guardar una petición
        /// </summary>
        /// <param name="request">Objeto contener de los parámetros</param> 
        /// <returns></returns>
        [Route("GuardarConsultaPost/")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<ResultadoGuardar>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult SaveConsultaPost(ConsultaRequest request)
        {

            string strIdentifier = request.strIdentifier;
            string strNombre = request.strNombre;
            string strApellido = request.strApellido;
            string strEmail = request.strEmail;
            string strTelefono = request.strTelefono;
            string sNombreCliente = request.sNombreCliente;

            String strDescripcion = request.strDescripcion;

            string strTitular = request.strTitular;
            string strCargo = request.strCargo;

            int iIdMateria = request.iIdMateria;
            int iIdTipoPeticion = request.iIdTipoPeticion;
            int iIdSubMateria = request.iIdSubMateria;
            int iIdProvincia = request.iIdProvincia;
            int iIdClientNav = request.iIdClientNav;
            int iCodCRM = request.iCodCRM;
            string strDisponibilidad = request.sDisponibilidad;

            int iEstado = 1;

            if ((iIdClientNav <= 0) || (iIdMateria <= 0) || (iIdSubMateria <= 0) || (iIdTipoPeticion <= 0) || (iIdProvincia <= 0) || (strNombre.Length <= 0)
                || (strEmail.Length <= 0) || (strDescripcion.Length <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            ResultadoGuardar objResult = _svc.GuardarConsulta(strIdentifier, strEmail, strNombre, strApellido, strDescripcion, strTelefono,
                strTitular, strCargo, sNombreCliente, strDisponibilidad, iEstado, iIdMateria, iIdTipoPeticion,
                iIdSubMateria, iIdProvincia, iIdClientNav, iCodCRM);


            if ((objResult.bEnviarEmail) && (objResult.iIdConsulta > 0))
            {
                _svc.SendEmailMessageAlta(strIdentifier, iCodCRM, strEmail, strNombre, objResult.iIdConsulta);
            }
            return new ObjectResult(objResult);
        }


        /// <summary>
        /// Método que reenvia por Correo la información de una Petición
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="iCodCRM">Código de Cliente CRM</param>
        /// <param name="strEmail">Dirección de correo de envio</param>
        /// <param name="idQuery">Código de la Petición</param>
        /// <returns></returns>
        [Route("ReSendEmailInfo/")]
        [HttpGet]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult ReSendEmailInfo(string strIdentifier, int iCodCRM, string strEmail, int idQuery)
        {

            if ((iCodCRM <= 0) || (String.IsNullOrEmpty(strEmail)) || (String.IsNullOrEmpty(strIdentifier))) 
                return BadRequest();

            var objResult  =  _svc.ReSendEmailInfo(strIdentifier, iCodCRM, strEmail, idQuery);

            return new ObjectResult(objResult);
        }

        /// <summary>
        /// Método que devuelve las peticiones por cliente y tipo
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="iIdClientNav">Identificador del cliente Navision</param>
        /// <param name="iCodCRM">Identificador del cliente Crm</param>
        /// <param name="iType">Tipo de petición</param>
        /// <param name="iMatter">Tipo de materia</param>
        /// <param name="iPageNum">Número de Página</param>
        /// <param name="iPageSize">Tamaño de Página</param>
        /// <returns></returns>
        [Route("RecuperarPeticiones/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ResultadoConsultas>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarPeticiones(string strIdentifier, int iCodCRM, int iIdClientNav, int iType, int iMatter, int iPageNum, int iPageSize)
        {

            if (((iCodCRM <= 0) && (iIdClientNav <= 0)) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var lstQueries =  _svc.RecuperarPeticiones(strIdentifier, iCodCRM, iIdClientNav, iType, iMatter, iPageNum, iPageSize);

            return lstQueries == null ? (ActionResult)NotFound() : new ObjectResult(lstQueries);
        }

        /// <summary>
        /// Método que devuelve las peticiones por cliente y tipo
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="iIdConsulta">Identificador de la Consulta</param>
        /// <returns></returns>
        [Route("RecuperarPeticion/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ResultadoConsultas>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult RecuperarPeticion(string strIdentifier, int iIdConsulta)
        {

            if ((iIdConsulta <= 0) && (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var lstQueries = _svc.RecuperarPeticion(strIdentifier, iIdConsulta);

            return lstQueries == null ? (ActionResult)NotFound() : new ObjectResult(lstQueries);
        }

        /// <summary>
        /// Método que guarda un comentario de una petición
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="icodQuestion">Identificador de la petición</param>
        /// <param name="iCodCRM">Identificador del cliente Crm</param>
        /// <param name="iIdClientNav">Identificador del cliente Navision</param> 
        /// <param name="strText">Texto a enviar</param>
        /// <param name="bCopy">Enviar copia</param>
        /// <returns></returns>
        [Route("EnviarComentarios/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ResultadoEnviar>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult EnviarComentarios(string strIdentifier, int icodQuestion, int iCodCRM, string strText, int iIdClientNav, Boolean bCopy)
        {

            if (((iCodCRM <= 0) && (iIdClientNav <= 0)) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var objResult = _svc.SendRequest(strIdentifier, icodQuestion, iCodCRM, strText, iIdClientNav, bCopy);

            return objResult == null ? (ActionResult)NotFound() : new ObjectResult(objResult);
        }

        /// <summary>
        /// Método Post que guarda un comentario de una petición
        /// </summary>
        /// <param name="request">Objeto contenedor de los parámetros</param> 
        /// <returns></returns>
        [Route("EnviarComentariosPost/")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<ResultadoEnviar>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult EnviarComentarios(EnviarComentarioRequest request)
        {

            if (request != null)
            {
                string strIdentifier = request.strIdentifier;

                int iCodCRM = request.iCodCRM;
                int icodQuestion = request.icodQuestion;
                int iIdClientNav = request.iIdClientNav;
                String strText = request.strText;
                Boolean bCopy = request.bCopy;

                if (((iCodCRM <= 0) && (iIdClientNav <= 0)) || (String.IsNullOrEmpty(strIdentifier)))
                    return BadRequest();

                var objResult = _svc.SendRequest(strIdentifier, icodQuestion, iCodCRM, strText, iIdClientNav, bCopy);

                return objResult == null ? (ActionResult)NotFound() : new ObjectResult(objResult);

            }
            else
            {
                return new ObjectResult(StatusCodes.Status400BadRequest);
            }


        }

        /// <summary>
        /// Método Get para guardar un arhivo en una petición
        /// </summary>
        /// <param name="strIdentifier">Identificador del Origen</param>
        /// <param name="icodQuestion">Identificador de la petición</param>
        /// <param name="sFileName">Nombre del archivo</param>
        /// <param name="strSize">Tamaño del archivo</param> 
        /// <param name="bdata">Contenido del archivo en bytes</param>
        /// <returns></returns>
        [Route("GuardarArchivo/")]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<int>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult GuardarArchivo(string strIdentifier, String sFileName, int icodQuestion, String strSize, Byte[] bdata) 
        {

            if ((icodQuestion <= 0) || (bdata == null) || (bdata.Length <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                return BadRequest();

            var objResult = _svc.SaveFile(strIdentifier, sFileName,icodQuestion, strSize, bdata);

            return new ObjectResult(objResult);
        }

        /// <summary>
        /// Método Post para guardar un arhivo en una petición
        /// </summary>
        /// <param name="request">Objeto contener de los parámetros</param> 
        /// <returns></returns>
        [Route("GuardarArchivoPost/")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<int>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        public IActionResult GuardarArchivoPost(GuardarArchivoRequest request)
        {

            if (request != null)
            {
                string strIdentifier = request.strIdentifier;

                String sFileName = request.sFileName;
                int icodQuestion = request.icodQuestion;
                String strSize = request.strSize;
                Byte[] bdata = request.bdata;

                if ((icodQuestion <= 0) || (bdata == null) || (bdata.Length <= 0) || (String.IsNullOrEmpty(strIdentifier)))
                    return BadRequest();

                var objResult = _svc.SaveFile(strIdentifier, sFileName, icodQuestion, strSize, bdata);

                return new ObjectResult(objResult);
            }
            else
            {
                return new ObjectResult(StatusCodes.Status400BadRequest);
            }
        }

        ///// <summary>
        ///// Método Post para guardar un arhivo en una petición
        ///// </summary>
        ///// <param name="request">Objeto contener de los parámetros</param> 
        ///// <returns></returns>
        //[Route("GuardarArchivoPost2/")]
        //[HttpPost]
        //[ProducesResponseType(typeof(IEnumerable<int>), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public IActionResult GuardarArchivoPost2(GuardarArchivo2Request request)
        //{

        //    if (request != null)
        //    {
             
        //        String sFileName = request.sFileName;
             
        //        Byte[] bdata = request.bdata;


        //        var objResult = _svc.SaveFile2(sFileName,bdata);

        //        return new ObjectResult(objResult);
        //    }
        //    else
        //    {
        //        return new ObjectResult(StatusCodes.Status400BadRequest);
        //    }
        //}

        ///// <summary>
        ///// Método Post para guardar un arhivo en una petición
        ///// </summary>
        ///// <param name="bdata">Objeto contener de los parámetros</param> 
        ///// <returns></returns>
        //[Route("GuardarArchivoPost3/")]
        //[HttpPost]
        //[ProducesResponseType(typeof(IEnumerable<int>), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(void), StatusCodes.Status400BadRequest)]
        //public IActionResult GuardarArchivoPost3(Byte[] bdata)
        //{

        //    if (bdata != null)
        //    {

        //        String sFileName = "ejemplo1.txt";

              


        //        var objResult = _svc.SaveFile2(sFileName, bdata);

        //        return new ObjectResult(objResult);
        //    }
        //    else
        //    {
        //        return new ObjectResult(StatusCodes.Status400BadRequest);
        //    }
        //}

    }
}
